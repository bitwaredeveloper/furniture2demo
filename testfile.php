<?php
use Magento\Framework\App\Bootstrap;
 
require __DIR__ . '/app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$obj = $bootstrap->getObjectManager();
$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

/* Code to Access Database Directly Starts */

$resource = $obj->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();


/* Code to Access Database Directly Ends */
?>

<?php

$eavAttribute = $obj->get('Magento\Eav\Model\ResourceModel\Entity\Attribute');
$proAttrId = $eavAttribute->getIdByCode("catalog_product", "name");
$proPriceAttrId = $eavAttribute->getIdByCode("catalog_product", "price");
		
$mp_tab = $resource->getTableName('furniture2demo.marketplace_product'); 
$cgf_tab = $resource->getTableName('furniture2demo.customer_grid_flat'); 
$cat_prod_ev_tab = $resource->getTableName('furniture2demo.catalog_product_entity_varchar'); 
$prod_dec_tab = $resource->getTableName('furniture2demo.catalog_product_entity_decimal'); 
$fact_prod_tab = $resource->getTableName('furniture2demo.factory_user_product_table'); 
//$tableName = $resource->getTableName('furniture2demo.marketplace_product'); 



//$fetch_prod_data="SELECT $mp_tab.mageproduct_id,$mp_tab.seller_id, $cgf_tab.name,$cat_prod_ev_tab.value, $cat_prod_ev_tab.attribute_id,$cat_prod_ev_tab.store_id FROM $cgf_tab JOIN $mp_tab ON $cgf_tab.entity_id = $mp_tab.seller_id JOIN  $cat_prod_ev_tab ON $mp_tab.mageproduct_id = $cat_prod_ev_tab.entity_id WHERE $cat_prod_ev_tab.store_id = 0 AND $cat_prod_ev_tab.attribute_id = $proAttrId";
 
$fetch_prod_data = "SELECT $mp_tab.mageproduct_id,$mp_tab.seller_id, $cgf_tab.name, $cat_prod_ev_tab.value,$prod_dec_tab.value FROM $cgf_tab JOIN $mp_tab ON $cgf_tab.entity_id = $mp_tab.seller_id JOIN  $cat_prod_ev_tab ON $mp_tab.mageproduct_id = $cat_prod_ev_tab.entity_id 
JOIN
$prod_dec_tab ON $mp_tab.mageproduct_id = $prod_dec_tab.entity_id $fact_prod_tab ON
 $mp_tab.mageproduct_id = $fact_prod_tab.product_id  WHERE
 $fact_prod_tab.product_id != $mp_tab.mageproduct_id AND $fact_prod_tab.vendor_id != $fact_prod_tab.seller_id AND 
 $fact_prod_tab.factory_user_id != 1 AND 
 $cat_prod_ev_tab.store_id = 0 AND $prod_dec_tab.store_id = 0 AND  $prod_dec_tab.attribute_id=77 AND $cat_prod_ev_tab.attribute_id = 73";


?>