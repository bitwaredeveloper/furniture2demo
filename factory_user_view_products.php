<?php
use Magento\Framework\App\Bootstrap;
 
require __DIR__ . '/app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$obj = $bootstrap->getObjectManager();
$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

/* Code to Access Database Directly Starts */

$resource = $obj->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();


/* Code to Access Database Directly Ends */
?>
<?php
$tableName = $resource->getTableName('furniture2demo.marketplace_product'); //gives table name with prefix
 
//Select Data from table 
$sql = "Select mageproduct_id FROM " . $tableName;
$result = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
 
$productIdAry = $result;
?>
<table class="table">
  <thead>
	<tr>
	  <th>Sr. No.</th>
	  <th>Product Name</th>
	  <th>Input Price</th>
	  <th>Action</th>
	</tr>
  </thead>
  <tbody>

<?php 
$i=0;
foreach($productIdAry as $productIdAryRlts){
	$i++;
	$product = $obj->get('Magento\Catalog\Model\ProductRepository')
					->getById($productIdAryRlts['mageproduct_id']);
	$productData = $product->getData();
?>
<tr>
<form action='' method='POST'>
  <td><?php echo $i;?></td>
  <td><?php echo $product->getName();?></td>
  <td><input type="text" name="factoryPriceValue"></td>
  <td>  
    <input type='submit'  name='submit' />
	<input type="hidden" name="product_id_form" value="<?php echo $productData['entity_id']?>">
 </td>
 </form>
</tr>	
<?php } ?>
</tbody>
</table>
<?php 
if(isset($_POST['submit']) ){
	if($_POST['factoryPriceValue'] != ''){ // Price field is not empty
		if (preg_match('/^[0-9]+$/', $_POST['factoryPriceValue'])) {
				// contains only 0-9
			$tableNameInsert = $resource->getTableName('furniture2demo.factory_user_table');
				//Insert Data into table
			$sqlInsert = "Insert Into " . $tableNameInsert . " (product_id, vendor_id, factory_user_id, factory_name,price) Values (".$productData['entity_id'].",'2','1','Dummy Name',".$_POST['factoryPriceValue'].")";

			$resultInsert = $connection->query($sqlInsert);
			if($resultInsert){
				echo "<div style='color:red;'>Price Updated Successfully!</div>"; 
			}else{
				echo "<div style='color:red;'>Problems in Updating Price. Please try again Later.</div>";
			}
		} else {
		  // contains other stuff
		  echo "<div style='color:red;'>Please enter numbers only!</div>";
		}
	}else{
		echo "<div style='color:red;'>Please fill the price field!</div>";
	}
}
?>


					
					
				 
     
      