<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Custom Living</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

<body>

<?php 

global $_SESSION;

use Magento\Framework\App\Bootstrap;
 
//require __DIR__ . '/app/bootstrap.php';

include_once '../app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$obj = $bootstrap->getObjectManager();
$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

/* Code to Access Database Directly Starts */

$resource = $obj->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();

/* Code to Access Database Directly Ends */


?>
	
<?php

/* Registration Code Starts */

if (isset($_POST["registration"])) {
  if ((($_POST["form-first-name"]) != '') and (($_POST["form-last-name"]) != '') and (($_POST["form-email"]) != '') and (($_POST["form-password"])) != ''){  
    $firstName = $_POST["form-first-name"];
    $lastName = $_POST["form-last-name"];
    $email = trim($_POST["form-email"]);
    $password = $_POST["form-password"];
    $password = md5($password);
	
	$usertableName = $resource->getTableName('furniture2demo.factory_user_data_table');
	
	//Select Data from table 
$sqlCheck = "Select email FROM " . $usertableName ." where email = '$email'";
$resultCheck = $connection->fetchAll($sqlCheck); // gives associated array, table fields as key in array.

if(!empty($resultCheck)){
	echo "<script>alert('This user already exits.')</script>";
}else{
	//Insert Data into table
	$sqlInsert = "Insert Into " . $usertableName . " 
		(first_name, last_name, email, password) 
			Values 
		('$firstName','$lastName','$email','$password')";
	
	$resultInsert = $connection->query($sqlInsert);
	
	if($resultInsert){
		echo "<script>alert('You have successfully signed up.')</script>"; 
	}else{
		echo "<script>alert('Problems in sigining up. Please try again later.')</script>";
	}
  }
  }else {
    echo "<script>alert('Please enter all the fields.')</script>";
  }

}

/* Registration Code Ends */

/* Login Code Starts */
if (isset($_POST["contrlogin"])) {
	if ((($_POST["login-email"]) != '') and (($_POST["login-pass"]) != '') ){
		$email = trim($_POST["login-email"]);
		$password = $_POST["login-pass"];
		$password = md5($password);	
		
$usertableName = $resource->getTableName('furniture2demo.factory_user_data_table');
	//Select Data from table 
$sqlCheck = "Select * FROM " . $usertableName ." where email = '$email' and password='$password'";
$resultCheck = $connection->fetchAll($sqlCheck); 

//echo"<pre>"; print_r($resultCheck); echo"</pre>";


if(empty($resultCheck)){
	echo "<script>alert('Email or Password wrong. Please check again.')</script>";
}else{ //echo "<script>alert('You have logged in.')</script>";?>
	<script>window.location = "http://103.224.243.154/magento2/furniture2demo/FactoryUser/";</script>
<?php	
	$cookie_name = "factoryuser";
	$cookie_value = "yes";
	
	$cookie_user = "user_id";
	$cookie_user_value = trim($resultCheck[0]['id']);
	
	$cookie_firstname = "firstname";
	$cookie_firstname_value = trim($resultCheck[0]['first_name']);
	
	$cookie_lastname = "lastname";
	$cookie_lastname_value = trim($resultCheck[0]['last_name']);
	
	setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); 
	setcookie($cookie_user, $cookie_user_value, time() + (86400 * 30), "/"); 
	setcookie($cookie_firstname, $cookie_firstname_value, time() + (86400 * 30), "/"); 
	setcookie($cookie_lastname, $cookie_lastname_value, time() + (86400 * 30), "/"); 
}
	}else{
		 echo "<script>alert('Please enter all the fields.')</script>";
	}
}

/* Login Code Ends */

/* Logout Code Starts */
if (isset($_POST["contrlogout"])) {
	
    unset($_COOKIE['factoryuser']);
    setcookie('factoryuser', null, -1, '/');
	
	unset($_COOKIE['user_id']);
    setcookie('user_id', null, -1, '/');
	
	unset($_COOKIE['firstname']);
    setcookie('firstname', null, -1, '/');
	
	unset($_COOKIE['lastname']);
    setcookie('lastname', null, -1, '/');
	
}

/* Logout Code Ends */
?>	

		<!-- Top menu -->
		<nav class="navbar navbar-inverse navbar-no-bg navbar-custom" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!--<a class="navbar-brand" href="index.php"></a>-->
					<a class="navbarbrand_text" href="index.php">Custom Living</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="top-navbar-1">
				
				<?php if(isset($_COOKIE['factoryuser'])) {?>
				
				  <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" class="navbar-form navbar-right"  name="logout-form" id="logout-form">
				  <div class="form-group">
                    <input type="submit" value="Logout" id="contrlogout" name="contrlogout" class="btn btn-danger logoutcustom_btn">
                    <p>&nbsp;</p>
                    </div>
					</form>
				  <?php } else { ?>
					
					<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" class="navbar-form navbar-right"  name="login-form" id="login-form">
                    <div class="form-group">
                      <input type="text" name="login-email" class="form-control" placeholder="Email">
                      <p>&nbsp;</p>
                    </div>
					<div class="form-group">
                      <input type="password"  name="login-pass" class="form-control" placeholder="Password">
					  <p>&nbsp;</p>
                    </div>
                    <div class="form-group">
                    <input type="submit" value="Login" id="contrlogin" name="contrlogin" class="btn btn-primary btn-custom-login">
                    <p>&nbsp;</p>
                    </div>
                  </form>
				
				  <?php } ?>
				</div>
			</div>
		</nav>
		
<?php if(isset($_COOKIE['factoryuser'])) {?>
<?php
/*bhavna */

?>
<?php

if(isset($_COOKIE['user_id'])) {
	$factoryUserId = $_COOKIE['user_id'];
}

if(isset($_COOKIE['firstname'])) {
	$factoryUserFirstName = $_COOKIE['firstname'];
}

if(isset($_COOKIE['lastname'])) {
	$factoryUserLastName = $_COOKIE['lastname'];
}

$factoryUserFullName = $factoryUserFirstName." ".$factoryUserLastName;

$eavAttribute = $obj->get('Magento\Eav\Model\ResourceModel\Entity\Attribute');
$proAttrId = $eavAttribute->getIdByCode("catalog_product", "name");
$proPriceAttrId = $eavAttribute->getIdByCode("catalog_product", "price");
		
$mp_tab = $resource->getTableName('furniture2demo.marketplace_product'); 
$cgf_tab = $resource->getTableName('furniture2demo.customer_entity'); 
$cat_prod_ev_tab = $resource->getTableName('furniture2demo.catalog_product_entity_varchar'); 
$prod_dec_tab = $resource->getTableName('furniture2demo.catalog_product_entity_decimal'); 
$fact_prod_tab = $resource->getTableName('furniture2demo.factory_user_product_table'); 

 
/* $fetch_prod_data = "SELECT $mp_tab.mageproduct_id,$mp_tab.seller_id, $cgf_tab.name as 'seller_name', $cat_prod_ev_tab.value as 'product_name',$prod_dec_tab.value as 'price' FROM $cgf_tab JOIN $mp_tab ON $cgf_tab.entity_id = $mp_tab.seller_id JOIN  $cat_prod_ev_tab ON $mp_tab.mageproduct_id = $cat_prod_ev_tab.entity_id 
JOIN
$prod_dec_tab ON $mp_tab.mageproduct_id = $prod_dec_tab.entity_id  WHERE $cat_prod_ev_tab.store_id = 0 AND $prod_dec_tab.store_id = 0 AND  $prod_dec_tab.attribute_id=$proPriceAttrId AND $cat_prod_ev_tab.attribute_id = $proAttrId"; */

$fetch_prod_data_all = 
			"SELECT $mp_tab.mageproduct_id,
					$mp_tab.seller_id, 
					$cgf_tab.firstname as 'first_name',
					$cgf_tab.lastname as 'last_name',
					$cat_prod_ev_tab.value as 'product_name',
					$prod_dec_tab.value as 'price'
FROM 
	$cgf_tab 
JOIN 
	$mp_tab ON $cgf_tab.entity_id = $mp_tab.seller_id 
JOIN 
	$cat_prod_ev_tab ON $mp_tab.mageproduct_id = $cat_prod_ev_tab.entity_id 
JOIN 
	$prod_dec_tab ON $mp_tab.mageproduct_id = $prod_dec_tab.entity_id 


WHERE(
	$cat_prod_ev_tab.store_id = 0 
AND 
	$prod_dec_tab.store_id = 0 
AND  
	$prod_dec_tab.attribute_id=$proPriceAttrId 
AND 
	$cat_prod_ev_tab.attribute_id = $proAttrId	
	)";

$resultValueAll = $connection->fetchAll($fetch_prod_data_all); 

//echo"<pre>"; echo"resultValueAll: ";print_r($resultValueAll); echo"</pre>"; exit;

$fetch_prod_data_not_required = 
			"SELECT $mp_tab.mageproduct_id,
					$mp_tab.seller_id, 
					$cgf_tab.firstname as 'first_name',
					$cgf_tab.lastname as 'last_name', 
					$cat_prod_ev_tab.value as 'product_name',
					$prod_dec_tab.value as 'price'
FROM 
	$cgf_tab 
JOIN 
	$mp_tab ON $cgf_tab.entity_id = $mp_tab.seller_id 
JOIN 
	$cat_prod_ev_tab ON $mp_tab.mageproduct_id = $cat_prod_ev_tab.entity_id 
JOIN 
	$prod_dec_tab ON $mp_tab.mageproduct_id = $prod_dec_tab.entity_id 
JOIN 
	$fact_prod_tab ON $prod_dec_tab.entity_id = $fact_prod_tab.product_id  

WHERE(
	$cat_prod_ev_tab.store_id = 0 
AND 
	$prod_dec_tab.store_id = 0 
AND  
	$prod_dec_tab.attribute_id=$proPriceAttrId 
AND 
	$cat_prod_ev_tab.attribute_id = $proAttrId
AND	
	($fact_prod_tab.product_id = $mp_tab.mageproduct_id 
AND 
	$fact_prod_tab.vendor_id = $mp_tab.seller_id 
AND 
	$fact_prod_tab.factory_user_id = $factoryUserId ) 	
	)";

//echo $fetch_prod_data; exit;
$resultValueNotReqd = $connection->fetchAll($fetch_prod_data_not_required); 

//echo"<pre>"; echo"resultValueNotReqd: ";print_r($resultValueNotReqd); echo"</pre>"; exit;



foreach($resultValueAll as $key=>$value){
    foreach($resultValueNotReqd as $key2=>$value2) {	
        if($value['mageproduct_id']==$value2['mageproduct_id']){         
            //optimization to avoid searching this again.
            unset($resultValueAll[$key]);
        }
    }
}





//echo"<pre>"; print_r($resultValueAll); echo"<pre>"; exit; 
?>

<div class="top-content" style="background-color:#fff !important;">
        	
<div class="inner-bg">
	<div class="container">
		<div class="row">
			<table class="table table-custom">
			  <thead class="table-inverse-custom">
				<tr>
				  <th>Sr. No.</th>
				  <th>Seller Name</th>
				  <th>Product Name</th>
				  <th>Input Price</th>
				  <th colspan="2">Action</th>
				</tr>
			  </thead>
				<tbody>
				<?php 
				$i = 0;
				
				foreach($resultValueAll as $resultValueData){
					
					$productId = $resultValueData['mageproduct_id'];
					$seller_id = $resultValueData['seller_id'];
					$first_name = $resultValueData['first_name'];
					$last_name = $resultValueData['last_name'];
					$seller_name = $first_name." ".$last_name;
					$product_name = $resultValueData['product_name'];
					$price = $resultValueData['price'];
					
					if($price == 0){ $i++; ?>
						<tr>
						<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" class="navbar-form navbar-right"  name="price-update-form" method='POST'>
						  <td><?php echo $i;?></td>
						  <td><?php echo $seller_name;?></td>
						  <td><?php echo $product_name;?></td>
						  <td><input class="factory_input" type="text" name="factoryPriceValue"></td>
						  <td>  
						    
							
							<input class="btn btn-success" type='submit'  name='submit' value='Submit'/>
							<input type="hidden" name="product_id" value="<?php echo $productId ?>">
							<input type="hidden" name="seller_id" value="<?php echo $seller_id ?>">
							<input type="hidden" name="seller_name" value="<?php echo $seller_name ?>">
							<input type="hidden" name="product_name" value="<?php echo $product_name ?>">
							
						 </td>
						 
						 </form>
						 
						 
						 <td>
						 
						 <?php
						 
							
							$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
							$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();
							//$allProducts = $objectManager->get('Magento\Catalog\Model\Product')->load($productId);
							$allProducts = $objectManager->create('\Magento\Catalog\Model\Product')->load($productId);
							
							$url = \Magento\Framework\App\ObjectManager::getInstance();
							$storeManager = $url->get('\Magento\Store\Model\StoreManagerInterface');
							$mediaurl= $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
							
							$productName = $allProducts->getName();
							$productSku = $allProducts->getSku();
							$productType = $allProducts->getTypeId();
							
							//$data=$allProducts->getData();
							$imageurlPath=$allProducts->getImage();
							if($imageurlPath)
							{
								$newImageUrl = $mediaurl.'catalog/product'.$imageurlPath;
							}
							else
							{
								$newImageUrl = $mediaurl.'catalog/product/no_image.jpg';
							}
							
							//print_r($data);
							//echo '<br/>image:'.$imageUrlNew;
							
							$productDescription = $allProducts->getDescription();
							$productWeight = $allProducts->getWeight();
							if($productWeight)
							{
								$productWeight=$productWeight;
							}
							else
							{
								$productWeight='&nbsp;';
							}

						 
						 ?>
						 
							<button class="btn btn-primary btn-lg viewcustom_btn" data-toggle="modal" data-target="#myModal<?php echo $productId; ?>">View Product</button>
							<!-- Modal -->
							<div class="modal fade" id="myModal<?php echo $productId; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
									<h4 class="modal-title" id="myModalLabel">Product Specification</h4>
								  </div>
								  <div class="modal-body modal-body-custom">
									<div class="prodSpecification">
										<div class="leftProdAttributes">
										<div class="col-md-12 text-center">
										<img class="imgcustom_model" src="<?php echo $newImageUrl; ?>" alt="" style="width:40%" />
										</div>
										</div>
										
										<div class="leftProdAttributes"><div class="col-md-4 text-bold">Product Name:</div><div class="col-md-8"> <?php echo $productName;?></div></div>
										<div class="leftProdAttributes"><div class="col-md-4 text-bold">Product SKU:</div><div class="col-md-8"> <?php echo $productSku;?></div></div>
										<div class="leftProdAttributes"><div class="col-md-4 text-bold">Seller Name:</div><div class="col-md-8"> <?php echo $seller_name;?></div></div>
										<div class="leftProdAttributes"><div class="col-md-4 text-bold">Product Type:</div><div class="col-md-8"> <?php echo $productType;?></div></div>
										<div class="leftProdAttributes"><div class="col-md-4 text-bold">Product Weight:</div><div class="col-md-8"> <?php echo $productWeight;?></div></div>
										<div class="leftProdAttributes"><div class="col-md-4 text-bold">Product Description:</div><div class="col-md-8"> <?php echo $productDescription;?></div></div>
										
									<div>
								  </div>
								  <div class="modal-footer modalcustom-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									<!--<button type="button" class="btn btn-primary">Save changes</button>-->
								  </div>
								</div>
							  </div>
							</div>
							<!-- End Modal -->
						 
						 </td>
						 
						</tr>												
					<?php }?>
						
				<?php } ?>
				</tbody>
			</table>			
		</div>
	</div>
</div>

</div>

<?php 
if(isset($_POST['submit']) ){
	if(isset($_POST['factoryPriceValue'])){ // Price field is not empty
		if (preg_match('/^[0-9]+$/', $_POST['factoryPriceValue'])) {
			
			$productId = $_POST['product_id'];
			$seller_id = $_POST['seller_id'];
			
				// contains only 0-9
			$tableNameInsert = $resource->getTableName('furniture2demo.factory_user_product_table');
				//Insert Data into table
			$sqlInsert = "Insert Into " . $tableNameInsert . " 
						(product_id, vendor_id, factory_user_id, factory_name,price) 
							Values 
						('$productId','$seller_id','$factoryUserId','$factoryUserFullName','". $_POST['factoryPriceValue']."')";

			$resultInsert = $connection->query($sqlInsert);
			//echo"</pre>"; print_r($resultInsert);echo"<pre>"; exit;
			if($resultInsert){
				echo "<script>alert('Price Updated Successfully!')</script>"; 
			}else{
				echo "<script>alert('Problems in Updating Price. Please try again Later.')</script>";
			}
		} else {
		  // contains other stuff
		  echo "<script>alert('Please enter numbers only!')</script>";
		}
	}else{
		echo "script>alert('Please fill the price field!')</script>";
	}
}
?>

<?php } else {?>
        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7 text">
                            <h1><strong>Custom Living</h1>
                            <div class="description">
                            	<p>
	                        Welcome to the home of Custom Living!
                            	</p>
                            </div>
                        </div>
                        <div class="col-sm-5 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Sign up</h3>
                            		
                        		</div>
                            </div>
                            <div class="form-bottom">
							
						<form role="form" class="registration-form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" name="form" id="form" method="post">
						
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-first-name">First name</label>
			                        	<input type="text" name="form-first-name" placeholder="First name..." class="form-first-name form-control" id="form-first-name">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-last-name">Last name</label>
			                        	<input type="text" name="form-last-name" placeholder="Last name..." class="form-last-name form-control" id="form-last-name">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-email">Email</label>
			                        	<input type="text" name="form-email" placeholder="Email..." class="form-email form-control" id="form-email">
			                        </div>
									<div class="form-group">
			                        	<label class="sr-only" for="form-password">Password</label>
			                        	<input type="password" name="form-password" placeholder="Pasword..." class="form-password form-control" id="form-password">
			                        </div>
			                    <input type="submit" value="Sign Up" name="registration" class="btn btn-success  btn-login-submit"> 
			               
			                    </form>
		                    </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
<?php } ?>

        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>