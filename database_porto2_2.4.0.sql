/*
SQLyog Ultimate v8.55 
MySQL - 5.5.5-10.1.13-MariaDB : Database - porto2_quick
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `admin_passwords` */

DROP TABLE IF EXISTS `admin_passwords`;

CREATE TABLE `admin_passwords` (
  `password_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Password Id',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'User Id',
  `password_hash` varchar(100) DEFAULT NULL COMMENT 'Password Hash',
  `expires` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Expires',
  `last_updated` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Last Updated',
  PRIMARY KEY (`password_id`),
  KEY `ADMIN_PASSWORDS_USER_ID` (`user_id`),
  CONSTRAINT `ADMIN_PASSWORDS_USER_ID_ADMIN_USER_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `admin_user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Admin Passwords';

/*Data for the table `admin_passwords` */

/*Table structure for table `admin_system_messages` */

DROP TABLE IF EXISTS `admin_system_messages`;

CREATE TABLE `admin_system_messages` (
  `identity` varchar(100) NOT NULL COMMENT 'Message id',
  `severity` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Problem type',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create date',
  PRIMARY KEY (`identity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Admin System Messages';

/*Data for the table `admin_system_messages` */

/*Table structure for table `admin_user` */

DROP TABLE IF EXISTS `admin_user`;

CREATE TABLE `admin_user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'User ID',
  `firstname` varchar(32) DEFAULT NULL COMMENT 'User First Name',
  `lastname` varchar(32) DEFAULT NULL COMMENT 'User Last Name',
  `email` varchar(128) DEFAULT NULL COMMENT 'User Email',
  `username` varchar(40) DEFAULT NULL COMMENT 'User Login',
  `password` varchar(255) NOT NULL COMMENT 'User Password',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'User Created Time',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'User Modified Time',
  `logdate` timestamp NULL DEFAULT NULL COMMENT 'User Last Login Time',
  `lognum` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'User Login Number',
  `reload_acl_flag` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Reload ACL',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'User Is Active',
  `extra` text COMMENT 'User Extra Data',
  `rp_token` text COMMENT 'Reset Password Link Token',
  `rp_token_created_at` timestamp NULL DEFAULT NULL COMMENT 'Reset Password Link Token Creation Date',
  `interface_locale` varchar(16) NOT NULL DEFAULT 'en_US' COMMENT 'Backend interface locale',
  `failures_num` smallint(6) DEFAULT '0' COMMENT 'Failure Number',
  `first_failure` timestamp NULL DEFAULT NULL COMMENT 'First Failure',
  `lock_expires` timestamp NULL DEFAULT NULL COMMENT 'Expiration Lock Dates',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `ADMIN_USER_USERNAME` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Admin User Table';

/*Data for the table `admin_user` */

/*Table structure for table `admin_user_session` */

DROP TABLE IF EXISTS `admin_user_session`;

CREATE TABLE `admin_user_session` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `session_id` varchar(128) NOT NULL COMMENT 'Session id value',
  `user_id` int(10) unsigned DEFAULT NULL COMMENT 'Admin User ID',
  `status` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Current Session status',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created Time',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
  `ip` varchar(15) NOT NULL COMMENT 'Remote user IP',
  PRIMARY KEY (`id`),
  KEY `ADMIN_USER_SESSION_SESSION_ID` (`session_id`),
  KEY `ADMIN_USER_SESSION_USER_ID` (`user_id`),
  CONSTRAINT `ADMIN_USER_SESSION_USER_ID_ADMIN_USER_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `admin_user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Admin User sessions table';

/*Data for the table `admin_user_session` */

/*Table structure for table `adminnotification_inbox` */

DROP TABLE IF EXISTS `adminnotification_inbox`;

CREATE TABLE `adminnotification_inbox` (
  `notification_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Notification id',
  `severity` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Problem type',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create date',
  `title` varchar(255) NOT NULL COMMENT 'Title',
  `description` text COMMENT 'Description',
  `url` varchar(255) DEFAULT NULL COMMENT 'Url',
  `is_read` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Flag if notification read',
  `is_remove` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Flag if notification might be removed',
  PRIMARY KEY (`notification_id`),
  KEY `ADMINNOTIFICATION_INBOX_SEVERITY` (`severity`),
  KEY `ADMINNOTIFICATION_INBOX_IS_READ` (`is_read`),
  KEY `ADMINNOTIFICATION_INBOX_IS_REMOVE` (`is_remove`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Adminnotification Inbox';

/*Data for the table `adminnotification_inbox` */

insert  into `adminnotification_inbox`(`notification_id`,`severity`,`date_added`,`title`,`description`,`url`,`is_read`,`is_remove`) values (1,1,'2016-09-20 20:28:28','Important Updates for WeltPixel Products',' In the past months we made multiple product improvements and added new features, we hope you will find them useful. You can download the latest release from your account. Leave a review or report a bug to any of our products, send us an email and you will receive a 10% discount code. Change logs at this link: http://weltpixel.com/blog/2016/09/19/weltpixels-last-30-days-updates/','http://weltpixel.com/blog/2016/09/19/weltpixels-last-30-days-updates/',1,1),(2,4,'2016-08-02 04:56:40','Title for the second push notification','Lorem ipsum, in dolorem content for notification 2/second','http://weltpixel.com/blog/2',1,1),(3,4,'2016-08-02 02:56:40','Title for the first push notification','Lorem ipsum, in dolorem content for notification1','http://weltpixel.com/blog',1,1);

/*Table structure for table `authorization_role` */

DROP TABLE IF EXISTS `authorization_role`;

CREATE TABLE `authorization_role` (
  `role_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Role ID',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent Role ID',
  `tree_level` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Role Tree Level',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Role Sort Order',
  `role_type` varchar(1) NOT NULL DEFAULT '0' COMMENT 'Role Type',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'User ID',
  `user_type` varchar(16) DEFAULT NULL COMMENT 'User Type',
  `role_name` varchar(50) DEFAULT NULL COMMENT 'Role Name',
  PRIMARY KEY (`role_id`),
  KEY `AUTHORIZATION_ROLE_PARENT_ID_SORT_ORDER` (`parent_id`,`sort_order`),
  KEY `AUTHORIZATION_ROLE_TREE_LEVEL` (`tree_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Admin Role Table';

/*Data for the table `authorization_role` */

insert  into `authorization_role`(`role_id`,`parent_id`,`tree_level`,`sort_order`,`role_type`,`user_id`,`user_type`,`role_name`) values (1,0,1,1,'G',0,'2','Administrators'),(2,1,2,0,'U',1,'2','admin'),(3,1,2,0,'U',2,'2','admin');

/*Table structure for table `authorization_rule` */

DROP TABLE IF EXISTS `authorization_rule`;

CREATE TABLE `authorization_rule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule ID',
  `role_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Role ID',
  `resource_id` varchar(255) DEFAULT NULL COMMENT 'Resource ID',
  `privileges` varchar(20) DEFAULT NULL COMMENT 'Privileges',
  `permission` varchar(10) DEFAULT NULL COMMENT 'Permission',
  PRIMARY KEY (`rule_id`),
  KEY `AUTHORIZATION_RULE_RESOURCE_ID_ROLE_ID` (`resource_id`,`role_id`),
  KEY `AUTHORIZATION_RULE_ROLE_ID_RESOURCE_ID` (`role_id`,`resource_id`),
  CONSTRAINT `AUTHORIZATION_RULE_ROLE_ID_AUTHORIZATION_ROLE_ROLE_ID` FOREIGN KEY (`role_id`) REFERENCES `authorization_role` (`role_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Admin Rule Table';

/*Data for the table `authorization_rule` */

insert  into `authorization_rule`(`rule_id`,`role_id`,`resource_id`,`privileges`,`permission`) values (1,1,'Magento_Backend::all',NULL,'allow');

/*Table structure for table `cache` */

DROP TABLE IF EXISTS `cache`;

CREATE TABLE `cache` (
  `id` varchar(200) NOT NULL COMMENT 'Cache Id',
  `data` mediumblob COMMENT 'Cache Data',
  `create_time` int(11) DEFAULT NULL COMMENT 'Cache Creation Time',
  `update_time` int(11) DEFAULT NULL COMMENT 'Time of Cache Updating',
  `expire_time` int(11) DEFAULT NULL COMMENT 'Cache Expiration Time',
  PRIMARY KEY (`id`),
  KEY `CACHE_EXPIRE_TIME` (`expire_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Caches';

/*Data for the table `cache` */

/*Table structure for table `cache_tag` */

DROP TABLE IF EXISTS `cache_tag`;

CREATE TABLE `cache_tag` (
  `tag` varchar(100) NOT NULL COMMENT 'Tag',
  `cache_id` varchar(200) NOT NULL COMMENT 'Cache Id',
  PRIMARY KEY (`tag`,`cache_id`),
  KEY `CACHE_TAG_CACHE_ID` (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tag Caches';

/*Data for the table `cache_tag` */

/*Table structure for table `captcha_log` */

DROP TABLE IF EXISTS `captcha_log`;

CREATE TABLE `captcha_log` (
  `type` varchar(32) NOT NULL COMMENT 'Type',
  `value` varchar(32) NOT NULL COMMENT 'Value',
  `count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Count',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Update Time',
  PRIMARY KEY (`type`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Count Login Attempts';

/*Data for the table `captcha_log` */

/*Table structure for table `catalog_category_entity` */

DROP TABLE IF EXISTS `catalog_category_entity`;

CREATE TABLE `catalog_category_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attriute Set ID',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent Category ID',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation Time',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
  `path` varchar(255) NOT NULL COMMENT 'Tree Path',
  `position` int(11) NOT NULL COMMENT 'Position',
  `level` int(11) NOT NULL DEFAULT '0' COMMENT 'Tree Level',
  `children_count` int(11) NOT NULL COMMENT 'Child Count',
  PRIMARY KEY (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_LEVEL` (`level`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COMMENT='Catalog Category Table';

/*Data for the table `catalog_category_entity` */

insert  into `catalog_category_entity`(`entity_id`,`attribute_set_id`,`parent_id`,`created_at`,`updated_at`,`path`,`position`,`level`,`children_count`) values (1,0,0,'2016-08-26 10:46:27','2016-08-26 17:09:36','1',0,0,48),(2,3,1,'2016-08-26 10:46:29','2016-08-26 17:09:36','1/2',1,1,47),(3,3,2,'2016-08-26 16:00:33','2016-08-26 16:25:28','1/2/3',1,2,18),(4,3,2,'2016-08-26 16:03:41','2016-08-26 17:05:20','1/2/4',2,2,21),(5,3,2,'2016-08-26 16:06:12','2016-08-26 17:09:36','1/2/5',3,2,5),(6,3,3,'2016-08-26 16:08:09','2016-08-26 16:16:41','1/2/3/6',1,3,4),(7,3,3,'2016-08-26 16:09:52','2016-08-26 16:20:28','1/2/3/7',2,3,4),(8,3,3,'2016-08-26 16:11:03','2016-08-26 16:23:04','1/2/3/8',3,3,3),(9,3,3,'2016-08-26 16:12:18','2016-08-26 16:25:28','1/2/3/9',4,3,3),(10,3,6,'2016-08-26 16:13:51','2016-08-26 16:13:51','1/2/3/6/10',1,4,0),(11,3,6,'2016-08-26 16:15:01','2016-08-26 16:15:02','1/2/3/6/11',2,4,0),(12,3,6,'2016-08-26 16:15:51','2016-08-26 16:15:51','1/2/3/6/12',3,4,0),(13,3,6,'2016-08-26 16:16:41','2016-08-26 16:16:42','1/2/3/6/13',4,4,0),(14,3,7,'2016-08-26 16:17:56','2016-08-26 16:17:56','1/2/3/7/14',1,4,0),(15,3,7,'2016-08-26 16:18:43','2016-08-26 16:18:43','1/2/3/7/15',2,4,0),(16,3,7,'2016-08-26 16:19:42','2016-08-26 16:19:42','1/2/3/7/16',3,4,0),(17,3,7,'2016-08-26 16:20:28','2016-08-26 16:20:28','1/2/3/7/17',4,4,0),(18,3,8,'2016-08-26 16:21:20','2016-08-26 16:21:20','1/2/3/8/18',1,4,0),(19,3,8,'2016-08-26 16:22:07','2016-08-26 16:22:07','1/2/3/8/19',2,4,0),(20,3,8,'2016-08-26 16:23:04','2016-08-26 16:23:04','1/2/3/8/20',3,4,0),(21,3,9,'2016-08-26 16:24:00','2016-08-26 16:24:00','1/2/3/9/21',1,4,0),(22,3,9,'2016-08-26 16:24:45','2016-08-26 16:24:45','1/2/3/9/22',2,4,0),(23,3,9,'2016-08-26 16:25:28','2016-08-26 16:25:28','1/2/3/9/23',3,4,0),(24,3,4,'2016-08-26 16:29:12','2016-08-26 16:55:10','1/2/4/24',1,3,6),(25,3,4,'2016-08-26 16:48:58','2016-08-26 17:00:08','1/2/4/25',2,3,6),(26,3,4,'2016-08-26 16:49:50','2016-08-26 17:05:20','1/2/4/26',3,3,6),(27,3,24,'2016-08-26 16:50:37','2016-08-26 16:50:37','1/2/4/24/27',1,4,0),(28,3,24,'2016-08-26 16:51:54','2016-08-26 16:51:54','1/2/4/24/28',2,4,0),(29,3,24,'2016-08-26 16:52:41','2016-08-26 16:52:41','1/2/4/24/29',3,4,0),(30,3,24,'2016-08-26 16:53:23','2016-08-26 16:53:23','1/2/4/24/30',4,4,0),(31,3,24,'2016-08-26 16:54:20','2016-08-26 16:54:20','1/2/4/24/31',5,4,0),(32,3,24,'2016-08-26 16:55:10','2016-08-26 16:55:11','1/2/4/24/32',6,4,0),(33,3,25,'2016-08-26 16:56:03','2016-08-26 16:56:03','1/2/4/25/33',1,4,0),(34,3,25,'2016-08-26 16:56:48','2016-08-26 16:56:48','1/2/4/25/34',2,4,0),(35,3,25,'2016-08-26 16:57:36','2016-08-26 16:57:36','1/2/4/25/35',3,4,0),(36,3,25,'2016-08-26 16:58:18','2016-08-26 16:58:18','1/2/4/25/36',4,4,0),(37,3,25,'2016-08-26 16:59:10','2016-08-26 16:59:10','1/2/4/25/37',5,4,0),(38,3,25,'2016-08-26 17:00:08','2016-08-26 17:00:08','1/2/4/25/38',6,4,0),(39,3,26,'2016-08-26 17:00:54','2016-08-26 17:00:54','1/2/4/26/39',1,4,0),(40,3,26,'2016-08-26 17:01:45','2016-08-26 17:01:45','1/2/4/26/40',2,4,0),(41,3,26,'2016-08-26 17:02:36','2016-08-26 17:02:36','1/2/4/26/41',3,4,0),(42,3,26,'2016-08-26 17:03:23','2016-08-26 17:03:23','1/2/4/26/42',4,4,0),(43,3,26,'2016-08-26 17:04:15','2016-08-26 17:04:15','1/2/4/26/43',5,4,0),(44,3,26,'2016-08-26 17:05:20','2016-08-26 17:05:20','1/2/4/26/44',6,4,0),(45,3,5,'2016-08-26 17:06:15','2016-08-26 17:06:15','1/2/5/45',1,3,0),(46,3,5,'2016-08-26 17:07:24','2016-08-26 17:07:24','1/2/5/46',2,3,0),(47,3,5,'2016-08-26 17:08:14','2016-08-26 17:08:14','1/2/5/47',3,3,0),(48,3,5,'2016-08-26 17:08:57','2016-08-26 17:09:36','1/2/5/48',4,3,1),(49,3,48,'2016-08-26 17:09:36','2016-08-26 17:09:36','1/2/5/48/49',1,4,0);

/*Table structure for table `catalog_category_entity_datetime` */

DROP TABLE IF EXISTS `catalog_category_entity_datetime`;

CREATE TABLE `catalog_category_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` datetime DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_CATEGORY_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DATETIME_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DATETIME_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_CATEGORY_ENTITY_DATETIME_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_ENTT_DTIME_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_ENTT_DTIME_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Datetime Attribute Backend Table';

/*Data for the table `catalog_category_entity_datetime` */

/*Table structure for table `catalog_category_entity_decimal` */

DROP TABLE IF EXISTS `catalog_category_entity_decimal`;

CREATE TABLE `catalog_category_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` decimal(12,4) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_CATEGORY_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DECIMAL_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DECIMAL_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_CATEGORY_ENTITY_DECIMAL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_ENTT_DEC_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_ENTT_DEC_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Decimal Attribute Backend Table';

/*Data for the table `catalog_category_entity_decimal` */

/*Table structure for table `catalog_category_entity_int` */

DROP TABLE IF EXISTS `catalog_category_entity_int`;

CREATE TABLE `catalog_category_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` int(11) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_CATEGORY_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_CATEGORY_ENTITY_INT_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_CATEGORY_ENTITY_INT_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_CATEGORY_ENTITY_INT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_ENTT_INT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_ENTT_INT_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=334 DEFAULT CHARSET=utf8 COMMENT='Catalog Category Integer Attribute Backend Table';

/*Data for the table `catalog_category_entity_int` */

insert  into `catalog_category_entity_int`(`value_id`,`attribute_id`,`store_id`,`entity_id`,`value`) values (1,69,0,1,1),(2,46,0,2,1),(3,69,0,2,1),(4,46,0,3,1),(5,53,0,3,11),(6,54,0,3,1),(7,69,0,3,1),(8,70,0,3,0),(9,71,0,3,0),(10,136,0,3,0),(11,150,0,3,0),(12,46,0,4,1),(13,54,0,4,1),(14,69,0,4,1),(15,70,0,4,0),(16,71,0,4,0),(17,136,0,4,0),(18,150,0,4,0),(19,46,0,5,1),(20,54,0,5,1),(21,69,0,5,1),(22,70,0,5,0),(23,71,0,5,0),(24,136,0,5,0),(25,150,0,5,0),(26,46,0,6,1),(27,54,0,6,1),(28,69,0,6,1),(29,70,0,6,0),(30,71,0,6,0),(31,136,0,6,0),(32,150,0,6,0),(33,46,0,7,1),(34,54,0,7,1),(35,69,0,7,1),(36,70,0,7,0),(37,71,0,7,0),(38,136,0,7,0),(39,150,0,7,0),(40,46,0,8,1),(41,54,0,8,1),(42,69,0,8,1),(43,70,0,8,0),(44,71,0,8,0),(45,136,0,8,0),(46,150,0,8,0),(47,46,0,9,1),(48,54,0,9,1),(49,69,0,9,1),(50,70,0,9,0),(51,71,0,9,0),(52,136,0,9,0),(53,150,0,9,0),(54,46,0,10,1),(55,54,0,10,1),(56,69,0,10,1),(57,70,0,10,0),(58,71,0,10,0),(59,136,0,10,0),(60,150,0,10,0),(61,46,0,11,1),(62,54,0,11,1),(63,69,0,11,1),(64,70,0,11,0),(65,71,0,11,0),(66,136,0,11,0),(67,150,0,11,0),(68,46,0,12,1),(69,54,0,12,1),(70,69,0,12,1),(71,70,0,12,0),(72,71,0,12,0),(73,136,0,12,0),(74,150,0,12,0),(75,46,0,13,1),(76,54,0,13,1),(77,69,0,13,1),(78,70,0,13,0),(79,71,0,13,0),(80,136,0,13,0),(81,150,0,13,0),(82,46,0,14,1),(83,54,0,14,1),(84,69,0,14,1),(85,70,0,14,0),(86,71,0,14,0),(87,136,0,14,0),(88,150,0,14,0),(89,46,0,15,1),(90,54,0,15,1),(91,69,0,15,1),(92,70,0,15,0),(93,71,0,15,0),(94,136,0,15,0),(95,150,0,15,0),(96,46,0,16,1),(97,54,0,16,1),(98,69,0,16,1),(99,70,0,16,0),(100,71,0,16,0),(101,136,0,16,0),(102,150,0,16,0),(103,46,0,17,1),(104,54,0,17,1),(105,69,0,17,1),(106,70,0,17,0),(107,71,0,17,0),(108,136,0,17,0),(109,150,0,17,0),(110,46,0,18,1),(111,54,0,18,1),(112,69,0,18,1),(113,70,0,18,0),(114,71,0,18,0),(115,136,0,18,0),(116,150,0,18,0),(117,46,0,19,1),(118,54,0,19,1),(119,69,0,19,1),(120,70,0,19,0),(121,71,0,19,0),(122,136,0,19,0),(123,150,0,19,0),(124,46,0,20,1),(125,54,0,20,1),(126,69,0,20,1),(127,70,0,20,0),(128,71,0,20,0),(129,136,0,20,0),(130,150,0,20,0),(131,46,0,21,1),(132,54,0,21,1),(133,69,0,21,1),(134,70,0,21,0),(135,71,0,21,0),(136,136,0,21,0),(137,150,0,21,0),(138,46,0,22,1),(139,54,0,22,1),(140,69,0,22,1),(141,70,0,22,0),(142,71,0,22,0),(143,136,0,22,0),(144,150,0,22,0),(145,46,0,23,1),(146,54,0,23,1),(147,69,0,23,1),(148,70,0,23,0),(149,71,0,23,0),(150,136,0,23,0),(151,150,0,23,0),(152,46,0,24,1),(153,54,0,24,1),(154,69,0,24,1),(155,70,0,24,0),(156,71,0,24,0),(157,136,0,24,0),(158,150,0,24,0),(159,46,0,25,1),(160,54,0,25,1),(161,69,0,25,1),(162,70,0,25,0),(163,71,0,25,0),(164,136,0,25,0),(165,150,0,25,0),(166,46,0,26,1),(167,54,0,26,1),(168,69,0,26,1),(169,70,0,26,0),(170,71,0,26,0),(171,136,0,26,0),(172,150,0,26,0),(173,46,0,27,1),(174,54,0,27,1),(175,69,0,27,1),(176,70,0,27,0),(177,71,0,27,0),(178,136,0,27,0),(179,150,0,27,0),(180,46,0,28,1),(181,54,0,28,1),(182,69,0,28,1),(183,70,0,28,0),(184,71,0,28,0),(185,136,0,28,0),(186,150,0,28,0),(187,46,0,29,1),(188,54,0,29,1),(189,69,0,29,1),(190,70,0,29,0),(191,71,0,29,0),(192,136,0,29,0),(193,150,0,29,0),(194,46,0,30,1),(195,54,0,30,1),(196,69,0,30,1),(197,70,0,30,0),(198,71,0,30,0),(199,136,0,30,0),(200,150,0,30,0),(201,46,0,31,1),(202,54,0,31,1),(203,69,0,31,1),(204,70,0,31,0),(205,71,0,31,0),(206,136,0,31,0),(207,150,0,31,0),(208,46,0,32,1),(209,54,0,32,1),(210,69,0,32,1),(211,70,0,32,0),(212,71,0,32,0),(213,136,0,32,0),(214,150,0,32,0),(215,46,0,33,1),(216,54,0,33,1),(217,69,0,33,1),(218,70,0,33,0),(219,71,0,33,0),(220,136,0,33,0),(221,150,0,33,0),(222,46,0,34,1),(223,54,0,34,1),(224,69,0,34,1),(225,70,0,34,0),(226,71,0,34,0),(227,136,0,34,0),(228,150,0,34,0),(229,46,0,35,1),(230,54,0,35,1),(231,69,0,35,1),(232,70,0,35,0),(233,71,0,35,0),(234,136,0,35,0),(235,150,0,35,0),(236,46,0,36,1),(237,54,0,36,1),(238,69,0,36,1),(239,70,0,36,0),(240,71,0,36,0),(241,136,0,36,0),(242,150,0,36,0),(243,46,0,37,1),(244,54,0,37,1),(245,69,0,37,1),(246,70,0,37,0),(247,71,0,37,0),(248,136,0,37,0),(249,150,0,37,0),(250,46,0,38,1),(251,54,0,38,1),(252,69,0,38,1),(253,70,0,38,0),(254,71,0,38,0),(255,136,0,38,0),(256,150,0,38,0),(257,46,0,39,1),(258,54,0,39,1),(259,69,0,39,1),(260,70,0,39,0),(261,71,0,39,0),(262,136,0,39,0),(263,150,0,39,0),(264,46,0,40,1),(265,54,0,40,1),(266,69,0,40,1),(267,70,0,40,0),(268,71,0,40,0),(269,136,0,40,0),(270,150,0,40,0),(271,46,0,41,1),(272,54,0,41,1),(273,69,0,41,1),(274,70,0,41,0),(275,71,0,41,0),(276,136,0,41,0),(277,150,0,41,0),(278,46,0,42,1),(279,54,0,42,1),(280,69,0,42,1),(281,70,0,42,0),(282,71,0,42,0),(283,136,0,42,0),(284,150,0,42,0),(285,46,0,43,1),(286,54,0,43,1),(287,69,0,43,1),(288,70,0,43,0),(289,71,0,43,0),(290,136,0,43,0),(291,150,0,43,0),(292,46,0,44,1),(293,54,0,44,1),(294,69,0,44,1),(295,70,0,44,0),(296,71,0,44,0),(297,136,0,44,0),(298,150,0,44,0),(299,46,0,45,1),(300,54,0,45,1),(301,69,0,45,1),(302,70,0,45,0),(303,71,0,45,0),(304,136,0,45,0),(305,150,0,45,0),(306,46,0,46,1),(307,54,0,46,1),(308,69,0,46,1),(309,70,0,46,0),(310,71,0,46,0),(311,136,0,46,0),(312,150,0,46,0),(313,46,0,47,1),(314,54,0,47,1),(315,69,0,47,1),(316,70,0,47,0),(317,71,0,47,0),(318,136,0,47,0),(319,150,0,47,0),(320,46,0,48,1),(321,54,0,48,1),(322,69,0,48,1),(323,70,0,48,0),(324,71,0,48,0),(325,136,0,48,0),(326,150,0,48,0),(327,46,0,49,1),(328,54,0,49,1),(329,69,0,49,1),(330,70,0,49,0),(331,71,0,49,0),(332,136,0,49,0),(333,150,0,49,0);

/*Table structure for table `catalog_category_entity_text` */

DROP TABLE IF EXISTS `catalog_category_entity_text`;

CREATE TABLE `catalog_category_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` text COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_CATEGORY_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_CATEGORY_ENTITY_TEXT_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_CATEGORY_ENTITY_TEXT_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_CATEGORY_ENTITY_TEXT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_ENTT_TEXT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_ENTT_TEXT_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Catalog Category Text Attribute Backend Table';

/*Data for the table `catalog_category_entity_text` */

insert  into `catalog_category_entity_text`(`value_id`,`attribute_id`,`store_id`,`entity_id`,`value`) values (1,64,0,3,'<move element=\"category.cms\" destination=\"content\" before=\"-\"/>'),(2,148,0,3,'<p style=\"margin: 0;\"><img style=\"position: absolute; right: -3px; top: -5px; height: 269px; width: auto; max-width: none; z-index: -1; border-radius: 8px;\" src=\"{{media url=\"wysiwyg/smartwave/porto/megamenu/fashion_b.png\"}}\" alt=\"\" /></p>'),(3,154,0,3,'<div class=\"row\" style=\"margin:0 -10px;\">\r\n    <div class=\"col-sm-6\" style=\"padding: 0 10px;\">\r\n        <img src=\"{{media url=\"wysiwyg/smartwave/porto/category/onepagecategory/banner_1_1.jpg\"}}\" alt=\"\" style=\"margin-top: 50px;\">\r\n    </div>\r\n    <div class=\"col-sm-6\" style=\"padding: 0 10px;\">\r\n        <img src=\"{{media url=\"wysiwyg/smartwave/porto/category/onepagecategory/banner_1_2.jpg\"}}\" alt=\"\" style=\"margin-top: 50px;\">\r\n    </div>\r\n</div>'),(4,144,0,4,'<div style=\"margin: 0 10px; padding: 10px 0 15px; border-bottom: 1px solid #eee; width: 63%; color: #000;\"><strong style=\"margin-right: 10px;\">SUGGESTIONS:</strong><a style=\"margin-right: 5px; color: #000;\" href=\"#\">3D</a><a style=\"margin-right: 5px; color: #000;\" href=\"#\">MOBILE</a><a style=\"margin-right: 5px; color: #000;\" href=\"#\">CAMERAS</a><a style=\"color: #000;\" href=\"#\">HEADSETS</a></div>'),(5,148,0,4,'<div class=\"menu-right-block\" style=\"position: relative; text-align: center;\"><img style=\"margin-top: 20px;\" src=\"{{media url=\"wysiwyg/smartwave/porto/megamenu/electronic.png\"}}\" alt=\"\" />\r\n<div style=\"position: absolute; top: -35px; left: -15px; text-align: left;\">\r\n<h2 style=\"font-size: 23px; font-weight: 600; color: #fff; background-color: #2e2e2e; line-height: 1; padding: 6px 50px 6px 8px; margin: 0 0 10px;\">SHOP NOW <strong style=\"font-weight: bold;\">3D</strong> <strong style=\"font-weight: 800;\">TV\'s</strong></h2>\r\n<a class=\"btn btn-default\" style=\"padding: 5px 7px 5px 15px; color: #fff; border: 0; font-size: 13px;\" href=\"#\">VIEW NOW <em class=\"porto-icon-right-dir\"></em></a></div>\r\n<div style=\"position: absolute; bottom: 8px; width: 60%; text-align: center; left: 50px; line-height: 14px; font-size: 13px;\">This is a custom block. You can add any images or links here.</div>\r\n</div>'),(6,154,0,4,'<div class=\"row\" style=\"margin:0 -10px;\">\r\n    <div class=\"col-sm-6\" style=\"padding: 0 10px;\">\r\n        <img src=\"{{media url=\"wysiwyg/smartwave/porto/category/onepagecategory/banner_2_1.jpg\"}}\" alt=\"\" style=\"margin-top: 50px;\">\r\n    </div>\r\n    <div class=\"col-sm-6\" style=\"padding: 0 10px;\">\r\n        <img src=\"{{media url=\"wysiwyg/smartwave/porto/category/onepagecategory/banner_2_2.jpg\"}}\" alt=\"\" style=\"margin-top: 50px;\">\r\n    </div>\r\n</div>'),(7,47,0,5,'<div class=\"category-banner\">\r\n<div class=\"full-width-image-banner\" style=\"background: url({{media url=\'wysiwyg/smartwave/porto/category/gear/full-width-banner.jpg\'}}) 50% 50% no-repeat; background-size: cover;\">\r\n<div class=\"content\" style=\"position: absolute; z-index: 1; top: 50%; width: 100%; text-align: center;\">\r\n<h2 style=\"color: #fff; font-weight: 600;\">CATEGORY <strong style=\"font-weight: 800;\">BANNER</strong></h2>\r\n<p style=\"color: #fff; font-weight: 300;\">Set banners and description for any category of your website.</p>\r\n</div>\r\n</div>\r\n</div>'),(8,154,0,5,'<div class=\"row\" style=\"margin:0 -10px;\">\r\n    <div class=\"col-sm-6\" style=\"padding: 0 10px;\">\r\n        <img src=\"{{media url=\"wysiwyg/smartwave/porto/category/onepagecategory/banner_7_1.jpg\"}}\" alt=\"\" style=\"margin-top: 50px;\">\r\n    </div>\r\n    <div class=\"col-sm-6\" style=\"padding: 0 10px;\">\r\n        <img src=\"{{media url=\"wysiwyg/smartwave/porto/category/onepagecategory/banner_7_2.jpg\"}}\" alt=\"\" style=\"margin-top: 50px;\">\r\n    </div>\r\n</div>');

/*Table structure for table `catalog_category_entity_varchar` */

DROP TABLE IF EXISTS `catalog_category_entity_varchar`;

CREATE TABLE `catalog_category_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_CATEGORY_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_CATEGORY_ENTITY_VARCHAR_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_CATEGORY_ENTITY_VARCHAR_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_CATEGORY_ENTITY_VARCHAR_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_ENTT_VCHR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_ENTT_VCHR_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=302 DEFAULT CHARSET=utf8 COMMENT='Catalog Category Varchar Attribute Backend Table';

/*Data for the table `catalog_category_entity_varchar` */

insert  into `catalog_category_entity_varchar`(`value_id`,`attribute_id`,`store_id`,`entity_id`,`value`) values (1,45,0,1,'Root Catalog'),(2,45,0,2,'Default Category'),(3,52,0,2,'PRODUCTS'),(4,45,0,3,'Fashion'),(5,52,0,3,'PRODUCTS_AND_PAGE'),(6,117,0,3,'fashion'),(7,118,0,3,'fashion'),(8,137,0,3,'staticwidth'),(9,138,0,3,'600px'),(10,139,0,3,'2'),(11,141,0,3,'label1'),(12,145,0,3,'0'),(13,147,0,3,'5'),(14,152,0,3,'porto-icon-shirt'),(15,153,0,3,'#1bd49e'),(16,45,0,4,'Electronics'),(17,52,0,4,'PRODUCTS'),(18,117,0,4,'electronics'),(19,118,0,4,'electronics'),(20,137,0,4,'fullwidth'),(21,139,0,4,'3'),(22,145,0,4,'0'),(23,147,0,4,'4'),(24,152,0,4,'porto-icon-computer'),(25,153,0,4,'#f69a33'),(26,45,0,5,'Gear'),(27,52,0,5,'PRODUCTS'),(28,117,0,5,'gear'),(29,118,0,5,'gear'),(30,137,0,5,'classic'),(31,145,0,5,'0'),(32,147,0,5,'0'),(33,152,0,5,'porto-icon-gift'),(34,153,0,5,'#2eacb3'),(35,45,0,6,'Women'),(36,52,0,6,'PRODUCTS'),(37,117,0,6,'women'),(38,118,0,6,'fashion/women'),(39,145,0,6,'0'),(40,147,0,6,'0'),(41,45,0,7,'Men'),(42,52,0,7,'PRODUCTS'),(43,117,0,7,'men'),(44,118,0,7,'fashion/men'),(45,145,0,7,'0'),(46,147,0,7,'0'),(47,45,0,8,'Jewellery'),(48,52,0,8,'PRODUCTS'),(49,117,0,8,'jewellery'),(50,118,0,8,'fashion/jewellery'),(51,145,0,8,'0'),(52,147,0,8,'0'),(53,45,0,9,'Kids Fashion'),(54,52,0,9,'PRODUCTS'),(55,117,0,9,'kids-fashion'),(56,118,0,9,'fashion/kids-fashion'),(57,145,0,9,'0'),(58,147,0,9,'0'),(59,45,0,10,'Tops & Blouses'),(60,52,0,10,'PRODUCTS'),(61,117,0,10,'tops-blouses'),(62,118,0,10,'fashion/women/tops-blouses'),(63,145,0,10,'0'),(64,147,0,10,'0'),(65,45,0,11,'Accessories'),(66,52,0,11,'PRODUCTS'),(67,117,0,11,'accessories'),(68,118,0,11,'fashion/women/accessories'),(69,145,0,11,'0'),(70,147,0,11,'0'),(71,45,0,12,'Bottoms & Skirts'),(72,52,0,12,'PRODUCTS'),(73,117,0,12,'bottoms-skirts'),(74,118,0,12,'fashion/women/bottoms-skirts'),(75,145,0,12,'0'),(76,147,0,12,'0'),(77,45,0,13,'Shoes & Boots'),(78,52,0,13,'PRODUCTS'),(79,117,0,13,'shoes-boots'),(80,118,0,13,'fashion/women/shoes-boots'),(81,145,0,13,'0'),(82,147,0,13,'0'),(83,45,0,14,'Accessories'),(84,52,0,14,'PRODUCTS'),(85,117,0,14,'accessories'),(86,118,0,14,'fashion/men/accessories'),(87,145,0,14,'0'),(88,147,0,14,'0'),(89,45,0,15,'Watch Fashion'),(90,52,0,15,'PRODUCTS'),(91,117,0,15,'watch-fashion'),(92,118,0,15,'fashion/men/watch-fashion'),(93,145,0,15,'0'),(94,147,0,15,'0'),(95,45,0,16,'Tees, Knits & Polos'),(96,52,0,16,'PRODUCTS'),(97,117,0,16,'tees-knits-polos'),(98,118,0,16,'fashion/men/tees-knits-polos'),(99,145,0,16,'0'),(100,147,0,16,'0'),(101,45,0,17,'Pants & Denim'),(102,52,0,17,'PRODUCTS'),(103,117,0,17,'pants-denim'),(104,118,0,17,'fashion/men/pants-denim'),(105,145,0,17,'0'),(106,147,0,17,'0'),(107,45,0,18,'Rings'),(108,52,0,18,'PRODUCTS'),(109,117,0,18,'rings'),(110,118,0,18,'fashion/jewellery/rings'),(111,145,0,18,'0'),(112,147,0,18,'0'),(113,45,0,19,'Earrings'),(114,52,0,19,'PRODUCTS'),(115,117,0,19,'earrings'),(116,118,0,19,'fashion/jewellery/earrings'),(117,145,0,19,'0'),(118,147,0,19,'0'),(119,45,0,20,'Pendants & Necklaces'),(120,52,0,20,'PRODUCTS'),(121,117,0,20,'pendants-necklaces'),(122,118,0,20,'fashion/jewellery/pendants-necklaces'),(123,145,0,20,'0'),(124,147,0,20,'0'),(125,45,0,21,'Casual Shoes'),(126,52,0,21,'PRODUCTS'),(127,117,0,21,'casual-shoes'),(128,118,0,21,'fashion/kids-fashion/casual-shoes'),(129,145,0,21,'0'),(130,147,0,21,'0'),(131,45,0,22,'Spring & Autumn'),(132,52,0,22,'PRODUCTS'),(133,117,0,22,'spring-autumn'),(134,118,0,22,'fashion/kids-fashion/spring-autumn'),(135,145,0,22,'0'),(136,147,0,22,'0'),(137,45,0,23,'Winter Sneakers'),(138,52,0,23,'PRODUCTS'),(139,117,0,23,'winter-sneakers'),(140,118,0,23,'fashion/kids-fashion/winter-sneakers'),(141,145,0,23,'0'),(142,147,0,23,'0'),(143,45,0,24,'Smart TVs'),(145,52,0,24,'PRODUCTS'),(146,117,0,24,'smart-tvs'),(147,118,0,24,'electronics/smart-tvs'),(148,145,0,24,'0'),(149,147,0,24,'0'),(150,45,0,25,'Cameras'),(152,52,0,25,'PRODUCTS'),(153,117,0,25,'cameras'),(154,118,0,25,'electronics/cameras'),(155,145,0,25,'0'),(156,147,0,25,'0'),(157,45,0,26,'Games'),(159,52,0,26,'PRODUCTS'),(160,117,0,26,'games'),(161,118,0,26,'electronics/games'),(162,145,0,26,'0'),(163,147,0,26,'0'),(164,45,0,27,'TV, Audio'),(165,52,0,27,'PRODUCTS'),(166,117,0,27,'tv-audio'),(167,118,0,27,'electronics/smart-tvs/tv-audio'),(168,145,0,27,'0'),(169,147,0,27,'0'),(170,45,0,28,'Computers & Tablets'),(171,52,0,28,'PRODUCTS'),(172,117,0,28,'computers-tablets'),(173,118,0,28,'electronics/smart-tvs/computers-tablets'),(174,145,0,28,'0'),(175,147,0,28,'0'),(176,45,0,29,'Home Office Equipments'),(177,52,0,29,'PRODUCTS'),(178,117,0,29,'home-office-equipments'),(179,118,0,29,'electronics/smart-tvs/home-office-equipments'),(180,145,0,29,'0'),(181,147,0,29,'0'),(182,45,0,30,'GPS Navigation'),(183,52,0,30,'PRODUCTS'),(184,117,0,30,'gps-navigation'),(185,118,0,30,'electronics/smart-tvs/gps-navigation'),(186,145,0,30,'0'),(187,147,0,30,'0'),(188,45,0,31,'Car Audio, Video & GPS'),(189,52,0,31,'PRODUCTS'),(190,117,0,31,'car-audio-video-gps'),(191,118,0,31,'electronics/smart-tvs/car-audio-video-gps'),(192,145,0,31,'0'),(193,147,0,31,'0'),(194,45,0,32,'Radios & Clock Radios'),(195,52,0,32,'PRODUCTS'),(196,117,0,32,'radios-clock-radios'),(197,118,0,32,'electronics/smart-tvs/radios-clock-radios'),(198,145,0,32,'0'),(199,147,0,32,'0'),(200,45,0,33,'Cell Phones & Accessories'),(201,52,0,33,'PRODUCTS'),(202,117,0,33,'cell-phones-accessories'),(203,118,0,33,'electronics/cameras/cell-phones-accessories'),(204,145,0,33,'0'),(205,147,0,33,'0'),(206,45,0,34,'Cameras & Photo'),(207,52,0,34,'PRODUCTS'),(208,117,0,34,'cameras-photo'),(209,118,0,34,'electronics/cameras/cameras-photo'),(210,145,0,34,'0'),(211,147,0,34,'0'),(212,45,0,35,'Photo Accessories'),(213,52,0,35,'PRODUCTS'),(214,117,0,35,'photo-accessories'),(215,118,0,35,'electronics/cameras/photo-accessories'),(216,145,0,35,'0'),(217,147,0,35,'0'),(218,45,0,36,'IP Phones'),(219,52,0,36,'PRODUCTS'),(220,117,0,36,'ip-phones'),(221,118,0,36,'electronics/cameras/ip-phones'),(222,145,0,36,'0'),(223,147,0,36,'0'),(224,45,0,37,'Samsung Galaxy Phones'),(225,52,0,37,'PRODUCTS'),(226,117,0,37,'samsung-galaxy-phones'),(227,118,0,37,'electronics/cameras/samsung-galaxy-phones'),(228,145,0,37,'0'),(229,147,0,37,'0'),(230,45,0,38,'iPad & Android Tablets'),(231,52,0,38,'PRODUCTS'),(232,117,0,38,'ipad-android-tablets'),(233,118,0,38,'electronics/cameras/ipad-android-tablets'),(234,145,0,38,'0'),(235,147,0,38,'0'),(236,45,0,39,'e-Book Readers'),(237,52,0,39,'PRODUCTS'),(238,117,0,39,'e-book-readers'),(239,118,0,39,'electronics/games/e-book-readers'),(240,145,0,39,'0'),(241,147,0,39,'0'),(242,45,0,40,'Video Games & Consolers'),(243,52,0,40,'PRODUCTS'),(244,117,0,40,'video-games-consolers'),(245,118,0,40,'electronics/games/video-games-consolers'),(246,145,0,40,'0'),(247,147,0,40,'0'),(248,45,0,41,'Printers & Scanners'),(249,52,0,41,'PRODUCTS'),(250,117,0,41,'printers-scanners'),(251,118,0,41,'electronics/games/printers-scanners'),(252,145,0,41,'0'),(253,147,0,41,'0'),(254,45,0,42,'Digital Picture Frames'),(255,52,0,42,'PRODUCTS'),(256,117,0,42,'digital-picture-frames'),(257,118,0,42,'electronics/games/digital-picture-frames'),(258,145,0,42,'0'),(259,147,0,42,'0'),(260,45,0,43,'3D Fashion Games'),(261,52,0,43,'PRODUCTS'),(262,117,0,43,'3d-fashion-games'),(263,118,0,43,'electronics/games/3d-fashion-games'),(264,145,0,43,'0'),(265,147,0,43,'0'),(266,45,0,44,'Game Machine & Devices'),(267,52,0,44,'PRODUCTS'),(268,117,0,44,'game-machine-devices'),(269,118,0,44,'electronics/games/game-machine-devices'),(270,145,0,44,'0'),(271,147,0,44,'0'),(272,45,0,45,'Bags'),(273,52,0,45,'PRODUCTS'),(274,117,0,45,'bags'),(275,118,0,45,'gear/bags'),(276,145,0,45,'0'),(277,147,0,45,'0'),(278,45,0,46,'Fitness Equipment'),(279,52,0,46,'PRODUCTS'),(280,117,0,46,'fitness-equipment'),(281,118,0,46,'gear/fitness-equipment'),(282,145,0,46,'0'),(283,147,0,46,'0'),(284,45,0,47,'Watches'),(285,52,0,47,'PRODUCTS'),(286,117,0,47,'watches'),(287,118,0,47,'gear/watches'),(288,145,0,47,'0'),(289,147,0,47,'0'),(290,45,0,48,'Training'),(291,52,0,48,'PRODUCTS'),(292,117,0,48,'training'),(293,118,0,48,'gear/training'),(294,145,0,48,'0'),(295,147,0,48,'0'),(296,45,0,49,'Video Download'),(297,52,0,49,'PRODUCTS'),(298,117,0,49,'video-download'),(299,118,0,49,'gear/training/video-download'),(300,145,0,49,'0'),(301,147,0,49,'0');

/*Table structure for table `catalog_category_product` */

DROP TABLE IF EXISTS `catalog_category_product`;

CREATE TABLE `catalog_category_product` (
  `entity_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  PRIMARY KEY (`entity_id`,`category_id`,`product_id`),
  UNIQUE KEY `CATALOG_CATEGORY_PRODUCT_CATEGORY_ID_PRODUCT_ID` (`category_id`,`product_id`),
  KEY `CATALOG_CATEGORY_PRODUCT_PRODUCT_ID` (`product_id`),
  CONSTRAINT `CAT_CTGR_PRD_CTGR_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`category_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_PRD_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COMMENT='Catalog Product To Category Linkage Table';

/*Data for the table `catalog_category_product` */

insert  into `catalog_category_product`(`entity_id`,`category_id`,`product_id`,`position`) values (1,2,1,1),(2,3,1,1),(3,6,1,1),(4,10,1,1),(5,2,2,1),(6,3,2,1),(7,6,2,1),(8,10,2,1),(9,2,3,1),(10,3,3,1),(11,6,3,1),(12,10,3,1),(13,2,4,1),(14,3,4,1),(15,6,4,1),(16,10,4,1),(17,2,5,1),(18,3,5,1),(19,6,5,1),(20,10,5,1),(21,2,6,1),(22,3,6,1),(23,6,6,1),(24,10,6,1),(25,2,7,1),(26,3,7,1),(27,6,7,1),(28,10,7,1),(29,2,8,1),(30,3,8,1),(31,6,8,1),(32,10,8,1),(33,2,9,1),(34,3,9,1),(35,6,9,1),(36,10,9,1),(37,2,10,1),(38,3,10,1),(39,6,10,1),(40,2,11,1),(41,3,11,1),(42,6,11,1),(43,2,12,1),(44,3,12,1),(45,6,12,1),(46,2,13,1),(47,3,13,1),(48,6,13,1),(49,2,14,1),(50,3,14,1),(51,6,14,1),(52,2,15,1),(53,3,15,1),(54,6,15,1);

/*Table structure for table `catalog_category_product_index` */

DROP TABLE IF EXISTS `catalog_category_product_index`;

CREATE TABLE `catalog_category_product_index` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `position` int(11) DEFAULT NULL COMMENT 'Position',
  `is_parent` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Parent',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `visibility` smallint(5) unsigned NOT NULL COMMENT 'Visibility',
  PRIMARY KEY (`category_id`,`product_id`,`store_id`),
  KEY `CAT_CTGR_PRD_IDX_PRD_ID_STORE_ID_CTGR_ID_VISIBILITY` (`product_id`,`store_id`,`category_id`,`visibility`),
  KEY `CAT_CTGR_PRD_IDX_STORE_ID_CTGR_ID_VISIBILITY_IS_PARENT_POSITION` (`store_id`,`category_id`,`visibility`,`is_parent`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Product Index';

/*Data for the table `catalog_category_product_index` */

insert  into `catalog_category_product_index`(`category_id`,`product_id`,`position`,`is_parent`,`store_id`,`visibility`) values (2,1,1,1,1,4),(2,2,1,1,1,4),(2,3,1,1,1,4),(2,4,1,1,1,4),(2,5,1,1,1,4),(2,6,1,1,1,4),(2,7,1,1,1,4),(2,8,1,1,1,4),(2,9,1,1,1,4),(2,15,1,1,1,4),(3,1,1,1,1,4),(3,2,1,1,1,4),(3,3,1,1,1,4),(3,4,1,1,1,4),(3,5,1,1,1,4),(3,6,1,1,1,4),(3,7,1,1,1,4),(3,8,1,1,1,4),(3,9,1,1,1,4),(3,15,1,1,1,4),(6,1,1,1,1,4),(6,2,1,1,1,4),(6,3,1,1,1,4),(6,4,1,1,1,4),(6,5,1,1,1,4),(6,6,1,1,1,4),(6,7,1,1,1,4),(6,8,1,1,1,4),(6,9,1,1,1,4),(6,15,1,1,1,4),(10,1,1,1,1,4),(10,2,1,1,1,4),(10,3,1,1,1,4),(10,4,1,1,1,4),(10,5,1,1,1,4),(10,6,1,1,1,4),(10,7,1,1,1,4),(10,8,1,1,1,4),(10,9,1,1,1,4),(2,1,1,1,2,4),(2,2,1,1,2,4),(2,3,1,1,2,4),(2,4,1,1,2,4),(2,5,1,1,2,4),(2,6,1,1,2,4),(2,7,1,1,2,4),(2,8,1,1,2,4),(2,9,1,1,2,4),(2,15,1,1,2,4),(3,1,1,1,2,4),(3,2,1,1,2,4),(3,3,1,1,2,4),(3,4,1,1,2,4),(3,5,1,1,2,4),(3,6,1,1,2,4),(3,7,1,1,2,4),(3,8,1,1,2,4),(3,9,1,1,2,4),(3,15,1,1,2,4),(6,1,1,1,2,4),(6,2,1,1,2,4),(6,3,1,1,2,4),(6,4,1,1,2,4),(6,5,1,1,2,4),(6,6,1,1,2,4),(6,7,1,1,2,4),(6,8,1,1,2,4),(6,9,1,1,2,4),(6,15,1,1,2,4),(10,1,1,1,2,4),(10,2,1,1,2,4),(10,3,1,1,2,4),(10,4,1,1,2,4),(10,5,1,1,2,4),(10,6,1,1,2,4),(10,7,1,1,2,4),(10,8,1,1,2,4),(10,9,1,1,2,4);

/*Table structure for table `catalog_category_product_index_tmp` */

DROP TABLE IF EXISTS `catalog_category_product_index_tmp`;

CREATE TABLE `catalog_category_product_index_tmp` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  `is_parent` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Parent',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `visibility` smallint(5) unsigned NOT NULL COMMENT 'Visibility',
  KEY `CAT_CTGR_PRD_IDX_TMP_PRD_ID_CTGR_ID_STORE_ID` (`product_id`,`category_id`,`store_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Category Product Indexer Temp Table';

/*Data for the table `catalog_category_product_index_tmp` */

/*Table structure for table `catalog_compare_item` */

DROP TABLE IF EXISTS `catalog_compare_item`;

CREATE TABLE `catalog_compare_item` (
  `catalog_compare_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Compare Item ID',
  `visitor_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Visitor ID',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store ID',
  PRIMARY KEY (`catalog_compare_item_id`),
  KEY `CATALOG_COMPARE_ITEM_PRODUCT_ID` (`product_id`),
  KEY `CATALOG_COMPARE_ITEM_VISITOR_ID_PRODUCT_ID` (`visitor_id`,`product_id`),
  KEY `CATALOG_COMPARE_ITEM_CUSTOMER_ID_PRODUCT_ID` (`customer_id`,`product_id`),
  KEY `CATALOG_COMPARE_ITEM_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_COMPARE_ITEM_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CATALOG_COMPARE_ITEM_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CATALOG_COMPARE_ITEM_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Compare Table';

/*Data for the table `catalog_compare_item` */

/*Table structure for table `catalog_eav_attribute` */

DROP TABLE IF EXISTS `catalog_eav_attribute`;

CREATE TABLE `catalog_eav_attribute` (
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `frontend_input_renderer` varchar(255) DEFAULT NULL COMMENT 'Frontend Input Renderer',
  `is_global` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Global',
  `is_visible` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Visible',
  `is_searchable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Searchable',
  `is_filterable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable',
  `is_comparable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Comparable',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `is_html_allowed_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is HTML Allowed On Front',
  `is_used_for_price_rules` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used For Price Rules',
  `is_filterable_in_search` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable In Search',
  `used_in_product_listing` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used In Product Listing',
  `used_for_sort_by` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used For Sorting',
  `apply_to` varchar(255) DEFAULT NULL COMMENT 'Apply To',
  `is_visible_in_advanced_search` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible In Advanced Search',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  `is_wysiwyg_enabled` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is WYSIWYG Enabled',
  `is_used_for_promo_rules` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used For Promo Rules',
  `is_required_in_admin_store` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Required In Admin Store',
  `is_used_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used in Grid',
  `is_visible_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible in Grid',
  `is_filterable_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable in Grid',
  `search_weight` float NOT NULL DEFAULT '1' COMMENT 'Search Weight',
  `additional_data` text COMMENT 'Additional swatch attributes data',
  PRIMARY KEY (`attribute_id`),
  KEY `CATALOG_EAV_ATTRIBUTE_USED_FOR_SORT_BY` (`used_for_sort_by`),
  KEY `CATALOG_EAV_ATTRIBUTE_USED_IN_PRODUCT_LISTING` (`used_in_product_listing`),
  CONSTRAINT `CATALOG_EAV_ATTRIBUTE_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog EAV Attribute Table';

/*Data for the table `catalog_eav_attribute` */

insert  into `catalog_eav_attribute`(`attribute_id`,`frontend_input_renderer`,`is_global`,`is_visible`,`is_searchable`,`is_filterable`,`is_comparable`,`is_visible_on_front`,`is_html_allowed_on_front`,`is_used_for_price_rules`,`is_filterable_in_search`,`used_in_product_listing`,`used_for_sort_by`,`apply_to`,`is_visible_in_advanced_search`,`position`,`is_wysiwyg_enabled`,`is_used_for_promo_rules`,`is_required_in_admin_store`,`is_used_in_grid`,`is_visible_in_grid`,`is_filterable_in_grid`,`search_weight`,`additional_data`) values (45,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(46,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(47,NULL,0,1,0,0,0,0,1,0,0,0,0,NULL,0,0,1,0,0,0,0,0,1,NULL),(48,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(49,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(50,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(51,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(52,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(53,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(54,NULL,1,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(55,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(56,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(57,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(58,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(59,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(60,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(61,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(62,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(63,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(64,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(65,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(66,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(67,'Magento\\Catalog\\Block\\Adminhtml\\Category\\Helper\\Sortby\\Available',0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(68,'Magento\\Catalog\\Block\\Adminhtml\\Category\\Helper\\Sortby\\DefaultSortby',0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(69,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(70,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(71,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(72,'Magento\\Catalog\\Block\\Adminhtml\\Category\\Helper\\Pricestep',0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(73,NULL,0,1,1,0,0,0,0,0,0,1,1,NULL,1,0,0,0,0,0,0,0,5,NULL),(74,NULL,1,1,1,0,1,0,0,0,0,0,0,NULL,1,0,0,0,0,0,0,0,6,NULL),(75,NULL,0,1,1,0,1,0,1,0,0,0,0,NULL,1,0,1,0,0,0,0,0,1,NULL),(76,NULL,0,1,1,0,1,0,1,0,0,1,0,NULL,1,0,1,0,0,1,0,0,1,NULL),(77,NULL,2,1,1,1,0,0,0,0,0,1,1,'simple,virtual,bundle,downloadable,configurable',1,0,0,0,0,0,0,0,1,NULL),(78,NULL,2,1,0,0,0,0,0,0,0,1,0,'simple,virtual,bundle,downloadable,configurable',0,0,0,0,0,1,0,1,1,NULL),(79,NULL,2,1,0,0,0,0,0,0,0,1,0,'simple,virtual,bundle,downloadable,configurable',0,0,0,0,0,1,0,0,1,NULL),(80,NULL,2,1,0,0,0,0,0,0,0,1,0,'simple,virtual,bundle,downloadable,configurable',0,0,0,0,0,1,0,0,1,NULL),(81,NULL,2,1,0,0,0,0,0,0,0,0,0,'simple,virtual,downloadable',0,0,0,0,0,1,0,1,1,NULL),(82,'Magento\\Catalog\\Block\\Adminhtml\\Product\\Helper\\Form\\Weight',1,1,0,0,0,0,0,0,0,0,0,'simple,virtual,bundle,downloadable,configurable',0,0,0,0,0,1,0,1,1,NULL),(83,NULL,1,1,1,1,1,0,0,0,0,0,0,'simple',1,0,0,0,0,1,0,1,1,NULL),(84,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,1,0,1,1,NULL),(85,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,1,0,1,1,NULL),(86,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,1,0,1,1,NULL),(87,NULL,0,1,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(88,NULL,0,1,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(89,NULL,0,1,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(90,NULL,1,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(91,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(92,NULL,2,1,0,0,0,0,0,0,0,0,0,'simple,virtual,bundle,downloadable,configurable',0,0,0,0,0,0,0,0,1,NULL),(93,NULL,1,1,1,1,1,0,0,0,0,0,0,'simple,virtual,configurable',1,0,0,0,0,1,0,1,1,NULL),(94,NULL,2,1,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,1,0,0,1,NULL),(95,NULL,2,1,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,1,0,0,1,NULL),(96,NULL,1,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(97,'Magento\\Framework\\Data\\Form\\Element\\Hidden',2,1,1,0,0,0,0,0,0,1,0,NULL,0,0,0,0,1,0,0,0,1,NULL),(98,NULL,0,0,0,0,0,0,0,0,0,0,0,'simple,virtual,bundle,downloadable,configurable',0,0,0,0,0,0,0,0,1,NULL),(99,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,1,0,0,0,1,NULL),(100,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,1,0,1,1,NULL),(101,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,1,0,0,1,NULL),(102,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,1,0,0,1,NULL),(103,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(104,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,1,0,0,1,NULL),(105,'Magento\\Catalog\\Block\\Adminhtml\\Product\\Helper\\Form\\Category',1,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(106,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(107,NULL,1,0,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(108,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(109,NULL,0,0,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(110,NULL,0,0,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(111,NULL,0,0,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(112,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(113,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(114,NULL,2,1,0,0,0,0,0,0,0,0,0,'simple,bundle,grouped,configurable',0,0,0,0,0,1,0,1,1,NULL),(115,'Magento\\CatalogInventory\\Block\\Adminhtml\\Form\\Field\\Stock',1,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(116,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,1,0,0,1,NULL),(117,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(118,NULL,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(119,NULL,0,1,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,1,0,1,1,NULL),(120,NULL,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(121,'Magento\\Msrp\\Block\\Adminhtml\\Product\\Helper\\Form\\Type',2,1,0,0,0,0,0,0,0,1,0,'simple,virtual,downloadable,bundle,configurable',0,0,0,0,0,1,0,1,1,NULL),(122,'Magento\\Msrp\\Block\\Adminhtml\\Product\\Helper\\Form\\Type\\Price',2,1,0,0,0,0,0,0,0,1,0,'simple,virtual,downloadable,bundle,configurable',0,0,0,0,0,0,0,0,1,NULL),(123,NULL,1,1,0,0,0,0,0,0,0,1,0,'bundle',0,0,0,0,0,0,0,0,1,NULL),(124,NULL,1,1,0,0,0,0,0,0,0,0,0,'bundle',0,0,0,0,0,0,0,0,1,NULL),(125,NULL,1,1,0,0,0,0,0,0,0,1,0,'bundle',0,0,0,0,0,0,0,0,1,NULL),(126,NULL,1,1,0,0,0,0,0,0,0,1,0,'bundle',0,0,0,0,0,0,0,0,1,NULL),(127,NULL,1,1,0,0,0,0,0,0,0,1,0,'bundle',0,0,0,0,0,0,0,0,1,NULL),(128,NULL,1,0,0,0,0,0,0,0,0,1,0,'downloadable',0,0,0,0,0,0,0,0,1,NULL),(129,NULL,0,0,0,0,0,0,0,0,0,0,0,'downloadable',0,0,0,0,0,0,0,0,1,NULL),(130,NULL,0,0,0,0,0,0,0,0,0,0,0,'downloadable',0,0,0,0,0,0,0,0,1,NULL),(131,NULL,1,0,0,0,0,0,0,0,0,1,0,'downloadable',0,0,0,0,0,0,0,0,1,NULL),(132,NULL,0,1,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(133,NULL,2,1,1,0,0,0,0,0,0,1,0,'simple,virtual,bundle,downloadable,configurable',0,0,0,0,0,1,0,1,1,NULL),(134,'Magento\\GiftMessage\\Block\\Adminhtml\\Product\\Helper\\Form\\Config',1,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,1,0,0,1,NULL),(135,NULL,1,1,0,0,0,0,0,0,0,1,0,'simple,configurable,virtual,bundle,downloadable',0,0,0,0,0,0,0,0,1,NULL),(136,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(137,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(138,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(139,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(140,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(141,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(142,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(143,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(144,NULL,0,1,0,0,0,0,1,0,0,0,0,NULL,0,0,1,0,0,0,0,0,1,NULL),(145,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(146,NULL,0,1,0,0,0,0,1,0,0,0,0,NULL,0,0,1,0,0,0,0,0,1,NULL),(147,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(148,NULL,0,1,0,0,0,0,1,0,0,0,0,NULL,0,0,1,0,0,0,0,0,1,NULL),(149,NULL,0,1,0,0,0,0,1,0,0,0,0,NULL,0,0,1,0,0,0,0,0,1,NULL),(150,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(151,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(152,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(153,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(154,NULL,0,1,0,0,0,0,1,0,0,0,0,NULL,0,0,1,0,0,0,0,0,1,NULL);

/*Table structure for table `catalog_product_bundle_option` */

DROP TABLE IF EXISTS `catalog_product_bundle_option`;

CREATE TABLE `catalog_product_bundle_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `required` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Required',
  `position` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Position',
  `type` varchar(255) DEFAULT NULL COMMENT 'Type',
  PRIMARY KEY (`option_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_OPTION_PARENT_ID` (`parent_id`),
  CONSTRAINT `CAT_PRD_BNDL_OPT_PARENT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`parent_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Option';

/*Data for the table `catalog_product_bundle_option` */

/*Table structure for table `catalog_product_bundle_option_value` */

DROP TABLE IF EXISTS `catalog_product_bundle_option_value`;

CREATE TABLE `catalog_product_bundle_option_value` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `option_id` int(10) unsigned NOT NULL COMMENT 'Option Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_BUNDLE_OPTION_VALUE_OPTION_ID_STORE_ID` (`option_id`,`store_id`),
  CONSTRAINT `CAT_PRD_BNDL_OPT_VAL_OPT_ID_CAT_PRD_BNDL_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_bundle_option` (`option_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Option Value';

/*Data for the table `catalog_product_bundle_option_value` */

/*Table structure for table `catalog_product_bundle_price_index` */

DROP TABLE IF EXISTS `catalog_product_bundle_price_index`;

CREATE TABLE `catalog_product_bundle_price_index` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `min_price` decimal(12,4) NOT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) NOT NULL COMMENT 'Max Price',
  PRIMARY KEY (`entity_id`,`website_id`,`customer_group_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_PRICE_INDEX_WEBSITE_ID` (`website_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_PRICE_INDEX_CUSTOMER_GROUP_ID` (`customer_group_id`),
  CONSTRAINT `CAT_PRD_BNDL_PRICE_IDX_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_BNDL_PRICE_IDX_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_BNDL_PRICE_IDX_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Price Index';

/*Data for the table `catalog_product_bundle_price_index` */

/*Table structure for table `catalog_product_bundle_selection` */

DROP TABLE IF EXISTS `catalog_product_bundle_selection`;

CREATE TABLE `catalog_product_bundle_selection` (
  `selection_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Selection Id',
  `option_id` int(10) unsigned NOT NULL COMMENT 'Option Id',
  `parent_product_id` int(10) unsigned NOT NULL COMMENT 'Parent Product Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `position` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Position',
  `is_default` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Default',
  `selection_price_type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Selection Price Type',
  `selection_price_value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Selection Price Value',
  `selection_qty` decimal(12,4) DEFAULT NULL COMMENT 'Selection Qty',
  `selection_can_change_qty` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Selection Can Change Qty',
  PRIMARY KEY (`selection_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_SELECTION_OPTION_ID` (`option_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_SELECTION_PRODUCT_ID` (`product_id`),
  CONSTRAINT `CAT_PRD_BNDL_SELECTION_OPT_ID_CAT_PRD_BNDL_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_bundle_option` (`option_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_BNDL_SELECTION_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Selection';

/*Data for the table `catalog_product_bundle_selection` */

/*Table structure for table `catalog_product_bundle_selection_price` */

DROP TABLE IF EXISTS `catalog_product_bundle_selection_price`;

CREATE TABLE `catalog_product_bundle_selection_price` (
  `selection_id` int(10) unsigned NOT NULL COMMENT 'Selection Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `selection_price_type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Selection Price Type',
  `selection_price_value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Selection Price Value',
  PRIMARY KEY (`selection_id`,`website_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_SELECTION_PRICE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `CAT_PRD_BNDL_SELECTION_PRICE_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_DCF37523AA05D770A70AA4ED7C2616E4` FOREIGN KEY (`selection_id`) REFERENCES `catalog_product_bundle_selection` (`selection_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Selection Price';

/*Data for the table `catalog_product_bundle_selection_price` */

/*Table structure for table `catalog_product_bundle_stock_index` */

DROP TABLE IF EXISTS `catalog_product_bundle_stock_index`;

CREATE TABLE `catalog_product_bundle_stock_index` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_id` smallint(5) unsigned NOT NULL COMMENT 'Stock Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `stock_status` smallint(6) DEFAULT '0' COMMENT 'Stock Status',
  PRIMARY KEY (`entity_id`,`website_id`,`stock_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Stock Index';

/*Data for the table `catalog_product_bundle_stock_index` */

/*Table structure for table `catalog_product_entity` */

DROP TABLE IF EXISTS `catalog_product_entity`;

CREATE TABLE `catalog_product_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set ID',
  `type_id` varchar(32) NOT NULL DEFAULT 'simple' COMMENT 'Type ID',
  `sku` varchar(64) DEFAULT NULL COMMENT 'SKU',
  `has_options` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Has Options',
  `required_options` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Required Options',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation Time',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
  PRIMARY KEY (`entity_id`),
  KEY `CATALOG_PRODUCT_ENTITY_ATTRIBUTE_SET_ID` (`attribute_set_id`),
  KEY `CATALOG_PRODUCT_ENTITY_SKU` (`sku`),
  CONSTRAINT `CAT_PRD_ENTT_ATTR_SET_ID_EAV_ATTR_SET_ATTR_SET_ID` FOREIGN KEY (`attribute_set_id`) REFERENCES `eav_attribute_set` (`attribute_set_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Table';

/*Data for the table `catalog_product_entity` */

insert  into `catalog_product_entity`(`entity_id`,`attribute_set_id`,`type_id`,`sku`,`has_options`,`required_options`,`created_at`,`updated_at`) values (1,4,'simple','Women Blouse',0,0,'2016-08-26 22:49:12','2016-08-26 22:49:12'),(2,4,'simple','Fashion Dress',0,0,'2016-08-26 22:49:18','2016-08-26 22:50:31'),(3,4,'simple','Blue Denim Jeans',0,0,'2016-08-26 22:50:34','2016-08-26 22:51:40'),(4,4,'simple','Sheri Collar Shirt',0,0,'2016-08-26 22:51:43','2016-08-26 22:52:54'),(5,4,'simple','Fashion Jacket',0,0,'2016-08-26 22:52:57','2016-08-26 22:53:46'),(6,4,'simple','Sample Product',0,0,'2016-08-26 22:53:49','2016-08-26 22:55:03'),(7,4,'simple','Elizabeth Kint Top',0,0,'2016-08-26 22:55:05','2016-08-26 22:56:11'),(8,4,'simple','Pink Shirt',0,0,'2016-08-26 22:56:13','2016-08-26 22:57:03'),(9,4,'simple','Sample Fashion',0,0,'2016-08-26 22:57:05','2016-08-26 22:59:15'),(10,9,'simple','Sample Configurable-White',0,0,'2016-08-26 23:04:41','2016-08-26 23:04:41'),(11,9,'simple','Sample Configurable-Black',0,0,'2016-08-26 23:04:43','2016-08-26 23:04:43'),(12,9,'simple','Sample Configurable-Red',0,0,'2016-08-26 23:04:44','2016-08-26 23:04:44'),(13,9,'simple','Sample Configurable-Blue',0,0,'2016-08-26 23:04:46','2016-08-26 23:04:46'),(14,9,'simple','Sample Configurable-Green',0,0,'2016-08-26 23:04:47','2016-08-26 23:04:47'),(15,9,'configurable','Sample Configurable',1,1,'2016-08-26 23:04:48','2016-08-26 23:04:48');

/*Table structure for table `catalog_product_entity_datetime` */

DROP TABLE IF EXISTS `catalog_product_entity_datetime`;

CREATE TABLE `catalog_product_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` datetime DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_ENTITY_DATETIME_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_PRODUCT_ENTITY_DATETIME_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_DTIME_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_DTIME_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Datetime Attribute Backend Table';

/*Data for the table `catalog_product_entity_datetime` */

insert  into `catalog_product_entity_datetime`(`value_id`,`attribute_id`,`store_id`,`entity_id`,`value`) values (1,79,0,6,'2016-08-26 14:55:03'),(2,94,0,6,'2016-08-26 14:55:03'),(3,101,0,6,'2016-08-26 14:55:03'),(5,94,0,7,'2016-08-26 00:00:00'),(6,101,0,7,'2016-08-26 00:00:00'),(9,101,0,8,'2016-08-26 00:00:00'),(11,101,0,9,'2016-08-26 00:00:00');

/*Table structure for table `catalog_product_entity_decimal` */

DROP TABLE IF EXISTS `catalog_product_entity_decimal`;

CREATE TABLE `catalog_product_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` decimal(12,4) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_DECIMAL_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `CATALOG_PRODUCT_ENTITY_DECIMAL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_DEC_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_DEC_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Decimal Attribute Backend Table';

/*Data for the table `catalog_product_entity_decimal` */

insert  into `catalog_product_entity_decimal`(`value_id`,`attribute_id`,`store_id`,`entity_id`,`value`) values (1,77,0,1,'70.0000'),(2,82,0,1,'1.0000'),(3,77,0,2,'90.0000'),(4,82,0,2,'1.0000'),(6,77,0,3,'200.0000'),(7,82,0,3,'1.0000'),(9,77,0,4,'70.0000'),(10,82,0,4,'1.0000'),(12,77,0,5,'33.0000'),(13,82,0,5,'1.0000'),(15,77,0,6,'70.0000'),(16,82,0,6,'1.0000'),(18,78,0,6,'50.0000'),(19,77,0,7,'220.0000'),(21,82,0,7,'1.0000'),(23,77,0,8,'90.0000'),(24,82,0,8,'1.0000'),(26,77,0,9,'90.0000'),(27,82,0,9,'1.0000'),(29,77,0,10,'120.0000'),(30,82,0,10,'1.0000'),(31,77,0,11,'120.0000'),(32,82,0,11,'1.0000'),(33,77,0,12,'120.0000'),(34,82,0,12,'1.0000'),(35,77,0,13,'120.0000'),(36,82,0,13,'1.0000'),(37,77,0,14,'120.0000'),(38,82,0,14,'1.0000'),(39,77,0,15,'120.0000'),(40,82,0,15,'1.0000');

/*Table structure for table `catalog_product_entity_gallery` */

DROP TABLE IF EXISTS `catalog_product_entity_gallery`;

CREATE TABLE `catalog_product_entity_gallery` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_GALLERY_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_GALLERY_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_PRODUCT_ENTITY_GALLERY_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_ENTITY_GALLERY_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_PRODUCT_ENTITY_GALLERY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_GLR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_GLR_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Gallery Attribute Backend Table';

/*Data for the table `catalog_product_entity_gallery` */

/*Table structure for table `catalog_product_entity_int` */

DROP TABLE IF EXISTS `catalog_product_entity_int`;

CREATE TABLE `catalog_product_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` int(11) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_ENTITY_INT_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_PRODUCT_ENTITY_INT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_INT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_INT_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Integer Attribute Backend Table';

/*Data for the table `catalog_product_entity_int` */

insert  into `catalog_product_entity_int`(`value_id`,`attribute_id`,`store_id`,`entity_id`,`value`) values (1,97,0,1,1),(2,99,0,1,4),(3,115,0,1,1),(4,133,0,1,2),(5,135,0,1,1),(6,97,0,2,1),(7,99,0,2,4),(8,115,0,2,1),(9,133,0,2,2),(10,135,0,2,1),(12,97,0,3,1),(13,99,0,3,4),(14,115,0,3,1),(15,133,0,3,2),(16,135,0,3,1),(18,97,0,4,1),(19,99,0,4,4),(20,115,0,4,1),(21,133,0,4,2),(22,135,0,4,1),(24,97,0,5,1),(25,99,0,5,4),(26,115,0,5,1),(27,133,0,5,2),(28,135,0,5,1),(30,97,0,6,1),(31,99,0,6,4),(32,115,0,6,1),(33,133,0,6,2),(34,135,0,6,1),(36,97,0,7,1),(37,99,0,7,4),(38,115,0,7,1),(39,133,0,7,2),(40,135,0,7,1),(42,97,0,8,1),(43,99,0,8,4),(44,115,0,8,1),(45,133,0,8,2),(46,135,0,8,1),(48,97,0,9,1),(49,99,0,9,4),(50,115,0,9,1),(51,133,0,9,2),(52,135,0,9,1),(54,93,0,10,4),(55,97,0,10,1),(56,99,0,10,1),(57,115,0,10,1),(58,133,0,10,2),(59,135,0,10,1),(60,93,0,11,5),(61,97,0,11,1),(62,99,0,11,1),(63,115,0,11,1),(64,133,0,11,2),(65,135,0,11,1),(66,93,0,12,6),(67,97,0,12,1),(68,99,0,12,1),(69,115,0,12,1),(70,133,0,12,2),(71,135,0,12,1),(72,93,0,13,7),(73,97,0,13,1),(74,99,0,13,1),(75,115,0,13,1),(76,133,0,13,2),(77,135,0,13,1),(78,93,0,14,8),(79,97,0,14,1),(80,99,0,14,1),(81,115,0,14,1),(82,133,0,14,2),(83,135,0,14,1),(84,97,0,15,1),(85,99,0,15,4),(86,115,0,15,1),(87,133,0,15,2),(88,135,0,15,1);

/*Table structure for table `catalog_product_entity_media_gallery` */

DROP TABLE IF EXISTS `catalog_product_entity_media_gallery`;

CREATE TABLE `catalog_product_entity_media_gallery` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  `media_type` varchar(32) NOT NULL DEFAULT 'image' COMMENT 'Media entry type',
  `disabled` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Visibility status',
  PRIMARY KEY (`value_id`),
  KEY `CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Media Gallery Attribute Backend Table';

/*Data for the table `catalog_product_entity_media_gallery` */

/*Table structure for table `catalog_product_entity_media_gallery_value` */

DROP TABLE IF EXISTS `catalog_product_entity_media_gallery_value`;

CREATE TABLE `catalog_product_entity_media_gallery_value` (
  `value_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Value ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `label` varchar(255) DEFAULT NULL COMMENT 'Label',
  `position` int(10) unsigned DEFAULT NULL COMMENT 'Position',
  `disabled` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Disabled',
  `record_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Record Id',
  PRIMARY KEY (`record_id`),
  KEY `CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_VALUE_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_VALUE_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_VALUE_VALUE_ID` (`value_id`),
  CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_VAL_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_VAL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_VAL_VAL_ID_CAT_PRD_ENTT_MDA_GLR_VAL_ID` FOREIGN KEY (`value_id`) REFERENCES `catalog_product_entity_media_gallery` (`value_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Media Gallery Attribute Value Table';

/*Data for the table `catalog_product_entity_media_gallery_value` */

/*Table structure for table `catalog_product_entity_media_gallery_value_to_entity` */

DROP TABLE IF EXISTS `catalog_product_entity_media_gallery_value_to_entity`;

CREATE TABLE `catalog_product_entity_media_gallery_value_to_entity` (
  `value_id` int(10) unsigned NOT NULL COMMENT 'Value media Entry ID',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Product entity ID',
  UNIQUE KEY `CAT_PRD_ENTT_MDA_GLR_VAL_TO_ENTT_VAL_ID_ENTT_ID` (`value_id`,`entity_id`),
  KEY `CAT_PRD_ENTT_MDA_GLR_VAL_TO_ENTT_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` (`entity_id`),
  CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_VAL_TO_ENTT_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_A6C6C8FAA386736921D3A7C4B50B1185` FOREIGN KEY (`value_id`) REFERENCES `catalog_product_entity_media_gallery` (`value_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Link Media value to Product entity table';

/*Data for the table `catalog_product_entity_media_gallery_value_to_entity` */

/*Table structure for table `catalog_product_entity_media_gallery_value_video` */

DROP TABLE IF EXISTS `catalog_product_entity_media_gallery_value_video`;

CREATE TABLE `catalog_product_entity_media_gallery_value_video` (
  `value_id` int(10) unsigned NOT NULL COMMENT 'Media Entity ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `provider` varchar(32) DEFAULT NULL COMMENT 'Video provider ID',
  `url` text COMMENT 'Video URL',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `description` text COMMENT 'Page Meta Description',
  `metadata` text COMMENT 'Video meta data',
  UNIQUE KEY `CAT_PRD_ENTT_MDA_GLR_VAL_VIDEO_VAL_ID_STORE_ID` (`value_id`,`store_id`),
  KEY `CAT_PRD_ENTT_MDA_GLR_VAL_VIDEO_STORE_ID_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_VAL_VIDEO_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_6FDF205946906B0E653E60AA769899F8` FOREIGN KEY (`value_id`) REFERENCES `catalog_product_entity_media_gallery` (`value_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Video Table';

/*Data for the table `catalog_product_entity_media_gallery_value_video` */

/*Table structure for table `catalog_product_entity_text` */

DROP TABLE IF EXISTS `catalog_product_entity_text`;

CREATE TABLE `catalog_product_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` text COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_ENTITY_TEXT_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_PRODUCT_ENTITY_TEXT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_TEXT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_TEXT_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Text Attribute Backend Table';

/*Data for the table `catalog_product_entity_text` */

insert  into `catalog_product_entity_text`(`value_id`,`attribute_id`,`store_id`,`entity_id`,`value`) values (1,75,0,1,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n	<ul>\r\n		<li>Any Product types that You want - Simple, Configurable, Bundled and Grouped Products</li>\r\n		<li>Downloadable/Digital Products, Virtual Products</li>\r\n		<li>Inventory Management with Backordered items</li>\r\n		<li>Customer Personalized Products - upload text for embroidery, monogramming, etc.</li>\r\n		<li>Create Store-specific attributes on the fly</li>\r\n		<li>Advanced Pricing Rules and support for Special Prices</li>\r\n		<li>Tax Rates per location, customer group and product type</li>\r\n                <li>Detailed Configuration Options in Theme Admin Penl</li>\r\n	</ul>'),(2,76,0,1,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'),(3,85,0,1,'Women Blouse'),(4,75,0,2,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n	<ul>\r\n		<li>Any Product types that You want - Simple, Configurable, Bundled and Grouped Products</li>\r\n		<li>Downloadable/Digital Products, Virtual Products</li>\r\n		<li>Inventory Management with Backordered items</li>\r\n		<li>Customer Personalized Products - upload text for embroidery, monogramming, etc.</li>\r\n		<li>Create Store-specific attributes on the fly</li>\r\n		<li>Advanced Pricing Rules and support for Special Prices</li>\r\n		<li>Tax Rates per location, customer group and product type</li>\r\n                <li>Detailed Configuration Options in Theme Admin Penl</li>\r\n	</ul>'),(5,76,0,2,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'),(6,85,0,2,'Women Blouse'),(8,75,0,3,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n	<ul>\r\n		<li>Any Product types that You want - Simple, Configurable, Bundled and Grouped Products</li>\r\n		<li>Downloadable/Digital Products, Virtual Products</li>\r\n		<li>Inventory Management with Backordered items</li>\r\n		<li>Customer Personalized Products - upload text for embroidery, monogramming, etc.</li>\r\n		<li>Create Store-specific attributes on the fly</li>\r\n		<li>Advanced Pricing Rules and support for Special Prices</li>\r\n		<li>Tax Rates per location, customer group and product type</li>\r\n                <li>Detailed Configuration Options in Theme Admin Penl</li>\r\n	</ul>'),(9,76,0,3,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'),(10,85,0,3,'Women Blouse'),(12,75,0,4,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n	<ul>\r\n		<li>Any Product types that You want - Simple, Configurable, Bundled and Grouped Products</li>\r\n		<li>Downloadable/Digital Products, Virtual Products</li>\r\n		<li>Inventory Management with Backordered items</li>\r\n		<li>Customer Personalized Products - upload text for embroidery, monogramming, etc.</li>\r\n		<li>Create Store-specific attributes on the fly</li>\r\n		<li>Advanced Pricing Rules and support for Special Prices</li>\r\n		<li>Tax Rates per location, customer group and product type</li>\r\n                <li>Detailed Configuration Options in Theme Admin Penl</li>\r\n	</ul>'),(13,76,0,4,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'),(14,85,0,4,'Women Blouse'),(16,75,0,5,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n	<ul>\r\n		<li>Any Product types that You want - Simple, Configurable, Bundled and Grouped Products</li>\r\n		<li>Downloadable/Digital Products, Virtual Products</li>\r\n		<li>Inventory Management with Backordered items</li>\r\n		<li>Customer Personalized Products - upload text for embroidery, monogramming, etc.</li>\r\n		<li>Create Store-specific attributes on the fly</li>\r\n		<li>Advanced Pricing Rules and support for Special Prices</li>\r\n		<li>Tax Rates per location, customer group and product type</li>\r\n                <li>Detailed Configuration Options in Theme Admin Penl</li>\r\n	</ul>'),(17,76,0,5,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'),(18,85,0,5,'Women Blouse'),(20,75,0,6,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n	<ul>\r\n		<li>Any Product types that You want - Simple, Configurable, Bundled and Grouped Products</li>\r\n		<li>Downloadable/Digital Products, Virtual Products</li>\r\n		<li>Inventory Management with Backordered items</li>\r\n		<li>Customer Personalized Products - upload text for embroidery, monogramming, etc.</li>\r\n		<li>Create Store-specific attributes on the fly</li>\r\n		<li>Advanced Pricing Rules and support for Special Prices</li>\r\n		<li>Tax Rates per location, customer group and product type</li>\r\n                <li>Detailed Configuration Options in Theme Admin Penl</li>\r\n	</ul>'),(21,76,0,6,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'),(22,85,0,6,'Women Blouse'),(24,75,0,7,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n	<ul>\r\n		<li>Any Product types that You want - Simple, Configurable, Bundled and Grouped Products</li>\r\n		<li>Downloadable/Digital Products, Virtual Products</li>\r\n		<li>Inventory Management with Backordered items</li>\r\n		<li>Customer Personalized Products - upload text for embroidery, monogramming, etc.</li>\r\n		<li>Create Store-specific attributes on the fly</li>\r\n		<li>Advanced Pricing Rules and support for Special Prices</li>\r\n		<li>Tax Rates per location, customer group and product type</li>\r\n                <li>Detailed Configuration Options in Theme Admin Penl</li>\r\n	</ul>'),(25,76,0,7,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'),(26,85,0,7,'Women Blouse'),(28,75,0,8,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n	<ul>\r\n		<li>Any Product types that You want - Simple, Configurable, Bundled and Grouped Products</li>\r\n		<li>Downloadable/Digital Products, Virtual Products</li>\r\n		<li>Inventory Management with Backordered items</li>\r\n		<li>Customer Personalized Products - upload text for embroidery, monogramming, etc.</li>\r\n		<li>Create Store-specific attributes on the fly</li>\r\n		<li>Advanced Pricing Rules and support for Special Prices</li>\r\n		<li>Tax Rates per location, customer group and product type</li>\r\n                <li>Detailed Configuration Options in Theme Admin Penl</li>\r\n	</ul>'),(29,76,0,8,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'),(30,85,0,8,'Women Blouse'),(32,75,0,9,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n	<ul>\r\n		<li>Any Product types that You want - Simple, Configurable, Bundled and Grouped Products</li>\r\n		<li>Downloadable/Digital Products, Virtual Products</li>\r\n		<li>Inventory Management with Backordered items</li>\r\n		<li>Customer Personalized Products - upload text for embroidery, monogramming, etc.</li>\r\n		<li>Create Store-specific attributes on the fly</li>\r\n		<li>Advanced Pricing Rules and support for Special Prices</li>\r\n		<li>Tax Rates per location, customer group and product type</li>\r\n                <li>Detailed Configuration Options in Theme Admin Penl</li>\r\n	</ul>'),(33,76,0,9,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'),(34,85,0,9,'Women Blouse'),(36,85,0,10,'Sample Configurable'),(37,85,0,11,'Sample Configurable'),(38,85,0,12,'Sample Configurable'),(39,85,0,13,'Sample Configurable'),(40,85,0,14,'Sample Configurable'),(41,85,0,15,'Sample Configurable');

/*Table structure for table `catalog_product_entity_tier_price` */

DROP TABLE IF EXISTS `catalog_product_entity_tier_price`;

CREATE TABLE `catalog_product_entity_tier_price` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `all_groups` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Applicable To All Customer Groups',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group ID',
  `qty` decimal(12,4) NOT NULL DEFAULT '1.0000' COMMENT 'QTY',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_E8AB433B9ACB00343ABB312AD2FAB087` (`entity_id`,`all_groups`,`customer_group_id`,`qty`,`website_id`),
  KEY `CATALOG_PRODUCT_ENTITY_TIER_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOG_PRODUCT_ENTITY_TIER_PRICE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `CAT_PRD_ENTT_TIER_PRICE_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_TIER_PRICE_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_TIER_PRICE_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Tier Price Attribute Backend Table';

/*Data for the table `catalog_product_entity_tier_price` */

/*Table structure for table `catalog_product_entity_varchar` */

DROP TABLE IF EXISTS `catalog_product_entity_varchar`;

CREATE TABLE `catalog_product_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_ENTITY_VARCHAR_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_PRODUCT_ENTITY_VARCHAR_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_VCHR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_VCHR_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Varchar Attribute Backend Table';

/*Data for the table `catalog_product_entity_varchar` */

insert  into `catalog_product_entity_varchar`(`value_id`,`attribute_id`,`store_id`,`entity_id`,`value`) values (1,73,0,1,'Women Blouse'),(2,84,0,1,'Women Blouse'),(3,86,0,1,'Women Blouse Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute '),(4,106,0,1,'container2'),(5,119,0,1,'women-blouse'),(6,134,0,1,'2'),(7,73,0,2,'Fashion Dress'),(8,84,0,2,'Women Blouse'),(9,86,0,2,'Women Blouse Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute '),(10,106,0,2,'container2'),(11,119,0,2,'women-blouse-1'),(12,134,0,2,'2'),(14,73,0,3,'Blue Denim Jeans'),(15,84,0,3,'Women Blouse'),(16,86,0,3,'Women Blouse Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute '),(17,106,0,3,'container2'),(18,119,0,3,'women-blouse-2'),(19,134,0,3,'2'),(21,73,0,4,'Sheri Collar Shirt'),(22,84,0,4,'Women Blouse'),(23,86,0,4,'Women Blouse Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute '),(24,106,0,4,'container2'),(25,119,0,4,'women-blouse-3'),(26,134,0,4,'2'),(28,73,0,5,'Fashion Jacket'),(29,84,0,5,'Women Blouse'),(30,86,0,5,'Women Blouse Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute '),(31,106,0,5,'container2'),(32,119,0,5,'women-blouse-4'),(33,134,0,5,'2'),(35,73,0,6,'Sample Product'),(36,84,0,6,'Women Blouse'),(37,86,0,6,'Women Blouse Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute '),(38,106,0,6,'container2'),(39,119,0,6,'women-blouse-5'),(40,134,0,6,'2'),(42,73,0,7,'Elizabeth Kint Top'),(43,84,0,7,'Women Blouse'),(44,86,0,7,'Women Blouse Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute '),(45,106,0,7,'container2'),(46,119,0,7,'women-blouse-6'),(47,134,0,7,'2'),(49,73,0,8,'Pink Shirt'),(50,84,0,8,'Women Blouse'),(51,86,0,8,'Women Blouse Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute '),(52,106,0,8,'container2'),(53,119,0,8,'women-blouse-7'),(54,134,0,8,'2'),(56,73,0,9,'Sample Fashion'),(57,84,0,9,'Women Blouse'),(58,86,0,9,'Women Blouse Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute '),(59,106,0,9,'container2'),(60,119,0,9,'women-blouse-8'),(61,134,0,9,'2'),(63,73,0,10,'Sample Configurable-White'),(64,84,0,10,'Sample Configurable'),(65,86,0,10,'Sample Configurable '),(66,106,0,10,'container2'),(67,119,0,10,'sample-configurable-white'),(68,134,0,10,'0'),(69,73,0,11,'Sample Configurable-Black'),(70,84,0,11,'Sample Configurable'),(71,86,0,11,'Sample Configurable '),(72,106,0,11,'container2'),(73,119,0,11,'sample-configurable-black'),(74,134,0,11,'0'),(75,73,0,12,'Sample Configurable-Red'),(76,84,0,12,'Sample Configurable'),(77,86,0,12,'Sample Configurable '),(78,106,0,12,'container2'),(79,119,0,12,'sample-configurable-red'),(80,134,0,12,'0'),(81,73,0,13,'Sample Configurable-Blue'),(82,84,0,13,'Sample Configurable'),(83,86,0,13,'Sample Configurable '),(84,106,0,13,'container2'),(85,119,0,13,'sample-configurable-blue'),(86,134,0,13,'0'),(87,73,0,14,'Sample Configurable-Green'),(88,84,0,14,'Sample Configurable'),(89,86,0,14,'Sample Configurable '),(90,106,0,14,'container2'),(91,119,0,14,'sample-configurable-green'),(92,134,0,14,'0'),(93,73,0,15,'Sample Configurable'),(94,84,0,15,'Sample Configurable'),(95,86,0,15,'Sample Configurable '),(96,106,0,15,'container2'),(97,119,0,15,'sample-configurable'),(98,134,0,15,'2');

/*Table structure for table `catalog_product_index_eav` */

DROP TABLE IF EXISTS `catalog_product_index_eav`;

CREATE TABLE `catalog_product_index_eav` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` int(10) unsigned NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_VALUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Index Table';

/*Data for the table `catalog_product_index_eav` */

insert  into `catalog_product_index_eav`(`entity_id`,`attribute_id`,`store_id`,`value`) values (15,93,1,4),(15,93,1,5),(15,93,1,6),(15,93,1,7),(15,93,1,8),(15,93,2,4),(15,93,2,5),(15,93,2,6),(15,93,2,7),(15,93,2,8);

/*Table structure for table `catalog_product_index_eav_decimal` */

DROP TABLE IF EXISTS `catalog_product_index_eav_decimal`;

CREATE TABLE `catalog_product_index_eav_decimal` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` decimal(12,4) NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_VALUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Decimal Index Table';

/*Data for the table `catalog_product_index_eav_decimal` */

/*Table structure for table `catalog_product_index_eav_decimal_idx` */

DROP TABLE IF EXISTS `catalog_product_index_eav_decimal_idx`;

CREATE TABLE `catalog_product_index_eav_decimal_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` decimal(12,4) NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_IDX_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_IDX_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_IDX_VALUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Decimal Indexer Index Table';

/*Data for the table `catalog_product_index_eav_decimal_idx` */

/*Table structure for table `catalog_product_index_eav_decimal_tmp` */

DROP TABLE IF EXISTS `catalog_product_index_eav_decimal_tmp`;

CREATE TABLE `catalog_product_index_eav_decimal_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` decimal(12,4) NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_TMP_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_TMP_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_TMP_VALUE` (`value`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Decimal Indexer Temp Table';

/*Data for the table `catalog_product_index_eav_decimal_tmp` */

/*Table structure for table `catalog_product_index_eav_idx` */

DROP TABLE IF EXISTS `catalog_product_index_eav_idx`;

CREATE TABLE `catalog_product_index_eav_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` int(10) unsigned NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_IDX_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_IDX_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_IDX_VALUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Indexer Index Table';

/*Data for the table `catalog_product_index_eav_idx` */

insert  into `catalog_product_index_eav_idx`(`entity_id`,`attribute_id`,`store_id`,`value`) values (15,93,1,4),(15,93,1,5),(15,93,1,6),(15,93,1,7),(15,93,1,8),(15,93,2,4),(15,93,2,5),(15,93,2,6),(15,93,2,7),(15,93,2,8);

/*Table structure for table `catalog_product_index_eav_tmp` */

DROP TABLE IF EXISTS `catalog_product_index_eav_tmp`;

CREATE TABLE `catalog_product_index_eav_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` int(10) unsigned NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_TMP_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_TMP_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_TMP_VALUE` (`value`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Indexer Temp Table';

/*Data for the table `catalog_product_index_eav_tmp` */

/*Table structure for table `catalog_product_index_price` */

DROP TABLE IF EXISTS `catalog_product_index_price`;

CREATE TABLE `catalog_product_index_price` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `final_price` decimal(12,4) DEFAULT NULL COMMENT 'Final Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_MIN_PRICE` (`min_price`),
  KEY `CAT_PRD_IDX_PRICE_WS_ID_CSTR_GROUP_ID_MIN_PRICE` (`website_id`,`customer_group_id`,`min_price`),
  CONSTRAINT `CATALOG_PRODUCT_INDEX_PRICE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_IDX_PRICE_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_IDX_PRICE_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Index Table';

/*Data for the table `catalog_product_index_price` */

insert  into `catalog_product_index_price`(`entity_id`,`customer_group_id`,`website_id`,`tax_class_id`,`price`,`final_price`,`min_price`,`max_price`,`tier_price`) values (1,0,1,2,'70.0000','70.0000','70.0000','70.0000',NULL),(1,1,1,2,'70.0000','70.0000','70.0000','70.0000',NULL),(1,2,1,2,'70.0000','70.0000','70.0000','70.0000',NULL),(1,3,1,2,'70.0000','70.0000','70.0000','70.0000',NULL),(2,0,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(2,1,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(2,2,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(2,3,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(3,0,1,2,'200.0000','200.0000','200.0000','200.0000',NULL),(3,1,1,2,'200.0000','200.0000','200.0000','200.0000',NULL),(3,2,1,2,'200.0000','200.0000','200.0000','200.0000',NULL),(3,3,1,2,'200.0000','200.0000','200.0000','200.0000',NULL),(4,0,1,2,'70.0000','70.0000','70.0000','70.0000',NULL),(4,1,1,2,'70.0000','70.0000','70.0000','70.0000',NULL),(4,2,1,2,'70.0000','70.0000','70.0000','70.0000',NULL),(4,3,1,2,'70.0000','70.0000','70.0000','70.0000',NULL),(5,0,1,2,'33.0000','33.0000','33.0000','33.0000',NULL),(5,1,1,2,'33.0000','33.0000','33.0000','33.0000',NULL),(5,2,1,2,'33.0000','33.0000','33.0000','33.0000',NULL),(5,3,1,2,'33.0000','33.0000','33.0000','33.0000',NULL),(6,0,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),(6,1,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),(6,2,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),(6,3,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),(7,0,1,2,'220.0000','220.0000','220.0000','220.0000',NULL),(7,1,1,2,'220.0000','220.0000','220.0000','220.0000',NULL),(7,2,1,2,'220.0000','220.0000','220.0000','220.0000',NULL),(7,3,1,2,'220.0000','220.0000','220.0000','220.0000',NULL),(8,0,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(8,1,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(8,2,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(8,3,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(9,0,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(9,1,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(9,2,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(9,3,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(10,0,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(10,1,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(10,2,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(10,3,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(11,0,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(11,1,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(11,2,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(11,3,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(12,0,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(12,1,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(12,2,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(12,3,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(13,0,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(13,1,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(13,2,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(13,3,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(14,0,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(14,1,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(14,2,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(14,3,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(15,0,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(15,1,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(15,2,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(15,3,1,2,'120.0000','120.0000','120.0000','120.0000',NULL);

/*Table structure for table `catalog_product_index_price_bundle_idx` */

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_idx`;

CREATE TABLE `catalog_product_index_price_bundle_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class Id',
  `price_type` smallint(5) unsigned NOT NULL COMMENT 'Price Type',
  `special_price` decimal(12,4) DEFAULT NULL COMMENT 'Special Price',
  `tier_percent` decimal(12,4) DEFAULT NULL COMMENT 'Tier Percent',
  `orig_price` decimal(12,4) DEFAULT NULL COMMENT 'Orig Price',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `base_tier` decimal(12,4) DEFAULT NULL COMMENT 'Base Tier',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Idx';

/*Data for the table `catalog_product_index_price_bundle_idx` */

/*Table structure for table `catalog_product_index_price_bundle_opt_idx` */

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_opt_idx`;

CREATE TABLE `catalog_product_index_price_bundle_opt_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `alt_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `alt_tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Opt Idx';

/*Data for the table `catalog_product_index_price_bundle_opt_idx` */

/*Table structure for table `catalog_product_index_price_bundle_opt_tmp` */

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_opt_tmp`;

CREATE TABLE `catalog_product_index_price_bundle_opt_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `alt_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `alt_tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Opt Tmp';

/*Data for the table `catalog_product_index_price_bundle_opt_tmp` */

/*Table structure for table `catalog_product_index_price_bundle_sel_idx` */

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_sel_idx`;

CREATE TABLE `catalog_product_index_price_bundle_sel_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `selection_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Selection Id',
  `group_type` smallint(5) unsigned DEFAULT '0' COMMENT 'Group Type',
  `is_required` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Required',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`,`selection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Sel Idx';

/*Data for the table `catalog_product_index_price_bundle_sel_idx` */

/*Table structure for table `catalog_product_index_price_bundle_sel_tmp` */

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_sel_tmp`;

CREATE TABLE `catalog_product_index_price_bundle_sel_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `selection_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Selection Id',
  `group_type` smallint(5) unsigned DEFAULT '0' COMMENT 'Group Type',
  `is_required` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Required',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`,`selection_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Sel Tmp';

/*Data for the table `catalog_product_index_price_bundle_sel_tmp` */

/*Table structure for table `catalog_product_index_price_bundle_tmp` */

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_tmp`;

CREATE TABLE `catalog_product_index_price_bundle_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class Id',
  `price_type` smallint(5) unsigned NOT NULL COMMENT 'Price Type',
  `special_price` decimal(12,4) DEFAULT NULL COMMENT 'Special Price',
  `tier_percent` decimal(12,4) DEFAULT NULL COMMENT 'Tier Percent',
  `orig_price` decimal(12,4) DEFAULT NULL COMMENT 'Orig Price',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `base_tier` decimal(12,4) DEFAULT NULL COMMENT 'Base Tier',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Tmp';

/*Data for the table `catalog_product_index_price_bundle_tmp` */

/*Table structure for table `catalog_product_index_price_cfg_opt_agr_idx` */

DROP TABLE IF EXISTS `catalog_product_index_price_cfg_opt_agr_idx`;

CREATE TABLE `catalog_product_index_price_cfg_opt_agr_idx` (
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent ID',
  `child_id` int(10) unsigned NOT NULL COMMENT 'Child ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`parent_id`,`child_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Config Option Aggregate Index Table';

/*Data for the table `catalog_product_index_price_cfg_opt_agr_idx` */

/*Table structure for table `catalog_product_index_price_cfg_opt_agr_tmp` */

DROP TABLE IF EXISTS `catalog_product_index_price_cfg_opt_agr_tmp`;

CREATE TABLE `catalog_product_index_price_cfg_opt_agr_tmp` (
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent ID',
  `child_id` int(10) unsigned NOT NULL COMMENT 'Child ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`parent_id`,`child_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Config Option Aggregate Temp Table';

/*Data for the table `catalog_product_index_price_cfg_opt_agr_tmp` */

/*Table structure for table `catalog_product_index_price_cfg_opt_idx` */

DROP TABLE IF EXISTS `catalog_product_index_price_cfg_opt_idx`;

CREATE TABLE `catalog_product_index_price_cfg_opt_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Config Option Index Table';

/*Data for the table `catalog_product_index_price_cfg_opt_idx` */

/*Table structure for table `catalog_product_index_price_cfg_opt_tmp` */

DROP TABLE IF EXISTS `catalog_product_index_price_cfg_opt_tmp`;

CREATE TABLE `catalog_product_index_price_cfg_opt_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Config Option Temp Table';

/*Data for the table `catalog_product_index_price_cfg_opt_tmp` */

/*Table structure for table `catalog_product_index_price_downlod_idx` */

DROP TABLE IF EXISTS `catalog_product_index_price_downlod_idx`;

CREATE TABLE `catalog_product_index_price_downlod_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Minimum price',
  `max_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Maximum price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Indexer Table for price of downloadable products';

/*Data for the table `catalog_product_index_price_downlod_idx` */

/*Table structure for table `catalog_product_index_price_downlod_tmp` */

DROP TABLE IF EXISTS `catalog_product_index_price_downlod_tmp`;

CREATE TABLE `catalog_product_index_price_downlod_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Minimum price',
  `max_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Maximum price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Temporary Indexer Table for price of downloadable products';

/*Data for the table `catalog_product_index_price_downlod_tmp` */

/*Table structure for table `catalog_product_index_price_final_idx` */

DROP TABLE IF EXISTS `catalog_product_index_price_final_idx`;

CREATE TABLE `catalog_product_index_price_final_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `orig_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Price',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `base_tier` decimal(12,4) DEFAULT NULL COMMENT 'Base Tier',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Final Index Table';

/*Data for the table `catalog_product_index_price_final_idx` */

/*Table structure for table `catalog_product_index_price_final_tmp` */

DROP TABLE IF EXISTS `catalog_product_index_price_final_tmp`;

CREATE TABLE `catalog_product_index_price_final_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `orig_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Price',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `base_tier` decimal(12,4) DEFAULT NULL COMMENT 'Base Tier',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Final Temp Table';

/*Data for the table `catalog_product_index_price_final_tmp` */

/*Table structure for table `catalog_product_index_price_idx` */

DROP TABLE IF EXISTS `catalog_product_index_price_idx`;

CREATE TABLE `catalog_product_index_price_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `final_price` decimal(12,4) DEFAULT NULL COMMENT 'Final Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_IDX_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_IDX_WEBSITE_ID` (`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_IDX_MIN_PRICE` (`min_price`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Index Table';

/*Data for the table `catalog_product_index_price_idx` */

insert  into `catalog_product_index_price_idx`(`entity_id`,`customer_group_id`,`website_id`,`tax_class_id`,`price`,`final_price`,`min_price`,`max_price`,`tier_price`) values (1,0,1,2,'70.0000','70.0000','70.0000','70.0000',NULL),(1,1,1,2,'70.0000','70.0000','70.0000','70.0000',NULL),(1,2,1,2,'70.0000','70.0000','70.0000','70.0000',NULL),(1,3,1,2,'70.0000','70.0000','70.0000','70.0000',NULL),(2,0,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(2,1,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(2,2,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(2,3,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(3,0,1,2,'200.0000','200.0000','200.0000','200.0000',NULL),(3,1,1,2,'200.0000','200.0000','200.0000','200.0000',NULL),(3,2,1,2,'200.0000','200.0000','200.0000','200.0000',NULL),(3,3,1,2,'200.0000','200.0000','200.0000','200.0000',NULL),(4,0,1,2,'70.0000','70.0000','70.0000','70.0000',NULL),(4,1,1,2,'70.0000','70.0000','70.0000','70.0000',NULL),(4,2,1,2,'70.0000','70.0000','70.0000','70.0000',NULL),(4,3,1,2,'70.0000','70.0000','70.0000','70.0000',NULL),(5,0,1,2,'33.0000','33.0000','33.0000','33.0000',NULL),(5,1,1,2,'33.0000','33.0000','33.0000','33.0000',NULL),(5,2,1,2,'33.0000','33.0000','33.0000','33.0000',NULL),(5,3,1,2,'33.0000','33.0000','33.0000','33.0000',NULL),(6,0,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),(6,1,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),(6,2,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),(6,3,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),(7,0,1,2,'220.0000','220.0000','220.0000','220.0000',NULL),(7,1,1,2,'220.0000','220.0000','220.0000','220.0000',NULL),(7,2,1,2,'220.0000','220.0000','220.0000','220.0000',NULL),(7,3,1,2,'220.0000','220.0000','220.0000','220.0000',NULL),(8,0,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(8,1,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(8,2,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(8,3,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(9,0,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(9,1,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(9,2,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(9,3,1,2,'90.0000','90.0000','90.0000','90.0000',NULL),(10,0,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(10,1,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(10,2,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(10,3,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(11,0,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(11,1,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(11,2,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(11,3,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(12,0,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(12,1,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(12,2,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(12,3,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(13,0,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(13,1,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(13,2,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(13,3,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(14,0,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(14,1,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(14,2,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(14,3,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(15,0,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(15,1,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(15,2,1,2,'120.0000','120.0000','120.0000','120.0000',NULL),(15,3,1,2,'120.0000','120.0000','120.0000','120.0000',NULL);

/*Table structure for table `catalog_product_index_price_opt_agr_idx` */

DROP TABLE IF EXISTS `catalog_product_index_price_opt_agr_idx`;

CREATE TABLE `catalog_product_index_price_opt_agr_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Option Aggregate Index Table';

/*Data for the table `catalog_product_index_price_opt_agr_idx` */

/*Table structure for table `catalog_product_index_price_opt_agr_tmp` */

DROP TABLE IF EXISTS `catalog_product_index_price_opt_agr_tmp`;

CREATE TABLE `catalog_product_index_price_opt_agr_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Option Aggregate Temp Table';

/*Data for the table `catalog_product_index_price_opt_agr_tmp` */

/*Table structure for table `catalog_product_index_price_opt_idx` */

DROP TABLE IF EXISTS `catalog_product_index_price_opt_idx`;

CREATE TABLE `catalog_product_index_price_opt_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Option Index Table';

/*Data for the table `catalog_product_index_price_opt_idx` */

/*Table structure for table `catalog_product_index_price_opt_tmp` */

DROP TABLE IF EXISTS `catalog_product_index_price_opt_tmp`;

CREATE TABLE `catalog_product_index_price_opt_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Option Temp Table';

/*Data for the table `catalog_product_index_price_opt_tmp` */

/*Table structure for table `catalog_product_index_price_tmp` */

DROP TABLE IF EXISTS `catalog_product_index_price_tmp`;

CREATE TABLE `catalog_product_index_price_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `final_price` decimal(12,4) DEFAULT NULL COMMENT 'Final Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_TMP_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_TMP_WEBSITE_ID` (`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_TMP_MIN_PRICE` (`min_price`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Temp Table';

/*Data for the table `catalog_product_index_price_tmp` */

/*Table structure for table `catalog_product_index_tier_price` */

DROP TABLE IF EXISTS `catalog_product_index_tier_price`;

CREATE TABLE `catalog_product_index_tier_price` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_TIER_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOG_PRODUCT_INDEX_TIER_PRICE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `CAT_PRD_IDX_TIER_PRICE_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_IDX_TIER_PRICE_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_IDX_TIER_PRICE_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Tier Price Index Table';

/*Data for the table `catalog_product_index_tier_price` */

/*Table structure for table `catalog_product_index_website` */

DROP TABLE IF EXISTS `catalog_product_index_website`;

CREATE TABLE `catalog_product_index_website` (
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `website_date` date DEFAULT NULL COMMENT 'Website Date',
  `rate` float DEFAULT '1' COMMENT 'Rate',
  PRIMARY KEY (`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_WEBSITE_WEBSITE_DATE` (`website_date`),
  CONSTRAINT `CAT_PRD_IDX_WS_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Website Index Table';

/*Data for the table `catalog_product_index_website` */

insert  into `catalog_product_index_website`(`website_id`,`website_date`,`rate`) values (1,'2016-09-05',1);

/*Table structure for table `catalog_product_link` */

DROP TABLE IF EXISTS `catalog_product_link`;

CREATE TABLE `catalog_product_link` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `linked_product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Linked Product ID',
  `link_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Link Type ID',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `CATALOG_PRODUCT_LINK_LINK_TYPE_ID_PRODUCT_ID_LINKED_PRODUCT_ID` (`link_type_id`,`product_id`,`linked_product_id`),
  KEY `CATALOG_PRODUCT_LINK_PRODUCT_ID` (`product_id`),
  KEY `CATALOG_PRODUCT_LINK_LINKED_PRODUCT_ID` (`linked_product_id`),
  CONSTRAINT `CATALOG_PRODUCT_LINK_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_LNK_LNKED_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`linked_product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_LNK_LNK_TYPE_ID_CAT_PRD_LNK_TYPE_LNK_TYPE_ID` FOREIGN KEY (`link_type_id`) REFERENCES `catalog_product_link_type` (`link_type_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product To Product Linkage Table';

/*Data for the table `catalog_product_link` */

/*Table structure for table `catalog_product_link_attribute` */

DROP TABLE IF EXISTS `catalog_product_link_attribute`;

CREATE TABLE `catalog_product_link_attribute` (
  `product_link_attribute_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product Link Attribute ID',
  `link_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Link Type ID',
  `product_link_attribute_code` varchar(32) DEFAULT NULL COMMENT 'Product Link Attribute Code',
  `data_type` varchar(32) DEFAULT NULL COMMENT 'Data Type',
  PRIMARY KEY (`product_link_attribute_id`),
  KEY `CATALOG_PRODUCT_LINK_ATTRIBUTE_LINK_TYPE_ID` (`link_type_id`),
  CONSTRAINT `CAT_PRD_LNK_ATTR_LNK_TYPE_ID_CAT_PRD_LNK_TYPE_LNK_TYPE_ID` FOREIGN KEY (`link_type_id`) REFERENCES `catalog_product_link_type` (`link_type_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Attribute Table';

/*Data for the table `catalog_product_link_attribute` */

insert  into `catalog_product_link_attribute`(`product_link_attribute_id`,`link_type_id`,`product_link_attribute_code`,`data_type`) values (1,1,'position','int'),(2,4,'position','int'),(3,5,'position','int'),(4,3,'position','int'),(5,3,'qty','decimal');

/*Table structure for table `catalog_product_link_attribute_decimal` */

DROP TABLE IF EXISTS `catalog_product_link_attribute_decimal`;

CREATE TABLE `catalog_product_link_attribute_decimal` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_link_attribute_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Product Link Attribute ID',
  `link_id` int(10) unsigned NOT NULL COMMENT 'Link ID',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CAT_PRD_LNK_ATTR_DEC_PRD_LNK_ATTR_ID_LNK_ID` (`product_link_attribute_id`,`link_id`),
  KEY `CATALOG_PRODUCT_LINK_ATTRIBUTE_DECIMAL_LINK_ID` (`link_id`),
  CONSTRAINT `CAT_PRD_LNK_ATTR_DEC_LNK_ID_CAT_PRD_LNK_LNK_ID` FOREIGN KEY (`link_id`) REFERENCES `catalog_product_link` (`link_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_AB2EFA9A14F7BCF1D5400056203D14B6` FOREIGN KEY (`product_link_attribute_id`) REFERENCES `catalog_product_link_attribute` (`product_link_attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Decimal Attribute Table';

/*Data for the table `catalog_product_link_attribute_decimal` */

/*Table structure for table `catalog_product_link_attribute_int` */

DROP TABLE IF EXISTS `catalog_product_link_attribute_int`;

CREATE TABLE `catalog_product_link_attribute_int` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_link_attribute_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Product Link Attribute ID',
  `link_id` int(10) unsigned NOT NULL COMMENT 'Link ID',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CAT_PRD_LNK_ATTR_INT_PRD_LNK_ATTR_ID_LNK_ID` (`product_link_attribute_id`,`link_id`),
  KEY `CATALOG_PRODUCT_LINK_ATTRIBUTE_INT_LINK_ID` (`link_id`),
  CONSTRAINT `CAT_PRD_LNK_ATTR_INT_LNK_ID_CAT_PRD_LNK_LNK_ID` FOREIGN KEY (`link_id`) REFERENCES `catalog_product_link` (`link_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_D6D878F8BA2A4282F8DDED7E6E3DE35C` FOREIGN KEY (`product_link_attribute_id`) REFERENCES `catalog_product_link_attribute` (`product_link_attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Integer Attribute Table';

/*Data for the table `catalog_product_link_attribute_int` */

/*Table structure for table `catalog_product_link_attribute_varchar` */

DROP TABLE IF EXISTS `catalog_product_link_attribute_varchar`;

CREATE TABLE `catalog_product_link_attribute_varchar` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_link_attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Link Attribute ID',
  `link_id` int(10) unsigned NOT NULL COMMENT 'Link ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CAT_PRD_LNK_ATTR_VCHR_PRD_LNK_ATTR_ID_LNK_ID` (`product_link_attribute_id`,`link_id`),
  KEY `CATALOG_PRODUCT_LINK_ATTRIBUTE_VARCHAR_LINK_ID` (`link_id`),
  CONSTRAINT `CAT_PRD_LNK_ATTR_VCHR_LNK_ID_CAT_PRD_LNK_LNK_ID` FOREIGN KEY (`link_id`) REFERENCES `catalog_product_link` (`link_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_DEE9C4DA61CFCC01DFCF50F0D79CEA51` FOREIGN KEY (`product_link_attribute_id`) REFERENCES `catalog_product_link_attribute` (`product_link_attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Varchar Attribute Table';

/*Data for the table `catalog_product_link_attribute_varchar` */

/*Table structure for table `catalog_product_link_type` */

DROP TABLE IF EXISTS `catalog_product_link_type`;

CREATE TABLE `catalog_product_link_type` (
  `link_type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link Type ID',
  `code` varchar(32) DEFAULT NULL COMMENT 'Code',
  PRIMARY KEY (`link_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Type Table';

/*Data for the table `catalog_product_link_type` */

insert  into `catalog_product_link_type`(`link_type_id`,`code`) values (1,'relation'),(3,'super'),(4,'up_sell'),(5,'cross_sell');

/*Table structure for table `catalog_product_option` */

DROP TABLE IF EXISTS `catalog_product_option`;

CREATE TABLE `catalog_product_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `type` varchar(50) DEFAULT NULL COMMENT 'Type',
  `is_require` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Required',
  `sku` varchar(64) DEFAULT NULL COMMENT 'SKU',
  `max_characters` int(10) unsigned DEFAULT NULL COMMENT 'Max Characters',
  `file_extension` varchar(50) DEFAULT NULL COMMENT 'File Extension',
  `image_size_x` smallint(5) unsigned DEFAULT NULL COMMENT 'Image Size X',
  `image_size_y` smallint(5) unsigned DEFAULT NULL COMMENT 'Image Size Y',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`option_id`),
  KEY `CATALOG_PRODUCT_OPTION_PRODUCT_ID` (`product_id`),
  CONSTRAINT `CAT_PRD_OPT_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Table';

/*Data for the table `catalog_product_option` */

/*Table structure for table `catalog_product_option_price` */

DROP TABLE IF EXISTS `catalog_product_option_price`;

CREATE TABLE `catalog_product_option_price` (
  `option_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Price ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `price_type` varchar(7) NOT NULL DEFAULT 'fixed' COMMENT 'Price Type',
  PRIMARY KEY (`option_price_id`),
  UNIQUE KEY `CATALOG_PRODUCT_OPTION_PRICE_OPTION_ID_STORE_ID` (`option_id`,`store_id`),
  KEY `CATALOG_PRODUCT_OPTION_PRICE_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_PRODUCT_OPTION_PRICE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_OPT_PRICE_OPT_ID_CAT_PRD_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_option` (`option_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Price Table';

/*Data for the table `catalog_product_option_price` */

/*Table structure for table `catalog_product_option_title` */

DROP TABLE IF EXISTS `catalog_product_option_title`;

CREATE TABLE `catalog_product_option_title` (
  `option_title_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Title ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`option_title_id`),
  UNIQUE KEY `CATALOG_PRODUCT_OPTION_TITLE_OPTION_ID_STORE_ID` (`option_id`,`store_id`),
  KEY `CATALOG_PRODUCT_OPTION_TITLE_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_PRODUCT_OPTION_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_OPT_TTL_OPT_ID_CAT_PRD_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_option` (`option_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Title Table';

/*Data for the table `catalog_product_option_title` */

/*Table structure for table `catalog_product_option_type_price` */

DROP TABLE IF EXISTS `catalog_product_option_type_price`;

CREATE TABLE `catalog_product_option_type_price` (
  `option_type_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Type Price ID',
  `option_type_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Type ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `price_type` varchar(7) NOT NULL DEFAULT 'fixed' COMMENT 'Price Type',
  PRIMARY KEY (`option_type_price_id`),
  UNIQUE KEY `CATALOG_PRODUCT_OPTION_TYPE_PRICE_OPTION_TYPE_ID_STORE_ID` (`option_type_id`,`store_id`),
  KEY `CATALOG_PRODUCT_OPTION_TYPE_PRICE_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_PRODUCT_OPTION_TYPE_PRICE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_B523E3378E8602F376CC415825576B7F` FOREIGN KEY (`option_type_id`) REFERENCES `catalog_product_option_type_value` (`option_type_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Type Price Table';

/*Data for the table `catalog_product_option_type_price` */

/*Table structure for table `catalog_product_option_type_title` */

DROP TABLE IF EXISTS `catalog_product_option_type_title`;

CREATE TABLE `catalog_product_option_type_title` (
  `option_type_title_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Type Title ID',
  `option_type_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Type ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`option_type_title_id`),
  UNIQUE KEY `CATALOG_PRODUCT_OPTION_TYPE_TITLE_OPTION_TYPE_ID_STORE_ID` (`option_type_id`,`store_id`),
  KEY `CATALOG_PRODUCT_OPTION_TYPE_TITLE_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_PRODUCT_OPTION_TYPE_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_C085B9CF2C2A302E8043FDEA1937D6A2` FOREIGN KEY (`option_type_id`) REFERENCES `catalog_product_option_type_value` (`option_type_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Type Title Table';

/*Data for the table `catalog_product_option_type_title` */

/*Table structure for table `catalog_product_option_type_value` */

DROP TABLE IF EXISTS `catalog_product_option_type_value`;

CREATE TABLE `catalog_product_option_type_value` (
  `option_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Type ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `sku` varchar(64) DEFAULT NULL COMMENT 'SKU',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`option_type_id`),
  KEY `CATALOG_PRODUCT_OPTION_TYPE_VALUE_OPTION_ID` (`option_id`),
  CONSTRAINT `CAT_PRD_OPT_TYPE_VAL_OPT_ID_CAT_PRD_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_option` (`option_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Type Value Table';

/*Data for the table `catalog_product_option_type_value` */

/*Table structure for table `catalog_product_relation` */

DROP TABLE IF EXISTS `catalog_product_relation`;

CREATE TABLE `catalog_product_relation` (
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent ID',
  `child_id` int(10) unsigned NOT NULL COMMENT 'Child ID',
  PRIMARY KEY (`parent_id`,`child_id`),
  KEY `CATALOG_PRODUCT_RELATION_CHILD_ID` (`child_id`),
  CONSTRAINT `CAT_PRD_RELATION_CHILD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`child_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_RELATION_PARENT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`parent_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Relation Table';

/*Data for the table `catalog_product_relation` */

insert  into `catalog_product_relation`(`parent_id`,`child_id`) values (15,10),(15,11),(15,12),(15,13),(15,14);

/*Table structure for table `catalog_product_super_attribute` */

DROP TABLE IF EXISTS `catalog_product_super_attribute`;

CREATE TABLE `catalog_product_super_attribute` (
  `product_super_attribute_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product Super Attribute ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Position',
  PRIMARY KEY (`product_super_attribute_id`),
  UNIQUE KEY `CATALOG_PRODUCT_SUPER_ATTRIBUTE_PRODUCT_ID_ATTRIBUTE_ID` (`product_id`,`attribute_id`),
  CONSTRAINT `CAT_PRD_SPR_ATTR_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Super Attribute Table';

/*Data for the table `catalog_product_super_attribute` */

insert  into `catalog_product_super_attribute`(`product_super_attribute_id`,`product_id`,`attribute_id`,`position`) values (1,15,93,0);

/*Table structure for table `catalog_product_super_attribute_label` */

DROP TABLE IF EXISTS `catalog_product_super_attribute_label`;

CREATE TABLE `catalog_product_super_attribute_label` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_super_attribute_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Super Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `use_default` smallint(5) unsigned DEFAULT '0' COMMENT 'Use Default Value',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CAT_PRD_SPR_ATTR_LBL_PRD_SPR_ATTR_ID_STORE_ID` (`product_super_attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_SUPER_ATTRIBUTE_LABEL_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_PRODUCT_SUPER_ATTRIBUTE_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_309442281DF7784210ED82B2CC51E5D5` FOREIGN KEY (`product_super_attribute_id`) REFERENCES `catalog_product_super_attribute` (`product_super_attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Super Attribute Label Table';

/*Data for the table `catalog_product_super_attribute_label` */

insert  into `catalog_product_super_attribute_label`(`value_id`,`product_super_attribute_id`,`store_id`,`use_default`,`value`) values (1,1,0,0,'Color');

/*Table structure for table `catalog_product_super_link` */

DROP TABLE IF EXISTS `catalog_product_super_link`;

CREATE TABLE `catalog_product_super_link` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent ID',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `CATALOG_PRODUCT_SUPER_LINK_PRODUCT_ID_PARENT_ID` (`product_id`,`parent_id`),
  KEY `CATALOG_PRODUCT_SUPER_LINK_PARENT_ID` (`parent_id`),
  CONSTRAINT `CAT_PRD_SPR_LNK_PARENT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`parent_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_SPR_LNK_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Super Link Table';

/*Data for the table `catalog_product_super_link` */

insert  into `catalog_product_super_link`(`link_id`,`product_id`,`parent_id`) values (1,10,15),(2,11,15),(3,12,15),(4,13,15),(5,14,15);

/*Table structure for table `catalog_product_website` */

DROP TABLE IF EXISTS `catalog_product_website`;

CREATE TABLE `catalog_product_website` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  PRIMARY KEY (`product_id`,`website_id`),
  KEY `CATALOG_PRODUCT_WEBSITE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `CATALOG_PRODUCT_WEBSITE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_WS_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product To Website Linkage Table';

/*Data for the table `catalog_product_website` */

insert  into `catalog_product_website`(`product_id`,`website_id`) values (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1);

/*Table structure for table `catalog_url_rewrite_product_category` */

DROP TABLE IF EXISTS `catalog_url_rewrite_product_category`;

CREATE TABLE `catalog_url_rewrite_product_category` (
  `url_rewrite_id` int(10) unsigned NOT NULL COMMENT 'url_rewrite_id',
  `category_id` int(10) unsigned NOT NULL COMMENT 'category_id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'product_id',
  KEY `CATALOG_URL_REWRITE_PRODUCT_CATEGORY_CATEGORY_ID_PRODUCT_ID` (`category_id`,`product_id`),
  KEY `CAT_URL_REWRITE_PRD_CTGR_PRD_ID_CAT_PRD_ENTT_ENTT_ID` (`product_id`),
  KEY `FK_BB79E64705D7F17FE181F23144528FC8` (`url_rewrite_id`),
  CONSTRAINT `CAT_URL_REWRITE_PRD_CTGR_CTGR_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`category_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_URL_REWRITE_PRD_CTGR_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_BB79E64705D7F17FE181F23144528FC8` FOREIGN KEY (`url_rewrite_id`) REFERENCES `url_rewrite` (`url_rewrite_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='url_rewrite_relation';

/*Data for the table `catalog_url_rewrite_product_category` */

insert  into `catalog_url_rewrite_product_category`(`url_rewrite_id`,`category_id`,`product_id`) values (84,3,1),(86,10,1),(85,6,1),(88,3,2),(90,10,2),(89,6,2),(92,3,3),(94,10,3),(93,6,3),(96,3,4),(98,10,4),(97,6,4),(100,3,5),(102,10,5),(101,6,5),(104,3,6),(106,10,6),(105,6,6),(108,3,7),(110,10,7),(109,6,7),(112,3,8),(114,10,8),(113,6,8),(116,3,9),(118,10,9),(117,6,9),(120,3,15),(121,6,15),(170,3,1),(171,6,1),(172,10,1),(174,3,2),(175,6,2),(176,10,2),(178,3,3),(179,6,3),(180,10,3),(182,3,4),(183,6,4),(184,10,4),(186,3,5),(187,6,5),(188,10,5),(190,3,6),(191,6,6),(192,10,6),(194,3,7),(195,6,7),(196,10,7),(198,3,8),(199,6,8),(200,10,8),(202,3,9),(203,6,9),(204,10,9),(206,3,15),(207,6,15);

/*Table structure for table `cataloginventory_stock` */

DROP TABLE IF EXISTS `cataloginventory_stock`;

CREATE TABLE `cataloginventory_stock` (
  `stock_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Stock Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_name` varchar(255) DEFAULT NULL COMMENT 'Stock Name',
  PRIMARY KEY (`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock';

/*Data for the table `cataloginventory_stock` */

insert  into `cataloginventory_stock`(`stock_id`,`website_id`,`stock_name`) values (1,0,'Default');

/*Table structure for table `cataloginventory_stock_item` */

DROP TABLE IF EXISTS `cataloginventory_stock_item`;

CREATE TABLE `cataloginventory_stock_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item Id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Id',
  `stock_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Stock Id',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `min_qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Min Qty',
  `use_config_min_qty` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Min Qty',
  `is_qty_decimal` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Qty Decimal',
  `backorders` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Backorders',
  `use_config_backorders` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Backorders',
  `min_sale_qty` decimal(12,4) NOT NULL DEFAULT '1.0000' COMMENT 'Min Sale Qty',
  `use_config_min_sale_qty` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Min Sale Qty',
  `max_sale_qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Max Sale Qty',
  `use_config_max_sale_qty` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Max Sale Qty',
  `is_in_stock` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is In Stock',
  `low_stock_date` timestamp NULL DEFAULT NULL COMMENT 'Low Stock Date',
  `notify_stock_qty` decimal(12,4) DEFAULT NULL COMMENT 'Notify Stock Qty',
  `use_config_notify_stock_qty` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Notify Stock Qty',
  `manage_stock` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Manage Stock',
  `use_config_manage_stock` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Manage Stock',
  `stock_status_changed_auto` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Stock Status Changed Automatically',
  `use_config_qty_increments` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Qty Increments',
  `qty_increments` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Increments',
  `use_config_enable_qty_inc` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Enable Qty Increments',
  `enable_qty_increments` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Enable Qty Increments',
  `is_decimal_divided` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Divided into Multiple Boxes for Shipping',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Divided into Multiple Boxes for Shipping',
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `CATALOGINVENTORY_STOCK_ITEM_PRODUCT_ID_WEBSITE_ID` (`product_id`,`website_id`),
  KEY `CATALOGINVENTORY_STOCK_ITEM_WEBSITE_ID` (`website_id`),
  KEY `CATALOGINVENTORY_STOCK_ITEM_STOCK_ID` (`stock_id`),
  CONSTRAINT `CATINV_STOCK_ITEM_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CATINV_STOCK_ITEM_STOCK_ID_CATINV_STOCK_STOCK_ID` FOREIGN KEY (`stock_id`) REFERENCES `cataloginventory_stock` (`stock_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock Item';

/*Data for the table `cataloginventory_stock_item` */

insert  into `cataloginventory_stock_item`(`item_id`,`product_id`,`stock_id`,`qty`,`min_qty`,`use_config_min_qty`,`is_qty_decimal`,`backorders`,`use_config_backorders`,`min_sale_qty`,`use_config_min_sale_qty`,`max_sale_qty`,`use_config_max_sale_qty`,`is_in_stock`,`low_stock_date`,`notify_stock_qty`,`use_config_notify_stock_qty`,`manage_stock`,`use_config_manage_stock`,`stock_status_changed_auto`,`use_config_qty_increments`,`qty_increments`,`use_config_enable_qty_inc`,`enable_qty_increments`,`is_decimal_divided`,`website_id`) values (1,1,1,'999.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),(2,2,1,'999.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),(3,3,1,'999.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),(4,4,1,'999.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),(5,5,1,'999.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),(6,6,1,'999.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),(7,7,1,'999.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),(8,8,1,'999.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),(9,9,1,'999.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),(10,10,1,'999.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),(11,11,1,'999.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),(12,12,1,'999.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),(13,13,1,'999.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),(14,14,1,'999.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),(15,15,1,'0.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0);

/*Table structure for table `cataloginventory_stock_status` */

DROP TABLE IF EXISTS `cataloginventory_stock_status`;

CREATE TABLE `cataloginventory_stock_status` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_id` smallint(5) unsigned NOT NULL COMMENT 'Stock Id',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `stock_status` smallint(5) unsigned NOT NULL COMMENT 'Stock Status',
  PRIMARY KEY (`product_id`,`website_id`,`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_STOCK_ID` (`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock Status';

/*Data for the table `cataloginventory_stock_status` */

insert  into `cataloginventory_stock_status`(`product_id`,`website_id`,`stock_id`,`qty`,`stock_status`) values (1,0,1,'999.0000',1),(2,0,1,'999.0000',1),(3,0,1,'999.0000',1),(4,0,1,'999.0000',1),(5,0,1,'999.0000',1),(6,0,1,'999.0000',1),(7,0,1,'999.0000',1),(8,0,1,'999.0000',1),(9,0,1,'999.0000',1),(10,0,1,'999.0000',1),(11,0,1,'999.0000',1),(12,0,1,'999.0000',1),(13,0,1,'999.0000',1),(14,0,1,'999.0000',1),(15,0,1,'0.0000',1);

/*Table structure for table `cataloginventory_stock_status_idx` */

DROP TABLE IF EXISTS `cataloginventory_stock_status_idx`;

CREATE TABLE `cataloginventory_stock_status_idx` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_id` smallint(5) unsigned NOT NULL COMMENT 'Stock Id',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `stock_status` smallint(5) unsigned NOT NULL COMMENT 'Stock Status',
  PRIMARY KEY (`product_id`,`website_id`,`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_IDX_STOCK_ID` (`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_IDX_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock Status Indexer Idx';

/*Data for the table `cataloginventory_stock_status_idx` */

insert  into `cataloginventory_stock_status_idx`(`product_id`,`website_id`,`stock_id`,`qty`,`stock_status`) values (1,0,1,'999.0000',1),(2,0,1,'999.0000',1),(3,0,1,'999.0000',1),(4,0,1,'999.0000',1),(5,0,1,'999.0000',1),(6,0,1,'999.0000',1),(7,0,1,'999.0000',1),(8,0,1,'999.0000',1),(9,0,1,'999.0000',1),(10,0,1,'999.0000',1),(11,0,1,'999.0000',1),(12,0,1,'999.0000',1),(13,0,1,'999.0000',1),(14,0,1,'999.0000',1),(15,0,1,'0.0000',1);

/*Table structure for table `cataloginventory_stock_status_tmp` */

DROP TABLE IF EXISTS `cataloginventory_stock_status_tmp`;

CREATE TABLE `cataloginventory_stock_status_tmp` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_id` smallint(5) unsigned NOT NULL COMMENT 'Stock Id',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `stock_status` smallint(5) unsigned NOT NULL COMMENT 'Stock Status',
  PRIMARY KEY (`product_id`,`website_id`,`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_TMP_STOCK_ID` (`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_TMP_WEBSITE_ID` (`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock Status Indexer Tmp';

/*Data for the table `cataloginventory_stock_status_tmp` */

/*Table structure for table `catalogrule` */

DROP TABLE IF EXISTS `catalogrule`;

CREATE TABLE `catalogrule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `from_date` date DEFAULT NULL COMMENT 'From',
  `to_date` date DEFAULT NULL COMMENT 'To',
  `is_active` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Active',
  `conditions_serialized` mediumtext COMMENT 'Conditions Serialized',
  `actions_serialized` mediumtext COMMENT 'Actions Serialized',
  `stop_rules_processing` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Stop Rules Processing',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `simple_action` varchar(32) DEFAULT NULL COMMENT 'Simple Action',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  PRIMARY KEY (`rule_id`),
  KEY `CATALOGRULE_IS_ACTIVE_SORT_ORDER_TO_DATE_FROM_DATE` (`is_active`,`sort_order`,`to_date`,`from_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule';

/*Data for the table `catalogrule` */

/*Table structure for table `catalogrule_customer_group` */

DROP TABLE IF EXISTS `catalogrule_customer_group`;

CREATE TABLE `catalogrule_customer_group` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  PRIMARY KEY (`rule_id`,`customer_group_id`),
  KEY `CATALOGRULE_CUSTOMER_GROUP_CUSTOMER_GROUP_ID` (`customer_group_id`),
  CONSTRAINT `CATALOGRULE_CUSTOMER_GROUP_RULE_ID_CATALOGRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `catalogrule` (`rule_id`) ON DELETE CASCADE,
  CONSTRAINT `CATRULE_CSTR_GROUP_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Rules To Customer Groups Relations';

/*Data for the table `catalogrule_customer_group` */

/*Table structure for table `catalogrule_group_website` */

DROP TABLE IF EXISTS `catalogrule_group_website`;

CREATE TABLE `catalogrule_group_website` (
  `rule_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Rule Id',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  PRIMARY KEY (`rule_id`,`customer_group_id`,`website_id`),
  KEY `CATALOGRULE_GROUP_WEBSITE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOGRULE_GROUP_WEBSITE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `CATALOGRULE_GROUP_WEBSITE_RULE_ID_CATALOGRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `catalogrule` (`rule_id`) ON DELETE CASCADE,
  CONSTRAINT `CATALOGRULE_GROUP_WEBSITE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE,
  CONSTRAINT `CATRULE_GROUP_WS_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule Group Website';

/*Data for the table `catalogrule_group_website` */

/*Table structure for table `catalogrule_product` */

DROP TABLE IF EXISTS `catalogrule_product`;

CREATE TABLE `catalogrule_product` (
  `rule_product_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Product Id',
  `rule_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Rule Id',
  `from_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'From Time',
  `to_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'To time',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group Id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Id',
  `action_operator` varchar(10) DEFAULT 'to_fixed' COMMENT 'Action Operator',
  `action_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Action Amount',
  `action_stop` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Action Stop',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  PRIMARY KEY (`rule_product_id`),
  UNIQUE KEY `IDX_EAA51B56FF092A0DCB795D1CEF812B7B` (`rule_id`,`from_time`,`to_time`,`website_id`,`customer_group_id`,`product_id`,`sort_order`),
  KEY `CATALOGRULE_PRODUCT_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOGRULE_PRODUCT_WEBSITE_ID` (`website_id`),
  KEY `CATALOGRULE_PRODUCT_FROM_TIME` (`from_time`),
  KEY `CATALOGRULE_PRODUCT_TO_TIME` (`to_time`),
  KEY `CATALOGRULE_PRODUCT_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule Product';

/*Data for the table `catalogrule_product` */

/*Table structure for table `catalogrule_product_price` */

DROP TABLE IF EXISTS `catalogrule_product_price`;

CREATE TABLE `catalogrule_product_price` (
  `rule_product_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Product PriceId',
  `rule_date` date NOT NULL COMMENT 'Rule Date',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group Id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Id',
  `rule_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Rule Price',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `latest_start_date` date DEFAULT NULL COMMENT 'Latest StartDate',
  `earliest_end_date` date DEFAULT NULL COMMENT 'Earliest EndDate',
  PRIMARY KEY (`rule_product_price_id`),
  UNIQUE KEY `CATRULE_PRD_PRICE_RULE_DATE_WS_ID_CSTR_GROUP_ID_PRD_ID` (`rule_date`,`website_id`,`customer_group_id`,`product_id`),
  KEY `CATALOGRULE_PRODUCT_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOGRULE_PRODUCT_PRICE_WEBSITE_ID` (`website_id`),
  KEY `CATALOGRULE_PRODUCT_PRICE_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule Product Price';

/*Data for the table `catalogrule_product_price` */

/*Table structure for table `catalogrule_website` */

DROP TABLE IF EXISTS `catalogrule_website`;

CREATE TABLE `catalogrule_website` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  PRIMARY KEY (`rule_id`,`website_id`),
  KEY `CATALOGRULE_WEBSITE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `CATALOGRULE_WEBSITE_RULE_ID_CATALOGRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `catalogrule` (`rule_id`) ON DELETE CASCADE,
  CONSTRAINT `CATALOGRULE_WEBSITE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Rules To Websites Relations';

/*Data for the table `catalogrule_website` */

/*Table structure for table `catalogsearch_fulltext_scope1` */

DROP TABLE IF EXISTS `catalogsearch_fulltext_scope1`;

CREATE TABLE `catalogsearch_fulltext_scope1` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` int(10) unsigned NOT NULL COMMENT 'Attribute_id',
  `data_index` longtext COMMENT 'Data index',
  PRIMARY KEY (`entity_id`,`attribute_id`),
  FULLTEXT KEY `FTI_FULLTEXT_DATA_INDEX` (`data_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='catalogsearch_fulltext_scope1';

/*Data for the table `catalogsearch_fulltext_scope1` */

insert  into `catalogsearch_fulltext_scope1`(`entity_id`,`attribute_id`,`data_index`) values (1,73,'Women Blouse'),(1,74,'Women Blouse'),(1,75,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Any Product types that You want - Simple, Configurable, Bundled and Grouped Products Downloadable/Digital Products, Virtual Products Inventory Management with Backordered items Customer Personalized Products - upload text for embroidery, monogramming, etc. Create Store-specific attributes on the fly Advanced Pricing Rules and support for Special Prices Tax Rates per location, customer group and product type Detailed Configuration Options in Theme Admin Penl'),(1,76,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(2,73,'Fashion Dress'),(2,74,'Fashion Dress'),(2,75,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Any Product types that You want - Simple, Configurable, Bundled and Grouped Products Downloadable/Digital Products, Virtual Products Inventory Management with Backordered items Customer Personalized Products - upload text for embroidery, monogramming, etc. Create Store-specific attributes on the fly Advanced Pricing Rules and support for Special Prices Tax Rates per location, customer group and product type Detailed Configuration Options in Theme Admin Penl'),(2,76,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(3,73,'Blue Denim Jeans'),(3,74,'Blue Denim Jeans'),(3,75,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Any Product types that You want - Simple, Configurable, Bundled and Grouped Products Downloadable/Digital Products, Virtual Products Inventory Management with Backordered items Customer Personalized Products - upload text for embroidery, monogramming, etc. Create Store-specific attributes on the fly Advanced Pricing Rules and support for Special Prices Tax Rates per location, customer group and product type Detailed Configuration Options in Theme Admin Penl'),(3,76,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(4,73,'Sheri Collar Shirt'),(4,74,'Sheri Collar Shirt'),(4,75,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Any Product types that You want - Simple, Configurable, Bundled and Grouped Products Downloadable/Digital Products, Virtual Products Inventory Management with Backordered items Customer Personalized Products - upload text for embroidery, monogramming, etc. Create Store-specific attributes on the fly Advanced Pricing Rules and support for Special Prices Tax Rates per location, customer group and product type Detailed Configuration Options in Theme Admin Penl'),(4,76,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(5,73,'Fashion Jacket'),(5,74,'Fashion Jacket'),(5,75,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Any Product types that You want - Simple, Configurable, Bundled and Grouped Products Downloadable/Digital Products, Virtual Products Inventory Management with Backordered items Customer Personalized Products - upload text for embroidery, monogramming, etc. Create Store-specific attributes on the fly Advanced Pricing Rules and support for Special Prices Tax Rates per location, customer group and product type Detailed Configuration Options in Theme Admin Penl'),(5,76,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(6,73,'Sample Product'),(6,74,'Sample Product'),(6,75,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Any Product types that You want - Simple, Configurable, Bundled and Grouped Products Downloadable/Digital Products, Virtual Products Inventory Management with Backordered items Customer Personalized Products - upload text for embroidery, monogramming, etc. Create Store-specific attributes on the fly Advanced Pricing Rules and support for Special Prices Tax Rates per location, customer group and product type Detailed Configuration Options in Theme Admin Penl'),(6,76,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(7,73,'Elizabeth Kint Top'),(7,74,'Elizabeth Kint Top'),(7,75,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Any Product types that You want - Simple, Configurable, Bundled and Grouped Products Downloadable/Digital Products, Virtual Products Inventory Management with Backordered items Customer Personalized Products - upload text for embroidery, monogramming, etc. Create Store-specific attributes on the fly Advanced Pricing Rules and support for Special Prices Tax Rates per location, customer group and product type Detailed Configuration Options in Theme Admin Penl'),(7,76,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(8,73,'Pink Shirt'),(8,74,'Pink Shirt'),(8,75,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Any Product types that You want - Simple, Configurable, Bundled and Grouped Products Downloadable/Digital Products, Virtual Products Inventory Management with Backordered items Customer Personalized Products - upload text for embroidery, monogramming, etc. Create Store-specific attributes on the fly Advanced Pricing Rules and support for Special Prices Tax Rates per location, customer group and product type Detailed Configuration Options in Theme Admin Penl'),(8,76,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(9,73,'Sample Fashion'),(9,74,'Sample Fashion'),(9,75,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Any Product types that You want - Simple, Configurable, Bundled and Grouped Products Downloadable/Digital Products, Virtual Products Inventory Management with Backordered items Customer Personalized Products - upload text for embroidery, monogramming, etc. Create Store-specific attributes on the fly Advanced Pricing Rules and support for Special Prices Tax Rates per location, customer group and product type Detailed Configuration Options in Theme Admin Penl'),(9,76,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(15,73,'Sample Configurable | Sample Configurable-White | Sample Configurable-Black | Sample Configurable-Red | Sample Configurable-Blue | Sample Configurable-Green'),(15,74,'Sample Configurable'),(15,93,'White | Black | Red | Blue | Green');

/*Table structure for table `catalogsearch_fulltext_scope2` */

DROP TABLE IF EXISTS `catalogsearch_fulltext_scope2`;

CREATE TABLE `catalogsearch_fulltext_scope2` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` int(10) unsigned NOT NULL COMMENT 'Attribute_id',
  `data_index` longtext COMMENT 'Data index',
  PRIMARY KEY (`entity_id`,`attribute_id`),
  FULLTEXT KEY `FTI_FULLTEXT_DATA_INDEX` (`data_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='catalogsearch_fulltext_scope2';

/*Data for the table `catalogsearch_fulltext_scope2` */

insert  into `catalogsearch_fulltext_scope2`(`entity_id`,`attribute_id`,`data_index`) values (1,73,'Women Blouse'),(1,74,'Women Blouse'),(1,75,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Any Product types that You want - Simple, Configurable, Bundled and Grouped Products Downloadable/Digital Products, Virtual Products Inventory Management with Backordered items Customer Personalized Products - upload text for embroidery, monogramming, etc. Create Store-specific attributes on the fly Advanced Pricing Rules and support for Special Prices Tax Rates per location, customer group and product type Detailed Configuration Options in Theme Admin Penl'),(1,76,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(2,73,'Fashion Dress'),(2,74,'Fashion Dress'),(2,75,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Any Product types that You want - Simple, Configurable, Bundled and Grouped Products Downloadable/Digital Products, Virtual Products Inventory Management with Backordered items Customer Personalized Products - upload text for embroidery, monogramming, etc. Create Store-specific attributes on the fly Advanced Pricing Rules and support for Special Prices Tax Rates per location, customer group and product type Detailed Configuration Options in Theme Admin Penl'),(2,76,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(3,73,'Blue Denim Jeans'),(3,74,'Blue Denim Jeans'),(3,75,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Any Product types that You want - Simple, Configurable, Bundled and Grouped Products Downloadable/Digital Products, Virtual Products Inventory Management with Backordered items Customer Personalized Products - upload text for embroidery, monogramming, etc. Create Store-specific attributes on the fly Advanced Pricing Rules and support for Special Prices Tax Rates per location, customer group and product type Detailed Configuration Options in Theme Admin Penl'),(3,76,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(4,73,'Sheri Collar Shirt'),(4,74,'Sheri Collar Shirt'),(4,75,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Any Product types that You want - Simple, Configurable, Bundled and Grouped Products Downloadable/Digital Products, Virtual Products Inventory Management with Backordered items Customer Personalized Products - upload text for embroidery, monogramming, etc. Create Store-specific attributes on the fly Advanced Pricing Rules and support for Special Prices Tax Rates per location, customer group and product type Detailed Configuration Options in Theme Admin Penl'),(4,76,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(5,73,'Fashion Jacket'),(5,74,'Fashion Jacket'),(5,75,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Any Product types that You want - Simple, Configurable, Bundled and Grouped Products Downloadable/Digital Products, Virtual Products Inventory Management with Backordered items Customer Personalized Products - upload text for embroidery, monogramming, etc. Create Store-specific attributes on the fly Advanced Pricing Rules and support for Special Prices Tax Rates per location, customer group and product type Detailed Configuration Options in Theme Admin Penl'),(5,76,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(6,73,'Sample Product'),(6,74,'Sample Product'),(6,75,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Any Product types that You want - Simple, Configurable, Bundled and Grouped Products Downloadable/Digital Products, Virtual Products Inventory Management with Backordered items Customer Personalized Products - upload text for embroidery, monogramming, etc. Create Store-specific attributes on the fly Advanced Pricing Rules and support for Special Prices Tax Rates per location, customer group and product type Detailed Configuration Options in Theme Admin Penl'),(6,76,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(7,73,'Elizabeth Kint Top'),(7,74,'Elizabeth Kint Top'),(7,75,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Any Product types that You want - Simple, Configurable, Bundled and Grouped Products Downloadable/Digital Products, Virtual Products Inventory Management with Backordered items Customer Personalized Products - upload text for embroidery, monogramming, etc. Create Store-specific attributes on the fly Advanced Pricing Rules and support for Special Prices Tax Rates per location, customer group and product type Detailed Configuration Options in Theme Admin Penl'),(7,76,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(8,73,'Pink Shirt'),(8,74,'Pink Shirt'),(8,75,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Any Product types that You want - Simple, Configurable, Bundled and Grouped Products Downloadable/Digital Products, Virtual Products Inventory Management with Backordered items Customer Personalized Products - upload text for embroidery, monogramming, etc. Create Store-specific attributes on the fly Advanced Pricing Rules and support for Special Prices Tax Rates per location, customer group and product type Detailed Configuration Options in Theme Admin Penl'),(8,76,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(9,73,'Sample Fashion'),(9,74,'Sample Fashion'),(9,75,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Any Product types that You want - Simple, Configurable, Bundled and Grouped Products Downloadable/Digital Products, Virtual Products Inventory Management with Backordered items Customer Personalized Products - upload text for embroidery, monogramming, etc. Create Store-specific attributes on the fly Advanced Pricing Rules and support for Special Prices Tax Rates per location, customer group and product type Detailed Configuration Options in Theme Admin Penl'),(9,76,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(15,73,'Sample Configurable | Sample Configurable-White | Sample Configurable-Black | Sample Configurable-Red | Sample Configurable-Blue | Sample Configurable-Green'),(15,74,'Sample Configurable'),(15,93,'White | Black | Red | Blue | Green');

/*Table structure for table `checkout_agreement` */

DROP TABLE IF EXISTS `checkout_agreement`;

CREATE TABLE `checkout_agreement` (
  `agreement_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Agreement Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `content` text COMMENT 'Content',
  `content_height` varchar(25) DEFAULT NULL COMMENT 'Content Height',
  `checkbox_text` text COMMENT 'Checkbox Text',
  `is_active` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Active',
  `is_html` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Html',
  `mode` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Applied mode',
  PRIMARY KEY (`agreement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Checkout Agreement';

/*Data for the table `checkout_agreement` */

/*Table structure for table `checkout_agreement_store` */

DROP TABLE IF EXISTS `checkout_agreement_store`;

CREATE TABLE `checkout_agreement_store` (
  `agreement_id` int(10) unsigned NOT NULL COMMENT 'Agreement Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  PRIMARY KEY (`agreement_id`,`store_id`),
  KEY `CHECKOUT_AGREEMENT_STORE_STORE_ID_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `CHECKOUT_AGREEMENT_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CHKT_AGRT_STORE_AGRT_ID_CHKT_AGRT_AGRT_ID` FOREIGN KEY (`agreement_id`) REFERENCES `checkout_agreement` (`agreement_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Checkout Agreement Store';

/*Data for the table `checkout_agreement_store` */

/*Table structure for table `cms_block` */

DROP TABLE IF EXISTS `cms_block`;

CREATE TABLE `cms_block` (
  `block_id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Block ID',
  `title` varchar(255) NOT NULL COMMENT 'Block Title',
  `identifier` varchar(255) NOT NULL COMMENT 'Block String Identifier',
  `content` mediumtext COMMENT 'Block Content',
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Block Creation Time',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Block Modification Time',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Block Active',
  PRIMARY KEY (`block_id`),
  FULLTEXT KEY `CMS_BLOCK_TITLE_IDENTIFIER_CONTENT` (`title`,`identifier`,`content`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COMMENT='CMS Block Table';

/*Data for the table `cms_block` */

insert  into `cms_block`(`block_id`,`title`,`identifier`,`content`,`creation_time`,`update_time`,`is_active`) values (1,'Porto - Homepage Slider 1','porto_homeslider_1','<div id=\"banner-slider-demo-1\" class=\"owl-carousel owl-banner-carousel owl-middle-narrow\">\n    <div class=\"item\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/01/slider/01_bg.png\"}}) repeat;\">\n        <div class=\"container\" style=\"position:relative\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/01/slider/01.png\"}}\" alt=\"\" />\n            <div class=\"content type1\" style=\"position:absolute;top:30%;left:10%;text-align:right\">\n                <h2 style=\"font-weight:600;line-height:1;color:#08c\">HUGE <b style=\"font-weight:800\">SALE</b></h2>\n                <p style=\"color:#777;font-weight:300;line-height:1;margin-bottom:15px\">Now starting at <span style=\"color:#535353;font-weight:400\">$99</span></p>\n                <a href=\"#\" style=\"font-weight:300;\">Shop now &gt;</a>\n            </div>\n        </div>\n    </div>\n    <div class=\"item\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/01/slider/02_bg.png\"}}) center center no-repeat;background-size:cover;\">\n        <div class=\"container\" style=\"position:relative\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/01/slider/02.png\"}}\" alt=\"\" />\n        </div>\n    </div>\n    <div class=\"item\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/01/slider/03_bg.png\"}}) center center no-repeat;background-size:cover;\">\n        <div class=\"container\" style=\"position:relative\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/01/slider/03.png\"}}\" alt=\"\" />\n        </div>\n    </div>\n</div>\n<script type=\"text/javascript\">\n    require([\n        \'jquery\',\n        \'owl.carousel/owl.carousel.min\'\n    ], function ($) {\n        $(\"#banner-slider-demo-1\").owlCarousel({\n            items: 1,\n            autoplay: true,\n            autoplayTimeout: 5000,\n            autoplayHoverPause: true,\n            dots: false,\n            nav: true,\n            navRewind: true,\n            animateIn: \'fadeIn\',\n            animateOut: \'fadeOut\',\n            loop: true,\n            navText: [\"<em class=\'porto-icon-chevron-left\'></em>\",\"<em class=\'porto-icon-chevron-right\'></em>\"]\n        });\n    });\n</script>\n<div class=\"homepage-bar\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-md-4\">\n                <em class=\"porto-icon-truck\" style=\"font-size:36px;\"></em><div class=\"text-area\"><h3>FREE SHIPPING & RETURN</h3><p>Free shipping on all orders over $99.</p></div>\n            </div>\n            <div class=\"col-md-4\">\n                <em class=\"porto-icon-dollar\"></em><div class=\"text-area\"><h3>MONEY BACK GUARANTEE</h3><p>100% money back guarantee.</p></div>\n            </div>\n            <div class=\"col-md-4\">\n                <em class=\"porto-icon-lifebuoy\" style=\"font-size:32px;\"></em><div class=\"text-area\"><h3>ONLINE SUPPORT 24/7</h3><p>Lorem ipsum dolor sit amet.</p></div>\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"single-images border-radius\" style=\"padding-top: 25px;\">\n    <div class=\"container\">\n        <div class=\"row\" style=\"margin-left:-10px;margin-right:-10px;\">\n            <div class=\"col-sm-3 col-xs-6\" style=\"padding-left:10px;padding-right:10px;padding-top:20px;\">\n                <a class=\"image-link\" href=\"#\">\n                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/01/slider/image_1.png\"}}\" alt=\"\" />\n                </a>\n            </div>\n            <div class=\"col-sm-3 col-xs-6\" style=\"padding-left:10px;padding-right:10px;padding-top:20px;\">\n                <a class=\"image-link\" href=\"#\">\n                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/01/slider/image_2.png\"}}\" alt=\"\" />\n                </a>\n            </div>\n            <div class=\"col-sm-3 col-xs-6\" style=\"padding-left:10px;padding-right:10px;padding-top:20px;\">\n                <a class=\"image-link\" href=\"#\">\n                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/01/slider/image_3.png\"}}\" alt=\"\" />\n                </a>\n            </div>\n            <div class=\"col-sm-3 col-xs-6\" style=\"padding-left:10px;padding-right:10px;padding-top:20px;\">\n                <a class=\"image-link\" href=\"#\">\n                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/01/slider/image_4.png\"}}\" alt=\"\" />\n                </a>\n            </div>\n         </div>\n    </div>\n</div>\n<div style=\"padding-top: 35px;\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-sm-12\">\n                <h2 class=\"filterproduct-title\"><span class=\"content\"><strong>Featured Products</strong></span></h2>\n                <div id=\"featured_product\" class=\"owl-top-narrow\">\n                    {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"featured_product\" product_count=\"10\" aspect_ratio=\"0\" image_width=\"212\" image_height=\"263\" template=\"owl_list.phtml\"}}\n                </div>\n                <script type=\"text/javascript\">\n                    require([\n                        \'jquery\',\n                        \'owl.carousel/owl.carousel.min\'\n                    ], function ($) {\n                        $(\"#featured_product .owl-carousel\").owlCarousel({\n                            autoplay: true,\n                            autoplayTimeout: 5000,\n                            autoplayHoverPause: true,\n                            loop: true,\n                            navRewind: true,\n                            margin: 10,\n                            nav: true,\n                            navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n                            dots: false,\n                            responsive: {\n                                0: {\n                                    items:2\n                                },\n                                768: {\n                                    items:3\n                                },\n                                992: {\n                                    items:4\n                                },\n                                1200: {\n                                    items:5\n                                }\n                            }\n                        });\n                    });\n                </script>\n            </div>\n        </div>\n    </div>\n</div>\n<div style=\"padding-top: 20px;\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-sm-12\">\n                <h2 class=\"filterproduct-title\"><span class=\"content\"><strong>New Products</strong></span></h2>\n                <div id=\"new_product\" class=\"owl-top-narrow\">\n                    {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"latest_product\" product_count=\"8\" hide-addtolinks=\"1\" aspect_ratio=\"0\" image_width=\"180\" image_height=\"180\" template=\"owl_list.phtml\"}}\n                </div>\n                <script type=\"text/javascript\">\n                    require([\n                        \'jquery\',\n                        \'owl.carousel/owl.carousel.min\'\n                    ], function ($) {\n                        $(\"#new_product .owl-carousel\").owlCarousel({\n                            autoplay: true,\n                            autoplayTimeout: 5000,\n                            autoplayHoverPause: true,\n                            loop: true,\n                            navRewind: true,\n                            margin: 10,\n                            nav: true,\n                            navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n                            dots: false,\n                            responsive: {\n                                0: {\n                                    items:2\n                                },\n                                640: {\n                                    items:3\n                                },\n                                768: {\n                                    items:4\n                                },\n                                992: {\n                                    items:5\n                                },\n                                1200: {\n                                    items:6\n                                }\n                            }\n                        });\n                    });\n                </script>\n            </div>\n        </div>\n    </div>\n</div>\n<div style=\"padding-top: 40px;\" class=\"custom-support\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-sm-4\">\n                <em class=\"porto-icon-star\" style=\"border-radius:50%;border:2px solid #cecece;color:#333;background-color:transparent;line-height:58px;font-size:25px;\"></em>\n                <div class=\"content\">\n                    <h2>Customer Support</h2>\n                    <em>YOU WON\'T BE ALONE</em>\n                    <p>We really care about you and your website as much as you do. Purchasing Porto or any other theme from us you get 100% free support.</p>\n                </div>\n            </div>\n            <div class=\"col-sm-4\">\n                <em class=\"porto-icon-reply\" style=\"border-radius:50%;border:2px solid #cecece;color:#333;background-color:transparent;line-height:58px;font-size:25px;\"></em>\n                <div class=\"content\">\n                    <h2>Fully Customizable</h2>\n                    <em>TONS OF OPTIONS</em>\n                    <p>With Porto you can customize the layout, colors and styles within only a few minutes. Start creating an amazing website right now!</p>\n                </div>\n            </div>\n            <div class=\"col-sm-4\">\n                <em class=\"porto-icon-paper-plane\" style=\"border-radius:50%;border:2px solid #cecece;color:#333;background-color:transparent;line-height:58px;font-size:25px;\"></em>\n                <div class=\"content\">\n                    <h2>Powerful Admin</h2>\n                    <em>MADE TO HELP YOU</em>\n                    <p>Porto has very powerful admin features to help customer to build their own shop in minutes without any special skills in web development.</p>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"parallax-wrapper\" style=\"margin: 50px 0;\">\n    <div class=\"overlay overlay-color\" style=\"background-color: #000000; opacity: 0.75; filter: alpha(opacity=75);\"></div>\n    <div class=\"parallax\" data-stellar-background-ratio=\"0.5\" style=\"background-image: url({{media url=\"wysiwyg/smartwave/porto/homepage/01/slider/parallax_bg.jpg\"}});\">\n        <div class=\"parallax-slider\" style=\"position: relative; z-index: 3;\">\n            <div class=\"container\">\n                <div id=\"parallax-slider-demo-1\" class=\"owl-carousel\">\n                    <div class=\"item\">\n                        <h2 style=\"font-weight:600;\">EXPLORE <b style=\"font-weight:800;\">PORTO</b></h2>\n                        <p style=\"font-weight:300;\">Premium theme, unlimited possibilities...</p>\n                        <a href=\"#\" style=\"font-weight:300;\">Purchase now &gt;</a>\n                    </div>\n                    <div class=\"item\">\n                        <h2 style=\"font-weight:600;\">EXPLORE <b style=\"font-weight:800;\">PORTO</b></h2>\n                        <p style=\"font-weight:300;\">Premium theme, unlimited possibilities...</p>\n                        <a href=\"#\" style=\"font-weight:300;\">Purchase now &gt;</a>\n                    </div>\n                    <div class=\"item\">\n                        <h2 style=\"font-weight:600;\">EXPLORE <b style=\"font-weight:800;\">PORTO</b></h2>\n                        <p style=\"font-weight:300;\">Premium theme, unlimited possibilities...</p>\n                        <a href=\"#\" style=\"font-weight:300;\">Purchase now &gt;</a>\n                    </div>\n                </div>\n            </div>\n            <script type=\"text/javascript\">\n                require([\n                    \'jquery\',\n                    \'owl.carousel/owl.carousel.min\'\n                ], function ($) {\n                    $(\"#parallax-slider-demo-1\").owlCarousel({\n                        items: 1,\n                        autoplay: true,\n                        autoplayTimeout: 5000,\n                        autoplayHoverPause: true,\n                        dots: true,\n                        nav: false,\n                        loop: true,\n                        navRewind: true\n                    });\n                });\n            </script>\n        </div>\n    </div>\n</div>','2016-08-26 10:55:53','2016-08-26 10:55:53',1),(2,'Porto - Custom Block for Header','porto_custom_block_for_header','<em class=\"porto-icon-phone\" style=\"margin-right: 5px;\"></em><span>(+404) 158 14 25 78</span><span class=\"split\"></span><a href=\"#\">CONTACT US</a>','2016-08-26 10:55:54','2016-08-26 10:55:54',1),(3,'Porto - Footer Links','porto_footer_links','<div class=\"block\">\n    <div class=\"block-title\"><strong>My Account</strong></div>\n    <div class=\"block-content\">\n        <ul class=\"links\">\n            <li><em class=\"porto-icon-right-dir theme-color\"></em><a href=\"{{store url=\"\"}}about-porto\" title=\"About us\">About us</a></li>\n            <li><em class=\"porto-icon-right-dir theme-color\"></em><a href=\"{{store url=\"\"}}contact\" title=\"Contact us\">Contact us</a></li>\n            <li><em class=\"porto-icon-right-dir theme-color\"></em><a href=\"{{store url=\"\"}}customer/account\" title=\"My account\">My account</a></li>\n            <li><em class=\"porto-icon-right-dir theme-color\"></em><a href=\"{{store url=\"\"}}sales/order/history\" title=\"Orders history\">Orders history</a></li>\n            <li><em class=\"porto-icon-right-dir theme-color\"></em><a href=\"{{store url=\"\"}}catalogsearch/advanced\" title=\"Advanced search\">Advanced search</a></li>\n        </ul>\n    </div>\n</div>','2016-08-26 10:55:54','2016-08-26 10:55:54',1),(4,'Porto - Footer Contact Information','porto_footer_contact_information','<div class=\"block\">\n    <div class=\"block-title\"><strong>Contact Information</strong></div>\n    <div class=\"block-content\">\n        <ul class=\"contact-info\">\n            <li><em class=\"porto-icon-location\"></em><p><b>Address:</b><br/>123 Street Name, City, England</p></li>\n            <li><em class=\"porto-icon-phone\"></em><p><b>Phone:</b><br/>(123) 456-7890</p></li>\n            <li><em class=\"porto-icon-mail\"></em><p><b>Email:</b><br/><a href=\"mailto:mail@example.com\">mail@example.com</a></p></li>\n            <li><em class=\"porto-icon-clock\"></em><p><b>Working Days/Hours:</b><br/>Mon - Sun / 9:00AM - 8:00PM</p></li>\n        </ul>\n    </div>\n</div>','2016-08-26 10:55:54','2016-08-26 10:55:54',1),(5,'Porto - Footer Features','porto_footer_features','<div class=\"block\">\n    <div class=\"block-title\"><strong>Main Features</strong></div>\n    <div class=\"block-content\">\n        <ul class=\"features\">\n            <li><em class=\"porto-icon-ok theme-color\"></em><a href=\"#\">Super Fast Magento Theme</a></li>\n            <li><em class=\"porto-icon-ok theme-color\"></em><a href=\"#\">1st Fully working Ajax Theme</a></li>\n            <li><em class=\"porto-icon-ok theme-color\"></em><a href=\"#\">10 Unique Homepage Layouts</a></li>\n            <li><em class=\"porto-icon-ok theme-color\"></em><a href=\"#\">Powerful Admin Panel</a></li>\n            <li><em class=\"porto-icon-ok theme-color\"></em><a href=\"#\">Mobile &amp; Retina Optimized</a></li>\n        </ul>\n    </div>\n</div>','2016-08-26 10:55:54','2016-08-26 10:55:54',1),(6,'Porto - Custom Block for Footer Bottom Area','porto_footer_bottom_custom_block','<ul class=\"social-icons\">\n    <li><a class=\"facebook-link\" href=\"#\"><em class=\"porto-icon-facebook\"></em></a></li>\n    <li><a class=\"twitter-link\" href=\"#\"><em class=\"porto-icon-twitter\"></em></a></li>\n    <li><a class=\"linkedin-link\" href=\"#\"><em class=\"porto-icon-linkedin-squared\"></em></a></li>\n</ul>\n<img src=\"{{media url=\"wysiwyg/smartwave/porto/footer/payments.png\"}}\" alt=\"\" />','2016-08-26 10:55:54','2016-08-26 10:55:54',1),(7,'Porto - Custom Block for Category Page Side bar','porto_category_side_custom_block','<h2 class=\"sidebar-title\">Featured</h2>\n<div class=\"sidebar-filterproducts custom-block\" style=\"padding-bottom: 20px;\">\n    <div id=\"featured_product\" class=\"owl-top-narrow\">\n        {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"featured_product\" product_count=\"9\" aspect_ratio=\"1\" image_width=\"150\" template=\"side_list.phtml\"}}\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            $(\"#featured_product .owl-carousel\").owlCarousel({\n                loop: false,\n                items: 1,\n                nav: true,\n                navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n                dots: false\n            });\n        });\n    </script>\n</div>\n<h2 style=\"font-weight:600;font-size:16px;color:#000;line-height:1;margin-bottom:15px;\">Custom HTML Block</h2>\n<h5 style=\"font-family:Arial;font-weight:400;font-size:11px;color:#878787;line-height:1;margin-bottom:13px;\">This is a custom sub-title.</h5>\n<p style=\"font-weight:400;font-size:14px;color:#666;line-height:1.42;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non placerat mi. Etiam non tellus </p>\n<div class=\"space\" style=\"margin-bottom: 45px;\"></div>','2016-08-26 10:55:54','2016-08-26 10:55:54',1),(8,'Porto - Custom Block for Product Page Side bar','porto_product_side_custom_block','<div class=\"custom-block custom-block-1\">\n    <div>\n        <em class=\"porto-icon-truck\"></em>\n        <h3>FREE SHIPPING</h3>\n        <p>Free shipping on all orders over $99.</p>\n    </div>\n    <div>\n        <em class=\"porto-icon-dollar\"></em>\n        <h3>MONEY BACK GUARANTEE</h3>\n        <p>100% money back guarantee.</p>\n    </div>\n    <div>\n        <em class=\"porto-icon-lifebuoy\"></em>\n        <h3>ONLINE SUPPORT 24/7</h3>\n        <p>Lorem ipsum dolor sit amet.</p>\n    </div>\n</div>\n\n<div class=\"custom-block\">\n    <div id=\"custom-owl-slider-product\" class=\"owl-carousel\">\n        <img class=\"owl-lazy\" data-src=\"{{media url=\"wysiwyg/smartwave/porto/product/ad1.jpg\"}}\" alt=\"\" />\n        <img class=\"owl-lazy\" data-src=\"{{media url=\"wysiwyg/smartwave/porto/product/ad2.jpg\"}}\" alt=\"\" />\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            $(\'#custom-owl-slider-product.owl-carousel\').owlCarousel({\n                items:1,\n                lazyLoad:true\n            });         \n        });\n    </script>\n</div>','2016-08-26 10:55:54','2016-08-26 10:55:54',1),(9,'Porto - Custom Menu(before)','porto_custom_menu_before','<ul>\n    <li class=\"ui-menu-item level0\">\n        <a href=\"{{store url=\'\'}}\" class=\"level-top\"><span>Home</span></a>\n    </li>\n</ul>','2016-08-26 10:55:54','2016-08-26 10:55:54',1),(10,'Porto - Custom Menu(after)','porto_custom_menu','<ul>\n    <li class=\"ui-menu-item level0\">\n        <a href=\"#\" class=\"level-top\"><span>Features</span></a>\n    </li>\n</ul>','2016-08-26 10:55:54','2016-08-26 10:55:54',1),(11,'Porto - Category Banner(Fashion)','porto_category_banner_fashion','<div id=\"fashion-category-banner\" class=\"owl-carousel owl-banner-carousel category-boxed-banner owl-bottom-narrow\">\n    <div class=\"item\">\n        <img src=\"{{media url=\"wysiwyg/smartwave/porto/category/fashion/02-01.png\"}}\" alt=\"\" />\n        <div class=\"banner-text\" style=\"top:37%;\">\n            <h2 style=\"background-color:transparent;color:#2b2b2b;margin:0;\">CATEGORY <b>BANNER</b></h2><br>\n            <p style=\"max-width: 280px;background-color:transparent;color:#2b2b2b;font-weight:400;\">Set banners and description for any category of your website.</p>\n        </div>\n    </div>\n    <div class=\"item\">\n        <img src=\"{{media url=\"wysiwyg/smartwave/porto/category/fashion/02-02.png\"}}\" alt=\"\" />\n        <div class=\"banner-text\" style=\"top:45%;\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/category/fashion/02-02-01.png\"}}\" alt=\"\" style=\"vertical-align:middle;width:auto;max-width:50%;display:inline-block;\">\n            <a href=\"#\" class=\"shop-now\" style=\"color:#2b2b2b;vertical-align:middle;\">Shop now &gt;</a>\n        </div>\n    </div>\n</div>\n<script type=\"text/javascript\">\n    require([\n        \'jquery\',\n        \'owl.carousel/owl.carousel.min\'\n    ], function ($) {\n        $(\"#fashion-category-banner\").owlCarousel({\n            items: 1,\n            autoplay: false,\n            autoplayTimeout: 5000,\n            autoplayHoverPause: true,\n            dots: true,\n            nav: false,\n            navRewind: true,\n            animateIn: \'fadeIn\',\n            animateOut: \'fadeOut\',\n            loop: true\n        });\n    });\n</script>','2016-08-26 10:55:55','2016-08-26 10:55:55',1),(12,'Porto - Custom Tab','porto_custom_tab','<table class=\"table table-sizing-guide\">\n    <thead>\n        <tr>\n            <th colspan=\"6\"><b>Clothing - Single Size Conversion (Continued)</b></th>\n        </tr>\n    </thead>\n    <tbody>\n        <tr>\n            <td>UK</td>\n            <td>18</td>\n            <td>20</td>\n            <td>22</td>\n            <td>24</td>\n            <td>26</td>\n        </tr>\n        <tr>\n            <td>European</td>\n            <td>46</td>\n            <td>48</td>\n            <td>50</td>\n            <td>52</td>\n            <td>54</td>\n        </tr>\n        <tr>\n            <td>US</td>\n            <td>14</td>\n            <td>16</td>\n            <td>18</td>\n            <td>20</td>\n            <td>22</td>\n        </tr>\n        <tr>\n            <td>Australia</td>\n            <td>8</td>\n            <td>10</td>\n            <td>12</td>\n            <td>14</td>\n            <td>16</td>\n        </tr>\n    </tbody>\n</table>','2016-08-26 10:55:55','2016-08-26 10:55:55',1),(13,'Porto - Homepage Slider 2','porto_homeslider_2','<div class=\"container\" style=\"padding:0 15px 20px 15px;\">\n    <div class=\"row\" style=\"margin:0 -10px;\">\n        <div class=\"col-md-9 md-f-right\" style=\"padding:0 10px; margin-top: 20px;\">\n            <div id=\"banner-slider-demo-2\" class=\"owl-carousel owl-bottom-absolute-narrow owl-border-radius-7 owl-banner-carousel sm-xs-margin-bottom\">\n                <div class=\"item\">\n                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/02/slider/slide1.jpg\"}}\" alt=\"\" />\n                </div>\n                <div class=\"item\">\n                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/02/slider/slide2.jpg\"}}\" alt=\"\" />\n                </div>\n            </div>\n            <script type=\"text/javascript\">\n                require([\n                    \'jquery\',\n                    \'owl.carousel/owl.carousel.min\'\n                ], function ($) {\n                    $(\"#banner-slider-demo-2\").owlCarousel({\n                        items: 1,\n                        autoplay: true,\n                        autoplayTimeout: 5000,\n                        autoplayHoverPause: true,\n                        dots: true,\n                        navRewind: true,\n                        animateIn: \'fadeIn\',\n                        animateOut: \'fadeOut\',\n                        loop: true,\n                        nav: false\n                    });\n                });\n            </script>\n        </div>\n        <div class=\"col-md-3\" style=\"padding:0 10px; margin-top: 20px;\">\n            <div class=\"row\"><div class=\"col-md-12 col-sm-6\">\n            <div class=\"side-custom-menu\" style=\"margin-bottom:11px;\">\n                <h2><em class=\"porto-icon-menu\"></em> TOP CATEGORIES</h2>\n                <ul>\n                    <li><a href=\"#\">Fashion</a></li>\n                    <li><a href=\"#\">Electronics</a></li>\n                    <li><a href=\"#\">Gifts</a></li>\n                    <li><a href=\"#\">Home & Garden</a></li>\n                    <li><a href=\"#\">Music</a></li>\n                    <li><a href=\"#\">Sports</a></li>\n                </ul>\n            </div>\n            </div>\n            <div class=\"col-md-12 col-sm-6\">\n            <div class=\"side-custom-menu\">\n                <h2><em class=\"porto-icon-menu\"></em> TRENDING</h2>\n                <ul>\n                    <li><a href=\"#\">Huge Sale - 70% Off</a></li>\n                    <li><a href=\"#\">New Arrivals</a></li>\n                </ul>\n            </div>\n            </div></div>\n        </div>\n    </div>\n</div>\n<div style=\"background-color:#f0f0ed;\">\n    <div class=\"container\">\n        <div class=\"homepage-bar\" style=\"border:1px solid #e1e1e1;border-radius:5px;margin:20px 0;\">\n            <div class=\"row\">\n                <div class=\"col-md-4\" style=\"text-align:center;\">\n                    <em class=\"porto-icon-truck\" style=\"font-size:36px;\"></em><div class=\"text-area\"><h3>FREE SHIPPING & RETURN</h3><p>Free shipping on all orders over $99.</p></div>\n                </div>\n                <div class=\"col-md-4\" style=\"text-align:center;\">\n                    <em class=\"porto-icon-dollar\"></em><div class=\"text-area\"><h3>MONEY BACK GUARANTEE</h3><p>100% money back guarantee.</p></div>\n                </div>\n                <div class=\"col-md-4\" style=\"text-align:center;\">\n                    <em class=\"porto-icon-lifebuoy\" style=\"font-size:32px;\"></em><div class=\"text-area\"><h3>ONLINE SUPPORT 24/7</h3><p>Lorem ipsum dolor sit amet.</p></div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\" style=\"padding-bottom:10px;margin:0 -10px;\">\n            <div class=\"col-sm-3 col-xs-6\" style=\"padding:0 10px;padding-bottom:20px;\">\n                <div style=\"margin-bottom:15px;\">\n                    <a class=\"image-link\" href=\"#\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/02/slider/image01.jpg\"}}\" alt=\"\" /></a>\n                </div>\n                <div style=\"margin-bottom:15px;\">\n                    <a class=\"image-link\" href=\"#\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/02/slider/image02.jpg\"}}\" alt=\"\" /></a>\n                </div>\n                <div style=\"margin-bottom:0px;\">\n                    <a class=\"image-link\" href=\"#\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/02/slider/image03.jpg\"}}\" alt=\"\" /></a>\n                </div>\n            </div>\n            <div class=\"col-sm-3 col-xs-6 sm-f-right\" style=\"padding:0 10px;padding-bottom:20px;\">\n                <div style=\"margin-bottom:10px;\">\n                    <a class=\"image-link\" href=\"#\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/02/slider/image11.jpg\"}}\" alt=\"\" /></a>\n                </div>\n                <div style=\"margin-bottom:11px;\">\n                    <a class=\"image-link\" href=\"#\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/02/slider/image12.jpg\"}}\" alt=\"\" /></a>\n                </div>\n                <div style=\"margin-bottom:0px;\">\n                    <a class=\"image-link\" href=\"#\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/02/slider/image13.jpg\"}}\" alt=\"\" /></a>\n                </div>\n            </div>\n            <div class=\"sm-clearer\"></div>\n            <div class=\"col-sm-6\" style=\"padding:0 10px;padding-bottom:20px;\">\n                <div style=\"margin-bottom:28px;\">\n                    <a class=\"image-link\" href=\"#\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/02/slider/image21.jpg\"}}\" alt=\"\" /></a>\n                </div>\n                <div style=\"margin-bottom:28px;\">\n                    <a class=\"image-link\" href=\"#\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/02/slider/image22.jpg\"}}\" alt=\"\" /></a>\n                </div>\n                <div style=\"margin-bottom:0px;\">\n                    <a class=\"image-link\" href=\"#\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/02/slider/image23.jpg\"}}\" alt=\"\" /></a>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>','2016-10-21 23:35:21','2016-10-21 23:35:21',1),(14,'Porto - Homepage Slider 3','porto_homeslider_3','<div class=\"full-screen-slider\">\n    <div id=\"banner-slider-demo-3\" class=\"owl-carousel owl-middle-narrow owl-banner-carousel\">\n        <div class=\"item\" style=\"background:#f0f0f0;background-image:linear-gradient(#e8e8e8,#f0f0f0);position:relative;\">\n            <div style=\"width:100%;height:100%;width:100vw;height:100vh;background:url({{media url=\"wysiwyg/smartwave/porto/homepage/03/slider/slide1.jpg\"}}) 80% center no-repeat; background-size:cover;\"></div>\n            <div class=\"slide1-content\" style=\"width:100%;height:100%;position:absolute;left:0;top:0;\">\n                <div class=\"container\" style=\"height:100%;position:relative;\">\n                    <div class=\"content-area\" style=\"position:absolute;top:37%;left:7%;text-align:center;\">\n                        <div style=\"margin:0;line-height:1;\">\n                            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/03/slider/quote.png\"}}\" alt=\"\" class=\"quote\" style=\"display:inline-block;vertical-align:middle;\"/>\n                            <em style=\"display:inline-block;vertical-align:middle;line-height:1;color:#fff;font-weight:300;font-style:normal;\">DO YOU NEED A NEW</em>\n                            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/03/slider/quote.png\"}}\" alt=\"\" class=\"quote\" style=\"display:inline-block;vertical-align:middle;\"/>\n                        </div>\n                        <h2 style=\"font-weight:800;color:#fff;line-height:1;\">eCOMMERCE?</h2>\n                        <p style=\"font-weight:300;color:#fff;line-height:1;\">Check our options and features.</p>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"item\" style=\"background:#f0f0f0;background-image:linear-gradient(#e8e8e8,#f0f0f0);position:relative;\">\n            <div style=\"width:100%;height:100%;width:100vw;height:100vh;background:url({{media url=\"wysiwyg/smartwave/porto/homepage/03/slider/slide2.jpg\"}}) 20% center no-repeat; background-size:cover;\"></div>\n            <div class=\"slide2-content\" style=\"width:100%;height:100%;position:absolute;left:0;top:0;\">\n                <div class=\"container\" style=\"height:100%;position:relative;\">\n                    <div class=\"content-area\" style=\"position:absolute;top:40%;right:20%;text-align:left;\">\n                        <em style=\"display:block;line-height:1;color:#fff;font-weight:300;font-style:normal;\">up to <b>70%</b> off!</em>\n                        <h2 style=\"font-weight:600;color:#fff;line-height:1;\">HUGE <b style=\"font-weight:800;\">SALE</b></h2>\n                        <p style=\"font-weight:600;color:#fff;line-height:1;\">Fashion<span class=\"split\">-</span>Electronics<span class=\"split\">-</span>Gifts<span class=\"split\">-</span>Music<span class=\"split\">-</span>Sports</p>\n                        <div style=\"text-align:right;\"><a href=\"#\" class=\"btn btn-default\" style=\"border:0;background-color:#fefefe;color:#777;font-weight:600;\">SHOP NOW</a></div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"item\" style=\"background:#f0f0f0;background-image:linear-gradient(#e8e8e8,#f0f0f0);position:relative;\">\n            <div style=\"width:100%;height:100%;width:100vw;height:100vh;background:url({{media url=\"wysiwyg/smartwave/porto/homepage/03/slider/slide3.jpg\"}}) center center no-repeat; background-size:cover;\"></div>\n            <div class=\"slide3-content\" style=\"width:100%;height:100%;position:absolute;left:0;top:0;\">\n                <div class=\"container\" style=\"height:100%;position:relative;\">\n                    <div class=\"content-area\" style=\"position:absolute;left:0;top:40%;text-align:center;width:100%;\">\n                        <div style=\"display:inline-block;\">\n                            <em style=\"display:block;line-height:1;margin:0;color:#fff;font-weight:300;font-style:normal;text-align:right;\">It’s time to feel</em>\n                            <h2 style=\"font-weight:600;color:#fff;line-height:1;text-align:center;\">THE <b style=\"font-weight:800;\">POWER</b></h2>\n                            <div style=\"text-align:right;\"><a href=\"#\" class=\"btn btn-default\" style=\"border:0;background-color:#fefefe;color:#777;font-weight:600;\">SHOP NOW</a></div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            var owl_3 = $(\"#banner-slider-demo-3\").owlCarousel({\n                items: 1,\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                dots: false,\n                navRewind: true,\n                animateIn: \'fadeIn\',\n                animateOut: \'fadeOut\',\n                loop: true,\n                nav: true,\n                navText: [\"<em class=\'porto-icon-chevron-left\'></em>\",\"<em class=\'porto-icon-chevron-right\'></em>\"]\n            });\n        });\n    </script>\n</div>','2016-10-21 23:35:21','2016-10-21 23:35:21',1),(15,'Porto - Homepage Slider 4','porto_homeslider_4','<div class=\"full-screen-slider\">\n    <div id=\"banner-slider-demo-4\" class=\"owl-carousel owl-middle-narrow owl-banner-carousel\">\n        <div class=\"item\" style=\"background:#f0f0f0;background-image:linear-gradient(#e8e8e8,#f0f0f0);position:relative;\">\n            <div style=\"width:100%;height:100%;background:url({{media url=\"wysiwyg/smartwave/porto/homepage/04/slider/slide1.jpg\"}}) 80% center no-repeat; background-size:cover;\"></div>\n            <div class=\"slide1-content\" style=\"width:100%;height:100%;position:absolute;left:0;top:0;\">\n                <div class=\"container\" style=\"height:100%;position:relative;\">\n                    <div class=\"content-area\" style=\"position:absolute;top:37%;left:7%;text-align:center;\">\n                        <div style=\"margin:0;line-height:1;\">\n                            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/04/slider/quote.png\"}}\" alt=\"\" class=\"quote\" style=\"display:inline-block;vertical-align:middle;\"/>\n                            <em style=\"display:inline-block;vertical-align:middle;line-height:1;color:#fff;font-weight:300;font-style:normal;\">DO YOU NEED A NEW</em>\n                            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/04/slider/quote.png\"}}\" alt=\"\" class=\"quote\" style=\"display:inline-block;vertical-align:middle;\"/>\n                        </div>\n                        <h2 style=\"font-weight:800;color:#fff;line-height:1;\">eCOMMERCE?</h2>\n                        <p style=\"font-weight:300;color:#fff;line-height:1;\">Check our options and features.</p>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"item\" style=\"background:#f0f0f0;background-image:linear-gradient(#e8e8e8,#f0f0f0);position:relative;\">\n            <div style=\"width:100%;height:100%;background:url({{media url=\"wysiwyg/smartwave/porto/homepage/04/slider/slide2.jpg\"}}) 20% center no-repeat; background-size:cover;\"></div>\n            <div class=\"slide2-content\" style=\"width:100%;height:100%;position:absolute;left:0;top:0;\">\n                <div class=\"container\" style=\"height:100%;position:relative;\">\n                    <div class=\"content-area\" style=\"position:absolute;top:40%;right:20%;text-align:left;\">\n                        <em style=\"display:block;line-height:1;color:#fff;font-weight:300;font-style:normal;\">up to <b>70%</b> off!</em>\n                        <h2 style=\"font-weight:600;color:#fff;line-height:1;\">HUGE <b style=\"font-weight:800;\">SALE</b></h2>\n                        <p style=\"font-weight:600;color:#fff;line-height:1;\">Fashion<span class=\"split\">-</span>Electronics<span class=\"split\">-</span>Gifts<span class=\"split\">-</span>Music<span class=\"split\">-</span>Sports</p>\n                        <div style=\"text-align:right;\"><a href=\"#\" class=\"btn btn-default\" style=\"border:0;background-color:#fefefe;color:#777;font-weight:600;\">SHOP NOW</a></div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"item\" style=\"background:#f0f0f0;background-image:linear-gradient(#e8e8e8,#f0f0f0);position:relative;\">\n            <div style=\"width:100%;height:100%;background:url({{media url=\"wysiwyg/smartwave/porto/homepage/04/slider/slide3.jpg\"}}) center center no-repeat; background-size:cover;\"></div>\n            <div class=\"slide3-content\" style=\"width:100%;height:100%;position:absolute;left:0;top:0;\">\n                <div class=\"container\" style=\"height:100%;position:relative;\">\n                    <div class=\"content-area\" style=\"position:absolute;left:0;top:40%;text-align:center;width:100%;\">\n                        <div style=\"display:inline-block;\">\n                            <em style=\"display:block;line-height:1;margin:0;color:#fff;font-weight:300;font-style:normal;text-align:right;\">It’s time to feel</em>\n                            <h2 style=\"font-weight:600;color:#fff;line-height:1;text-align:center;\">THE <b style=\"font-weight:800;\">POWER</b></h2>\n                            <div style=\"text-align:right;\"><a href=\"#\" class=\"btn btn-default\" style=\"border:0;background-color:#fefefe;color:#777;font-weight:600;\">SHOP NOW</a></div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            $(\"#banner-slider-demo-4\").owlCarousel({\n                items: 1,\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                navRewind: true,\n                dots: false,\n                animateIn: \'fadeIn\',\n                animateOut: \'fadeOut\',\n                loop: true,\n                nav: true,\n                navText: [\"<em class=\'porto-icon-chevron-left\'></em>\",\"<em class=\'porto-icon-chevron-right\'></em>\"]\n            });\n            $(\".main-container\").remove();\n        });\n    </script>\n</div>\n<div class=\"row\" style=\"margin:0;\">\n    <div class=\"col-sm-3 col-xs-6\" style=\"padding:0;\">\n        <a class=\"image-link\" href=\"#\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/04/slider/image01.jpg\"}}\" alt=\"\" />\n        </a>\n    </div>\n    <div class=\"col-sm-3 col-xs-6\" style=\"padding:0;\">\n        <a class=\"image-link\" href=\"#\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/04/slider/image02.jpg\"}}\" alt=\"\" />\n        </a>\n    </div>\n    <div class=\"col-sm-3 col-xs-6\" style=\"padding:0;\">\n        <a class=\"image-link\" href=\"#\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/04/slider/image03.jpg\"}}\" alt=\"\" />\n        </a>\n    </div>\n    <div class=\"col-sm-3 col-xs-6\" style=\"padding:0;\">\n        <a class=\"image-link\" href=\"#\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/04/slider/image04.jpg\"}}\" alt=\"\" />\n        </a>\n    </div>\n</div>\n<div class=\"row\" style=\"margin:0;background-color:#f9f9f9;\">\n    <div class=\"col-sm-6 sm-f-right\" style=\"padding:0;\">\n        <div id=\"half-image-1\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/04/slider/men.jpg\"}}) center left no-repeat;background-size:cover;width:100%;\">\n        <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/04/slider/men.jpg\"}}\" style=\"width:100%;visibility:hidden;\" alt=\"\" />\n        </div>\n    </div>\n    <div id=\"half-content-1\" class=\"col-sm-6\" style=\"padding:4% 6% 1.5%;position:relative;text-align:left;\">\n        <h3 class=\"fullwidth-filter-title\">Amazing New<br/>Collection</h3>\n        <p class=\"fullwidth-filter-description\">Yes, this is our new collection, check it<br/>out our new arrivals.</p>\n        <div id=\"men_product\" class=\"hide-addtocart hide-addtolinks no-border\" style=\"margin-top: 25px;\">\n            {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"men_product\" product_count=\"4\" aspect_ratio=\"0\" image_width=\"400\" image_height=\"496\" template=\"owl_list.phtml\"}}\n        </div>\n        <script type=\"text/javascript\">\n            require([\n                \'jquery\',\n                \'owl.carousel/owl.carousel.min\'\n            ], function ($) {\n                $(\"#men_product .owl-carousel\").owlCarousel({\n                    loop: true,\n                    margin: 10,\n                    autoplay: true,\n                    autoplayTimeout: 5000,\n                    autoplayHoverPause: true,\n                    navRewind: true,\n                    nav: false,\n                    dots: true,\n                    responsive: {\n                        0: {\n                            items:1\n                        },\n                        640: {\n                            items:2\n                        }\n                    }\n                });\n            });\n        </script>\n    </div>\n</div>\n<div class=\"row\" style=\"margin:0;background-color:#f9f9f9;\">\n    <div class=\"col-sm-6\" style=\"padding:0;\">\n        <div id=\"half-image-2\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/04/slider/women.jpg\"}}) center right no-repeat;background-size:cover;width:100%;\">\n        <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/04/slider/women.jpg\"}}\" style=\"width:100%;visibility:hidden;\" alt=\"\" />\n        </div>\n    </div>\n    <div id=\"half-content-2\" class=\"col-sm-6\" style=\"padding:4% 6% 1.5%;position:relative;text-align:left;\">\n        <h3 class=\"fullwidth-filter-title\">Amazing New<br/>Collection</h3>\n        <p class=\"fullwidth-filter-description\">Yes, this is our new collection, check it<br/>out our new arrivals.</p>\n        <div id=\"women_product\" class=\"hide-addtocart hide-addtolinks no-border\" style=\"margin-top: 25px;\">\n            {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"women_product\" product_count=\"4\" aspect_ratio=\"0\" image_width=\"400\" image_height=\"496\" template=\"owl_list.phtml\"}}\n        </div>\n        <script type=\"text/javascript\">\n            require([\n                \'jquery\',\n                \'owl.carousel/owl.carousel.min\'\n            ], function ($) {\n            $(\"#women_product .owl-carousel\").owlCarousel({\n                loop: true,\n                margin: 10,\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                navRewind: true,\n                nav: false,\n                dots: true,\n                responsive: {\n                    0: {\n                        items:1\n                    },\n                    640: {\n                        items:2\n                    }\n                }\n            });\n        });\n    </script>\n    </div>\n</div>\n<div style=\"text-align:left; padding: 30px;\">\n    <h3 class=\"fullwidth-filter-title\">Amazing New<br/>Collection</h3>\n    <div id=\"fashion_product\" class=\"hide-addtocart hide-addtolinks no-border\" style=\"margin-top: 25px;\">\n         {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"latest_product\" product_count=\"8\" aspect_ratio=\"0\" image_width=\"400\" image_height=\"496\" template=\"owl_list.phtml\"}}\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            $(\"#fashion_product .owl-carousel\").owlCarousel({\n                loop: true,\n                margin: 10,\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                navRewind: true,\n                nav: false,\n                dots: true,\n                responsive: {\n                    0: {\n                        items:2\n                    },\n                    768: {\n                        items:3\n                    },\n                    992: {\n                        items:4\n                    },\n                    1200: {\n                        items:5\n                    }\n                }\n            });\n        });\n    </script>\n</div>\n<script type=\"text/javascript\">\n    require([\n        \'jquery\'\n    ], function ($) {\n        $(\"#half-image-1\").css(\"min-height\",$(\"#half-content-1\").outerHeight()+\"px\");\n        $(\"#half-image-2\").css(\"min-height\",$(\"#half-content-2\").outerHeight()+\"px\");\n    setTimeout(function(){\n        $(\"#half-image-1\").css(\"min-height\",$(\"#half-content-1\").outerHeight()+\"px\");\n        $(\"#half-image-2\").css(\"min-height\",$(\"#half-content-2\").outerHeight()+\"px\");\n    }, 5000);\n    $(window).resize(function(){\n        setTimeout(function(){\n            $(\"#half-image-1\").css(\"min-height\",$(\"#half-content-1\").outerHeight()+\"px\");\n            $(\"#half-image-2\").css(\"min-height\",$(\"#half-content-2\").outerHeight()+\"px\");\n        }, 500);\n    });\n});\n</script>\n<style type=\"text/css\">\n.product-item-photo {\n    border-radius: 0;\n    border: 0;\n}\n</style>','2016-10-21 23:35:21','2016-10-21 23:35:21',1),(16,'Porto - Homepage Slider 5','porto_homeslider_5','<div style=\"padding:29px 0 36px;background-color:#f4f4f4;\">\n    <div class=\"container\">\n        <div class=\"slider-with-side\">\n            <div class=\"slider-area\">\n                <div id=\"banner-slider-demo-5\" class=\"owl-carousel owl-bottom-narrow owl-banner-carousel\">\n                    <div class=\"item\">\n                        <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/05/slider/slide2.jpg\"}}\" alt=\"\" />\n                    </div>\n                    <div class=\"item\">\n                        <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/05/slider/slide1.jpg\"}}\" alt=\"\" />\n                        <div class=\"content\" style=\"position:absolute;width:100%;height:100%;left:0;top:0;\">\n                            <div class=\"ribbon\" style=\"position:absolute;text-align:right;\">\n                                <em style=\"font-weight:300;line-height:1;color:#fff;font-style:normal;\">FEEL</em>\n                                <h3 style=\"font-weight:700;line-height:1;color:#fff;\">The</h3>\n                                <h5 style=\"font-weight:600;line-height:1;color:#fff;\">Power!</h5>\n                            </div>\n                            <div class=\"text-area\" style=\"left:3%;bottom:15%;position:absolute;text-align:left;\">\n                                <h2 style=\"font-weight:600;line-height:1;color:#fff;background-color:#202020;background-color:rgba(23,23,23,.9);\">EXPLORE <b style=\"font-weight:800;\">PORTO</b></h2><br/>\n                                <p style=\"line-height:1.2;color:#fff;background-color:#444;background-color:rgba(23,23,23,.75);\">With <b>PORTO</b> you can customize the<br/>banners and styles whithin minutes.</p>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <script type=\"text/javascript\">\n                    require([\n                        \'jquery\',\n                        \'owl.carousel/owl.carousel.min\'\n                    ], function ($) {\n                        $(\"#banner-slider-demo-5\").owlCarousel({\n                            items: 1,\n                            autoplay: true,\n                            autoplayTimeout: 5000,\n                            autoplayHoverPause: true,\n                            dots: true,\n                            nav: false,\n                            navRewind: true,\n                            animateIn: \'fadeIn\',\n                            animateOut: \'fadeOut\',\n                            loop: true\n                        });\n                    });\n                </script>\n            </div>\n            <div class=\"side-area\">\n                <div class=\"item1\">\n                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/05/slider/right-banner1.jpg\"}}\" alt=\"\" />\n                    <div class=\"content\">\n                        <em  style=\"color:#fff;font-weight:400;font-style:italic;line-height:1;\">fashion</em>\n                        <h2 style=\"color:#fff;font-weight:700;line-height:1;\">TRENDS</h2>\n                        <a href=\"#\" style=\"color:#000;font-weight:300;line-height:1;\">Shop now &gt;</a>\n                    </div>\n                </div>\n                <div class=\"item2\">\n                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/05/slider/right-banner2.jpg\"}}\" alt=\"\" />\n                    <div class=\"content\">\n                        <h2 style=\"color:#000;font-weight:300;line-height:1;\"><b style=\"font-weight:600;font-style:italic;\">Deals</b> + PROMOS</h2>\n                        <p style=\"color:#777;font-weight:300;font-style:italic;vertical-align:bottom;line-height:1.2;float:left;\">Limited sales in<br/>our categories.</p>\n                        <a href=\"#\" class=\"btn btn-default\" style=\"color:#fff;font-weight:400;vertical-align:bottom;line-height:1;float:left;\">SHOP NOW <i class=\"icon-right-dir\"></i></a>\n                        <div class=\"clearer\"></div>\n                    </div>\n                </div>\n                <div class=\"item3\">\n                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/05/slider/right-banner3.jpg\"}}\" alt=\"\" />\n                    <div class=\"content\">\n                        <h2  style=\"color:#fff;font-weight:600;line-height:1;\">HUGE <b style=\"font-weight:800;\">SALE</b></h2>\n                        <p style=\"color:#b2b2b2;font-weight:300;line-height:1;\">Now starting at <b style=\"font-weight:600;\">$99</b></p>\n                        <a href=\"#\" style=\"font-weight:300;line-height:1;\">Shop now &gt;</a>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"homepage-bar\" style=\"background-color:#171717;border:0;padding:11px 0;text-align:center;line-height:1;\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-md-4\" style=\"border-color:#353535;\">\n                <em class=\"porto-icon-truck\" style=\"font-size:36px;color:#fff;\"></em><div class=\"text-area\"><h3 style=\"color:#fff;\">FREE SHIPPING & RETURN</h3><p style=\"color:#fff;\">Free shipping on all orders over $99.</p></div>\n            </div>\n            <div class=\"col-md-4\" style=\"border-color:#353535;\">\n                <em class=\"porto-icon-dollar\" style=\"color:#fff;\"></em><div class=\"text-area\"><h3 style=\"color:#fff;\">MONEY BACK GUARANTEE</h3><p style=\"color:#fff;\">100% money back guarantee.</p></div>\n            </div>\n            <div class=\"col-md-4\" style=\"border-color:#353535;\">\n                <em class=\"porto-icon-lifebuoy\" style=\"font-size:32px;color:#fff;\"></em><div class=\"text-area\"><h3 style=\"color:#fff;\">ONLINE SUPPORT 24/7</h3><p style=\"color:#fff;\">Lorem ipsum dolor sit amet.</p></div>\n            </div>\n        </div>\n    </div>\n</div>','2016-10-21 23:35:21','2016-10-21 23:35:21',1),(17,'Porto - Homepage Slider 6','porto_homeslider_6','<div style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/06/slider/banner-img.jpg\"}}) 73% 0 no-repeat; color: #3d3734; background-size: cover;\">\n    <div class=\"banner-content-6\">\n        <div class=\"container\" style=\"text-align: left;\">\n            <div class=\"row\" style=\"padding-left:5%\">\n                <div class=\"col-sm-8\">\n                    <div class=\"text-content\" style=\"display: inline-block\">\n                        <em style=\"font-style: normal; display: block;font-weight:600;\">NEW ARRIVALS</em>\n                        <h2 style=\"line-height: 1;font-weight:800;margin:0;color:#3d3734\">FASHION</h2>\n                        <p><span style=\"vertical-align: top;font-weight:300;\">UP TO <b style=\"font-weight:800;\">70% OFF</b> IN THE NEW COLLECTION</span></p>\n                        <p><a href=\"#\" class=\"btn btn-default\" style=\"vertical-align: top; font-weight: 400;\">SHOP NOW</a></p>\n                    </div>\n                    <div class=\"owl-middle-outer-narrow\" style=\"text-align: left;\">\n                        <p class=\"filter-title-type-2\"><span style=\"width:215px\">SELECTED <b>PRODUCTS</b></span><span class=\"title-line\">&nbsp;</span></p>\n                        <div id=\"featured_products\" class = \"hide-addtocart hide-addtolinks\">\n                            {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"latest_product\" product_count=\"8\" aspect_ratio=\"0\" image_width=\"250\" image_height=\"250\" template=\"owl_list.phtml\"}}\n                        </div>\n                        <script type=\"text/javascript\">\n                            require([\n                                \'jquery\',\n                                \'owl.carousel/owl.carousel.min\'\n                            ], function ($) {\n                                $(\"#featured_products .owl-carousel\").owlCarousel({\n                                    autoplay: true,\n                                    autoplayTimeout: 5000,\n                                    autoplayHoverPause: true,\n                                    margin: 10,\n                                    nav: true,\n                                    navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n                                    dots: false,\n                                    navRewind: true,\n                                    animateIn: \'fadeIn\',\n                                    animateOut: \'fadeOut\',\n                                    loop: true,\n                                    responsive: {\n                                        0: {\n                                            items:1\n                                        },\n                                        320: {\n                                            items:1\n                                        },\n                                        480: {\n                                            items:2\n                                        },\n                                        768: {\n                                            items:2\n                                        },\n                                        992: {\n                                            items:3\n                                        },\n                                        1200: {\n                                            items:3\n                                        }\n                                    }\n                                });\n                            });\n                        </script>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class=\"masonry-grid\" id=\"masonry_grid_18\">\n    <div class=\"grid-sizer\"></div>\n    <div class=\"masonry-grid-item w2\"><a href=\"javascript:void(0)\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/06/slider/grid_item_01.jpg\"}}\" alt=\"\" /></a></div>\n    <div class=\"masonry-grid-item\"><a href=\"javascript:void(0)\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/06/slider/grid_item_02.jpg\"}}\" alt=\"\" /></a></div>\n    <div class=\"masonry-grid-item\"><a href=\"javascript:void(0)\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/06/slider/grid_item_03.jpg\"}}\" alt=\"\" /></a></div>\n    <div class=\"masonry-grid-item w3\"><a href=\"javascript:void(0)\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/06/slider/grid_item_04.jpg\"}}\" alt=\"\" /></a></div>\n    <div class=\"masonry-grid-item w4\"><a href=\"javascript:void(0)\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/06/slider/grid_item_05.jpg\"}}\" alt=\"\" /></a></div>\n    <div class=\"masonry-grid-item\"><a href=\"javascript:void(0)\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/06/slider/grid_item_06.jpg\"}}\" alt=\"\" /></a></div>\n    <div class=\"masonry-grid-item\"><a href=\"javascript:void(0)\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/06/slider/grid_item_07.jpg\"}}\" alt=\"\" /></a></div>\n    <div class=\"masonry-grid-item w2\"><a href=\"javascript:void(0)\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/06/slider/grid_item_08.jpg\"}}\" alt=\"\" /></a></div>\n    <div class=\"masonry-grid-item\"><a href=\"javascript:void(0)\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/06/slider/grid_item_09.jpg\"}}\" alt=\"\" /></a></div>\n    <div class=\"masonry-grid-item\"><a href=\"javascript:void(0)\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/06/slider/grid_item_10.jpg\"}}\" alt=\"\" /></a></div>\n    <div class=\"masonry-grid-item\"><a href=\"javascript:void(0)\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/06/slider/grid_item_11.jpg\"}}\" alt=\"\" /></a></div>\n</div>\n<script type=\"text/javascript\">\n    require([\n        \'jquery\',\n        \'Smartwave_Porto/js/imagesloaded\',\n        \'Smartwave_Porto/js/packery.pkgd\'\n    ], function ($, imagesLoaded, Packery) {\n        $.bridget( \'packery\', Packery );\n        var $container = $(\'#masonry_grid_18\').imagesLoaded(function(){\n            $container.packery({\n                itemSelector: \".masonry-grid-item\",\n                columnWidth: \".grid-sizer\",\n                percentPosition: true\n            });\n        });\n    });\n</script>\n<style type=\"text/css\">\n    .masonry-grid{margin: 3px;position:relative;}\n    .grid-sizer, .masonry-grid-item{width:25%;padding:3px}\n    .masonry-grid-item a{display:block;position:relative;}\n    .masonry-grid-item a:before{width:100%;height:100%;display:block;content:\"\";position:absolute;visibility:hidden;opacity:0;filter:alpha(opacity=0);background-color:#000;transition:0.3s all;}\n    .masonry-grid-item a:hover:before{visibility:visible;opacity:0.2;filter:alpha(opacity=20);}\n    .masonry-grid-item.w2{width:50%;}\n    .masonry-grid-item.w3{width:75%;}\n    .masonry-grid-item.w4{width:100%;}\n    .masonry-grid img{width:100%;}\n    .owl-middle-outer-narrow .owl-controls .owl-buttons div{color:#3d3734}\n    @media(max-width:767px){\n        .masonry-grid-item{width:50%}\n        .masonry-grid-item.w2,.masonry-grid-item.w3{width:100%}\n    }\n    @media(max-width:480px){\n        .masonry-grid-item{width:100%}\n    }\n    @media(min-width:768px){\n        .page-header.type5 {\n            background: transparent;\n            position: absolute;\n            width: 100%;\n            top: 0;\n            left: 0;\n        }\n    }\n</style>','2016-10-21 23:35:21','2016-10-21 23:35:21',1),(18,'Porto - Homepage Slider 7','porto_homeslider_7','<div class=\"container\">\n    <div id=\"banner-slider-demo-7\" class=\"owl-carousel owl-middle-narrow owl-banner-carousel\">\n        <div class=\"item\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/07/slider/slide1.jpg\"}}\" alt=\"\" style=\"width: 100%;\" />\n        </div>\n        <div class=\"item\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/07/slider/slide2.jpg\"}}\" alt=\"\" style=\"width: 100%;\" />\n        </div>\n        <div class=\"item\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/07/slider/slide3.jpg\"}}\" alt=\"\" style=\"width: 100%;\" />\n        </div>\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            $(\"#banner-slider-demo-7\").owlCarousel({\n                items: 1,\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                dots: false,\n                nav: true,\n                navRewind: true,\n                animateIn: \'fadeIn\',\n                animateOut: \'fadeOut\',\n                loop: true,\n                navText: [\"<em class=\'porto-icon-chevron-left\'></em>\",\"<em class=\'porto-icon-chevron-right\'></em>\"]\n            });\n        });\n    </script>\n\n</div>','2016-10-21 23:35:21','2016-10-21 23:35:21',1),(19,'Porto - Homepage Slider 8','porto_homeslider_8','<div class=\"container\">\n    <div id=\"banner-slider-demo-8\" class=\"owl-carousel owl-middle-narrow owl-banner-carousel\">\n        <div class=\"item\" style=\"background:#f0f0f0;background-image:linear-gradient(#e8e8e8,#f0f0f0);position:relative;\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/08/slider/slide1.jpg\"}}\" alt=\"\" />\n            <div class=\"slide1-content\" style=\"width:100%;height:100%;position:absolute;left:0;top:0;\">\n                <div class=\"container\" style=\"height:100%;position:relative;\">\n                    <div class=\"content-area\" style=\"position:absolute;top:37%;width:100%;text-align:center;\">\n                        <h2 style=\"font-weight:800;color:#fff;line-height:1;\">eCOMMERCE?</h2>\n                        <p style=\"font-weight:300;color:#fff;line-height:1;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/03/slider/quote.png\"}}\" alt=\"\" class=\"quote\" style=\"display:inline-block;vertical-align:middle;\"/>&nbsp;&nbsp;&nbsp;&nbsp;Check our options and features.&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/03/slider/quote.png\"}}\" alt=\"\" class=\"quote\" style=\"display:inline-block;vertical-align:middle;\"/></p>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"item\" style=\"background:#f0f0f0;background-image:linear-gradient(#e8e8e8,#f0f0f0);position:relative;\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/08/slider/slide2.jpg\"}}\" alt=\"\" />\n            <div class=\"slide2-content\" style=\"width:100%;height:100%;position:absolute;left:0;top:0;\">\n                <div class=\"container\" style=\"height:100%;position:relative;\">\n                    <div class=\"content-area\" style=\"position:absolute;top:27%;right:23%;text-align:left;\">\n                        <em style=\"display:block;line-height:1;color:#fff;font-weight:300;font-style:normal;\">up to <b>70%</b> off!</em>\n                        <h2 style=\"font-weight:600;color:#fff;line-height:1;\">HUGE <b style=\"font-weight:800;\">SALE</b></h2>\n                        <p style=\"font-weight:600;color:#fff;line-height:1;\">Fashion<span class=\"split\">-</span>Electronics<span class=\"split\">-</span>Gifts<span class=\"split\">-</span>Music<span class=\"split\">-</span>Sports</p>\n                        <div style=\"text-align:right;\"><a href=\"#\" class=\"btn btn-default\" style=\"border:0;background-color:#fefefe;color:#777;font-weight:600;\">SHOP NOW</a></div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"item\" style=\"background:#f0f0f0;background-image:linear-gradient(#e8e8e8,#f0f0f0);position:relative;\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/08/slider/slide3.jpg\"}}\" alt=\"\" />\n            <div class=\"slide3-content\" style=\"width:100%;height:100%;position:absolute;left:0;top:0;\">\n                <div class=\"container\" style=\"height:100%;position:relative;\">\n                    <div class=\"content-area\" style=\"position:absolute;left:17%;top:32%;\">\n                        <div style=\"display:inline-block;\">\n                            <em style=\"display:block;line-height:1;margin:0;color:#fff;font-weight:300;font-style:normal;text-align:left;\">It’s time to feel</em>\n                            <h2 style=\"font-weight:600;color:#fff;line-height:1;text-align:center;\">THE <b style=\"font-weight:800;\">POWER</b></h2>\n                            <div style=\"text-align:right;\"><a href=\"#\" class=\"btn btn-default\" style=\"border:0;background-color:#fefefe;color:#777;font-weight:600;\">SHOP NOW</a></div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            var owl_8 = $(\"#banner-slider-demo-8\").owlCarousel({\n                items: 1,\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                dots: false,\n                navRewind: true,\n                animateIn: \'fadeIn\',\n                animateOut: \'fadeOut\',\n                loop: true,\n                nav: true,\n                navText: [\"<em class=\'porto-icon-chevron-left\'></em>\",\"<em class=\'porto-icon-chevron-right\'></em>\"]\n            });\n        });\n    </script>\n</div>','2016-10-21 23:35:22','2016-10-21 23:35:22',1),(20,'Porto - Homepage Slider 10','porto_homeslider_10','<div class=\"homepage-grid-banner\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-sm-6\">\n                <div class=\"grid1\" style=\"position:relative;\">\n                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/10/slider/grid1.jpg\"}}\" alt=\"\" />\n                    <div class=\"ribbon theme-border-color\" style=\"position:absolute;right:0;top:0;color:#fff;\">\n                        <div style=\"position:absolute;right:9%;top:10%;width:90%;text-align:right;\">\n                            <em style=\"font-weight:300;font-style:normal;color:#fff;margin-right:26%;\">UP TO</em>\n                            <h4 style=\"font-weight:700;color:#fff;\">70%</h4>\n                            <h5 style=\"font-weight:600;color:#fff;\">OFF</h5>\n                        </div>\n                    </div>\n                    <div class=\"content\" style=\"position:absolute;right:15%;bottom:14%;text-align:right;\">\n                        <h2 style=\"font-weight:800;font-style:italic;color:#fff;margin-bottom:0;\">HUGE <b style=\"font-weight:600;\">SALE</b></h2>\n                        <p style=\"font-weight:300;color:#fff;\">Now starting at <b style=\"font-weight:600;\">$99</b></p>\n                        <a href=\"#\">Shop now &gt;</a>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-sm-3\">\n                <div class=\"grid2\" style=\"position:relative;\">\n                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/10/slider/grid2.jpg\"}}\" alt=\"\" />\n                    <div class=\"content\" style=\"position:absolute;right:10%;top:5%;text-align:right;\">\n                        <h3 style=\"font-weight:400;color:#121213;\">amazing</h3>\n                        <h2 style=\"font-weight:800;color:#121213;margin-bottom:10px;\">COLLECTION</h2>\n                        <a style=\"color:#121213;margin:0;\" href=\"#\">Shop now &gt;</a>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-sm-3\">\n                <div class=\"grid3 col-sm-margin\" style=\"position:relative;\">\n                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/10/slider/grid3.jpg\"}}\" alt=\"\" />\n                    <div class=\"content\" style=\"position:absolute;right:11.55%;top:25%;text-align:right;\">\n                        <p style=\"font-weight:300;color:#313131;\">MOBILE</p>\n                        <p style=\"font-weight:700;color:#313131;\">MEGA SALE</p>\n                        <h4 style=\"font-weight:400;color:#888;\"><span>$</span>450.<span>00</span></h4>\n                        <h3 class=\"theme-color\" style=\"font-weight:400\"><span>$</span>350.<span>00</span></h3>\n                        <a href=\"#\">Shop now &gt;</a>\n                    </div>\n                </div>\n                <div class=\"grid4\" style=\"position:relative;\">\n                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/10/slider/grid4.jpg\"}}\" alt=\"\" />\n                    <div class=\"content\" style=\"position:absolute;left:0;top:17%;text-align:center;width:100%;\">\n                        <h2 style=\"font-weight:300;color:#000;\"><b style=\"font-weight:600;font-style:italic;\">Deals</b> +<br>PROMOS</h2>\n                        <p style=\"font-weight:300;color:#000;opacity:0.7;filter:alpha(opacity=70);font-style:italic;\">Limited sales in<br>our categories.</p>\n                        <a class=\"btn btn-default\" href=\"#\">SHOP NOW <em class=\"porto-icon-right-open\"></em></a>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>','2016-10-21 23:35:22','2016-10-21 23:35:22',1),(21,'Porto - Homepage Slider 11','porto_homeslider_11','<div id=\"banner-slider-demo-11\" class=\"owl-carousel owl-banner-carousel owl-middle-narrow\">\n    <div class=\"item\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/11/slider/slide01_bg.png\"}}) repeat;\">\n        <div class=\"container\" style=\"position:relative\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/11/slider/slide01_img.png\"}}\" alt=\"\">\n            <div class=\"slide-1\" style=\"position: absolute; bottom: 27%; width:100%;\">\n                <div class=\"container\" style=\"text-align: left;\">\n            <div class=\"text-content\" style=\"display: inline-block; margin-left: 6%;\">\n            <em style=\"font-style: normal; text-align: left; display: block; color: #2c3232;font-weight:600;\">PROFESSIONAL</em>\n            <h2 style=\"color: #2c3232; line-height: 1;font-weight:800;margin:0;\">COSMETICS</h2>\n                <p><span style=\"color: #2c3232; vertical-align: top; font-weight: 300;\">Up to <b style=\"font-weight:800;\">70% OFF</b> in the new collection.</span></p>\n                <a href=\"#\" class=\"btn btn-default\" style=\"background-color: #b74173; color: #fff; vertical-align: top; font-weight: 600;\">SHOP NOW</a>\n            </div>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class=\"item\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/11/slider/slide02_bg.png\"}}) center center no-repeat;background-size:cover;\">\n        <div class=\"container\" style=\"position:relative\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/11/slider/slide02_img.png\"}}\" alt=\"\">\n            <div class=\"slide-2\" style=\"position: absolute; bottom: 27%; width:100%;\">\n                <div class=\"container\" style=\"text-align: center;\">\n            <div class=\"text-content\" style=\"display: inline-block; margin-left: 6%;\">\n            <em style=\"font-style: normal; display: block; color: #2c3232;font-weight:600;\">CHECK OUT THE NEW</em>\n            <h2 style=\"color: #2c3232; line-height: 1;font-weight:800;margin:0;\">GLOSS FOR LIPS</h2>\n                <p><span style=\"color: #2c3232; vertical-align: top; font-weight: 300;\">Starting at $9.99</span></p>\n                <a href=\"#\" class=\"btn btn-default\" style=\"background-color: #b74173; color: #fff; vertical-align: top; font-weight: 600;\">SHOP NOW</a>\n            </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<script type=\"text/javascript\">\n    require([\n        \'jquery\',\n        \'owl.carousel/owl.carousel.min\'\n    ], function ($) {\n        $(\"#banner-slider-demo-11\").owlCarousel({\n            items: 1,\n            autoplay: true,\n            autoplayTimeout: 5000,\n            autoplayHoverPause: true,\n            dots: false,\n            nav: true,\n            navRewind: true,\n            animateIn: \'fadeIn\',\n            animateOut: \'fadeOut\',\n            loop: true,\n            navText: [\"<em class=\'porto-icon-chevron-left\'></em>\",\"<em class=\'porto-icon-chevron-right\'></em>\"]\n        });\n    });\n</script>\n<div class=\"container\">\n    {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"featured_product\" product_count=\"8\" column_count=\"4\" aspect_ratio=\"0\" image_width=\"250\" image_height=\"250\" template=\"flex_grid.phtml\"}}\n</div>\n<div style=\"background-color:#efefef\">\n    <div class=\"container\">\n        <div id=\"brands-slider-demo-11\" class=\"owl-no-narrow flex-owl-slider\">\n            <div class=\"owl-carousel\">\n                <div class=\"item\" style=\"padding-top:10px;text-align:center;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand1.png\"}}\" alt=\"\" style=\"display: inline-block;\"/></div>\n                <div class=\"item\" style=\"padding-top:10px;text-align:center;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand2.png\"}}\" alt=\"\" style=\"display: inline-block;\"/></div>\n                <div class=\"item\" style=\"padding-top:10px;text-align:center;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand3.png\"}}\" alt=\"\" style=\"display: inline-block;\"/></div>\n                <div class=\"item\" style=\"padding-top:10px;text-align:center;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand4.png\"}}\" alt=\"\" style=\"display: inline-block;\"/></div>\n            </div>\n        </div>\n        <script type=\"text/javascript\">\n            require([\n                \'jquery\',\n                \'owl.carousel/owl.carousel.min\'\n            ], function ($) {\n                $(\"#brands-slider-demo-11 .owl-carousel\").owlCarousel({\n                    autoplay: false,\n                    autoplayTimeout: 5000,\n                    autoplayHoverPause: true,\n                    margin: 0,\n                    nav: false,\n                    dots: false,\n                    loop: false,\n                    responsive: {\n                        0: {\n                            items:2\n                        },\n                        640: {\n                            items:2\n                        },\n                        768: {\n                            items:2\n                        },\n                        992: {\n                            items:3\n                        },\n                        1200: {\n                            items:4\n                        }\n                    }\n                });\n            });\n        </script>\n    </div>\n</div>','2016-10-21 23:35:22','2016-10-21 23:35:22',1),(22,'Porto - Homepage Slider 13','porto_homeslider_13','<style type=\"text/css\">\n    .page-main, .footer-middle { display: none; }\n    .product-item-photo { padding: 0; border: 0; border-radius: 0; }\n    #parallax_02 .product-item-name > a, #parallax_02 .product.name a > a, #parallax_02 .price-box .price, #parallax_02 .product-item .rating-summary .rating-result > span:before { color: #333 !important; }\n    #parallax_03 .product-item-name > a, #parallax_03 .product.name a > a, #parallax_03 .price-box .price, #parallax_03 .product-item .rating-summary .rating-result > span:before { color: #fff !important; }\n</style>\n<div id=\"parallax_01\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/13/slider/parallax_01.jpg\"}}) 50% 0 no-repeat fixed; color: #fff; height: 100vh;width: 100%; padding: 0; position: relative;\">\n    <div class=\"parallax-content-13\" style=\"width: 100%; text-align: center; position: absolute; top: 37%;\">\n        <div style=\"margin:0;line-height:1;\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/13/slider/quote.png\"}}\" alt=\"\" class=\"quote\" style=\"display:inline-block;vertical-align:middle;\"/>\n            <em style=\"display:inline-block;vertical-align:middle;line-height:1;color:#fff;font-weight:300;font-style:normal;\">DO YOU NEED A NEW</em>\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/13/slider/quote.png\"}}\" alt=\"\" class=\"quote\" style=\"display:inline-block;vertical-align:middle;\"/>\n        </div>\n        <h2 style=\"font-weight:800;color:#fff;line-height:1;\">eCOMMERCE?</h2>\n        <p style=\"font-weight:300;color:#fff;line-height:1;\">Check our options and features.</p>\n    </div>\n</div>\n<div id=\"parallax_02\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/13/slider/parallax_02.jpg\"}}) 50% 0 no-repeat fixed; color: #fff; width: 100%;\">\n    <div class=\"parallax-content-13\">\n        <div class=\"container\" style=\"text-align: right;\">\n            <div class=\"text-content\" style=\"display: inline-block;\">\n                <em style=\"font-style: normal; text-align: right; display: block; color: #333;font-weight:600;\">NEW ARRIVALS</em>\n                <h2 style=\"color: #333; line-height: 1;font-weight:800;margin:0;\">Fashion Dresses</h2>\n                <p><span style=\"color: #333; vertical-align: top;\">Up to <b style=\"font-weight:800;\">70% OFF</b> in the new collection.</span>&nbsp;<a href=\"#\" class=\"btn btn-default\" style=\"background-color: #333; vertical-align: top; font-weight: 600; color: #fff;\">SHOP NOW</a></p>\n            </div>\n            <div style=\"text-align: left;\">\n                <p style=\"color: #333; font-size: 15px;margin:0;font-weight:600;margin-bottom:15px;\">FEATURED PRODUCTS</p>\n                <div id=\"featured_products_1\" class=\"hide-addtocart hide-addtolinks\">\n                    {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"featured_product\" product_count=\"10\" aspect_ratio=\"1\" image_width=\"272\" image_height=\"337\" template=\"owl_list.phtml\"}}\n                </div>\n                <script type=\"text/javascript\">\n                    require([\n                        \'jquery\',\n                        \'owl.carousel/owl.carousel.min\'\n                    ], function ($) {\n                        $(\"#featured_products_1 .owl-carousel\").owlCarousel({\n                            autoplay: true,\n                            autoplayTimeout: 5000,\n                            autoplayHoverPause: true,\n                            loop: true,\n                            navRewind: true,\n                            margin: 10,\n                            nav: false,\n                            dots: true,\n                            responsive: {\n                                0: {\n                                    items:2\n                                },\n                                768: {\n                                    items:3\n                                },\n                                992: {\n                                    items:4\n                                },\n                                1200: {\n                                    items:5\n                                }\n                            }\n                        });\n                    });\n                </script>\n            </div>\n        </div>\n    </div>\n</div>\n<div id=\"parallax_03\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/13/slider/parallax_03.jpg\"}}) 50% 0 no-repeat fixed; color: #fff; width: 100%;\">\n    <div class=\"parallax-content-13\">\n        <div class=\"container\" style=\"text-align: left;\">\n            <div class=\"text-content\" style=\"display: inline-block;\">\n                <em style=\"font-style: normal; text-align: left; display: block; color: #fff;font-weight:600;\">NEW ARRIVALS</em>\n                <h2 style=\"color: #fff; line-height: 1;font-weight:800;margin:0;\">Fashion Sunglasses</h2>\n                <p><span style=\"color: #fff; vertical-align: top;\">Up to <b style=\"font-weight:800;\">70% OFF</b> in the new collection.</span>&nbsp;<a href=\"#\" class=\"btn btn-default\" style=\"background-color: #fff; color: #333; vertical-align: top; font-weight: 600;\">SHOP NOW</a></p>\n            </div>\n            <div style=\"text-align: left;\">\n                <p style=\"color: #fff; font-size: 15px;margin:0;font-weight:600;text-align: right; margin-bottom: 15px;\">FEATURED PRODUCTS</p>\n                <div id=\"featured_products_2\" class=\"hide-addtocart hide-addtolinks\">\n                    {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"featured_product\" product_count=\"10\" aspect_ratio=\"1\" image_width=\"272\" image_height=\"337\" template=\"owl_list.phtml\"}}\n                </div>\n                <script type=\"text/javascript\">\n                    require([\n                        \'jquery\',\n                        \'owl.carousel/owl.carousel.min\'\n                    ], function ($) {\n                        $(\"#featured_products_2 .owl-carousel\").owlCarousel({\n                            autoplay: true,\n                            autoplayTimeout: 5000,\n                            autoplayHoverPause: true,\n                            loop: true,\n                            navRewind: true,\n                            margin: 10,\n                            nav: false,\n                            dots: true,\n                            responsive: {\n                                0: {\n                                    items:2\n                                },\n                                768: {\n                                    items:3\n                                },\n                                992: {\n                                    items:4\n                                },\n                                1200: {\n                                    items:5\n                                }\n                            }\n                        });\n                    });\n                </script>\n            </div>\n        </div>\n    </div>\n</div>\n<div id=\"parallax_04\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/13/slider/parallax_04.jpg\"}}) 50% 0 no-repeat fixed; color: #fff; height: 100vh;margin: 0; padding: 0;\">\n    <div style=\"width: 100%; height: 100%; position: relative;\">\n        <div class=\"parallax-content-13\" style=\"width: 100%; text-align: center; position: absolute; top: 37%;\">\n            <div style=\"margin:0;line-height:1;\">\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/13/slider/quote_black.png\"}}\" alt=\"\" class=\"quote\" style=\"display:inline-block;vertical-align:middle;\"/>\n                <em style=\"display:inline-block;vertical-align:middle;line-height:1;color:#333;font-weight:300;font-style:normal;\">LET THE MUSIC PLAY</em>\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/13/slider/quote_black.png\"}}\" alt=\"\" class=\"quote\" style=\"display:inline-block;vertical-align:middle;\"/>\n            </div>\n            <h2 style=\"font-weight:800;color:#333;line-height:1;\">Connect with the best tech!</h2>\n            <p style=\"font-weight:300;color:#333;line-height:1;\">Up to 70% OFF in the new collection.</p>\n            <a href=\"#\" class=\"btn btn-default\" style=\"background-color: #333; color: #fff; border-radius: 0; border: 0;\">SHOP NOW</a>\n        </div>\n    </div>\n</div>','2016-10-21 23:35:22','2016-10-21 23:35:22',1),(23,'Porto - Homepage Slider 14','porto_homeslider_14','<div class=\"full-screen-slider\">\n    <div id=\"banner-slider-demo-14\" class=\"owl-carousel owl-middle-narrow owl-banner-carousel\">\n        <div class=\"item\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/14/slider/slide1.jpg\"}}) 71% top no-repeat; background-size:cover; position: relative;\">\n            <div class=\"slide-1\" style=\"position: absolute; top: 35%; width:100%;\">\n                <div class=\"container\" style=\"text-align: left;\">\n            <div class=\"text-content\" style=\"display: inline-block;\">\n            <em style=\"font-style: normal; text-align: left; display: block; color: #fff; font-weight: 600; line-height: 1;\">NEW ARRIVALS</em>\n            <h2 style=\"color: #fff; line-height: 1;font-weight:800;margin:0;\">Fashion</h2>\n                <p><span style=\"color: #fff; vertical-align: top; font-weight: 300;\">Up to <b style=\"font-weight:800;\">70% OFF</b> in the new collection.</span></p>\n                <a href=\"#\" class=\"btn btn-default\" style=\"background-color: #fff; color: #333; vertical-align: top; font-weight: 600;\">SHOP NOW</a>\n            </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"item\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/14/slider/slide2.jpg\"}}) 71% top no-repeat; background-size:cover; position: relative;\">\n            <div class=\"slide-2\" style=\"position: absolute; top: 35%; width:100%;\">\n                <div class=\"container\" style=\"text-align: left;\">\n            <div class=\"text-content\" style=\"display: inline-block;\">\n            <em style=\"font-style: normal; text-align: left; display: block; color: #fff; font-weight: 600; line-height: 1;\">EXCLUSIVE</em>\n            <h2 style=\"color: #fff; line-height: 1;font-weight:800;margin:0;\">Sunglasses</h2>\n                <p style=\"text-align:right;\"><span style=\"color: #fff; vertical-align: top; font-weight: 300;\">Incredible prices</span></p>\n                <a href=\"#\" style=\"float:right; color: #fff; vertical-align: top; font-weight: 400;\">SHOP NOW</a>\n            </div>\n                </div>\n            </div>\n        </div>\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            var owl_3 = $(\"#banner-slider-demo-14\").owlCarousel({\n                items: 1,\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                dots: false,\n                navRewind: true,\n                animateIn: \'fadeIn\',\n                animateOut: \'fadeOut\',\n                loop: true,\n                nav: true,\n                navText: [\"<em class=\'porto-icon-chevron-left\'></em>\",\"<em class=\'porto-icon-chevron-right\'></em>\"]\n            });\n        });\n    </script>\n</div>\n<div class=\"container\">\n    <div class=\"single-images\" style=\"padding-top: 30px;\">\n        <div class=\"row\">\n            <div class=\"col-sm-4\" style=\"padding-bottom:15px;\">\n                <a class=\"image-link\" href=\"#\">\n                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/14/slider/image_link_1.jpg\"}}\" alt=\"\" />\n                    <span class=\"category-title\">SUNGLASSES</span>\n                </a>\n            </div>\n            <div class=\"col-sm-4\" style=\"padding-bottom:15px;\">\n                <a class=\"image-link\" href=\"#\">\n                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/14/slider/image_link_2.jpg\"}}\" alt=\"\" />\n                    <span class=\"category-title\">WOMAN DRESSES</span>\n                </a>\n            </div>\n            <div class=\"col-sm-4\" style=\"padding-bottom:15px;\">\n                <a class=\"image-link\" href=\"#\">\n                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/14/slider/image_link_3.jpg\"}}\" alt=\"\" />\n                    <span class=\"category-title\">WOMAN BAGS</span>\n                </a>\n            </div>\n         </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"col-sm-12\">\n            <h2 style=\"margin-top:30px;font-size:19px;font-weight:600;margin-bottom:20px;text-align:center;\" class=\"theme-color\">WEEKLY FEATURED PRODUCTS</h2>\n            <div id=\"featured_product\" class=\"hide-addtocart hide-addtolinks\">\n                {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"featured_product\" product_count=\"10\" aspect_ratio=\"0\" image_width=\"212\" image_height=\"263\" template=\"owl_list.phtml\"}}\n            </div>\n            <script type=\"text/javascript\">\n                require([\n                    \'jquery\',\n                    \'owl.carousel/owl.carousel.min\'\n                ], function ($) {\n                    $(\"#featured_product .owl-carousel\").owlCarousel({\n                        autoplay: true,\n                        autoplayTimeout: 5000,\n                        autoplayHoverPause: true,\n                        loop: true,\n                        navRewind: true,\n                        margin: 10,\n                        nav: false,\n                        navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n                        dots: false,\n                        responsive: {\n                            0: {\n                                items:2\n                            },\n                            768: {\n                                items:3\n                            },\n                            992: {\n                                items:4\n                            },\n                            1200: {\n                                items:5\n                            }\n                        }\n                    });\n                });\n            </script>\n        </div>\n    </div>\n</div>\n<div id=\"parallax_01\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/14/slider/parallax_img.jpg\"}}) 80% 0 no-repeat fixed; color: #fff; padding: 100px 0; position: relative;\">\n        <div class=\"overlay overlay-color\" style=\"background-color: #000000; opacity: 0.65; filter: alpha(opacity=65);top:0;\"></div>\n    <div class=\"parallax-slider\" style=\"position: relative; z-index: 3;\">\n            <div class=\"container\">\n                <div id=\"parallax-slider-demo-14\" class=\"owl-carousel owl-theme\">\n                    <div class=\"item\">\n                        <h2 style=\"font-weight:600;\">SPECIAL <b style=\"font-weight:800;\">PROMO</b></h2>\n                        <p style=\"font-weight:300;\">Up to <b style=\"font-weight:800;\">70% OFF</b> in the new collection.</p>\n                        <a href=\"#\" style=\"font-weight:300;\">Purchase now &gt;</a>\n                    </div>\n                    <div class=\"item\">\n                        <h2 style=\"font-weight:600;\">SPECIAL <b style=\"font-weight:800;\">PROMO</b></h2>\n                        <p style=\"font-weight:300;\">Up to <b style=\"font-weight:800;\">70% OFF</b> in the new collection.</p>\n                        <a href=\"#\" style=\"font-weight:300;\">Purchase now &gt;</a>\n                    </div>\n                    <div class=\"item\">\n                        <h2 style=\"font-weight:600;\">SPECIAL <b style=\"font-weight:800;\">PROMO</b></h2>\n                        <p style=\"font-weight:300;\">Up to <b style=\"font-weight:800;\">70% OFF</b> in the new collection.</p>\n                        <a href=\"#\" style=\"font-weight:300;\">Purchase now &gt;</a>\n                    </div>\n                </div>\n            </div>\n            <script type=\"text/javascript\">\n                require([\n                    \'jquery\',\n                    \'owl.carousel/owl.carousel.min\'\n                ], function ($) {\n                    var owl_4 = $(\"#parallax-slider-demo-14\").owlCarousel({\n                        items: 1,\n                        autoplay: true,\n                        autoplayTimeout: 5000,\n                        autoplayHoverPause: true,\n                        dots: true,\n                        navRewind: true,\n                        animateIn: \'fadeIn\',\n                        animateOut: \'fadeOut\',\n                        loop: true,\n                        nav: false,\n                        navText: [\"<em class=\'porto-icon-chevron-left\'></em>\",\"<em class=\'porto-icon-chevron-right\'></em>\"]\n                    });\n                });\n            </script>\n        </div>\n</div>','2016-10-21 23:35:22','2016-10-21 23:35:22',1),(24,'Porto - Homepage Slider 15','porto_homeslider_15','<div class=\"container\" style=\"padding:20px 15px 0;font-family:Georgia;\">\n    <div id=\"banner-slider-demo-15\" class=\"owl-carousel owl-middle-narrow owl-banner-carousel\">\n        <div class=\"item\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/15/slider/slide1.jpg\"}}\" alt=\"\" />\n            <div class=\"content\" style=\"position:absolute; right:9.5%;top:40%;text-align:right;\">\n                <em style=\"font-weight:400;color:#666666;float:left;font-style:normal;\">Porto Presents...</em>\n                <div class=\"clearfix\"></div>\n                <h2 style=\"font-weight:400;color:#141414;margin:0;line-height:1;\">The Bride\'s Dream</h2>\n                <hr style=\"width: 50%;border-color:#151515;float:right;\">\n                <div class=\"clearfix\"></div>\n                <a href=\"#\" style=\"color:#2b262f;font-weight:700;\"><span>SHOP NOW</span></a>\n            </div>\n        </div>\n        <div class=\"item\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/15/slider/slide2.jpg\"}}\" alt=\"\" />\n            <div class=\"content\" style=\"position:absolute; right:9.5%;top:40%;text-align:right;\">\n                <em style=\"font-weight:400;color:#666666;float:left;font-style:normal;\">Porto Presents...</em>\n                <div class=\"clearfix\"></div>\n                <h2 style=\"font-weight:400;color:#141414;margin:0;line-height:1;\">The Bride\'s Dream</h2>\n                <hr style=\"width: 50%;border-color:#151515;float:right;\">\n                <div class=\"clearfix\"></div>\n                <a href=\"#\" style=\"color:#2b262f;font-weight:700;\"><span>SHOP NOW</span></a>\n            </div>\n        </div>\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            $(\"#banner-slider-demo-15\").owlCarousel({\n                items: 1,\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                dots: false,\n                nav: true,\n                navRewind: true,\n                animateIn: \'fadeIn\',\n                animateOut: \'fadeOut\',\n                loop: true,\n                navText: [\"<em class=\'porto-icon-chevron-left\'></em>\",\"<em class=\'porto-icon-chevron-right\'></em>\"]\n            });\n        });\n    </script>\n</div>','2016-10-21 23:35:22','2016-10-21 23:35:22',1),(25,'Porto - Homepage Slider 16','porto_homeslider_16','<div class=\"full-screen-slider\">\n    <div id=\"banner-slider-demo-16\" class=\"owl-carousel owl-bottom-narrow owl-banner-carousel\">\n        <div class=\"item\" style=\"background:#f0f0f0;background-image:linear-gradient(#e8e8e8,#f0f0f0);position:relative;\">\n            <div style=\"width:100%;height:100%;width:100vw;height:100vh;background:url({{media url=\"wysiwyg/smartwave/porto/homepage/16/slider/01.jpg\"}}) 80% center no-repeat; background-size:cover;\"></div>\n            <div class=\"slide1-content\" style=\"width:100%;position:absolute;left:0;top:40%;text-align:center;\">\n                <div class=\"text-content\" style=\"display: inline-block;\">\n                    <em style=\"font-style: normal; text-align: right; display: block; color: #fff;font-weight:600;\">NEW ARRIVALS</em>\n                    <h2 style=\"color: #fff; line-height: 1;font-weight:800;margin:0;\">Fashion Sunglasses</h2>\n                    <p><span style=\"color: #fff; vertical-align: top;\">Up to <b style=\"font-weight:800;\">70% OFF</b> in the new collection.</span>&nbsp;<a href=\"#\" class=\"btn btn-default\" style=\"background-color: #fff; color: #333; vertical-align: top; font-weight: 600;float:right;\">SHOP NOW</a></p>\n                </div>\n            </div>\n        </div>\n        <div class=\"item\" style=\"background:#f0f0f0;background-image:linear-gradient(#e8e8e8,#f0f0f0);position:relative;\">\n            <div style=\"width:100%;height:100%;width:100vw;height:100vh;background:url({{media url=\"wysiwyg/smartwave/porto/homepage/16/slider/02.jpg\"}}) 20% center no-repeat; background-size:cover;\"></div>\n            <div class=\"slide2-content\" style=\"width:100%;position:absolute;left:0;top:40%;text-align:center;\">\n                <div class=\"text-content\" style=\"display: inline-block;\">\n                    <em style=\"font-style: normal; text-align: right; display: block; color: #fff;font-weight:600;\">NEW COLLECTION</em>\n                    <h2 style=\"color: #fff; line-height: 1;font-weight:800;margin:0;\">Women Dresses</h2>\n                    <p><span style=\"color: #fff; vertical-align: top;\">Up to <b style=\"font-weight:800;\">70% OFF</b> in the new collection.</span>&nbsp;<a href=\"#\" class=\"btn btn-default\" style=\"background-color: #fff; color: #333; vertical-align: top; font-weight: 600;float:right;\">SHOP NOW</a></p>\n                </div>\n            </div>\n        </div>\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            var owl_16 = $(\"#banner-slider-demo-16\").owlCarousel({\n                items: 1,\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                dots: true,\n                navRewind: true,\n                animateIn: \'fadeIn\',\n                animateOut: \'fadeOut\',\n                loop: true,\n                nav: false,\n                navText: [\"<em class=\'porto-icon-chevron-left\'></em>\",\"<em class=\'porto-icon-chevron-right\'></em>\"]\n            });\n        });\n    </script>\n</div>','2016-10-21 23:35:22','2016-10-21 23:35:22',1),(26,'Porto - Homepage Slider 17','porto_homeslider_17','<div id=\"banner-slider-demo-17\" class=\"owl-carousel owl-banner-carousel owl-middle-narrow\">\n    <div class=\"item\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/17/slider/layer1.jpg\"}}) no-repeat; background-size: cover;\">\n        <div class=\"container\" style=\"position:relative\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/17/slider/layer2.png\"}}\" alt=\"\">\n            <div class=\"content-17 type1\" style=\"position:absolute;top:30%;left:9%;text-align:right\">\n                <em style=\"font-weight:300;font-style:normal;color:#a39a8c;float:left;\">Feel the real</em>\n                <div class=\"clearfix\"></div>\n                <h2 style=\"font-weight:400;font-style:italic;line-height:1;color:#79a939;margin:0;\"><b style=\"font-weight:800\">MOBILE</b> music</h2>\n                <p style=\"color:#010101;font-weight:700;line-height:1;margin-bottom:15px\">$999</p>\n                <a href=\"#\" style=\"font-weight:600;color:#1e1e1e;\"><span>SHOP NOW</span><i class=\"porto-icon-angle-right theme-bg-color\"></i></a>\n            </div>\n        </div>\n    </div>\n    <div class=\"item\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/17/slider/layer3.jpg\"}}) no-repeat; background-size: cover;\">\n        <div class=\"container\" style=\"position:relative\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/17/slider/layer4.png\"}}\" alt=\"\">\n        </div>\n    </div>\n</div>\n<script type=\"text/javascript\">\n    require([\n        \'jquery\',\n        \'owl.carousel/owl.carousel.min\'\n    ], function ($) {\n        $(\"#banner-slider-demo-17\").owlCarousel({\n            items: 1,\n            autoplay: true,\n            autoplayTimeout: 5000,\n            autoplayHoverPause: true,\n            dots: false,\n            nav: true,\n            navRewind: true,\n            animateIn: \'fadeIn\',\n            animateOut: \'fadeOut\',\n            loop: true,\n            navText: [\"<em class=\'porto-icon-chevron-left\'></em>\",\"<em class=\'porto-icon-chevron-right\'></em>\"]\n        });\n    });\n</script>\n<div class=\"homepage-bar\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-md-4\">\n                <em class=\"porto-icon-truck\" style=\"font-size:36px;\"></em><div class=\"text-area\"><h3>FREE SHIPPING & RETURN</h3><p>Free shipping on all orders over $99.</p></div>\n            </div>\n            <div class=\"col-md-4\">\n                <em class=\"porto-icon-dollar\"></em><div class=\"text-area\"><h3>MONEY BACK GUARANTEE</h3><p>100% money back guarantee.</p></div>\n            </div>\n            <div class=\"col-md-4\">\n                <em class=\"porto-icon-lifebuoy\" style=\"font-size:32px;\"></em><div class=\"text-area\"><h3>ONLINE SUPPORT 24/7</h3><p>Lorem ipsum dolor sit amet.</p></div>\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"container\">\n    <div class=\"filterproducts-tab\" style=\"margin-top: 40px;\">\n        <div class=\"data items\" data-mage-init=\'{\"tabs\":{\"openedState\":\"active\"}}\'>\n            <div class=\"data item title\" aria-labeledby=\"tab-label-featured-title\" data-role=\"collapsible\" id=\"tab-label-featured\">\n                <a class=\"data switch\" tabindex=\"-1\" data-toggle=\"switch\" href=\"#featured\" id=\"tab-label-featured-title\">Featured</a>\n            </div>\n            <div class=\"data item content\" id=\"featured\" data-role=\"content\">\n                <div id=\"featured_product\" class=\"owl-top-narrow hide-addtolinks hide-addtocart\">\n                    {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"featured_product\" product_count=\"10\" aspect_ratio=\"0\" image_width=\"212\" image_height=\"263\" template=\"owl_list.phtml\"}}\n                </div>\n                <script type=\"text/javascript\">\n                    require([\n                        \'jquery\',\n                        \'owl.carousel/owl.carousel.min\'\n                    ], function ($) {\n                        $(\"#featured_product .owl-carousel\").owlCarousel({\n                            autoplay: true,\n                            autoplayTimeout: 5000,\n                            autoplayHoverPause: true,\n                            loop: true,\n                            navRewind: true,\n                            margin: 10,\n                            nav: true,\n                            navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n                            dots: false,\n                            responsive: {\n                                0: {\n                                    items:2\n                                },\n                                768: {\n                                    items:3\n                                },\n                                992: {\n                                    items:3\n                                },\n                                1200: {\n                                    items:4\n                                }\n                            }\n                        });\n                    });\n                </script>\n            </div>\n            <div class=\"data item title\" aria-labeledby=\"tab-label-latest-title\" data-role=\"collapsible\" id=\"tab-label-latest\">\n                <a class=\"data switch\" tabindex=\"-1\" data-toggle=\"switch\" href=\"#latest\" id=\"tab-label-latest-title\">Latest</a>\n            </div>\n            <div class=\"data item content\" id=\"latest\" data-role=\"content\">\n                <div id=\"latest_product\" class=\"owl-top-narrow hide-addtolinks hide-addtocart\">\n                    {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"featured_product\" product_count=\"10\" aspect_ratio=\"0\" image_width=\"212\" image_height=\"263\" template=\"owl_list.phtml\"}}\n                </div>\n                <script type=\"text/javascript\">\n                    require([\n                        \'jquery\',\n                        \'owl.carousel/owl.carousel.min\'\n                    ], function ($) {\n                        $(\"#latest_product .owl-carousel\").owlCarousel({\n                            autoplay: true,\n                            autoplayTimeout: 5000,\n                            autoplayHoverPause: true,\n                            loop: true,\n                            navRewind: true,\n                            margin: 10,\n                            nav: true,\n                            navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n                            dots: false,\n                            responsive: {\n                                0: {\n                                    items:2\n                                },\n                                768: {\n                                    items:3\n                                },\n                                992: {\n                                    items:3\n                                },\n                                1200: {\n                                    items:4\n                                }\n                            }\n                        });\n                    });\n                </script>\n            </div>\n        </div>\n    </div>\n</div>\n<div style=\"padding-top: 40px;\" class=\"custom-support\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-sm-4\">\n                <em class=\"porto-icon-star\" style=\"border-radius:50%;border:2px solid #cecece;color:#333;background-color:transparent;line-height:58px;font-size:25px;\"></em>\n                <div class=\"content\">\n                    <h2>Customer Support</h2>\n                    <em>YOU WON\'T BE ALONE</em>\n                    <p>We really care about you and your website as much as you do. Purchasing Porto or any other theme from us you get 100% free support.</p>\n                </div>\n            </div>\n            <div class=\"col-sm-4\">\n                <em class=\"porto-icon-reply\" style=\"border-radius:50%;border:2px solid #cecece;color:#333;background-color:transparent;line-height:58px;font-size:25px;\"></em>\n                <div class=\"content\">\n                    <h2>Fully Customizable</h2>\n                    <em>TONS OF OPTIONS</em>\n                    <p>With Porto you can customize the layout, colors and styles within only a few minutes. Start creating an amazing website right now!</p>\n                </div>\n            </div>\n            <div class=\"col-sm-4\">\n                <em class=\"porto-icon-paper-plane\" style=\"border-radius:50%;border:2px solid #cecece;color:#333;background-color:transparent;line-height:58px;font-size:25px;\"></em>\n                <div class=\"content\">\n                    <h2>Powerful Admin</h2>\n                    <em>MADE TO HELP YOU</em>\n                    <p>Porto has very powerful admin features to help customer to build their own shop in minutes without any special skills in web development.</p>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"parallax-wrapper\" style=\"margin: 50px 0 20px;\">\n    <div class=\"overlay overlay-color\" style=\"background-color: #000000; opacity: 0.75; filter: alpha(opacity=75);\"></div>\n    <div class=\"parallax\" style=\"background-image: url({{media url=\"wysiwyg/smartwave/porto/homepage/17/slider/parallax_bg.jpg\"}});\">\n        <div class=\"parallax-slider\" style=\"position: relative; z-index: 3;\">\n            <div class=\"container\">\n                <div id=\"parallax-slider-demo-17\" class=\"owl-carousel\">\n                    <div class=\"item\">\n                        <h2 style=\"font-weight:600;\">EXPLORE <b style=\"font-weight:800;\">PORTO</b></h2>\n                        <p style=\"font-weight:300;\">Premium theme, unlimited possibilities...</p>\n                        <a href=\"#\" style=\"font-weight:300;\">Purchase now &gt;</a>\n                    </div>\n                    <div class=\"item\">\n                        <h2 style=\"font-weight:600;\">EXPLORE <b style=\"font-weight:800;\">PORTO</b></h2>\n                        <p style=\"font-weight:300;\">Premium theme, unlimited possibilities...</p>\n                        <a href=\"#\" style=\"font-weight:300;\">Purchase now &gt;</a>\n                    </div>\n                    <div class=\"item\">\n                        <h2 style=\"font-weight:600;\">EXPLORE <b style=\"font-weight:800;\">PORTO</b></h2>\n                        <p style=\"font-weight:300;\">Premium theme, unlimited possibilities...</p>\n                        <a href=\"#\" style=\"font-weight:300;\">Purchase now &gt;</a>\n                    </div>\n                </div>\n            </div>\n            <script type=\"text/javascript\">\n                require([\n                    \'jquery\',\n                    \'owl.carousel/owl.carousel.min\'\n                ], function ($) {\n                    $(\"#parallax-slider-demo-17\").owlCarousel({\n                        items: 1,\n                        autoplay: true,\n                        autoplayTimeout: 5000,\n                        autoplayHoverPause: true,\n                        dots: true,\n                        nav: false,\n                        loop: true,\n                        navRewind: true\n                    });\n                });\n            </script>\n        </div>\n    </div>\n</div>','2016-10-21 23:35:22','2016-10-21 23:35:22',1),(27,'Porto - Homepage Slider 18','porto_homeslider_18','<div class=\"container\">\n    <div id=\"banner-slider-demo-18\" class=\"owl-carousel owl-middle-narrow owl-banner-carousel\">\n        <div class=\"item\" style=\"position: relative;\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/18/slider/01.jpg\"}}\" alt=\"\"/>\n            <div class=\"content-1\" style=\"background-color: #b81d21; color: #fff;\">\n                <div>\n                    <p style=\"color:#fff;\">Furniture &amp; Garden</p>\n                    <h3 style=\"color:#fff;\">HUGE<br><b>SALE</b></h3>\n                </div>\n                <div style=\"text-align:right;\">\n                    <em style=\"color:#fff;\">up to</em>\n                    <h4 style=\"color:#fff;\">40%</h4>\n                    <h5 style=\"color:#fff;\">OFF</h5>\n                </div>\n            </div>\n        </div>\n        <div class=\"item\" style=\"position: relative;\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/18/slider/02.jpg\"}}\" alt=\"\"/>\n            <div class=\"content-2\" style=\"background-color: #333333; color: #fff;\">\n                <div>\n                    <p style=\"color:#fff;\">Furniture &amp; Garden</p>\n                    <h3 style=\"color:#fff;\">SPECIAL <b>PROMO</b></h3>\n                </div>\n                <div style=\"text-align:right;border: 1px solid #ccc;border-width: 0 1px;\">\n                    <em style=\"color:#fff;\">up to</em>\n                    <h4 style=\"color:#fff;\">40%</h4>\n                    <h5 style=\"color:#fff;\">OFF</h5>\n                </div>\n                <div>\n                    <a style=\"color:#fff;\" href=\"#\">SHOP NOW <i class=\"porto-icon-right-dir\"></i></a>\n                </div>\n            </div>\n        </div>\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            $(\"#banner-slider-demo-18\").owlCarousel({\n                items: 1,\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                dots: false,\n                nav: true,\n                navRewind: true,\n                animateIn: \'fadeIn\',\n                animateOut: \'fadeOut\',\n                loop: true,\n                navText: [\"<em class=\'porto-icon-chevron-left\'></em>\",\"<em class=\'porto-icon-chevron-right\'></em>\"]\n            });\n        });\n    </script>\n\n</div>','2016-10-21 23:35:22','2016-10-21 23:35:22',1),(28,'Porto - Custom Block for Footer Bottom Area(for Home 3)','porto_footer_bottom_custom_block_1_3','<ul class=\"social-icons\">\n    <li><a class=\"facebook-link\" href=\"#\"><em class=\"porto-icon-facebook\"></em></a></li>\n    <li><a class=\"twitter-link\" href=\"#\"><em class=\"porto-icon-twitter\"></em></a></li>\n    <li><a class=\"linkedin-link\" href=\"#\"><em class=\"porto-icon-linkedin-squared\"></em></a></li>\n</ul>\n<ul class=\"links\">\n<li class=\"first\">(+404) 158 14 25 78</li>\n<li class=\"last\"><a href=\"http://newsmartwave.net/magento/porto/index.php/blog\">BLOG</a></li>\n</ul>','2016-10-21 23:35:22','2016-10-21 23:35:22',1),(29,'Porto - Custom Block(Right) for Footer Bottom Area(for Home 3)','porto_footer_bottom_custom_block_2_3','<img src=\"{{media url=\"wysiwyg/smartwave/porto/footer/payments.png\"}}\" alt=\"\" />','2016-10-21 23:35:22','2016-10-21 23:35:22',1),(30,'Porto - Footer Top Custom Block','porto_footer_top_custom_block','<div class=\"homepage-bar\" style=\"background-color: #08c;color: #fff;padding: 10px 0;border: 0;\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-md-4\">\n                <i class=\"porto-icon-truck\" style=\"font-size:36px;color: #fff;\"></i><div class=\"text-area\"><h3 style=\"color: #fff;\">FREE SHIPPING & RETURN</h3><p>Free shipping on all orders over $99.</p></div>\n            </div>\n            <div class=\"col-md-4\" style=\"border-color: #149ce0;\">\n                <i class=\"porto-icon-dollar\" style=\"color: #fff;\"></i><div class=\"text-area\"><h3 style=\"color: #fff;\">MONEY BACK GUARANTEE</h3><p>100% money back guarantee.</p></div>\n            </div>\n            <div class=\"col-md-4\" style=\"border-color: #149ce0;\">\n                <i class=\"porto-icon-lifebuoy\" style=\"font-size:32px;color: #fff;\"></i><div class=\"text-area\"><h3 style=\"color: #fff;\">ONLINE SUPPORT 24/7</h3><p>Lorem ipsum dolor sit amet.</p></div>\n            </div>\n        </div>\n    </div>\n</div>','2016-10-21 23:35:22','2016-10-21 23:35:22',1),(31,'Porto - Footer Top Custom Block for Home4','porto_footer_top_custom_block_home4','<div class=\"homepage-bar\" style=\"background-color: transparent;color: #fff;padding: 10px 0;border: 0;\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-md-4\">\n                <em class=\"porto-icon-truck\" style=\"font-size:36px;color: #fff;\"></em><div class=\"text-area\"><h3 style=\"color: #fff;\">FREE SHIPPING & RETURN</h3><p>Free shipping on all orders over $99.</p></div>\n            </div>\n            <div class=\"col-md-4\" style=\"border-color: #149ce0;\">\n                <em class=\"porto-icon-dollar\" style=\"color: #fff;\"></em><div class=\"text-area\"><h3 style=\"color: #fff;\">MONEY BACK GUARANTEE</h3><p>100% money back guarantee.</p></div>\n            </div>\n            <div class=\"col-md-4\" style=\"border-color: #149ce0;\">\n                <em class=\"porto-icon-lifebuoy\" style=\"font-size:32px;color: #fff;\"></em><div class=\"text-area\"><h3 style=\"color: #fff;\">ONLINE SUPPORT 24/7</h3><p>Lorem ipsum dolor sit amet.</p></div>\n            </div>\n        </div>\n    </div>\n</div>\n<style type=\"text/css\">\n.footer-top > .container {\n    width: 100%;\n}\n.footer-top > .container > .row > .col-md-12 {\n    padding: 0;\n}\n</style>','2016-10-21 23:35:23','2016-10-21 23:35:23',1),(32,'Porto - Footer - About Porto','porto_footer_about_porto','<div class=\"block\">\n    <div class=\"block-title\"><strong><span>ABOUT PORTO</span></strong></div>\n    <div class=\"block-content\">\n        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tristique augue eget enim feugiat blandit. Phasellus bibendum ex leo, at egestas lacus pellentesque in x leo, at egestas lacus pellentesque in. x leo, at egestas lacus pellentesque.</p>\n        <a href=\"http://themeforest.net/item/porto-ultimate-responsive-magento-theme/9725864\" style=\"font-weight:600;\">Buy Porto eCommerce</a>\n    </div>\n    <ul class=\"contact-info\" style=\"margin-top: 30px;\">\n        <li><em class=\"porto-icon-phone\" style=\"color: #bbb;\"></em><p><b>Need Help?</b><br>(123) 456-7890</p></li>\n    </ul>\n    <div style=\"margin: 20px 0 0 20px;\">\n        <ul class=\"social-icons\">\n            <li><a class=\"facebook-link\" href=\"#\"><em class=\"porto-icon-facebook\"></em></a></li>\n            <li><a class=\"twitter-link\" href=\"#\"><em class=\"porto-icon-twitter\"></em></a></li>\n            <li><a class=\"linkedin-link\" href=\"#\"><em class=\"porto-icon-linkedin-squared\"></em></a></li>\n        </ul>\n    </div>\n</div>','2016-10-21 23:35:23','2016-10-21 23:35:23',1),(33,'Porto - Footer - Featured Products','porto_footer_featured_products','<div class=\"block\">\n    <div class=\"block-title\"><strong><span>FEATURED PRODUCTS</span></strong></div>\n    <div class=\"block-content\">\n        {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"featured_small_list\" product_count=\"3\" aspect_ratio=\"0\" image_width=\"212\" image_height=\"212\" template=\"small_list.phtml\"}}\n    </div>\n</div>','2016-10-21 23:35:23','2016-10-21 23:35:23',1),(34,'Porto - Footer Middle Twitter Widget','porto_footer_middle_twitter_widget','<div class=\"block\">\n    <div class=\"block-title\"><strong><span>FOLLOW US</span></strong></div>\n    <div class=\"block-content\">\n        <a class=\"twitter-timeline\" href=\"//twitter.com/twitterdev\" data-widget-id=\"362597660089274368\" data-screen-name=\"eternalfriend38\" data-theme=\"dark\" height=\"285\">Tweets by @eternalfriend38</a>\n        <script>window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src=\"//platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,\"script\",\"twitter-wjs\"));</script>\n    </div>\n</div>','2016-10-21 23:35:23','2016-10-21 23:35:23',1),(35,'Porto - Footer - 2nd Row - Shop','porto_footer_2nd_row_shop','<div class=\"block\">\n    <div class=\"block-title\"><strong><span>SHOP</span></strong></div>\n    <div class=\"block-content\">\n        <ul class=\"links\">\n            <li><em class=\"porto-icon-right-open theme-color\"></em><a href=\"#\">Fashion Promo</a></li>\n            <li><em class=\"porto-icon-right-open theme-color\"></em><a href=\"#\">Discounts</a></li>\n            <li><em class=\"porto-icon-right-open theme-color\"></em><a href=\"#\">Outlet</a></li>\n        </ul>\n    </div>\n</div>','2016-10-21 23:35:23','2016-10-21 23:35:23',1),(36,'Porto - Footer - 2nd Row - Popular Tags','porto_footer_2nd_row_popular_tags','<div class=\"block\">\n    <div class=\"block-title\"><strong><span>CUSTOM LINKS</span></strong></div>\n    <div class=\"block-content\">\n    <ul class=\"links\">\n            <li><em class=\"porto-icon-right-open theme-color\"></em><a href=\"#\">Advanced Search</a></li>\n            <li><em class=\"porto-icon-right-open theme-color\"></em><a href=\"#\">Promotions</a></li>\n            <li><em class=\"porto-icon-right-open theme-color\"></em><a href=\"#\">Terms and Conditions</a></li>\n        </ul>\n    </div>\n</div>','2016-10-21 23:35:23','2016-10-21 23:35:23',1),(37,'Porto - Footer - 2nd Row - My Account','porto_footer_2nd_row_myaccount','<div class=\"block\">\n    <div class=\"block-title\"><strong><span>MY ACCOUNT</span></strong></div>\n    <div class=\"block-content\">\n        <ul class=\"links\">\n            <li><em class=\"porto-icon-right-open theme-color\"></em><a href=\"{{store url=\"\"}}customer/account\" title=\"My Account\">My Account</a></li>\n            <li><em class=\"porto-icon-right-open theme-color\"></em><a href=\"{{store url=\"\"}}about-porto\" title=\"About us\">About Us</a></li>\n            <li><em class=\"porto-icon-right-open theme-color\"></em><a href=\"{{store url=\"\"}}contacts\" title=\"Contact us\">Store Locations</a></li>\n        </ul>\n    </div>\n</div>','2016-10-21 23:35:23','2016-10-21 23:35:23',1),(38,'Porto - Footer - 2nd Row - Contact Information','porto_footer_2nd_row_contact_information','<div class=\"block\">\n    <div class=\"block-title\"><strong><span>CONTACT INFORMATION</span></strong></div>\n    <div class=\"block-content\">\n        <ul class=\"contact-info\" style=\"padding-left: 5px;\">\n            <li><em class=\"porto-icon-location theme-color\">&nbsp;</em>&nbsp;&nbsp;<p>123 Street Name, City, England</p></li>\n            <li><em class=\"porto-icon-phone theme-color\">&nbsp;</em>&nbsp;&nbsp;<p>(123)456-7890</p></li>\n            <li><em class=\"porto-icon-mail theme-color\">&nbsp;</em>&nbsp;&nbsp;<p><a href=\"mailto:mail@example.com\">mail@example.com</a></p></li>\n        </ul>\n    </div>\n</div>','2016-10-21 23:35:23','2016-10-21 23:35:23',1),(39,'Porto - Custom Block for Footer Bottom Area for Home 8','porto_footer_bottom_custom_block_home8','<img src=\"{{media url=\"wysiwyg/smartwave/porto/footer/payments.png\"}}\" alt=\"\" style=\"margin-left: 15px;\" />','2016-10-21 23:35:23','2016-10-21 23:35:23',1),(40,'Porto - Footer Top Custom Block for Home 11','porto_footer_top_custom_block_home11','<div class=\"homepage-bar\">\n    <div class=\"row\">\n        <div class=\"col-md-3 col-sm-6\">\n            <em class=\"porto-icon-truck\" style=\"color: #fff;\"></em>\n            <div class=\"text-area\"><h3 style=\"color: #fff;\">FREE SHIPPING</h3><p>Free shipping on all orders over $99.</p></div>\n        </div>\n        <div class=\"col-md-3 col-sm-6\">\n            <em class=\"porto-icon-dollar\" style=\"color: #fff;\"></em>\n            <div class=\"text-area\"><h3 style=\"color: #fff;\">MONEY BACK GUARANTEE</h3><p>100% money back guarantee</p></div>\n        </div>\n        <div class=\"col-md-3 col-sm-6 sm-bd-0\">\n            <em class=\"porto-icon-lifebuoy\" style=\"color: #fff;\"></em>\n            <div class=\"text-area\"><h3 style=\"color: #fff;\">ONLINE SUPPORT 24/7</h3><p>Lorem ipsum dolor sit amet.</p></div>\n        </div>\n        <div class=\"col-md-3 col-sm-6\">\n            <em class=\"porto-icon-chat\" style=\"color: #fff;\"></em>\n            <div class=\"text-area\"><h3 style=\"color: #fff;\">LIVE CHAT</h3><p>Lorem ipsum dolor sit amet.</p></div>\n        </div>\n    </div>\n</div>','2016-10-21 23:35:23','2016-10-21 23:35:23',1),(41,'Porto - Custom Header Block for Type 8','porto_custom_block_type_8','<a href=\"#\">FASHION PROMO</a>\n<a href=\"#\">WOMAN SHOES</a>\n<a href=\"#\">50% OFF FASHION</a>','2016-10-21 23:35:23','2016-10-21 23:35:23',1),(42,'Porto - Footer Middle Logo Block for Home12','porto_footer_middle_logo_block_for_12','<div class=\"block\">\n    <div class=\"block-title\"><strong><span>Who We Are</span></strong></div>\n    <div class=\"block-content\">\n        <img src=\"{{media url=\"wysiwyg/smartwave/porto/footer/footer_logo_red.png\"}}\" alt=\"\" />\n        <p style=\"margin: 20px 0 0;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br/><br/>\n        <span style=\"font-weight: 600;\"><a href=\"http://themeforest.net/item/porto-ecommerce-ultimate-magento-theme/9725864?ref=SW-THEMES&amp;license=regular&amp;open_purchase_for_item_id=9725864&amp;purchasable=source\">Buy Porto eCommerce</a></span></p>\n    </div>\n</div>','2016-10-21 23:35:23','2016-10-21 23:35:23',1),(43,'Porto - Footer Middle Logo Block for Home15','porto_footer_middle_logo_block_for_15','<div class=\"block\">\n    <div class=\"block-title\"><strong><span>Who We Are</span></strong></div>\n    <div class=\"block-content\">\n        <img src=\"{{media url=\"wysiwyg/smartwave/porto/footer/footer_logo_black.png\"}}\" alt=\"\" />\n        <p style=\"margin: 20px 0 0;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br/><br/>\n        <span style=\"font-weight: 600;\"><a href=\"http://themeforest.net/item/porto-ecommerce-ultimate-magento-theme/9725864?ref=SW-THEMES&amp;license=regular&amp;open_purchase_for_item_id=9725864&amp;purchasable=source\">Buy Porto eCommerce</a></span></p>\n    </div>\n</div>','2016-10-21 23:35:23','2016-10-21 23:35:23',1),(44,'Porto - Custom Notice Block','porto_custom_notice','<div class=\"top-newsletter\" style=\"background-color: #2b262f; padding: 10px 0;text-align:left\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-sm-6 a-left-sm\">\n                <span style=\"color: #fff; font-size: 16px; font-family: Georgia; line-height: 30px;\"><b>SALE:</b> use coupon code <b>PORTO</b> to save 30%</span>\n            </div>\n            <div class=\"col-sm-6 a-right-sm\">\n                <div style=\"display: inline-block; vertical-align: middle; padding-right: 20px;\">\n                    <p style=\"margin: 0; color: #fff; font-family: Georgia; font-size: 15px; font-weight: 700; line-height: 15px;\">Be the First to Know</p>\n                    <p style=\"margin: 0; font-size: 12px; color: #a7a7a7; line-height: 15px;\">Sign up for newsletter today.</p>\n                </div>\n                <div style=\"display: inline-block; vertical-align: middle; position: relative;\">\n                    {{block class=\"Magento\\Newsletter\\Block\\Subscribe\" name=\"subscribe_form\" template=\"subscribe_form.phtml\"}}\n                </div>\n            </div>\n        </div>\n    </div>\n</div>','2016-10-21 23:35:23','2016-10-21 23:35:23',1),(45,'Porto - Custom Block for Header Type 10','porto_custom_block_for_header_type_10','<div style=\"font-size:15px;color:#fff;line-height:1.4;margin-bottom:15px;\">\n<em class=\"porto-icon-phone\" style=\"margin-right: 5px;\"></em><span>(+404) 158 14 25 78</span>\n</div>\n<ul class=\"social-icons\">\n    <li><a class=\"facebook-link\" href=\"#\"><em class=\"porto-icon-facebook\"></em></a></li>\n    <li><a class=\"twitter-link\" href=\"#\"><em class=\"porto-icon-twitter\"></em></a></li>\n    <li><a class=\"linkedin-link\" href=\"#\"><em class=\"porto-icon-linkedin-squared\"></em></a></li>\n</ul>\n<address>©Copyright 2016 by SW-THEMES.</address>','2016-10-21 23:35:23','2016-10-21 23:35:23',1),(46,'Porto - Homepage Slider 19','porto_homeslider_19','<div class=\"full-screen-slider\">\n    <div id=\"banner-slider-demo-19\" class=\"owl-carousel owl-bottom-narrow owl-banner-carousel\">\n        <div class=\"item\">\n            <div style=\"width:100%;height:100%;background:url({{media url=\"wysiwyg/smartwave/porto/homepage/19/slider/slide1.jpg\"}}) 80% center no-repeat; background-size:cover;\"></div>\n            <div class=\"slide-content\" style=\"position: absolute; z-index: 5; left: 10.5%; top: 35%; text-align: left;\">\n                <h2 style=\"color: #000;\">WINTER SALE<br/>70% OFF</h2>\n                <p style=\"color: #000;\">Shop new styles added to sale online.</p>\n                <a class=\"btn-line\" href=\"#\">SHOP NOW</a>\n            </div>\n        </div>\n        <div class=\"item\">\n            <div style=\"width:100%;height:100%;background:url({{media url=\"wysiwyg/smartwave/porto/homepage/19/slider/slide2.jpg\"}}) 80% center no-repeat; background-size:cover;\"></div>\n            <div class=\"slide-content\" style=\"position: absolute; z-index: 5; left: 10.5%; top: 35%; text-align: left;\">\n                <h2 style=\"color: #000;\">NEW SUMMER<br/>COLLECTION</h2>\n                <p style=\"color: #000;\">New styles added to summer collection.</p>\n                <a class=\"btn-line\" href=\"#\">SHOP NOW</a>\n            </div>\n        </div>\n        <div class=\"item\">\n            <div style=\"width:100%;height:100%;background:url({{media url=\"wysiwyg/smartwave/porto/homepage/19/slider/slide3.jpg\"}}) 80% center no-repeat; background-size:cover;\"></div>\n            <div class=\"slide-content\" style=\"position: absolute; z-index: 5; left: 10.5%; top: 35%; text-align: left;\">\n                <h2 style=\"color: #000;\">CONVERSE<br/>30% OFF</h2>\n                <p style=\"color: #000;\">Outlet converse 30% off for you.</p>\n                <a class=\"btn-line\" href=\"#\">SHOP NOW</a>\n            </div>\n        </div>\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            var owl_3 = $(\"#banner-slider-demo-19\").owlCarousel({\n                items: 1,\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                dots: true,\n                navRewind: true,\n                animateIn: \'fadeIn\',\n                animateOut: \'fadeOut\',\n                loop: true,\n                nav: false,\n                navText: [\"<em class=\'porto-icon-chevron-left\'></em>\",\"<em class=\'porto-icon-chevron-right\'></em>\"]\n            });\n        });\n    </script>\n</div>\n<style type=\"text/css\">\n#banner-slider-demo-19 .owl-controls {\n    text-align: left;\n    padding-left: 10%;\n    bottom: 8%;\n}\n#banner-slider-demo-19 .owl-dots .owl-dot span {\n    width: 12px;\n    height: 12px;\n    border-width: 2px;\n}\n</style>','2016-10-21 23:35:23','2016-10-21 23:35:23',1),(47,'Porto - Custom Block for Header Type12','porto_custom_block_for_header_type12','<a href=\"https://www.facebook.com/\" title=\"Facebook\" target=\"_blank\"><em class=\"porto-icon-facebook\"></em></a>\n<a href=\"https://twitter.com/\" title=\"Twitter\" target=\"_blank\"><em class=\"porto-icon-twitter\"></em></a>\n<a href=\"https://www.instagram.com/\" title=\"Instagram\" target=\"_blank\"><em class=\"porto-icon-instagram\"></em></a>','2016-10-21 23:35:23','2016-10-21 23:35:23',1),(48,'Porto - Footer Middle - 1st Column for Demo19','porto_footer_middle_1_for_19','<img src=\"{{media url=\"wysiwyg/smartwave/porto/footer/logo_white_plus.png\"}}\" alt=\"\" />','2016-10-21 23:35:24','2016-10-21 23:35:24',1),(49,'Porto - Footer Middle - 2nd Column for Demo19','porto_footer_middle_2_for_19','<div class=\"block\">\n    <div class=\"block-title\"><strong>SHOP</strong></div>\n    <div class=\"block-content\">\n        <ul class=\"links\">\n            <li><a href=\"#\" title=\"Fashion & Clothes\">Fashion & Clothes</a></li>\n            <li><a href=\"#\" title=\"Electronics & Computers\">Electronics & Computers</a></li>\n            <li><a href=\"#\" title=\"Toys & Hobbies\">Toys & Hobbies</a></li>\n            <li><a href=\"#\" title=\"Home & Garden\">Home & Garden</a></li>\n            <li><a href=\"#\" title=\"Decor & Furniture\">Decor & Furniture</a></li>\n            <li><a href=\"#\" title=\"Sports & Fitness\">Sports & Fitness</a></li>\n            <li><a href=\"#\" title=\"Gifts\">Gifts</a></li>\n        </ul>\n    </div>\n</div>','2016-10-21 23:35:24','2016-10-21 23:35:24',1),(50,'Porto - Footer Middle - 3rd Column for Demo19','porto_footer_middle_3_for_19','<div class=\"block\">\n    <div class=\"block-title\"><strong>MENU</strong></div>\n    <div class=\"block-content\">\n        <ul class=\"links\">\n            <li><a href=\"#\" title=\"My Account\">My Account</a></li>\n            <li><a href=\"#\" title=\"Daily Deal\">Daily Deal</a></li>\n            <li><a href=\"#\" title=\"My Wishlist\">My Wishlist</a></li>\n            <li><a href=\"#\" title=\"Blog\">Blog</a></li>\n            <li><a href=\"#\" title=\"Login\">Login</a></li>\n            <li><a href=\"#\" title=\"About Us\">About Us</a></li>\n            <li><a href=\"#\" title=\"Contact\">Contact</a></li>\n        </ul>\n    </div>\n</div>','2016-10-21 23:35:24','2016-10-21 23:35:24',1),(51,'Porto - Footer Middle - 4th Column for Demo19','porto_footer_middle_4_for_19','<div class=\"block\" style=\"margin-bottom:50px;\">\n    <div class=\"block-title\"><strong>FOLLOW US</strong></div>\n    <div class=\"block-content\">\n        <a href=\"https://www.facebook.com/\" title=\"Facebook\" target=\"_blank\" style=\"font-size: 22px;margin: 0 10px;margin-left: -10px;\"><i class=\"porto-icon-facebook\"></i></a>\n        <a href=\"https://twitter.com/\" title=\"Twitter\" target=\"_blank\" style=\"font-size: 22px;margin: 0 10px;\"><i class=\"porto-icon-twitter\"></i></a>\n        <a href=\"https://www.instagram.com/\" title=\"Instagram\" target=\"_blank\" style=\"font-size: 22px;margin: 0 10px;\"><i class=\"porto-icon-instagram\"></i></a>\n    </div>\n</div>\n<div class=\"block\">\n    <div class=\"block-title\"><strong>NEWSLETTER</strong></div>\n    <div class=\"block-content\">\n        {{block class=\"Magento\\Newsletter\\Block\\Subscribe\" name=\"subscribe_form\" template=\"subscribe_form.phtml\"}}\n    </div>\n</div>','2016-10-21 23:35:24','2016-10-21 23:35:24',1),(52,'Porto - Homepage Slider 20','porto_homeslider_20','<div class=\"full-screen-slider\">\n    <div id=\"banner-slider-demo-20\" class=\"owl-carousel owl-banner-carousel\" data-slider-id=\"1\">\n        <div class=\"item\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/20/slider/01.jpg\"}}) center center / cover no-repeat; position: relative;\">\n            <div class=\"content\" style=\"left:20%;top:30%;width:auto;\">\n                <h2 style=\"color: #fff;\">Running<br>shoes</h2>\n                <a href=\"#\">SEE MORE</a>\n            </div>\n        </div>\n        <div class=\"item\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/20/slider/02.jpg\"}}) center center / cover no-repeat; position: relative;\">\n            <div class=\"content\" style=\"left:20%;top:30%;width:auto;\">\n                <h2 style=\"color: #fff;\">Running smart<br>watch</h2>\n                <a href=\"#\">SEE MORE</a>\n            </div>\n        </div>\n        <div class=\"item\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/20/slider/03.jpg\"}}) center center / cover no-repeat; position: relative;\">\n            <div class=\"content\" style=\"left:20%;top:30%;width:auto;\">\n                <h2 style=\"color: #fff;\">Football<br>accessories</h2>\n                <a href=\"#\">SEE MORE</a>\n            </div>\n        </div>\n    </div>\n    <div class=\"owl-thumbs\" data-slider-id=\"1\">\n        <button class=\"owl-thumb-item\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/20/slider/01_thumb.jpg\"}}\"/></button>\n        <button class=\"owl-thumb-item\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/20/slider/02_thumb.jpg\"}}\"/></button>\n        <button class=\"owl-thumb-item\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/20/slider/03_thumb.jpg\"}}\"/></button>\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel\'\n        ], function ($) {\n            var owl_20 = $(\"#banner-slider-demo-20\").owlCarousel({\n                items: 1,\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                dots: false,\n                navRewind: true,\n                animateIn: \'fadeIn\',\n                animateOut: \'fadeOut\',\n                thumbs: true,\n                thumbImage: false,\n                thumbsPrerendered: true,\n                thumbContainerClass: \'owl-thumbs\',\n                thumbItemClass: \'owl-thumb-item\',\n                loop: true,\n                nav: false,\n                navText: [\"<em class=\'porto-icon-chevron-left\'></em>\",\"<em class=\'porto-icon-chevron-right\'></em>\"]\n            });\n        });\n    </script>\n</div>','2016-10-21 23:35:24','2016-10-21 23:35:24',1),(53,'Porto - Footer Middle - 1st Column for Demo20','porto_footer_middle_1_for_20','<div class=\"row\">\n    <div class=\"col-md-6\">\n        <div class=\"block\">\n            <div class=\"block-content\">\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/footer/logo_white_plus.png\"}}\" alt=\"\" />\n            </div>\n        </div>\n    </div>\n    <div class=\"col-md-6\">\n        <div class=\"block\">\n            <div class=\"block-title\"><strong>Features</strong></div>\n            <div class=\"block-content\">\n                <ul class=\"links\">\n                    <li><a href=\"#\" title=\"Men\">MEN</a></li>\n                    <li><a href=\"#\" title=\"Women\">WOMEN</a></li>\n                    <li><a href=\"#\" title=\"Boys\">BOYS</a></li>\n                    <li><a href=\"#\" title=\"Girls\">GIRLS</a></li>\n                    <li><a href=\"#\" title=\"New Arrivals\">NEW ARRIVALS</a></li>\n                    <li><a href=\"#\" title=\"Shoes\">SHOES</a></li>\n                    <li><a href=\"#\" title=\"Clothes\">CLOTHES</a></li>\n                    <li><a href=\"#\" title=\"Accessories\">ACCESSORIES</a></li>\n                </ul>\n            </div>\n        </div>\n    </div>\n</div>','2016-10-21 23:35:24','2016-10-21 23:35:24',1),(54,'Porto - Footer Middle - 2nd Column for Demo20','porto_footer_middle_2_for_20','<div class=\"block\">\n    <div class=\"block-title\"><strong>Menu</strong></div>\n    <div class=\"block-content\">\n        <ul class=\"links\">\n            <li><a href=\"#\" title=\"About Us\">ABOUT US</a></li>\n            <li><a href=\"#\" title=\"Contact Us\">CONTACT US</a></li>\n            <li><a href=\"#\" title=\"My Account\">MY ACCOUNT</a></li>\n            <li><a href=\"#\" title=\"Blog\">ORDERS HISTORY</a></li>\n            <li><a href=\"#\" title=\"My Wishlist\">MY WISHLIST</a></li>\n            <li><a href=\"#\" title=\"Blog\">BLOG</a></li>\n            <li><a href=\"#\" title=\"Login\">LOGIN</a></li>\n        </ul>\n    </div>\n</div>','2016-10-21 23:35:24','2016-10-21 23:35:24',1),(55,'Porto - Footer Middle - 3rd Column for Demo20','porto_footer_middle_3_for_20','<div class=\"block\">\n    <div class=\"block-title\"><strong>Contact us</strong></div>\n    <div class=\"block-content\">\n        <ul class=\"contact-info\">\n            <li><p><b style=\"color:#fff;\">ADDRESS:</b><br/>123 STREET NAME, CITY, ENGLAND</p></li>\n            <li><p><b style=\"color:#fff;\">PHONE:</b><br/>(123) 456-7890</p></li>\n            <li><p><b style=\"color:#fff;\">EMAIL:</b><br/><a href=\"mailto:mail@example.com\">MAIL@EXAMPLE.COM</a></p></li>\n            <li><p><b style=\"color:#fff;\">WORKING DAYS/HOURS:</b><br/>MON - SUN / 9:00AM - 8:00PM<br/><span style=\"color: #0f0;\">OPEN NOW</span></p></li>\n        </ul>\n    </div>\n</div>','2016-10-21 23:35:24','2016-10-21 23:35:24',1),(56,'Porto - Footer Middle - 4th Column for Demo20','porto_footer_middle_4_for_20','<div class=\"row\">\n    <div class=\"col-md-6\">\n        <div class=\"block\" style=\"margin-bottom:50px;\">\n            <div class=\"block-title\"><strong>Follow us</strong></div>\n            <div class=\"block-content\">\n                <p><a href=\"https://www.facebook.com/\" title=\"Facebook\" target=\"_blank\" style=\"font-size: 22px;\"><i class=\"porto-icon-facebook\" style=\"color:#fff;vertical-align:middle;\"></i><span style=\"font-size: 13px;vertical-align:middle;margin-left:5px;\">FACEBOOK</span></a></p>\n                <p><a href=\"https://twitter.com/\" title=\"Twitter\" target=\"_blank\" style=\"font-size: 22px;\"><i class=\"porto-icon-twitter\" style=\"color:#fff;vertical-align:middle;\"></i><span style=\"font-size: 13px;vertical-align:middle;margin-left:5px;\">TWITTER</span></a></p>\n                <p><a href=\"https://www.instagram.com/\" title=\"Instagram\" target=\"_blank\" style=\"font-size: 22px;\"><i class=\"porto-icon-instagram\" style=\"color:#fff;vertical-align:middle;\"></i><span style=\"font-size: 13px;vertical-align:middle;margin-left:5px;\">INSTAGRAM</span></a></p>\n            </div>\n        </div>\n    </div>\n    <div class=\"col-md-6\">\n        <div class=\"block\">\n            <div class=\"block-title\"><strong>Join us</strong></div>\n            <div class=\"block-content\">\n                <p>JOIN THE WORLD<br/>OF PORTO ECOMMERCE</p>\n                {{block class=\"Magento\\Newsletter\\Block\\Subscribe\" name=\"subscribe_form\" template=\"subscribe_form.phtml\"}}\n            </div>\n        </div>\n    </div>\n</div>','2016-10-21 23:35:24','2016-10-21 23:35:24',1);

/*Table structure for table `cms_block_store` */

DROP TABLE IF EXISTS `cms_block_store`;

CREATE TABLE `cms_block_store` (
  `block_id` smallint(6) NOT NULL COMMENT 'Block ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  PRIMARY KEY (`block_id`,`store_id`),
  KEY `CMS_BLOCK_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `CMS_BLOCK_STORE_BLOCK_ID_CMS_BLOCK_BLOCK_ID` FOREIGN KEY (`block_id`) REFERENCES `cms_block` (`block_id`) ON DELETE CASCADE,
  CONSTRAINT `CMS_BLOCK_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS Block To Store Linkage Table';

/*Data for the table `cms_block_store` */

insert  into `cms_block_store`(`block_id`,`store_id`) values (1,0),(2,0),(3,0),(4,0),(5,0),(6,0),(7,0),(8,0),(9,0),(10,0),(11,0),(12,0),(13,0),(14,0),(15,0),(16,0),(17,0),(18,0),(19,0),(20,0),(21,0),(22,0),(23,0),(24,0),(25,0),(26,0),(27,0),(28,0),(29,0),(30,0),(31,0),(32,0),(33,0),(34,0),(35,0),(36,0),(37,0),(38,0),(39,0),(40,0),(41,0),(42,0),(43,0),(44,0),(45,0),(46,0),(47,0),(48,0),(49,0),(50,0),(51,0),(52,0),(53,0),(54,0),(55,0),(56,0);

/*Table structure for table `cms_page` */

DROP TABLE IF EXISTS `cms_page`;

CREATE TABLE `cms_page` (
  `page_id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Page ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Page Title',
  `page_layout` varchar(255) DEFAULT NULL COMMENT 'Page Layout',
  `meta_keywords` text COMMENT 'Page Meta Keywords',
  `meta_description` text COMMENT 'Page Meta Description',
  `identifier` varchar(100) DEFAULT NULL COMMENT 'Page String Identifier',
  `content_heading` varchar(255) DEFAULT NULL COMMENT 'Page Content Heading',
  `content` mediumtext COMMENT 'Page Content',
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Page Creation Time',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Page Modification Time',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Page Active',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Page Sort Order',
  `layout_update_xml` text COMMENT 'Page Layout Update Content',
  `custom_theme` varchar(100) DEFAULT NULL COMMENT 'Page Custom Theme',
  `custom_root_template` varchar(255) DEFAULT NULL COMMENT 'Page Custom Template',
  `custom_layout_update_xml` text COMMENT 'Page Custom Layout Update Content',
  `custom_theme_from` date DEFAULT NULL COMMENT 'Page Custom Theme Active From Date',
  `custom_theme_to` date DEFAULT NULL COMMENT 'Page Custom Theme Active To Date',
  `meta_title` varchar(255) DEFAULT NULL COMMENT 'Page Meta Title',
  PRIMARY KEY (`page_id`),
  KEY `CMS_PAGE_IDENTIFIER` (`identifier`),
  FULLTEXT KEY `CMS_PAGE_TITLE_META_KEYWORDS_META_DESCRIPTION_IDENTIFIER_CONTENT` (`title`,`meta_keywords`,`meta_description`,`identifier`,`content`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='CMS Page Table';

/*Data for the table `cms_page` */

insert  into `cms_page`(`page_id`,`title`,`page_layout`,`meta_keywords`,`meta_description`,`identifier`,`content_heading`,`content`,`creation_time`,`update_time`,`is_active`,`sort_order`,`layout_update_xml`,`custom_theme`,`custom_root_template`,`custom_layout_update_xml`,`custom_theme_from`,`custom_theme_to`,`meta_title`) values (1,'404 Not Found','2columns-right','Page keywords','Page description','no-route','Whoops, our bad...','<dl>\r\n<dt>The page you requested was not found, and we have a fine guess why.</dt>\r\n<dd>\r\n<ul class=\"disc\">\r\n<li>If you typed the URL directly, please make sure the spelling is correct.</li>\r\n<li>If you clicked on a link to get here, the link is outdated.</li>\r\n</ul></dd>\r\n</dl>\r\n<dl>\r\n<dt>What can you do?</dt>\r\n<dd>Have no fear, help is near! There are many ways you can get back on track with Magento Store.</dd>\r\n<dd>\r\n<ul class=\"disc\">\r\n<li><a href=\"#\" onclick=\"history.go(-1); return false;\">Go back</a> to the previous page.</li>\r\n<li>Use the search bar at the top of the page to search for your products.</li>\r\n<li>Follow these links to get you back on track!<br /><a href=\"{{store url=\"\"}}\">Store Home</a> <span class=\"separator\">|</span> <a href=\"{{store url=\"customer/account\"}}\">My Account</a></li></ul></dd></dl>\r\n','2016-08-26 10:46:08','2016-08-26 10:46:08',1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'Home page','1column',NULL,NULL,'home','Home Page','<p>CMS homepage content goes here.</p>\r\n','2016-08-26 10:46:09','2016-08-26 10:47:05',1,0,'<!--\n    <referenceContainer name=\"right\">\n        <action method=\"unsetChild\"><argument name=\"alias\" xsi:type=\"string\">right.reports.product.viewed</argument></action>\n        <action method=\"unsetChild\"><argument name=\"alias\" xsi:type=\"string\">right.reports.product.compared</argument></action>\n    </referenceContainer>-->',NULL,NULL,NULL,NULL,NULL,NULL),(3,'Enable Cookies','1column',NULL,NULL,'enable-cookies','What are Cookies?','<div class=\"enable-cookies cms-content\">\r\n<p>\"Cookies\" are little pieces of data we send when you visit our store. Cookies help us get to know you better and personalize your experience. Plus they help protect you and other shoppers from fraud.</p>\r\n<p style=\"margin-bottom: 20px;\">Set your browser to accept cookies so you can buy items, save items, and receive customized recommendations. Here’s how:</p>\r\n<ul>\r\n<li><a href=\"https://support.google.com/accounts/answer/61416?hl=en\" target=\"_blank\">Google Chrome</a></li>\r\n<li><a href=\"http://windows.microsoft.com/en-us/internet-explorer/delete-manage-cookies\" target=\"_blank\">Internet Explorer</a></li>\r\n<li><a href=\"http://support.apple.com/kb/PH19214\" target=\"_blank\">Safari</a></li>\r\n<li><a href=\"https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences\" target=\"_blank\">Mozilla/Firefox</a></li>\r\n</ul>\r\n</div>','2016-08-26 10:46:09','2016-08-26 10:46:09',1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'Privacy and Cookie Policy','1column',NULL,NULL,'privacy-policy-cookie-restriction-mode','Privacy and Cookie Policy','<div class=\"privacy-policy cms-content\">\n    <div class=\"message info\">\n        <span>\n            Please replace this text with you Privacy Policy.\n            Please add any additional cookies your website uses below (e.g. Google Analytics).\n        </span>\n    </div>\n    <p>\n        This privacy policy sets out how this website (hereafter \"the Store\") uses and protects any information that\n        you give the Store while using this website. The Store is committed to ensuring that your privacy is protected.\n        Should we ask you to provide certain information by which you can be identified when using this website, then\n        you can be assured that it will only be used in accordance with this privacy statement. The Store may change\n        this policy from time to time by updating this page. You should check this page from time to time to ensure\n        that you are happy with any changes.\n    </p>\n    <h2>What we collect</h2>\n    <p>We may collect the following information:</p>\n    <ul>\n        <li>name</li>\n        <li>contact information including email address</li>\n        <li>demographic information such as postcode, preferences and interests</li>\n        <li>other information relevant to customer surveys and/or offers</li>\n    </ul>\n    <p>\n        For the exhaustive list of cookies we collect see the <a href=\"#list\">List of cookies we collect</a> section.\n    </p>\n    <h2>What we do with the information we gather</h2>\n    <p>\n        We require this information to understand your needs and provide you with a better service,\n        and in particular for the following reasons:\n    </p>\n    <ul>\n        <li>Internal record keeping.</li>\n        <li>We may use the information to improve our products and services.</li>\n        <li>\n            We may periodically send promotional emails about new products, special offers or other information which we\n            think you may find interesting using the email address which you have provided.\n        </li>\n        <li>\n            From time to time, we may also use your information to contact you for market research purposes.\n            We may contact you by email, phone, fax or mail. We may use the information to customise the website\n            according to your interests.\n        </li>\n    </ul>\n    <h2>Security</h2>\n    <p>\n        We are committed to ensuring that your information is secure. In order to prevent unauthorised access or\n        disclosure, we have put in place suitable physical, electronic and managerial procedures to safeguard and\n        secure the information we collect online.\n    </p>\n    <h2>How we use cookies</h2>\n    <p>\n        A cookie is a small file which asks permission to be placed on your computer\'s hard drive.\n        Once you agree, the file is added and the cookie helps analyse web traffic or lets you know when you visit\n        a particular site. Cookies allow web applications to respond to you as an individual. The web application\n        can tailor its operations to your needs, likes and dislikes by gathering and remembering information about\n        your preferences.\n    </p>\n    <p>\n        We use traffic log cookies to identify which pages are being used. This helps us analyse data about web page\n        traffic and improve our website in order to tailor it to customer needs. We only use this information for\n        statistical analysis purposes and then the data is removed from the system.\n    </p>\n    <p>\n        Overall, cookies help us provide you with a better website, by enabling us to monitor which pages you find\n        useful and which you do not. A cookie in no way gives us access to your computer or any information about you,\n        other than the data you choose to share with us. You can choose to accept or decline cookies.\n        Most web browsers automatically accept cookies, but you can usually modify your browser setting\n        to decline cookies if you prefer. This may prevent you from taking full advantage of the website.\n    </p>\n    <h2>Links to other websites</h2>\n    <p>\n        Our website may contain links to other websites of interest. However, once you have used these links\n        to leave our site, you should note that we do not have any control over that other website.\n        Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst\n        visiting such sites and such sites are not governed by this privacy statement.\n        You should exercise caution and look at the privacy statement applicable to the website in question.\n    </p>\n    <h2>Controlling your personal information</h2>\n    <p>You may choose to restrict the collection or use of your personal information in the following ways:</p>\n    <ul>\n        <li>\n            whenever you are asked to fill in a form on the website, look for the box that you can click to indicate\n            that you do not want the information to be used by anybody for direct marketing purposes\n        </li>\n        <li>\n            if you have previously agreed to us using your personal information for direct marketing purposes,\n            you may change your mind at any time by letting us know using our Contact Us information\n        </li>\n    </ul>\n    <p>\n        We will not sell, distribute or lease your personal information to third parties unless we have your permission\n        or are required by law to do so. We may use your personal information to send you promotional information\n        about third parties which we think you may find interesting if you tell us that you wish this to happen.\n    </p>\n    <p>\n        You may request details of personal information which we hold about you under the Data Protection Act 1998.\n        A small fee will be payable. If you would like a copy of the information held on you please email us this\n        request using our Contact Us information.\n    </p>\n    <p>\n        If you believe that any information we are holding on you is incorrect or incomplete,\n        please write to or email us as soon as possible, at the above address.\n        We will promptly correct any information found to be incorrect.\n    </p>\n    <h2><a name=\"list\"></a>List of cookies we collect</h2>\n    <p>The table below lists the cookies we collect and what information they store.</p>\n    <table class=\"data-table data-table-definition-list\">\n        <thead>\n        <tr>\n            <th>Cookie Name</th>\n            <th>Cookie Description</th>\n        </tr>\n        </thead>\n        <tbody>\n            <tr>\n                <th>FORM_KEY</th>\n                <td>Stores randomly generated key used to prevent forged requests.</td>\n            </tr>\n            <tr>\n                <th>PHPSESSID</th>\n                <td>Your session ID on the server.</td>\n            </tr>\n            <tr>\n                <th>GUEST-VIEW</th>\n                <td>Allows guests to view and edit their orders.</td>\n            </tr>\n            <tr>\n                <th>PERSISTENT_SHOPPING_CART</th>\n                <td>A link to information about your cart and viewing history, if you have asked for this.</td>\n            </tr>\n            <tr>\n                <th>STF</th>\n                <td>Information on products you have emailed to friends.</td>\n            </tr>\n            <tr>\n                <th>STORE</th>\n                <td>The store view or language you have selected.</td>\n            </tr>\n            <tr>\n                <th>USER_ALLOWED_SAVE_COOKIE</th>\n                <td>Indicates whether a customer allowed to use cookies.</td>\n            </tr>\n            <tr>\n                <th>MAGE-CACHE-SESSID</th>\n                <td>Facilitates caching of content on the browser to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>MAGE-CACHE-STORAGE</th>\n                <td>Facilitates caching of content on the browser to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>MAGE-CACHE-STORAGE-SECTION-INVALIDATION</th>\n                <td>Facilitates caching of content on the browser to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>MAGE-CACHE-TIMEOUT</th>\n                <td>Facilitates caching of content on the browser to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>SECTION-DATA-IDS</th>\n                <td>Facilitates caching of content on the browser to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>PRIVATE_CONTENT_VERSION</th>\n                <td>Facilitates caching of content on the browser to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>X-MAGENTO-VARY</th>\n                <td>Facilitates caching of content on the server to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>MAGE-TRANSLATION-FILE-VERSION</th>\n                <td>Facilitates translation of content to other languages.</td>\n            </tr>\n            <tr>\n                <th>MAGE-TRANSLATION-STORAGE</th>\n                <td>Facilitates translation of content to other languages.</td>\n            </tr>\n        </tbody>\n    </table>\n</div>','2016-08-26 10:46:09','2016-08-26 10:46:09',1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'Porto - Homepage 1','1column',NULL,NULL,'porto_home_1',NULL,'<h2 class=\"filterproduct-title\" style=\"margin:0 0 10px;\"><span class=\"content\"><strong>Featured Brands</strong></span></h2>\n<div id=\"brands-slider-demo-1\" class=\"brands-slider\">\n    <div class=\"owl-carousel\">\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand1.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand2.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand3.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand4.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand5.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand1.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand2.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand3.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand4.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand5.png\"}}\" alt=\"\" /></div>\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            $(\"#brands-slider-demo-1 .owl-carousel\").owlCarousel({\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                margin: 30,\n                nav: false,\n                dots: true,\n                loop: true,\n                responsive: {\n                    0: {\n                        items:2\n                    },\n                    640: {\n                        items:3\n                    },\n                    768: {\n                        items:4\n                    },\n                    992: {\n                        items:5\n                    },\n                    1200: {\n                        items:6\n                    }\n                }\n            });\n        });\n    </script>\n</div>\n<h2 class=\"filterproduct-title\" style=\"margin-top:40px;margin-bottom: 5px;\"><span class=\"content\"><strong>FROM THE BLOG</strong></span></h2>\n<div id=\"latest_news\" class=\"owl-top-narrow\">\n    <div class=\"recent-posts\">\n        <div class=\"owl-carousel\">\n            <div class=\"item\">\n                <div class=\"row\">\n                    <div class=\"col-sm-5\">\n                        <div class=\"post-image\">\n                            <img src=\"{{media url=\"wysiwyg/smartwave/porto/blog/03.jpg\"}}\" alt=\"\" />\n                        </div>\n                    </div>\n                    <div class=\"col-sm-7\">\n                        <div class=\"post-date\">\n                            <span class=\"day\">08</span>\n                            <span class=\"month\">Dec</span>\n                        </div>\n                        <div class=\"postTitle\">\n                            <h2><a href=\"#\">Post Format – Video</a></h2>\n                        </div>\n                        <div class=\"postContent\"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non placerat mi…</p></div>\n                        <a class=\"readmore\" href=\"#\">Read more &gt;</a>\n                    </div>\n                </div>\n            </div>\n            <div class=\"item\">\n                <div class=\"row\">\n                    <div class=\"col-sm-5\">\n                        <div class=\"post-image\">\n                            <img src=\"{{media url=\"wysiwyg/smartwave/porto/blog/04.jpg\"}}\" alt=\"\" />\n                        </div>\n                    </div>\n                    <div class=\"col-sm-7\">\n                        <div class=\"post-date\">\n                            <span class=\"day\">08</span>\n                            <span class=\"month\">Dec</span>\n                        </div>\n                        <div class=\"postTitle\">\n                            <h2><a href=\"#\">Post Format – Image Gallery</a></h2>\n                        </div>\n                        <div class=\"postContent\"><p>Euismod atras vulputate iltricies etri elit. Class aptent taciti sociosqu ad litora torquent…</p></div>\n                        <a class=\"readmore\" href=\"#\">Read more &gt;</a>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            $(\"#latest_news .owl-carousel\").owlCarousel({\n                loop: false,\n                margin: 10,\n                nav: true,\n                navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n                dots: false,\n                responsive: {\n                    0: {\n                        items:1\n                    },\n                    640: {\n                        items:2\n                    },\n                    768: {\n                        items:2\n                    },\n                    992: {\n                        items:2\n                    },\n                    1200: {\n                        items:2\n                    }\n                }\n            });\n        });\n    </script>\n</div>','2016-08-26 10:56:15','2016-08-26 10:56:15',1,0,'<referenceContainer name=\"page.top\">\n    <block class=\"Magento\\Cms\\Block\\Block\" name=\"home_slider\">\n        <arguments>\n            <argument name=\"block_id\" xsi:type=\"string\">porto_homeslider_1</argument>\n        </arguments>\n    </block>\n</referenceContainer>',NULL,NULL,NULL,NULL,NULL,NULL),(6,'Porto - About Us','1column',NULL,NULL,'about-porto',NULL,'<br/>\n<div class=\"entry-content\">\n    <div class=\"row\">\n        <div class=\"row-wrapper container\">\n            <div class=\"row\">\n                <div class=\"col-md-12\">\n                    <h2 class=\"word-rotator-title\">The New Way to <strong><span class=\"word-rotate\"><span class=\"word-rotate-items\" style=\"top: 0px;\"><span>success.</span><span>advance.</span><span>progress.</span></span></span></strong></h2>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"row-wrapper container\">\n            <div class=\"row\">\n                <div class=\"col-md-10\">\n                    <p class=\"lead\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non <span class=\"alternative-font\">metus.</span> pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.</p> \n                </div>\n                <div class=\"col-md-2\">\n                    <a class=\"btn btn-primary push-top\" title=\"Join Our Team!\" href=\"#\">Join Our Team!</a>\n                </div> \n            </div>\n        </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"row-wrapper container\">\n            <div class=\"row\">\n                <div class=\"col-md-12\">\n                    <hr class=\"tall\"/>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"row-wrapper container\">\n            <div class=\"row\">\n                <div class=\"col-md-12\">\n                    <h3 class=\"\"><strong>Who</strong> We Are</h3>\n                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc <a href=\"#\">vehicula</a> lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing sit amet. In eu justo a felis faucibus ornare vel id metus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In eu libero ligula. Fusce eget metus lorem, ac viverra leo. Nullam convallis, arcu vel pellentesque sodales, nisi est varius diam, ac ultrices sem ante quis sem. Proin ultricies volutpat sapien, nec scelerisque ligula mollis lobortis. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing <span class=\"alternative-font\">metus</span> sit amet. In eu justo a felis faucibus ornare vel id metus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In eu libero ligula. Fusce eget metus lorem, ac viverra leo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In eu libero ligula. Fusce eget metus lorem, ac viverra leo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In eu libero ligula.</p>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"row-wrapper container\">\n            <div class=\"row\">\n                <div class=\"col-md-12\">\n                    <hr class=\"tall\"/>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"row-wrapper container\">\n            <div class=\"row\">\n                <div class=\"col-md-12\">\n                    <h3 class=\"push-top\">Our <strong>History</strong></h3>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"row-wrapper container\" style=\"margin-bottom: 25px;\">\n            <div class=\"row\">\n                <div class=\"col-md-12\">\n                    <div class=\"history\">\n                        <div class=\"thumb\">\n                            <img width=\"150\" height=\"150\" src=\"{{media url=\"wysiwyg/smartwave/porto/aboutus/office-1-150x150.jpg\"}}\" class=\"attachment-thumbnail\" alt=\"office-1\"/>\n                        </div>\n                        <div class=\"featured-box\" style=\"height: auto;\">\n                            <div class=\"box-content\">\n                                <h4><strong>2012</strong></h4>\n                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus,</p>\n                            </div>\n                        </div><!--.feature-box-->\n                    </div><!--.history-->\n                    <div class=\"history\">\n                        <div class=\"thumb\">\n                            <img width=\"150\" height=\"150\" src=\"{{media url=\"wysiwyg/smartwave/porto/aboutus/office-2-150x150.jpg\"}}\" class=\"attachment-thumbnail\" alt=\"office-2\"/>\n                        </div>\n                        <div class=\"featured-box\" style=\"height: auto;\">\n                            <div class=\"box-content\">\n                                <h4><strong>2010</strong></h4>\n                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia.</p>\n                            </div>\n                        </div><!--.feature-box-->\n                    </div><!--.history-->\n                    <div class=\"history\">\n                        <div class=\"thumb\">\n                            <img width=\"150\" height=\"150\" src=\"{{media url=\"wysiwyg/smartwave/porto/aboutus/office-3-150x150.jpg\"}}\" class=\"attachment-thumbnail\" alt=\"office-3\"/>\n                        </div>\n                        <div class=\"featured-box\" style=\"height: auto;\">\n                            <div class=\"box-content\">\n                                <h4><strong>2005</strong></h4>\n                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus,</p>\n                            </div>\n                        </div><!--.feature-box-->\n                    </div><!--.history-->\n                    <div class=\"history\">\n                        <div class=\"thumb\">\n                            <img width=\"150\" height=\"150\" src=\"{{media url=\"wysiwyg/smartwave/porto/aboutus/office-4-150x150.jpg\"}}\" class=\"attachment-thumbnail\" alt=\"office-4\"/>\n                        </div>\n                        <div class=\"featured-box\" style=\"height: auto;\">\n                            <div class=\"box-content\">\n                                <h4><strong>2000</strong></h4>\n                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus,</p>\n                            </div>\n                        </div><!--.feature-box-->\n                    </div><!--.history-->\n                </div>\n            </div>\n        </div>\n    </div>\n</div>','2016-08-26 10:56:15','2016-08-26 10:56:15',1,0,'',NULL,NULL,NULL,NULL,NULL,NULL),(7,'404 Not Found 2','1column',NULL,NULL,'no-route-2',NULL,'<h1 style=\"text-align:center;margin: 20px 0; font-size: 70px;margin-top:70px\"><span style=\"display: inline-block;vertical-align:middle;\">404</span><i class=\"porto-icon-doc\" style=\"display: inline-block;vertical-align:middle;\"></i></h1>\n<p style=\"text-align:center; font-size: 15px;\">You might want to check that URL again or head over to our <a href=\"{{config path=\"web/unsecure/base_url\"}}\">homepage.</a></p>','2016-08-26 10:56:15','2016-08-26 10:56:15',1,0,'',NULL,NULL,NULL,NULL,NULL,NULL),(8,'Porto - Homepage 2','1column',NULL,NULL,'porto_home_2',NULL,'<h2 class=\"filterproduct-title\" style=\"margin-top:30px;\"><span class=\"content\"><strong>Galaxy phones Just Arrived</strong></span></h2>\n<div id=\"new_product\" class=\"owl-top-narrow\">\n    {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"latest_product\" product_count=\"10\" aspect_ratio=\"0\" image_width=\"250\" image_height=\"250\" template=\"owl_list.phtml\"}}\n</div>\n<script type=\"text/javascript\">\n    require([\n        \'jquery\',\n        \'owl.carousel/owl.carousel.min\'\n    ], function ($) {\n        $(\"#new_product .owl-carousel\").owlCarousel({\n            autoplay: true,\n            autoplayTimeout: 5000,\n            autoplayHoverPause: true,\n            margin: 10,\n            nav: true,\n            navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n            dots: false,\n            navRewind: true,\n            animateIn: \'fadeIn\',\n            animateOut: \'fadeOut\',\n            loop: true,\n            responsive: {\n                0: {\n                    items:2\n                },\n                768: {\n                    items:3\n                },\n                992: {\n                    items:4\n                },\n                1200: {\n                    items:5\n                }\n            }\n        });\n    });\n</script>','2016-10-21 23:35:34','2016-10-21 23:35:34',1,0,'<referenceContainer name=\"page.top\">\n    <block class=\"Magento\\Cms\\Block\\Block\" name=\"home_slider\">\n        <arguments>\n            <argument name=\"block_id\" xsi:type=\"string\">porto_homeslider_2</argument>\n        </arguments>\n    </block>\n</referenceContainer>',NULL,NULL,NULL,NULL,NULL,NULL),(9,'Porto - Homepage 3','1column',NULL,NULL,'porto_home_3',NULL,'<style type=\"text/css\">\nhtml, body {\n    overflow: hidden;\n}\nbody.boxed {\n    padding: 0;\n}\nbody.boxed .page-wrapper {\n    width: 100%;\n}\n.cms-index-index .page-header.type3 {\n    position: fixed;\n    top: 0;\n    left: 0;\n    z-index: 2;\n    background-color: rgba(0,0,0,.5);\n    width: 100%;\n    max-height: 100%;\n}\n.page-footer {\n    position: fixed;\n    bottom: 0;\n    width: 100%;\n    left: 0;\n    z-index: 1;\n}\n.page-main, .footer-top, .footer-middle {\n    display: none;\n}\n.footer-bottom {\n    padding: 10px 0;\n    background-color: #fff;\n    background-color: rgba(255,255,255,0.85);\n}\n@media (max-width: 1199px) {\n    .footer-bottom .custom-block.f-right, .footer-bottom .custom-block ul.links {\n        display: none;\n    }\n}\n@media (max-width: 767px) {\n    .footer-bottom .custom-block {\n        text-align: center;\n    }\n    .footer-bottom .social-icons {\n        float: none;\n        margin: 0;\n    }\n    .footer-bottom address {\n        float: none;\n        text-align: center;\n    }\n}\n@media (max-width: 640px) {\n    .footer-bottom address {\n        display: none;\n    }\n}\n</style>','2016-10-21 23:35:35','2016-10-21 23:35:35',1,0,'<referenceContainer name=\"page.top\">\n    <block class=\"Magento\\Cms\\Block\\Block\" name=\"home_slider\">\n        <arguments>\n            <argument name=\"block_id\" xsi:type=\"string\">porto_homeslider_3</argument>\n        </arguments>\n    </block>\n</referenceContainer>',NULL,NULL,NULL,NULL,NULL,NULL),(10,'Porto - Homepage 4','1column',NULL,NULL,'porto_home_4',NULL,'<style type=\"text/css\">\nbody.boxed {\n    padding: 0;\n}\nbody.boxed .page-wrapper {\n    width: 100%;\n}\n.page-header.type4 {\n    position: absolute;\n    left: 0;\n    top: 0;\n    width: 100%;\n    z-index: 2;\n    background: transparent;\n}\n.page-main {\n    display: none;\n}\n</style>','2016-10-21 23:35:35','2016-10-21 23:35:35',1,0,'<referenceContainer name=\"page.top\">\n    <block class=\"Magento\\Cms\\Block\\Block\" name=\"home_slider\">\n        <arguments>\n            <argument name=\"block_id\" xsi:type=\"string\">porto_homeslider_4</argument>\n        </arguments>\n    </block>\n</referenceContainer>',NULL,NULL,NULL,NULL,NULL,NULL),(11,'Porto - Homepage 5','1column',NULL,NULL,'porto_home_5',NULL,'<div class=\"row\" style=\"margin-top: 40px;\">\n    <div class=\"col-md-9 col-sm-8\">\n        <div class=\"filterproducts-tab\">\n            <div class=\"data items\" data-mage-init=\'{\"tabs\":{\"openedState\":\"active\"}}\'>\n                <div class=\"data item title\" aria-labeledby=\"tab-label-featured-title\" data-role=\"collapsible\" id=\"tab-label-featured\">\n                    <a class=\"data switch\" tabindex=\"-1\" data-toggle=\"switch\" href=\"#featured\" id=\"tab-label-featured-title\">Featured</a>\n                </div>\n                <div class=\"data item content hide-addtolinks\" id=\"featured\" data-role=\"content\">\n                    {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"featured_product\" product_count=\"8\" column_count=\"4\" move_actions=\"1\" aspect_ratio=\"0\" image_width=\"250\" image_height=\"250\" template=\"grid.phtml\"}}\n                </div>\n                <div class=\"data item title\" aria-labeledby=\"tab-label-latest-title\" data-role=\"collapsible\" id=\"tab-label-latest\">\n                    <a class=\"data switch\" tabindex=\"-1\" data-toggle=\"switch\" href=\"#latest\" id=\"tab-label-latest-title\">Latest</a>\n                </div>\n                <div class=\"data item content hide-addtolinks\" id=\"latest\" data-role=\"content\">\n                    {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"latest_product\" product_count=\"8\" column_count=\"4\" move_actions=\"1\" aspect_ratio=\"0\" image_width=\"250\" image_height=\"250\" template=\"grid.phtml\"}}\n                </div>\n            </div>\n        </div>\n        <div class=\"grid-images\" style=\"margin:20px 0;\">\n            <div class=\"row\">\n                <div class=\"col-sm-4\">\n                    <div class=\"grid1\">\n                        <a href=\"#\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/05/content/grid1.jpg\"}}\" alt=\"\" /></a>\n                    </div>\n                    <div class=\"grid2\">\n                        <a href=\"#\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/05/content/grid2.jpg\"}}\" alt=\"\"></a>\n                    </div>\n                </div>\n                <div class=\"col-sm-8\">\n                    <div class=\"grid3\">\n                        <a href=\"#\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/05/content/grid3.jpg\"}}\" alt=\"\"></a>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <h2 class=\"filterproduct-title\" style=\"margin:0 0 10px;\"><span class=\"content\"><strong>Featured Brands</strong></span></h2>\n        <div id=\"brands-slider-demo-8\" class=\"brands-slider\">\n            <div class=\"owl-carousel\">\n                <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand1.png\"}}\" alt=\"\" /></div>\n                <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand2.png\"}}\" alt=\"\" /></div>\n                <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand3.png\"}}\" alt=\"\" /></div>\n                <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand4.png\"}}\" alt=\"\" /></div>\n                <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand5.png\"}}\" alt=\"\" /></div>\n                <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand1.png\"}}\" alt=\"\" /></div>\n                <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand2.png\"}}\" alt=\"\" /></div>\n                <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand3.png\"}}\" alt=\"\" /></div>\n                <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand4.png\"}}\" alt=\"\" /></div>\n                <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand5.png\"}}\" alt=\"\" /></div>\n            </div>\n        <script type=\"text/javascript\">\n            require([\n                \'jquery\',\n                \'owl.carousel/owl.carousel.min\'\n            ], function ($) {\n                $(\"#brands-slider-demo-8 .owl-carousel\").owlCarousel({\n                    autoplay: true,\n                    autoplayTimeout: 5000,\n                    autoplayHoverPause: true,\n                    margin: 30,\n                    nav: false,\n                    dots: true,\n                    loop: true,\n                    responsive: {\n                        0: {\n                            items:2\n                        },\n                        640: {\n                            items:3\n                        },\n                        768: {\n                            items:4\n                        },\n                        992: {\n                            items:5\n                        },\n                        1200: {\n                            items:6\n                        }\n                    }\n                });\n            });\n        </script>\n        </div>\n    </div>\n    <div class=\"col-md-3 col-sm-4 sidebar\">\n        <div class=\"custom-block\" style=\"margin-top:44px;padding-bottom:37px;margin-bottom:15px;\">\n            <div class=\"block block-border\">\n                <div class=\"block-title\">Category</div>\n                <div class=\"block-content\">\n                    {{block class=\"Smartwave\\Porto\\Block\\CategoryCollection\" name=\"category_sidebar\" template=\"Smartwave_Porto::category_side.phtml\"}}\n                </div>\n            </div>\n        </div>\n        <h2 class=\"filterproduct-title\" style=\"margin-top:40px;margin-bottom: 5px;\"><span class=\"content\"><strong>FROM THE BLOG</strong></span></h2>\n        <div id=\"latest_news\" class=\"custom-block\">\n            <div class=\"recent-posts\">\n                <div class=\"owl-carousel\">\n                    <div class=\"item\">\n                        <div class=\"row\">\n                            <div class=\"col-sm-5\">\n                                <div class=\"post-image\">\n                                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/blog/03.jpg\"}}\" alt=\"\" />\n                                </div>\n                            </div>\n                            <div class=\"col-sm-7\">\n                                <div class=\"post-date\">\n                                    <span class=\"day\">08</span>\n                                    <span class=\"month\">Dec</span>\n                                </div>\n                                <div class=\"postTitle\">\n                                    <h2><a href=\"#\">Post Format – Video</a></h2>\n                                </div>\n                                <div class=\"postContent\"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non placerat mi…</p></div>\n                                <a class=\"readmore\" href=\"#\">Read more &gt;</a>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"item\">\n                        <div class=\"row\">\n                            <div class=\"col-sm-5\">\n                                <div class=\"post-image\">\n                                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/blog/04.jpg\"}}\" alt=\"\" />\n                                </div>\n                            </div>\n                            <div class=\"col-sm-7\">\n                                <div class=\"post-date\">\n                                    <span class=\"day\">08</span>\n                                    <span class=\"month\">Dec</span>\n                                </div>\n                                <div class=\"postTitle\">\n                                    <h2><a href=\"#\">Post Format – Image Gallery</a></h2>\n                                </div>\n                                <div class=\"postContent\"><p>Euismod atras vulputate iltricies etri elit. Class aptent taciti sociosqu ad litora torquent…</p></div>\n                                <a class=\"readmore\" href=\"#\">Read more &gt;</a>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <script type=\"text/javascript\">\n                require([\n                    \'jquery\',\n                    \'owl.carousel/owl.carousel.min\'\n                ], function ($) {\n                    $(\"#latest_news .owl-carousel\").owlCarousel({\n                        items: 1,\n                        loop: true,\n                        nav: false,\n                        dots: true\n                    });\n                });\n            </script>\n        </div>\n        <h2 style=\"font-weight:600;font-size:20px;color:#000;line-height:1;\">Custom HTML Block</h2>\n        <h5 style=\"font-family:Arial;font-weight:400;font-size:11px;color:#878787;line-height:1;margin-bottom:13px;\">This is a custom sub-title.</h5>\n        <p style=\"font-weight:400;font-size:14px;color:#666;line-height:1.42;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non placerat mi. Etiam non tellus </p>\n    </div>\n</div>\n<style type=\"text/css\">\n.recent-posts .item .col-sm-5, .recent-posts .item .col-sm-7 {\n    width: 100%;\n}\n</style>','2016-10-21 23:35:35','2016-10-21 23:35:35',1,0,'<referenceContainer name=\"page.top\">\n    <block class=\"Magento\\Cms\\Block\\Block\" name=\"home_slider\">\n        <arguments>\n            <argument name=\"block_id\" xsi:type=\"string\">porto_homeslider_5</argument>\n        </arguments>\n    </block>\n</referenceContainer>',NULL,NULL,NULL,NULL,NULL,NULL),(12,'Porto - Homepage 6','1column',NULL,NULL,'porto_home_6',NULL,'<p class=\"filter-title-type-2\" style=\"margin-top:10px;\"><span class=\"title-line\">&nbsp;</span><span style=\"width: 315px;text-align: center;font-weight: 600;color: #3d3734;\">WEEKLY FEATURED PRODUCTS</span><span class=\"title-line\">&nbsp;</span></p>\n<div id=\"weekly_featured_product\" class=\"hide-addtocart hide-addtolinks owl-middle-outer-narrow\">\n    {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"featured_product\" product_count=\"10\" aspect_ratio=\"0\" image_width=\"250\" image_height=\"250\" template=\"owl_list.phtml\"}}\n</div>\n<script type=\"text/javascript\">\n    require([\n        \'jquery\',\n        \'owl.carousel/owl.carousel.min\'\n    ], function ($) {\n        $(\"#weekly_featured_product .owl-carousel\").owlCarousel({\n            autoplay: true,\n            autoplayTimeout: 5000,\n            autoplayHoverPause: true,\n            margin: 10,\n            nav: false,\n            dots: false,\n            navRewind: true,\n            animateIn: \'fadeIn\',\n            animateOut: \'fadeOut\',\n            loop: true,\n            responsive: {\n                0: {\n                    items:1\n                },\n                320: {\n                    items:1\n                },\n                480: {\n                    items:2\n                },\n                768: {\n                    items:3\n                },\n                992: {\n                    items:4\n                },\n                1200: {\n                    items:5\n                }\n            }\n        });\n    });\n</script>\n<p class=\"filter-title-type-2\" style=\"margin-top:30px\"><span class=\"title-line\">&nbsp;</span><span style=\"width: 225px;text-align: center;font-weight: 600;color: #3d3734;\">FEATURED BRANDS</span><span class=\"title-line\">&nbsp;</span></p>\n<div class=\"owl-middle-outer-narrow\">\n    <div id=\"brands-slider-demo-18\" class=\"owl-carousel\">\n        <img class=\"owl-lazy\" data-src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand1.jpg\"}}\" alt=\"\" />\n        <img class=\"owl-lazy\" data-src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand2.jpg\"}}\" alt=\"\" />\n        <img class=\"owl-lazy\" data-src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand3.jpg\"}}\" alt=\"\" />\n        <img class=\"owl-lazy\" data-src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand4.jpg\"}}\" alt=\"\" />\n        <img class=\"owl-lazy\" data-src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand5.jpg\"}}\" alt=\"\" />\n        <img class=\"owl-lazy\" data-src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand6.jpg\"}}\" alt=\"\" />\n        <img class=\"owl-lazy\" data-src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand1.jpg\"}}\" alt=\"\" />\n        <img class=\"owl-lazy\" data-src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand2.jpg\"}}\" alt=\"\" />\n        <img class=\"owl-lazy\" data-src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand3.jpg\"}}\" alt=\"\" />\n        <img class=\"owl-lazy\" data-src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand4.jpg\"}}\" alt=\"\" />\n        <img class=\"owl-lazy\" data-src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand5.jpg\"}}\" alt=\"\" />\n        <img class=\"owl-lazy\" data-src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand6.jpg\"}}\" alt=\"\" />\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            $(\"#brands-slider-demo-18\").owlCarousel({\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                margin: 10,\n                nav: false,\n                dots: false,\n                navRewind: true,\n                animateIn: \'fadeIn\',\n                animateOut: \'fadeOut\',\n                loop: true,\n                lazyLoad: true,\n                responsive: {\n                    0: {\n                        items:1\n                    },\n                    320: {\n                        items:1\n                    },\n                    480: {\n                        items:2\n                    },\n                    640: {\n                        items:3\n                    },\n                    768: {\n                        items:4\n                    },\n                    992: {\n                        items:5\n                    },\n                    1200: {\n                        items:6\n                    }\n                }\n            });\n        });\n    </script>\n</div>','2016-10-21 23:35:35','2016-10-21 23:35:35',1,0,'<referenceContainer name=\"page.top\">\n    <block class=\"Magento\\Cms\\Block\\Block\" name=\"home_slider\">\n        <arguments>\n            <argument name=\"block_id\" xsi:type=\"string\">porto_homeslider_6</argument>\n        </arguments>\n    </block>\n</referenceContainer>',NULL,NULL,NULL,NULL,NULL,NULL),(13,'Porto - Homepage 7','1column',NULL,NULL,'porto_home_7',NULL,'<div class=\"row\" style=\"margin: 15px -8px 0 -8px;\">\n    <div class=\"col-sm-4 one-product\" style=\"padding-left: 8px; padding-right: 8px;\">\n        <h4 class=\"filterproduct-title\">FEATURED</h4>\n        <div id=\"featured_product\" class=\"hide-addtocart hide-addtolinks\">\n            {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"featured_product\" product_count=\"3\" aspect_ratio=\"0\" image_width=\"350\" image_height=\"350\" template=\"owl_list.phtml\"}}\n        </div>\n        <script type=\"text/javascript\">\n            require([\n                \'jquery\',\n                \'owl.carousel/owl.carousel.min\'\n            ], function ($) {\n                $(\"#featured_product .owl-carousel\").owlCarousel({\n                    autoplay: true,\n                    autoplayTimeout: 5000,\n                    autoplayHoverPause: true,\n                    nav: false,\n                    dots: true,\n                    navRewind: true,\n                    animateIn: \'fadeIn\',\n                    animateOut: \'fadeOut\',\n                    loop: true,\n                    items: 1\n                });\n            });\n        </script>\n    </div>\n    <div class=\"col-sm-4 one-product\" style=\"padding-left: 8px; padding-right: 8px;\">\n        <h4 class=\"filterproduct-title\">SALES</h4>\n        <div id=\"sales_product\" class=\"hide-addtocart hide-addtolinks\">\n            {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"featured_product\" product_count=\"3\" aspect_ratio=\"0\" image_width=\"350\" image_height=\"350\" template=\"owl_list.phtml\"}}\n        </div>\n        <script type=\"text/javascript\">\n            require([\n                \'jquery\',\n                \'owl.carousel/owl.carousel.min\'\n            ], function ($) {\n                $(\"#sales_product .owl-carousel\").owlCarousel({\n                    autoplay: true,\n                    autoplayTimeout: 5000,\n                    autoplayHoverPause: true,\n                    nav: false,\n                    dots: true,\n                    navRewind: true,\n                    animateIn: \'fadeIn\',\n                    animateOut: \'fadeOut\',\n                    loop: true,\n                    items: 1\n                });\n            });\n        </script>\n    </div>\n    <div class=\"col-sm-4 one-product\" style=\"padding-left: 8px; padding-right: 8px;\">\n        <h4 class=\"filterproduct-title\">NEW ARRIVALS</h4>\n        <div id=\"new_product\" class=\"hide-addtocart hide-addtolinks\">\n            {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"featured_product\" product_count=\"3\" aspect_ratio=\"0\" image_width=\"350\" image_height=\"350\" template=\"owl_list.phtml\"}}\n        </div>\n        <script type=\"text/javascript\">\n            require([\n                \'jquery\',\n                \'owl.carousel/owl.carousel.min\'\n            ], function ($) {\n                $(\"#new_product .owl-carousel\").owlCarousel({\n                    autoplay: true,\n                    autoplayTimeout: 5000,\n                    autoplayHoverPause: true,\n                    nav: false,\n                    dots: true,\n                    navRewind: true,\n                    animateIn: \'fadeIn\',\n                    animateOut: \'fadeOut\',\n                    loop: true,\n                    items: 1\n                });\n            });\n        </script>\n    </div>\n</div>\n<div class=\"single-images\" style=\"padding-top: 15px;\">\n    <div class=\"row\" style=\"margin-left:-7px;margin-right:-7px;\">\n        <div class=\"col-sm-3 col-xs-6\" style=\"padding-left:7px;padding-right:7px;padding-bottom:15px;\">\n            <a class=\"image-link\" href=\"#\">\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/07/content/grid1.jpg\"}}\" alt=\"\" />\n            </a>\n        </div>\n        <div class=\"col-sm-3 col-xs-6\" style=\"padding-left:7px;padding-right:7px;padding-bottom:15px;\">\n            <a class=\"image-link\" href=\"#\">\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/07/content/grid2.jpg\"}}\" alt=\"\" />\n            </a>\n        </div>\n        <div class=\"col-sm-3 col-xs-6\" style=\"padding-left:7px;padding-right:7px;padding-bottom:15px;\">\n            <a class=\"image-link\" href=\"#\">\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/07/content/grid3.jpg\"}}\" alt=\"\" />\n            </a>\n        </div>\n        <div class=\"col-sm-3 col-xs-6\" style=\"padding-left:7px;padding-right:7px;padding-bottom:15px;\">\n            <a class=\"image-link\" href=\"#\">\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/07/content/grid4.jpg\"}}\" alt=\"\" />\n            </a>\n        </div>\n     </div>\n</div>\n<div style=\"padding: 10px 20px 20px;background-color: #fff;\">\n    {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"featured_product\" product_count=\"8\" column_count=\"4\" aspect_ratio=\"1\" image_width=\"300\" template=\"grid.phtml\"}}\n</div>\n<div class=\"shop-features\" style=\"background-color:#fff; margin-top:13px; padding: 35px 0;\">\n    <div class=\"row\">\n        <div class=\"col-sm-4\">\n            <em class=\"porto-icon-star\" style=\"border:0;\"></em>\n            <h3>DEDICATED SERVICE</h3>\n            <p style=\"padding: 0 60px;\">Consult our specialists for help with an order, customization, or design advice.</p>\n            <a href=\"#\">Get in Touch &gt;</a>\n        </div>\n        <div class=\"col-sm-4\">\n            <em class=\"porto-icon-reply\" style=\"border:0;\"></em>\n            <h3>FREE RETURNS</h3>\n            <p style=\"padding: 0 60px;\">We stand behind our goods and services and want you to be satisfied with them.</p>\n            <a href=\"#\">Returns Policy &gt;</a>\n        </div>\n        <div class=\"col-sm-4\">\n            <em class=\"porto-icon-paper-plane\" style=\"border:0;\"></em>\n            <h3>INTERNATIONAL SHIPPING</h3>\n            <p style=\"padding: 0 70px;\">Currently over 50 countries qualify for express international shipping.</p>\n            <a href=\"#\">Learn More &gt;</a>\n        </div>\n    </div>\n</div>\n<div class=\"col-md-12\" style=\"padding: 20px;background-color:#fff;\">\n    <div id=\"brands-slider-demo-7\" class=\"brands-slider\">\n        <div class=\"owl-carousel\">\n            <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand1.png\"}}\" alt=\"\" /></div>\n            <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand2.png\"}}\" alt=\"\" /></div>\n            <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand3.png\"}}\" alt=\"\" /></div>\n            <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand4.png\"}}\" alt=\"\" /></div>\n            <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand5.png\"}}\" alt=\"\" /></div>\n            <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand1.png\"}}\" alt=\"\" /></div>\n            <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand2.png\"}}\" alt=\"\" /></div>\n            <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand3.png\"}}\" alt=\"\" /></div>\n            <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand4.png\"}}\" alt=\"\" /></div>\n            <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand5.png\"}}\" alt=\"\" /></div>\n        </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            $(\"#brands-slider-demo-7 .owl-carousel\").owlCarousel({\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                margin: 30,\n                nav: false,\n                dots: true,\n                loop: true,\n                responsive: {\n                    0: {\n                        items:2\n                    },\n                    640: {\n                        items:3\n                    },\n                    768: {\n                        items:4\n                    },\n                    992: {\n                        items:5\n                    },\n                    1200: {\n                        items:6\n                    }\n                }\n            });\n        });\n    </script>\n    </div>\n</div>','2016-10-21 23:35:35','2016-10-21 23:35:35',1,0,'<referenceContainer name=\"page.top\">\n    <block class=\"Magento\\Cms\\Block\\Block\" name=\"home_slider\">\n        <arguments>\n            <argument name=\"block_id\" xsi:type=\"string\">porto_homeslider_7</argument>\n        </arguments>\n    </block>\n</referenceContainer>',NULL,NULL,NULL,NULL,NULL,NULL),(14,'Porto - Homepage 8','1column',NULL,NULL,'porto_home_8',NULL,'<div class=\"homepage-bar\" style=\"border:1px solid #e1e1e1;margin:0 0 20px 0;\">\n    <div class=\"row\">\n        <div class=\"col-md-4\" style=\"text-align:center;height:60px;padding-top:5px;padding-bottom:5px;\">\n            <em class=\"porto-icon-truck\" style=\"font-size:36px;\"></em><div class=\"text-area\"><h3>FREE SHIPPING & RETURN</h3><p>Free shipping on all orders over $99.</p></div>\n        </div>\n        <div class=\"col-md-4\" style=\"text-align:center;height:60px;padding-top:5px;padding-bottom:5px;\">\n            <em class=\"porto-icon-dollar\"></em><div class=\"text-area\"><h3>MONEY BACK GUARANTEE</h3><p>100% money back guarantee.</p></div>\n        </div>\n        <div class=\"col-md-4\" style=\"text-align:center;height:60px;padding-top:5px;padding-bottom:5px;\">\n            <em class=\"porto-icon-lifebuoy\" style=\"font-size:32px;\"></em><div class=\"text-area\"><h3>ONLINE SUPPORT 24/7</h3><p>Lorem ipsum dolor sit amet.</p></div>\n        </div>\n    </div>\n</div>\n<div class=\"row\">\n    <div class=\"col-md-3 col-lg-2-4\">\n        <div class=\"home-side-menu-type2\" style=\"margin-bottom:20px;\">\n            <h2><em class=\"porto-icon-menu\"></em> SHOP CATEGORIES</h2>\n            <div class=\"side-block side-menu-type2\">\n                {{block class=\"Smartwave\\Porto\\Block\\CategoryCollection\" name=\"category_sidebar\" template=\"Smartwave_Porto::category_side_2.phtml\"}}\n            </div>\n        </div>\n        <div class=\"featured-box-2\">\n            <h3><em class=\"porto-icon-star\"></em> DEDICATED SERVICE</h3>\n            <p>Consult our specialists for help with an order, customization, or design advice.</p>\n            <a href=\"#\">Get in Touch &gt;</a>\n        </div>\n        <div class=\"featured-box-2\">\n            <h3><em class=\"porto-icon-reply\"></em> FREE RETURNS</h3>\n            <p>We stand behind our goods and services and want you to be satisfied with them.</p>\n            <a href=\"#\">Returns Policy &gt;</a>\n        </div>\n    </div>\n    <div class=\"col-md-9 col-lg-9-6\">\n        <h3 style=\"font-size: 15px;font-weight: 600;color: #333;margin-top:0;margin-bottom:15px;\">FEATURED PRODUCTS</h3>\n        {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"featured_product\" product_count=\"8\" column_count=\"4\" move_actions=\"1\" aspect_ratio=\"0\" image_width=\"250\" image_height=\"250\" template=\"grid.phtml\"}}\n    </div>\n</div>\n<h2 class=\"filterproduct-title\" style=\"margin-bottom: 4px;\"><span class=\"content\"><strong>Featured Brands</strong></span></h2>\n<div id=\"brands-slider-demo-8\" class=\"brands-slider owl-top-narrow\">\n    <div class=\"owl-carousel\">\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand1.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand2.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand3.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand4.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand5.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand6.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand1.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand2.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand3.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand4.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand5.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand6.jpg\"}}\" alt=\"\" /></div>\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            $(\"#brands-slider-demo-8 .owl-carousel\").owlCarousel({\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                margin: 30,\n                nav: true,\n                navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n                dots: false,\n                loop: true,\n                responsive: {\n                    0: {\n                        items:2\n                    },\n                    640: {\n                        items:3\n                    },\n                    768: {\n                        items:4\n                    },\n                    992: {\n                        items:5\n                    },\n                    1200: {\n                        items:6\n                    }\n                }\n            });\n        });\n    </script>\n</div>\n<h2 class=\"filterproduct-title\" style=\"margin-top: 30px;\"><span class=\"content\"><strong>New Arrivals</strong></span></h2>\n<div id=\"new_product\" class=\"owl-top-narrow hide-addtocart hide-addtolinks\">\n    {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"new_product\" product_count=\"12\" aspect_ratio=\"0\" image_width=\"212\" image_height=\"263\" template=\"owl_list.phtml\"}}\n</div>\n<script type=\"text/javascript\">\n    require([\n        \'jquery\',\n        \'owl.carousel/owl.carousel.min\'\n    ], function ($) {\n        $(\"#new_product .owl-carousel\").owlCarousel({\n            autoplay: true,\n            autoplayTimeout: 5000,\n            autoplayHoverPause: true,\n            loop: true,\n            navRewind: true,\n            margin: 10,\n            nav: true,\n            navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n            dots: false,\n            responsive: {\n                0: {\n                    items:1\n                },\n                320: {\n                    items:1\n                },\n                480: {\n                    items:2\n                },\n                640: {\n                    items:3\n                },\n                768: {\n                    items:4\n                },\n                992: {\n                    items:6\n                },\n                1200: {\n                    items:7\n                },\n                1350: {\n                    items:8\n                }\n            }\n        });\n    });\n</script>','2016-10-21 23:35:35','2016-10-21 23:35:35',1,0,'<referenceContainer name=\"page.top\">\n    <block class=\"Magento\\Cms\\Block\\Block\" name=\"home_slider\">\n        <arguments>\n            <argument name=\"block_id\" xsi:type=\"string\">porto_homeslider_8</argument>\n        </arguments>\n    </block>\n</referenceContainer>',NULL,NULL,NULL,NULL,NULL,NULL),(15,'Porto - Homepage 9','1column',NULL,NULL,'porto_home_9',NULL,'<div class=\"homepage-bar\" style=\"border:1px solid #e1e1e1;border-radius:5px;margin:0 0 20px 0;\">\n    <div class=\"row\">\n        <div class=\"col-md-4\" style=\"text-align:center;height:70px;padding-top:10px;padding-bottom:10px;\">\n            <em class=\"porto-icon-truck\" style=\"font-size:36px;\"></em><div class=\"text-area\"><h3>FREE SHIPPING & RETURN</h3><p>Free shipping on all orders over $99.</p></div>\n        </div>\n        <div class=\"col-md-4\" style=\"text-align:center;height:70px;padding-top:10px;padding-bottom:10px;\">\n            <em class=\"porto-icon-dollar\"></em><div class=\"text-area\"><h3>MONEY BACK GUARANTEE</h3><p>100% money back guarantee.</p></div>\n        </div>\n        <div class=\"col-md-4\" style=\"text-align:center;height:70px;padding-top:10px;padding-bottom:10px;\">\n            <em class=\"porto-icon-lifebuoy\" style=\"font-size:32px;\"></em><div class=\"text-area\"><h3>ONLINE SUPPORT 24/7</h3><p>Lorem ipsum dolor sit amet.</p></div>\n        </div>\n    </div>\n</div>\n<div class=\"row\" style=\"margin: 0 -10px;\">\n    <div class=\"col-md-3\" style=\"padding: 0 10px;\">\n        <div class=\"home-side-menu\">\n            <h2 class=\"side-menu-title\">CATEGORIES</h2>\n            {{block class=\"Smartwave\\Megamenu\\Block\\Topmenu\" name=\"sw.sidenav\" template=\"Smartwave_Megamenu::sidemenu.phtml\" ttl=\"3600\"}}\n        </div>\n    </div>\n    <div class=\"col-md-9\" style=\"padding: 0 10px;\">\n        <div id=\"banner-slider-demo-9\" class=\"owl-carousel owl-banner-carousel owl-bottom-narrow\">\n            <div class=\"item\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/09/content/01_bg.png\"}}) repeat;border-radius:5px;\">\n                <div style=\"position:relative\">\n                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/09/content/01.png\"}}\" alt=\"\" />\n                    <div class=\"content type1\" style=\"position:absolute;top:30%;left:10%;text-align:right\">\n                        <h2 style=\"font-weight:600;line-height:1;color:#08c\">HUGE <b style=\"font-weight:800\">SALE</b></h2>\n                        <p style=\"color:#777;font-weight:300;line-height:1;margin-bottom:15px\">Now starting at <span style=\"color:#535353;font-weight:400\">$99</span></p>\n                        <a href=\"#\" style=\"font-weight:300;\">Shop now &gt;</a>\n                    </div>\n                </div>\n            </div>\n            <div class=\"item\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/09/content/02_bg.png\"}}) center center no-repeat;background-size:cover;border-radius:5px;\">\n                <div style=\"position:relative\">\n                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/09/content/02.png\"}}\" alt=\"\" />\n                </div>\n            </div>\n            <div class=\"item\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/09/content/03_bg.png\"}}) center center no-repeat;background-size:cover;border-radius:5px;\">\n                <div style=\"position:relative\">\n                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/09/content/03.png\"}}\" alt=\"\" />\n                </div>\n            </div>\n        </div>\n        <script type=\"text/javascript\">\n            require([\n                \'jquery\',\n                \'owl.carousel/owl.carousel.min\'\n            ], function ($) {\n                $(\"#banner-slider-demo-9\").owlCarousel({\n                    items: 1,\n                    autoplay: true,\n                    autoplayTimeout: 5000,\n                    autoplayHoverPause: true,\n                    dots: true,\n                    nav: false,\n                    navRewind: true,\n                    animateIn: \'fadeIn\',\n                    animateOut: \'fadeOut\',\n                    loop: true\n                });\n            });\n        </script>\n    </div>\n</div>\n<div class=\"row\" style=\"margin: 0 -10px;\">\n    <div class=\"col-md-3\" style=\"padding: 0 10px;\">\n        <div id=\"ads-slider-demo-9\" class=\"owl-carousel\">\n            <div class=\"item\" style=\"text-align:center;\">\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/09/content/ad2.png\"}}\" alt=\"\" style=\"display:inline-block;\"/>\n            </div>\n            <div class=\"item\" style=\"text-align:center;\">\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/09/content/ad1.png\"}}\" alt=\"\" style=\"display:inline-block;\" />\n            </div>\n            <div class=\"item\" style=\"text-align:center;\">\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/09/content/ad2.png\"}}\" alt=\"\" style=\"display:inline-block;\" />\n            </div>\n        </div>\n        <script type=\"text/javascript\">\n            require([\n                \'jquery\',\n                \'owl.carousel/owl.carousel.min\'\n            ], function ($) {\n                $(\"#ads-slider-demo-9\").owlCarousel({\n                    items: 1,\n                    autoplay: true,\n                    autoplayTimeout: 5000,\n                    autoplayHoverPause: true,\n                    dots: true,\n                    nav: false,\n                    navRewind: true,\n                    animateIn: \'fadeIn\',\n                    animateOut: \'fadeOut\',\n                    loop: true\n                });\n            });\n        </script>\n        <div class=\"block block-subscribe home-sidebar-block\" style=\"margin-top:40px;margin-bottom:30px;\">\n            <div class=\"block-title\">\n                <strong><span>Be the First to Know</span></strong>\n            </div>\n            <div class=\"block-content\">\n                <p>Keep up on our always evolving product features and technology.<br>Enter your e-mail and subscribe to our newsletter.</p>\n                {{block class=\"Magento\\Newsletter\\Block\\Subscribe\" name=\"home.newsletter\" template=\"subscribe_form.phtml\"}}\n            </div>\n        </div>\n        <h2 class=\"filterproduct-title\" style=\"margin-top:40px;margin-bottom: 5px;\"><span class=\"content\"><strong>TESTIMONIALS</strong></span></h2>\n        <div id=\"testimonials-slider-demo-9\" class=\"owl-carousel\">\n            <div class=\"item\">\n                <blockquote class=\"testimonial\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est.</p>\n                </blockquote>\n                <div class=\"testimonial-arrow-down\"></div>\n                <div class=\"testimonial-author\">\n                    <div class=\"img-thumbnail img-thumbnail-small\"><img width=\"60\" height=\"60\" src=\"{{media url=\"wysiwyg/smartwave/porto/testimonials/client-1-11.jpg\"}}\" alt=\"\"></div>\n                    <p><strong>John Smith</strong><span>CEO &amp; Founder - Okler</span></p>\n                </div>\n            </div>\n            <div class=\"item\">\n                <blockquote class=\"testimonial\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est.</p>\n                </blockquote>\n                <div class=\"testimonial-arrow-down\"></div>\n                <div class=\"testimonial-author\">\n                    <div class=\"img-thumbnail img-thumbnail-small\"><img width=\"60\" height=\"60\" src=\"{{media url=\"wysiwyg/smartwave/porto/testimonials/client-1-11.jpg\"}}\" alt=\"\"></div>\n                    <p><strong>John Smith</strong><span>CEO &amp; Founder - Okler</span></p>\n                </div>\n            </div>\n        </div>\n        <script type=\"text/javascript\">\n            require([\n                \'jquery\',\n                \'owl.carousel/owl.carousel.min\'\n            ], function ($) {\n                $(\"#testimonials-slider-demo-9\").owlCarousel({\n                    items: 1,\n                    autoplay: true,\n                    autoplayTimeout: 5000,\n                    autoplayHoverPause: true,\n                    dots: true,\n                    nav: false,\n                    navRewind: true,\n                    animateIn: \'fadeIn\',\n                    animateOut: \'fadeOut\',\n                    loop: true\n                });\n            });\n        </script>\n        <h2 class=\"filterproduct-title\" style=\"margin-top:40px;margin-bottom: 5px;\"><span class=\"content\"><strong>FROM THE BLOG</strong></span></h2>\n        <div id=\"latest_news\" class=\"custom-block\">\n            <div class=\"recent-posts\">\n                <div class=\"owl-carousel\">\n                    <div class=\"item\">\n                        <div class=\"row\">\n                            <div class=\"col-sm-5\">\n                                <div class=\"post-image\">\n                                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/blog/03.jpg\"}}\" alt=\"\" />\n                                </div>\n                            </div>\n                            <div class=\"col-sm-7\">\n                                <div class=\"post-date\">\n                                    <span class=\"day\">08</span>\n                                    <span class=\"month\">Dec</span>\n                                </div>\n                                <div class=\"postTitle\">\n                                    <h2><a href=\"#\">Post Format – Video</a></h2>\n                                </div>\n                                <div class=\"postContent\"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non placerat mi…</p></div>\n                                <a class=\"readmore\" href=\"#\">Read more &gt;</a>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"item\">\n                        <div class=\"row\">\n                            <div class=\"col-sm-5\">\n                                <div class=\"post-image\">\n                                    <img src=\"{{media url=\"wysiwyg/smartwave/porto/blog/04.jpg\"}}\" alt=\"\" />\n                                </div>\n                            </div>\n                            <div class=\"col-sm-7\">\n                                <div class=\"post-date\">\n                                    <span class=\"day\">08</span>\n                                    <span class=\"month\">Dec</span>\n                                </div>\n                                <div class=\"postTitle\">\n                                    <h2><a href=\"#\">Post Format – Image Gallery</a></h2>\n                                </div>\n                                <div class=\"postContent\"><p>Euismod atras vulputate iltricies etri elit. Class aptent taciti sociosqu ad litora torquent…</p></div>\n                                <a class=\"readmore\" href=\"#\">Read more &gt;</a>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <script type=\"text/javascript\">\n                require([\n                    \'jquery\',\n                    \'owl.carousel/owl.carousel.min\'\n                ], function ($) {\n                    $(\"#latest_news .owl-carousel\").owlCarousel({\n                        items: 1,\n                        loop: true,\n                        nav: false,\n                        dots: true\n                    });\n                });\n            </script>\n            <style type=\"text/css\">.recent-posts .item .col-sm-5, .recent-posts .item .col-sm-7{width:100%;}</style>\n        </div>\n    </div>\n    <div class=\"col-md-9\" style=\"padding: 0 10px;\">\n        <div class=\"single-images\" style=\"padding-top: 15px;\">\n            <div class=\"row\" style=\"margin-left:-10px;margin-right:-10px;\">\n                <div class=\"col-sm-4\" style=\"padding-left:10px;padding-right:10px;padding-bottom:15px;\">\n                    <a class=\"image-link\" href=\"#\">\n                        <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/09/content/img1.jpg\"}}\" alt=\"\" />\n                    </a>\n                </div>\n                <div class=\"col-sm-4\" style=\"padding-left:10px;padding-right:10px;padding-bottom:15px;\">\n                    <a class=\"image-link\" href=\"#\">\n                        <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/09/content/img2.jpg\"}}\" alt=\"\" />\n                    </a>\n                </div>\n                <div class=\"col-sm-4\" style=\"padding-left:10px;padding-right:10px;padding-bottom:15px;\">\n                    <a class=\"image-link\" href=\"#\">\n                        <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/09/content/img3.jpg\"}}\" alt=\"\" />\n                    </a>\n                </div>\n             </div>\n        </div>\n        <h2 class=\"filterproduct-title\"><span class=\"content\"><strong>FEATURED FASHION DRESS</strong></span></h2>\n        <div id=\"featured_product\" class=\"owl-top-narrow\">\n            {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"featured_product\" product_count=\"10\" aspect_ratio=\"0\" image_width=\"212\" image_height=\"263\" template=\"owl_list.phtml\"}}\n        </div>\n        <script type=\"text/javascript\">\n            require([\n                \'jquery\',\n                \'owl.carousel/owl.carousel.min\'\n            ], function ($) {\n                $(\"#featured_product .owl-carousel\").owlCarousel({\n                    autoplay: true,\n                    autoplayTimeout: 5000,\n                    autoplayHoverPause: true,\n                    loop: true,\n                    navRewind: true,\n                    margin: 10,\n                    nav: true,\n                    navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n                    dots: false,\n                    responsive: {\n                        0: {\n                            items:2\n                        },\n                        768: {\n                            items:3\n                        },\n                        992: {\n                            items:3\n                        },\n                        1200: {\n                            items:4\n                        }\n                    }\n                });\n            });\n        </script>\n        <hr style=\"background-image: linear-gradient(to right, transparent, rgba(0, 0, 0, 0.2), transparent);border: 0;height: 1px;margin: 30px 0 0;\">\n        <div class=\"row\">\n            <div class=\"col-sm-4\">\n                <h2 class=\"filterproduct-title\" style=\"background-image:none;margin-top:20px;margin-bottom:5px;\"><span class=\"content\"><strong>New</strong></span></h2>\n                {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"latest_small_list\" product_count=\"3\" aspect_ratio=\"0\" image_width=\"212\" image_height=\"212\" template=\"small_list.phtml\"}}\n            </div>\n            <div class=\"col-sm-4\">\n                <h2 class=\"filterproduct-title\" style=\"background-image:none;margin-top:20px;margin-bottom:5px;\"><span class=\"content\"><strong>Hot</strong></span></h2>\n                {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"latest_small_list\" product_count=\"3\" aspect_ratio=\"0\" image_width=\"212\" image_height=\"212\" template=\"small_list.phtml\"}}\n            </div>\n            <div class=\"col-sm-4\">\n                <h2 class=\"filterproduct-title\" style=\"background-image:none;margin-top:20px;margin-bottom:5px;\"><span class=\"content\"><strong>Sale</strong></span></h2>\n                {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"latest_small_list\" product_count=\"3\" aspect_ratio=\"0\" image_width=\"212\" image_height=\"212\" template=\"small_list.phtml\"}}\n            </div>\n        </div>\n        <div class=\"shop-features\" style=\"background-color:#fff; margin-top:13px;\">\n            <h2 class=\"shop-features-title\"><span>WHY SHOP WITH US?</span></h2>\n            <div class=\"row\">\n                <div class=\"col-sm-4\">\n                    <em class=\"porto-icon-star\"></em>\n                    <h3>DEDICATED SERVICE</h3>\n                    <p>Consult our specialists for help with an order, customization, or design advice.</p>\n                    <a href=\"#\">Get in Touch &gt;</a>\n                </div>\n                <div class=\"col-sm-4\">\n                    <em class=\"porto-icon-reply\"></em>\n                    <h3>FREE RETURNS</h3>\n                    <p>We stand behind our goods and services and want you to be satisfied with them.</p>\n                    <a href=\"#\">Returns Policy &gt;</a>\n                </div>\n                <div class=\"col-sm-4\">\n                    <em class=\"porto-icon-paper-plane\"></em>\n                    <h3>INTERNATIONAL SHIPPING</h3>\n                    <p>Currently over 50 countries qualify for express international shipping.</p>\n                    <a href=\"#\">Learn More &gt;</a>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<style type=\"text/css\">\n@media (min-width: 768px) {\n    .page-header .nav-sections {\n        display: none;\n    }\n}\n</style>','2016-10-21 23:35:35','2016-10-21 23:35:35',1,0,'',NULL,NULL,NULL,NULL,NULL,NULL),(16,'Porto - Homepage 10','1column',NULL,NULL,'porto_home_10',NULL,'<h2 class=\"filterproduct-title\"><span class=\"content\"><strong>FEATURED PRODUCTS</strong></span></h2>\n<div id=\"featured_product\" class=\"owl-top-narrow\">\n    {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"featured_product\" product_count=\"10\" aspect_ratio=\"1\" image_width=\"272\" image_height=\"337\" template=\"owl_list.phtml\"}}\n</div>\n<script type=\"text/javascript\">\n    require([\n        \'jquery\',\n        \'owl.carousel/owl.carousel.min\'\n    ], function ($) {\n        $(\"#featured_product .owl-carousel\").owlCarousel({\n            autoplay: true,\n            autoplayTimeout: 5000,\n            autoplayHoverPause: true,\n            loop: true,\n            navRewind: true,\n            margin: 10,\n            nav: true,\n            navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n            dots: false,\n            responsive: {\n                0: {\n                    items:2\n                },\n                768: {\n                    items:3\n                },\n                992: {\n                    items:3\n                },\n                1200: {\n                    items:4\n                }\n            }\n        });\n    });\n</script>\n<h2 class=\"filterproduct-title\" style=\"margin-bottom: 4px; margin-top: 20px;\"><span class=\"content\"><strong>BROWSE OUR CATEGORIES</strong></span></h2>\n<div id=\"category_slider\" class=\"owl-top-narrow\">\n    <div class=\"owl-carousel\">\n        <div class=\"item\" style=\"padding-top:0;\">\n            <a class=\"single-image\" href=\"#\">\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/10/content/category1.jpg\"}}\" alt=\"\" />\n                <p style=\"position:absolute;width:100%;bottom:13px;text-align:center;\"><span style=\"background-color:#333;background-color:rgba(23,23,23,.9);font-size:20.28px;color:#fff;font-weight:800;line-height:37px;display:inline-block;padding:0 10px;\">HOME & GARDEN</span></p>\n            </a>\n        </div>\n        <div class=\"item\" style=\"padding-top:0;\">\n            <a class=\"single-image\" href=\"#\">\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/10/content/category2.jpg\"}}\" alt=\"\" />\n                <p style=\"position:absolute;width:100%;bottom:13px;text-align:center;\"><span style=\"background-color:#333;background-color:rgba(23,23,23,.9);font-size:20.28px;color:#fff;font-weight:800;line-height:37px;display:inline-block;padding:0 10px;\">SPORTS</span></p>\n            </a>\n        </div>\n        <div class=\"item\" style=\"padding-top:0;\">\n            <a class=\"single-image\" href=\"#\">\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/10/content/category3.jpg\"}}\" alt=\"\" />\n                <p style=\"position:absolute;width:100%;bottom:13px;text-align:center;\"><span style=\"background-color:#333;background-color:rgba(23,23,23,.9);font-size:20.28px;color:#fff;font-weight:800;line-height:37px;display:inline-block;padding:0 10px;\">FASHION</span></p>\n            </a>\n        </div>\n        <div class=\"item\" style=\"padding-top:0;\">\n            <a class=\"single-image\" href=\"#\">\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/10/content/category4.jpg\"}}\" alt=\"\" />\n                <p style=\"position:absolute;width:100%;bottom:13px;text-align:center;\"><span style=\"background-color:#333;background-color:rgba(23,23,23,.9);font-size:20.28px;color:#fff;font-weight:800;line-height:37px;display:inline-block;padding:0 10px;\">ELECTRONICS</span></p>\n            </a>\n        </div>\n        <div class=\"item\" style=\"padding-top:0;\">\n            <a class=\"single-image\" href=\"#\">\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/10/content/category1.jpg\"}}\" alt=\"\" />\n                <p style=\"position:absolute;width:100%;bottom:13px;text-align:center;\"><span style=\"background-color:#333;background-color:rgba(23,23,23,.9);font-size:20.28px;color:#fff;font-weight:800;line-height:37px;display:inline-block;padding:0 10px;\">HOME & GARDEN</span></p>\n            </a>\n        </div>\n        <div class=\"item\" style=\"padding-top:0;\">\n            <a class=\"single-image\" href=\"#\">\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/10/content/category2.jpg\"}}\" alt=\"\" />\n                <p style=\"position:absolute;width:100%;bottom:13px;text-align:center;\"><span style=\"background-color:#333;background-color:rgba(23,23,23,.9);font-size:20.28px;color:#fff;font-weight:800;line-height:37px;display:inline-block;padding:0 10px;\">SPORTS</span></p>\n            </a>\n        </div>\n        <div class=\"item\" style=\"padding-top:0;\">\n            <a class=\"single-image\" href=\"#\">\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/10/content/category3.jpg\"}}\" alt=\"\" />\n                <p style=\"position:absolute;width:100%;bottom:13px;text-align:center;\"><span style=\"background-color:#333;background-color:rgba(23,23,23,.9);font-size:20.28px;color:#fff;font-weight:800;line-height:37px;display:inline-block;padding:0 10px;\">FASHION</span></p>\n            </a>\n        </div>\n        <div class=\"item\" style=\"padding-top:0;\">\n            <a class=\"single-image\" href=\"#\">\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/10/content/category4.jpg\"}}\" alt=\"\" />\n                <p style=\"position:absolute;width:100%;bottom:13px;text-align:center;\"><span style=\"background-color:#333;background-color:rgba(23,23,23,.9);font-size:20.28px;color:#fff;font-weight:800;line-height:37px;display:inline-block;padding:0 10px;\">ELECTRONICS</span></p>\n            </a>\n        </div>\n    </div>\n</div>\n<script type=\"text/javascript\">\n    require([\n        \'jquery\',\n        \'owl.carousel/owl.carousel.min\'\n    ], function ($) {\n        $(\"#category_slider .owl-carousel\").owlCarousel({\n            autoplay: true,\n            autoplayTimeout: 5000,\n            autoplayHoverPause: true,\n            loop: true,\n            navRewind: true,\n            margin: 10,\n            nav: true,\n            navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n            dots: false,\n            responsive: {\n                0: {\n                    items:2\n                },\n                768: {\n                    items:3\n                },\n                992: {\n                    items:3\n                },\n                1200: {\n                    items:4\n                }\n            }\n        });\n    });\n</script>\n<h2 class=\"filterproduct-title\" style=\"margin-top:20px;margin-bottom:5px;\"><span class=\"content\"><strong>JUST ARRIVED</strong></span></h2>\n{{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"featured_product\" product_count=\"12\" column_count=\"6\" move_actions=\"1\" aspect_ratio=\"0\" image_width=\"250\" image_height=\"250\" template=\"grid.phtml\"}}\n<div class=\"row\" style=\"margin-top:20px;\">\n    <div class=\"col-sm-6\">\n        <h2 class=\"filterproduct-title\" style=\"margin-top:20px;margin-bottom:5px;background:none;\"><span class=\"content\"><strong>LATEST BLOG POSTS</strong></span></h2>\n        <div id=\"latest_news\" class=\"recent-posts\">\n            <div class=\"owl-carousel\">\n                <div class=\"item\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-5\">\n                            <div class=\"post-image\">\n                                <img src=\"{{media url=\"wysiwyg/smartwave/porto/blog/03.jpg\"}}\" alt=\"\" />\n                            </div>\n                        </div>\n                        <div class=\"col-sm-7\">\n                            <div class=\"post-date\">\n                                <span class=\"day\">08</span>\n                                <span class=\"month\">Dec</span>\n                            </div>\n                            <div class=\"postTitle\">\n                                <h2><a href=\"#\">Post Format – Video</a></h2>\n                            </div>\n                            <div class=\"postContent\"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non placerat mi…</p></div>\n                            <a class=\"readmore\" href=\"#\">Read more &gt;</a>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"item\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-5\">\n                            <div class=\"post-image\">\n                                <img src=\"{{media url=\"wysiwyg/smartwave/porto/blog/04.jpg\"}}\" alt=\"\" />\n                            </div>\n                        </div>\n                        <div class=\"col-sm-7\">\n                            <div class=\"post-date\">\n                                <span class=\"day\">08</span>\n                                <span class=\"month\">Dec</span>\n                            </div>\n                            <div class=\"postTitle\">\n                                <h2><a href=\"#\">Post Format – Image Gallery</a></h2>\n                            </div>\n                            <div class=\"postContent\"><p>Euismod atras vulputate iltricies etri elit. Class aptent taciti sociosqu ad litora torquent…</p></div>\n                            <a class=\"readmore\" href=\"#\">Read more &gt;</a>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <script type=\"text/javascript\">\n            require([\n                \'jquery\',\n                \'owl.carousel/owl.carousel.min\'\n            ], function ($) {\n                $(\"#latest_news .owl-carousel\").owlCarousel({\n                    items: 1,\n                    loop: true,\n                    nav: false,\n                    dots: true\n                });\n            });\n        </script>\n    </div>\n    <div class=\"col-sm-6\">\n        <h2 class=\"filterproduct-title\" style=\"margin-top:20px;margin-bottom:5px;background:none;\"><span class=\"content\"><strong>WHAT CLIENT\'S SAY</strong></span></h2>\n        <div id=\"testimonials-slider-demo-9\" class=\"owl-carousel\">\n            <div class=\"item\">\n                <blockquote class=\"testimonial\">\n                    <p>Hello, I want to thank you for creating a great template and for the excellent and quick support and help that you have been providing to me as I begin to work with it.</p>\n                </blockquote>\n                <div class=\"testimonial-arrow-down\"></div>\n                <div class=\"testimonial-author\">\n                    <div class=\"img-thumbnail img-thumbnail-small\"><img width=\"60\" height=\"60\" src=\"{{media url=\"wysiwyg/smartwave/porto/testimonials/client-1-11.jpg\"}}\" alt=\"\"></div>\n                    <p><strong>John Smith</strong><span>CEO &amp; Founder - Okler</span></p>\n                </div>\n            </div>\n            <div class=\"item\">\n                <blockquote class=\"testimonial\">\n                    <p>Hello, I want to thank you for creating a great template and for the excellent and quick support and help that you have been providing to me as I begin to work with it.</p>\n                </blockquote>\n                <div class=\"testimonial-arrow-down\"></div>\n                <div class=\"testimonial-author\">\n                    <div class=\"img-thumbnail img-thumbnail-small\"><img width=\"60\" height=\"60\" src=\"{{media url=\"wysiwyg/smartwave/porto/testimonials/client-1-11.jpg\"}}\" alt=\"\"></div>\n                    <p><strong>John Smith</strong><span>CEO &amp; Founder - Okler</span></p>\n                </div>\n            </div>\n        </div>\n        <script type=\"text/javascript\">\n            require([\n                \'jquery\',\n                \'owl.carousel/owl.carousel.min\'\n            ], function ($) {\n                $(\"#testimonials-slider-demo-9\").owlCarousel({\n                    items: 1,\n                    autoplay: true,\n                    autoplayTimeout: 5000,\n                    autoplayHoverPause: true,\n                    dots: true,\n                    nav: false,\n                    navRewind: true,\n                    animateIn: \'fadeIn\',\n                    animateOut: \'fadeOut\',\n                    loop: true\n                });\n            });\n        </script>\n    </div>\n</div>\n<h2 class=\"filterproduct-title\" style=\"margin-bottom: 4px;\"><span class=\"content\"><strong>Featured Brands</strong></span></h2>\n<div id=\"brands-slider-demo-10\" class=\"brands-slider owl-top-narrow\">\n    <div class=\"owl-carousel\">\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand1.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand2.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand3.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand4.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand5.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand6.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand1.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand2.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand3.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand4.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand5.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand6.jpg\"}}\" alt=\"\" /></div>\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            $(\"#brands-slider-demo-10 .owl-carousel\").owlCarousel({\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                margin: 30,\n                nav: true,\n                navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n                dots: false,\n                loop: true,\n                responsive: {\n                    0: {\n                        items:2\n                    },\n                    640: {\n                        items:3\n                    },\n                    768: {\n                        items:4\n                    },\n                    992: {\n                        items:5\n                    },\n                    1200: {\n                        items:6\n                    }\n                }\n            });\n        });\n    </script>\n</div>','2016-10-21 23:35:35','2016-10-21 23:35:35',1,0,'<referenceContainer name=\"page.top\">\n    <block class=\"Magento\\Cms\\Block\\Block\" name=\"home_slider\">\n        <arguments>\n            <argument name=\"block_id\" xsi:type=\"string\">porto_homeslider_10</argument>\n        </arguments>\n    </block>\n</referenceContainer>',NULL,NULL,NULL,NULL,NULL,NULL),(17,'Porto - Homepage 11','1column',NULL,NULL,'porto_home_11',NULL,'{{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"featured_product\" product_count=\"8\" column_count=\"4\" aspect_ratio=\"0\" image_width=\"250\" image_height=\"250\" template=\"flex_grid.phtml\"}}\n<style type=\"text/css\">\n.products-grid.flex-grid {\n    margin: 0;\n}\n.page-main, .columns .column.main {\n    padding-top: 0;\n    padding-bottom: 0;\n}\n</style>\n<script type=\"text/javascript\">\n    require([\n        \'jquery\'        \n    ], function ($) {\n        $(\'.products.grid .product-items li.product-item:nth-child(2n)\').addClass(\'nth-child-2n\');\n        $(\'.products.grid .product-items li.product-item:nth-child(2n+1)\').addClass(\'nth-child-2np1\');\n        $(\'.products.grid .product-items li.product-item:nth-child(3n)\').addClass(\'nth-child-3n\');\n        $(\'.products.grid .product-items li.product-item:nth-child(3n+1)\').addClass(\'nth-child-3np1\');\n        $(\'.products.grid .product-items li.product-item:nth-child(4n)\').addClass(\'nth-child-4n\');\n        $(\'.products.grid .product-items li.product-item:nth-child(4n+1)\').addClass(\'nth-child-4np1\');\n        $(\'.products.grid .product-items li.product-item:nth-child(5n)\').addClass(\'nth-child-5n\');\n        $(\'.products.grid .product-items li.product-item:nth-child(5n+1)\').addClass(\'nth-child-5np1\');\n        $(\'.products.grid .product-items li.product-item:nth-child(6n)\').addClass(\'nth-child-6n\');\n        $(\'.products.grid .product-items li.product-item:nth-child(6n+1)\').addClass(\'nth-child-6np1\');\n        $(\'.products.grid .product-items li.product-item:nth-child(7n)\').addClass(\'nth-child-7n\');\n        $(\'.products.grid .product-items li.product-item:nth-child(7n+1)\').addClass(\'nth-child-7np1\');\n        $(\'.products.grid .product-items li.product-item:nth-child(8n)\').addClass(\'nth-child-8n\');\n        $(\'.products.grid .product-items li.product-item:nth-child(8n+1)\').addClass(\'nth-child-8np1\');\n    });\n</script>','2016-10-21 23:35:35','2016-10-21 23:35:35',1,0,'<referenceContainer name=\"page.top\">\n    <block class=\"Magento\\Cms\\Block\\Block\" name=\"home_slider\">\n        <arguments>\n            <argument name=\"block_id\" xsi:type=\"string\">porto_homeslider_11</argument>\n        </arguments>\n    </block>\n</referenceContainer>',NULL,NULL,NULL,NULL,NULL,NULL),(18,'Porto - Homepage 12','1column',NULL,NULL,'porto_home_12',NULL,'<div class=\"row\" style=\"background-color:#fff;margin-top:-20px;\">\n    <div class=\"col-md-12\">\n        <div class=\"row\">\n            <div class=\"col-md-3\"></div>\n            <div class=\"col-md-9\">\n                <div id=\"banner-slider-demo-12\" class=\"owl-carousel owl-banner-carousel owl-bottom-narrow\">\n                    <div class=\"item\">\n                        <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/12/slider/slide01.jpg\"}}\" alt=\"\" />\n                        <div class=\"content\">\n                            <span style=\"font-weight:300;color:#303030;display:inline-block;vertical-align:middle\">Now starting at <b style=\"font-weight:600;\">$99</b></span>\n                            <a class=\"btn btn-default\" style=\"background-color:#303030;color:#fff;position:inline-block;vertical-align:middle;font-weight:600\">SHOP NOW <i class=\"icon-right-dir\"></i></a>\n                        </div>\n                    </div>\n                    <div class=\"item\">\n                        <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/12/slider/slide02.jpg\"}}\" alt=\"\" />\n                    </div>\n                </div>\n                <script type=\"text/javascript\">\n                    require([\n                        \'jquery\',\n                        \'owl.carousel/owl.carousel.min\'\n                    ], function ($) {\n                        $(\"#banner-slider-demo-12\").owlCarousel({\n                            items: 1,\n                            autoplay: true,\n                            autoplayTimeout: 5000,\n                            autoplayHoverPause: true,\n                            dots: true,\n                            nav: false,\n                            navRewind: true,\n                            animateIn: \'fadeIn\',\n                            animateOut: \'fadeOut\',\n                            loop: true\n                        });\n                    });\n                </script>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"homepage-bar\">\n                <div class=\"col-md-4\">\n                    <em class=\"porto-icon-truck\"></em><div class=\"text-area\"><h3>FREE SHIPPING &amp; FREE RETURNS</h3><p>Free shipping on all orders over $99.</p></div>\n                </div>\n                <div class=\"col-md-4\">\n                    <em class=\"porto-icon-dollar\"></em><div class=\"text-area\"><h3>MONEY BACK GUARANTEE</h3><p>100% money back guarantee.</p></div>\n                </div>\n                <div class=\"col-md-4\">\n                    <em class=\"porto-icon-lifebuoy\"></em><div class=\"text-area\"><h3>ONLINE SUPPORT 24/7</h3><p>Lorem ipsum dolor sit amet.</p></div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"row\" style=\"background-color:#fff;margin-top:10px;\">\n    <div class=\"col-md-12\">\n        {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"latest_product\" product_count=\"8\" column_count=\"4\" aspect_ratio=\"1\" image_width=\"300\" template=\"grid.phtml\"}}\n    </div>\n</div>\n<div class=\"row\" style=\"background-color:#fff;margin:10px -15px;\">\n    <a href=\"#\" style=\"display:block;\"><img src=\"http://192.168.0.143/magento/porto/media/wysiwyg/porto/homepage/content/12/huge_sale.jpg\" alt=\"\" style=\"width: 100%;\"></a>\n</div>\n<div class=\"row\" style=\"margin: 15px -23px 0 -23px;\">\n    <div class=\"col-sm-4 one-product\" style=\"padding-left: 8px; padding-right: 8px;\">\n        <h4 class=\"filterproduct-title\">FEATURED</h4>\n        <div id=\"featured_product\" class=\"hide-addtocart hide-addtolinks\">\n            {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"featured_product\" product_count=\"3\" aspect_ratio=\"0\" image_width=\"350\" image_height=\"350\" template=\"owl_list.phtml\"}}\n        </div>\n        <script type=\"text/javascript\">\n            require([\n                \'jquery\',\n                \'owl.carousel/owl.carousel.min\'\n            ], function ($) {\n                $(\"#featured_product .owl-carousel\").owlCarousel({\n                    autoplay: true,\n                    autoplayTimeout: 5000,\n                    autoplayHoverPause: true,\n                    nav: false,\n                    dots: true,\n                    navRewind: true,\n                    animateIn: \'fadeIn\',\n                    animateOut: \'fadeOut\',\n                    loop: true,\n                    items: 1\n                });\n            });\n        </script>\n    </div>\n    <div class=\"col-sm-4 one-product\" style=\"padding-left: 8px; padding-right: 8px;\">\n        <h4 class=\"filterproduct-title\">SALES</h4>\n        <div id=\"sales_product\" class=\"hide-addtocart hide-addtolinks\">\n            {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"featured_product\" product_count=\"3\" aspect_ratio=\"0\" image_width=\"350\" image_height=\"350\" template=\"owl_list.phtml\"}}\n        </div>\n        <script type=\"text/javascript\">\n            require([\n                \'jquery\',\n                \'owl.carousel/owl.carousel.min\'\n            ], function ($) {\n                $(\"#sales_product .owl-carousel\").owlCarousel({\n                    autoplay: true,\n                    autoplayTimeout: 5000,\n                    autoplayHoverPause: true,\n                    nav: false,\n                    dots: true,\n                    navRewind: true,\n                    animateIn: \'fadeIn\',\n                    animateOut: \'fadeOut\',\n                    loop: true,\n                    items: 1\n                });\n            });\n        </script>\n    </div>\n    <div class=\"col-sm-4 one-product\" style=\"padding-left: 8px; padding-right: 8px;\">\n        <h4 class=\"filterproduct-title\">NEW ARRIVALS</h4>\n        <div id=\"new_product\" class=\"hide-addtocart hide-addtolinks\">\n            {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"featured_product\" product_count=\"3\" aspect_ratio=\"0\" image_width=\"350\" image_height=\"350\" template=\"owl_list.phtml\"}}\n        </div>\n        <script type=\"text/javascript\">\n            require([\n                \'jquery\',\n                \'owl.carousel/owl.carousel.min\'\n            ], function ($) {\n                $(\"#new_product .owl-carousel\").owlCarousel({\n                    autoplay: true,\n                    autoplayTimeout: 5000,\n                    autoplayHoverPause: true,\n                    nav: false,\n                    dots: true,\n                    navRewind: true,\n                    animateIn: \'fadeIn\',\n                    animateOut: \'fadeOut\',\n                    loop: true,\n                    items: 1\n                });\n            });\n        </script>\n    </div>\n</div>\n<div class=\"row\" style=\"background-color:#fff;margin-top:15px;padding:15px 0;\">\n    <div class=\"col-md-12\">\n        <h2 class=\"filterproduct-title\" style=\"background:none;\"><span class=\"content\" style=\"background:none;\"><strong>FASHION SELECTION</strong></span></h2>\n        <div id=\"featured_product_list\" class=\"owl-top-narrow\">\n            {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"featured_product\" product_count=\"10\" aspect_ratio=\"1\" image_width=\"272\" image_height=\"337\" template=\"owl_list.phtml\"}}\n        </div>\n        <script type=\"text/javascript\">\n            require([\n                \'jquery\',\n                \'owl.carousel/owl.carousel.min\'\n            ], function ($) {\n                $(\"#featured_product_list .owl-carousel\").owlCarousel({\n                    autoplay: true,\n                    autoplayTimeout: 5000,\n                    autoplayHoverPause: true,\n                    loop: true,\n                    navRewind: true,\n                    margin: 10,\n                    nav: true,\n                    navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n                    dots: false,\n                    responsive: {\n                        0: {\n                            items:2\n                        },\n                        768: {\n                            items:3\n                        },\n                        992: {\n                            items:3\n                        },\n                        1200: {\n                            items:4\n                        }\n                    }\n                });\n            });\n        </script>\n    </div>\n</div>\n<style type=\"text/css\">\n.page-main {\n    background-color: transparent;\n    padding-bottom: 0;\n}\n.homepage-bar {\n    border-color: #f5f5f5;\n    border-top: 0;\n}\n.homepage-bar p {\n    font-size: 11px;\n    margin-left: 5px;\n}\n.homepage-bar h3 {\n    font-size: 13px;\n}\n.homepage-bar .col-md-4 em {\n    font-size: 20px;\n    margin-top: -10px;\n}\n.homepage-bar .col-md-4 {\n    text-align: center;\n    border-color: #f5f5f5;\n}\n.product-item-photo {\n    padding: 0;\n}\n</style>','2016-10-21 23:35:36','2016-10-21 23:35:36',1,0,'',NULL,NULL,NULL,NULL,NULL,NULL),(19,'Porto - Homepage 13','1column',NULL,NULL,'porto_home_13',NULL,'<p></p>','2016-10-21 23:35:36','2016-10-21 23:35:36',1,0,'<referenceContainer name=\"page.top\">\n    <block class=\"Magento\\Cms\\Block\\Block\" name=\"home_slider\">\n        <arguments>\n            <argument name=\"block_id\" xsi:type=\"string\">porto_homeslider_13</argument>\n        </arguments>\n    </block>\n</referenceContainer>',NULL,NULL,NULL,NULL,NULL,NULL),(20,'Porto - Homepage 14','1column',NULL,NULL,'porto_home_14',NULL,'<style type=\"text/css\">\nbody.boxed {\n    padding: 0;\n}\nbody.boxed .page-wrapper {\n    width: 100%;\n}\n.cms-index-index .page-header.type9 {\n    background-color: transparent;\n    position: absolute;\n    width: 100%;\n    left: 0;\n    top: 0;\n    border-bottom: 1px solid rgba(255,255,255,.25);\n}\n</style>\n\n<div class=\"row\">\n    <div class=\"col-sm-12\">\n        <h2 style=\"margin-top:30px;font-size:19px;font-weight:600;margin-bottom:20px;text-align:center;\" class=\"theme-color\">NEW ARRIVALS</h2>\n        <div id=\"latest_product\" class=\"hide-addtocart hide-addtolinks\">\n            {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"latest_product\" product_count=\"10\" aspect_ratio=\"0\" image_width=\"212\" image_height=\"263\" template=\"owl_list.phtml\"}}\n        </div>\n        <script type=\"text/javascript\">\n            require([\n                \'jquery\',\n                \'owl.carousel/owl.carousel.min\'\n            ], function ($) {\n                $(\"#latest_product .owl-carousel\").owlCarousel({\n                    autoplay: true,\n                    autoplayTimeout: 5000,\n                    autoplayHoverPause: true,\n                    loop: true,\n                    navRewind: true,\n                    margin: 10,\n                    nav: false,\n                    navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n                    dots: false,\n                    responsive: {\n                        0: {\n                            items:2\n                        },\n                        768: {\n                            items:3\n                        },\n                        992: {\n                            items:4\n                        },\n                        1200: {\n                            items:5\n                        }\n                    }\n                });\n            });\n        </script>\n    </div>\n</div>\n<div class=\"row\">\n    <div class=\"col-sm-12\">\n        <h2 style=\"margin-top:30px;font-size:19px;font-weight:600;margin-bottom:20px;text-align:center;\" class=\"theme-color\">FROM THE BLOG</h2>\n        <div id=\"latest_news\" class=\"owl-top-narrow\">\n            <!--{-{block class=\"Magefan\\Blog\\Block\\Sidebar\\Recent\" name=\"blog.home.recent\" template=\"recent_home.phtml\"}-}-->\n			<div class=\"recent-posts\">\n				<div class=\"owl-carousel\">\n					<div class=\"item\">\n						<div class=\"row\">\n							<div class=\"col-sm-5\">\n								<div class=\"post-image\">\n									<img src=\"{{media url=\"wysiwyg/smartwave/porto/blog/03.jpg\"}}\" alt=\"\" />\n								</div>\n							</div>\n							<div class=\"col-sm-7\">\n								<div class=\"post-date\">\n									<span class=\"day\">08</span>\n									<span class=\"month\">Dec</span>\n								</div>\n								<div class=\"postTitle\">\n									<h2><a href=\"#\">Post Format – Video</a></h2>\n								</div>\n								<div class=\"postContent\"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non placerat mi…</p></div>\n								<a class=\"readmore\" href=\"#\">Read more &gt;</a>\n							</div>\n						</div>\n					</div>\n					<div class=\"item\">\n						<div class=\"row\">\n							<div class=\"col-sm-5\">\n								<div class=\"post-image\">\n									<img src=\"{{media url=\"wysiwyg/smartwave/porto/blog/04.jpg\"}}\" alt=\"\" />\n								</div>\n							</div>\n							<div class=\"col-sm-7\">\n								<div class=\"post-date\">\n									<span class=\"day\">08</span>\n									<span class=\"month\">Dec</span>\n								</div>\n								<div class=\"postTitle\">\n									<h2><a href=\"#\">Post Format – Image Gallery</a></h2>\n								</div>\n								<div class=\"postContent\"><p>Euismod atras vulputate iltricies etri elit. Class aptent taciti sociosqu ad litora torquent…</p></div>\n								<a class=\"readmore\" href=\"#\">Read more &gt;</a>\n							</div>\n						</div>\n					</div>\n				</div>\n			</div>\n			<script type=\"text/javascript\">\n				require([\n					\'jquery\',\n					\'owl.carousel/owl.carousel.min\'\n				], function ($) {\n					$(\"#latest_news .owl-carousel\").owlCarousel({\n						loop: false,\n						margin: 10,\n						nav: false,\n						navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n						dots: false,\n						responsive: {\n							0: {\n								items:1\n							},\n							640: {\n								items:2\n							},\n							768: {\n								items:2\n							},\n							992: {\n								items:2\n							},\n							1200: {\n								items:2\n							}\n						}\n					});\n				});\n			</script>\n        </div>\n    </div>\n</div>','2016-10-21 23:35:36','2016-10-21 23:35:36',1,0,'<referenceContainer name=\"page.top\">\n    <block class=\"Magento\\Cms\\Block\\Block\" name=\"home_slider\">\n        <arguments>\n            <argument name=\"block_id\" xsi:type=\"string\">porto_homeslider_14</argument>\n        </arguments>\n    </block>\n</referenceContainer>',NULL,NULL,NULL,NULL,NULL,NULL),(21,'Porto - Homepage 15','1column',NULL,NULL,'porto_home_15',NULL,'<style type=\"text/css\">\n.columns .column.main {\n    padding-bottom: 0;\n}\n.page-main {\n    padding-top: 8px;\n    padding-bottom: 8px;\n}\n</style>\n<div class=\"row\" style=\"margin-left:-8px;margin-right:-8px;\">\n    <div class=\"col-sm-4\" style=\"margin:8px 0;padding-left: 8px;padding-right: 8px;\">\n        <div style=\"background-color:#2b262f;position:relative;\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/15/content/blank.png\"}}\" alt=\"\" style=\"width:100%;max-height:480px;\">\n            <div class=\"img-desc-home15\" style=\"position:absolute;top:50%;width:100%;font-family:Georgia;\">\n                <h3 style=\"color:#f2f4f3;font-weight:400;line-height:1;\">The Wedding Day</h3>\n                <hr style=\"margin:15px 0;border-color:#ada4b5;\">\n                <p style=\"color:#ada4b5;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a arcu lacinia, ornare lorem maximus, consequat mauris. Nam vitae risus at leo convallis sagittis.</p>\n                <a href=\"#\" style=\"color:#fff;\">SHOP NOW</a>\n            </div>\n        </div>\n    </div>\n    <div class=\"col-sm-8\" style=\"margin:8px 0;padding-left: 8px;padding-right: 8px;\">\n        <a href=\"#\" class=\"image-link\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/15/content/image_link_new_1.jpg\"}}\" alt=\"\" style=\"width:100%;\">\n        </a>\n    </div>\n</div>\n<div class=\"row\" style=\"margin-left:-8px;margin-right:-8px;\">\n    <div class=\"col-sm-8\" style=\"margin:8px 0;padding-left: 8px;padding-right: 8px;\">\n        <a href=\"#\" class=\"image-link\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/15/content/image_link_new_2.jpg\"}}\" alt=\"\" style=\"width:100%;\">\n        </a>\n    </div>\n    <div class=\"col-sm-4\" style=\"margin:8px 0;padding-left: 8px;padding-right: 8px;\">\n        <div style=\"background-color:#b7a788;position:relative;\">\n            <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/15/content/blank.png\"}}\" alt=\"\" style=\"width:100%;max-height:480px;\">\n            <div class=\"img-desc-home15\" style=\"position:absolute;top:50%;width:100%;font-family:Georgia;\">\n                <h3 style=\"color:#2b262e;font-weight:400;line-height:1;\">Valentine’s Day</h3>\n                <hr style=\"margin:15px 0;border-color:#564a34;\">\n                <p style=\"color:#564a34;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a arcu lacinia, ornare lorem maximus, consequat mauris. Nam vitae risus at leo convallis sagittis.</p>\n                <a href=\"#\" style=\"color:#2b262f;\">SHOP NOW</a>\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"row\" style=\"margin-left:-8px;margin-right:-8px;\">\n    <div class=\"col-md-3 col-sm-6 col-xs-6\" style=\"padding-left: 8px;padding-right: 8px;\">\n        <a class=\"image-link\" href=\"#\" style=\"margin:8px 0;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/15/content/image_link_new_3.jpg\"}}\" alt=\"\" style=\"width:100%;\"></a>\n    </div>\n    <div class=\"col-md-3 col-sm-6 col-xs-6 md-f-right\" style=\"padding-left: 8px;padding-right: 8px;\">\n        <a class=\"image-link\" href=\"#\" style=\"margin:8px 0;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/15/content/image_link_new_5.jpg\"}}\" alt=\"\" style=\"width:100%;\"></a>\n    </div>\n    <div class=\"col-md-6 col-sm-12 col-xs-12\" style=\"padding-left: 8px;padding-right: 8px;\">\n        <a class=\"image-link\" href=\"#\" style=\"margin:8px 0;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/15/content/image_link_new_4.jpg\"}}\" alt=\"\" style=\"width:100%;\"></a>\n    </div>\n</div>','2016-10-21 23:35:36','2016-10-21 23:35:36',1,0,'<referenceContainer name=\"page.top\">\n    <block class=\"Magento\\Cms\\Block\\Block\" name=\"home_slider\">\n        <arguments>\n            <argument name=\"block_id\" xsi:type=\"string\">porto_homeslider_15</argument>\n        </arguments>\n    </block>\n</referenceContainer>',NULL,NULL,NULL,NULL,NULL,NULL),(22,'Porto - Homepage 16','1column',NULL,NULL,'porto_home_16',NULL,'<style type=\"text/css\">\n.page-main {\n    display: none;\n}\n.page-header.type10 {\n    background-color: rgba(0,0,0,.5);\n}\n.page-footer {\n    display: none;\n}\n.full-screen-slider {\n    position: fixed;\n    left: 0;\n    top: 0;\n    z-index: 1;\n    width: 100vw;\n    height: 100vh;\n}\n</style>','2016-10-21 23:35:36','2016-10-21 23:35:36',1,0,'<referenceContainer name=\"page.top\">\n    <block class=\"Magento\\Cms\\Block\\Block\" name=\"home_slider\">\n        <arguments>\n            <argument name=\"block_id\" xsi:type=\"string\">porto_homeslider_16</argument>\n        </arguments>\n    </block>\n</referenceContainer>',NULL,NULL,NULL,NULL,NULL,NULL),(23,'Porto - Homepage 17','1column',NULL,NULL,'porto_home_17',NULL,'<h2 class=\"filterproduct-title\" style=\"margin-bottom: 4px;\"><span class=\"content\"><strong>Featured Brands</strong></span></h2>\n<div id=\"brands-slider-demo-17\" class=\"brands-slider owl-top-narrow\">\n    <div class=\"owl-carousel\">\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand1.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand2.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand3.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand4.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand5.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand6.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand1.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand2.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand3.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand4.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand5.jpg\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/brand_logos/brand6.jpg\"}}\" alt=\"\" /></div>\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            $(\"#brands-slider-demo-17 .owl-carousel\").owlCarousel({\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                margin: 30,\n                nav: true,\n                navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n                dots: false,\n                loop: true,\n                responsive: {\n                    0: {\n                        items:2\n                    },\n                    640: {\n                        items:3\n                    },\n                    768: {\n                        items:4\n                    },\n                    992: {\n                        items:5\n                    },\n                    1200: {\n                        items:6\n                    }\n                }\n            });\n        });\n    </script>\n</div>\n<div class=\"row\">\n    <div class=\"col-sm-3\">\n        <h2 class=\"filterproduct-title\" style=\"background-image:none;margin-top:20px;margin-bottom:5px;\"><span class=\"content\"><strong>Featured</strong></span></h2>\n        {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"latest_small_list\" product_count=\"3\" aspect_ratio=\"0\" image_width=\"212\" image_height=\"212\" template=\"small_list.phtml\"}}\n    </div>\n    <div class=\"col-sm-3\">\n        <h2 class=\"filterproduct-title\" style=\"background-image:none;margin-top:20px;margin-bottom:5px;\"><span class=\"content\"><strong>New</strong></span></h2>\n        {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"latest_small_list\" product_count=\"3\" aspect_ratio=\"0\" image_width=\"212\" image_height=\"212\" template=\"small_list.phtml\"}}\n    </div>\n    <div class=\"col-sm-3\">\n        <h2 class=\"filterproduct-title\" style=\"background-image:none;margin-top:20px;margin-bottom:5px;\"><span class=\"content\"><strong>Hot</strong></span></h2>\n        {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"latest_small_list\" product_count=\"3\" aspect_ratio=\"0\" image_width=\"212\" image_height=\"212\" template=\"small_list.phtml\"}}\n    </div>\n    <div class=\"col-sm-3\">\n        <h2 class=\"filterproduct-title\" style=\"background-image:none;margin-top:20px;margin-bottom:5px;\"><span class=\"content\"><strong>Sale</strong></span></h2>\n        {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\LatestList\" name=\"latest_small_list\" product_count=\"3\" aspect_ratio=\"0\" image_width=\"212\" image_height=\"212\" template=\"small_list.phtml\"}}\n    </div>\n</div>\n<style type=\"text/css\">\n.homepage-bar .col-md-4 {\n    padding-top: 15px;\n    padding-bottom: 15px;\n    height: 80px;\n    text-align: center;\n}\n</style>','2016-10-21 23:35:36','2016-10-21 23:35:36',1,0,'<referenceContainer name=\"page.top\">\n    <block class=\"Magento\\Cms\\Block\\Block\" name=\"home_slider\">\n        <arguments>\n            <argument name=\"block_id\" xsi:type=\"string\">porto_homeslider_17</argument>\n        </arguments>\n    </block>\n</referenceContainer>',NULL,NULL,NULL,NULL,NULL,NULL),(24,'Porto - Homepage 18','1column',NULL,NULL,'porto_home_18',NULL,'<div class=\"single-images\">\n    <div class=\"row\" style=\"margin-left:-10px;margin-right:-10px;\">\n        <div class=\"col-sm-4\" style=\"padding:10px\">\n            <div class=\"single-image-label\">SHOP LIVING ROOM</div>\n            <a class=\"image-link\" href=\"#\">\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/18/content/image01.jpg\"}}\" alt=\"\">\n            </a>\n            <div class=\"caption\">LIVING ROOM STARTING AT $999</div>\n        </div>\n        <div class=\"col-sm-4\" style=\"padding:10px\">\n            <div class=\"single-image-label\">SHOP DINING ROOM</div>\n            <a class=\"image-link\" href=\"#\">\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/18/content/image02.jpg\"}}\" alt=\"\">\n            </a>\n            <div class=\"caption\">DINING ROOM STARTING AT $999</div>\n        </div>\n        <div class=\"col-sm-4\" style=\"padding:10px\">\n            <div class=\"single-image-label\">SHOP BEDROOM</div>\n            <a class=\"image-link\" href=\"#\">\n                <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/18/content/image03.jpg\"}}\" alt=\"\">\n            </a>\n            <div class=\"caption\">BEDROOM STARTING AT $999</div>\n        </div>\n     </div>\n</div>\n<div style=\"padding-top: 20px;\">\n    <h2 class=\"filterproduct-title\" style=\"background-image:none;font-size:17px;text-transform:none;color:#828385;font-weight:600;\"><span class=\"content\"><strong>Top Selling Items</strong></span></h2>\n    <div id=\"featured_product\" class=\"owl-top-narrow hide-addtolinks\">\n        {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"featured_product\" product_count=\"10\" move_actions=\"1\" aspect_ratio=\"0\" image_width=\"212\" image_height=\"263\" template=\"owl_list.phtml\"}}\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            $(\"#featured_product .owl-carousel\").owlCarousel({\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                loop: true,\n                navRewind: true,\n                margin: 10,\n                nav: true,\n                navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n                dots: false,\n                responsive: {\n                    0: {\n                        items:2\n                    },\n                    768: {\n                        items:3\n                    },\n                    992: {\n                        items:4\n                    },\n                    1200: {\n                        items:5\n                    }\n                }\n            });\n        });\n    </script>\n</div>\n<div id=\"brands-slider-demo-18\" class=\"brands-slider\" style=\"padding-top: 50px;\">\n    <div class=\"owl-carousel\">\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand1.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand2.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand3.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand4.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand5.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand1.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand2.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand3.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand4.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand5.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand1.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand2.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand3.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand4.png\"}}\" alt=\"\" /></div>\n        <div class=\"item\" style=\"padding-top:10px;\"><img src=\"{{media url=\"wysiwyg/smartwave/porto/brands/brand5.png\"}}\" alt=\"\" /></div>\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            $(\"#brands-slider-demo-18 .owl-carousel\").owlCarousel({\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                margin: 30,\n                nav: false,\n                navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n                dots: true,\n                loop: true,\n                responsive: {\n                    0: {\n                        items:2\n                    },\n                    640: {\n                        items:3\n                    },\n                    768: {\n                        items:4\n                    },\n                    992: {\n                        items:5\n                    },\n                    1200: {\n                        items:6\n                    }\n                }\n            });\n        });\n    </script>\n</div>','2016-10-21 23:35:36','2016-10-21 23:35:36',1,0,'<referenceContainer name=\"page.top\">\n    <block class=\"Magento\\Cms\\Block\\Block\" name=\"home_slider\">\n        <arguments>\n            <argument name=\"block_id\" xsi:type=\"string\">porto_homeslider_18</argument>\n        </arguments>\n    </block>\n</referenceContainer>',NULL,NULL,NULL,NULL,NULL,NULL),(25,'Porto - Homepage 19','1column',NULL,NULL,'porto_home_19',NULL,'{{block class=\"Smartwave\\Porto\\Block\\Template\" template=\"Smartwave_Megamenu::onepagecategory/onepagecategory.phtml\" aspect_ratio=\"1\" image_width=\"300\" image_height=\"300\" lazy_owl=\"1\" columns=\"{ 0:{items:2},768:{items:3},992:{items:4},1200:{items:5} }\" product_count=\"12\"}}','2016-10-21 23:35:37','2016-10-21 23:35:37',1,0,'<referenceContainer name=\"page.top\">\n    <block class=\"Magento\\Cms\\Block\\Block\" name=\"home_slider\">\n        <arguments>\n            <argument name=\"block_id\" xsi:type=\"string\">porto_homeslider_19</argument>\n        </arguments>\n    </block>\n</referenceContainer>',NULL,NULL,NULL,NULL,NULL,NULL),(26,'Porto - Homepage 20','1column',NULL,NULL,'porto_home_20',NULL,'<div style=\"padding: 0 15px;margin-bottom: 50px;\">\n    <h2 class=\"filterproduct-title\"><span class=\"content\"><strong>Featured Items</strong></span></h2>\n    <div id=\"featured_product\">\n        {{block class=\"Smartwave\\Filterproducts\\Block\\Home\\FeaturedList\" name=\"featured_product\" product_count=\"10\" aspect_ratio=\"1\" image_width=\"212\" category_id=\"94\" template=\"owl_list.phtml\"}}\n    </div>\n    <script type=\"text/javascript\">\n        require([\n            \'jquery\',\n            \'owl.carousel/owl.carousel.min\'\n        ], function ($) {\n            $(\"#featured_product .owl-carousel\").owlCarousel({\n                autoplay: true,\n                autoplayTimeout: 5000,\n                autoplayHoverPause: true,\n                loop: true,\n                navRewind: true,\n                margin: 10,\n                nav: false,\n                navText: [\"<em class=\'porto-icon-left-open-huge\'></em>\",\"<em class=\'porto-icon-right-open-huge\'></em>\"],\n                dots: true,\n                responsive: {\n                    0: {\n                        items:2\n                    },\n                    768: {\n                        items:3\n                    },\n                    992: {\n                        items:4\n                    },\n                    1200: {\n                        items:5\n                    }\n                }\n            });\n        });\n    </script>\n</div>\n<div id=\"home-content-slider-demo-20\" class=\"owl-carousel owl-bottom-narrow owl-banner-carousel\">\n    <div class=\"item\" style=\"position:relative;\">\n        <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/20/content/home_shoes.jpg\"}}\" alt=\"\" />\n        <div class=\"content\" style=\"position:absolute;right:15%;top:20%\">\n            <h3>Adidas Ultra Boost</h3>\n            <p><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/20/content/shoes.png\"}}\" alt=\"\" /></p>\n            <a href=\"#\">Shop Now</a>\n        </div>\n    </div>\n    <div class=\"item\" style=\"position:relative;\">\n        <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/20/content/home_shoes.jpg\"}}\" alt=\"\" />\n        <div class=\"content\" style=\"position:absolute;right:15%;top:20%\">\n            <h3>Adidas Ultra Boost</h3>\n            <p><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/20/content/shoes.png\"}}\" alt=\"\" /></p>\n            <a href=\"#\">Shop Now</a>\n        </div>\n    </div>\n    <div class=\"item\" style=\"position:relative;\">\n        <img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/20/content/home_shoes.jpg\"}}\" alt=\"\" />\n        <div class=\"content\" style=\"position:absolute;right:15%;top:20%\">\n            <h3>Adidas Ultra Boost</h3>\n            <p><img src=\"{{media url=\"wysiwyg/smartwave/porto/homepage/20/content/shoes.png\"}}\" alt=\"\" /></p>\n            <a href=\"#\">Shop Now</a>\n        </div>\n    </div>\n</div>\n<script type=\"text/javascript\">\n    require([\n        \'jquery\',\n        \'owl.carousel/owl.carousel.min\'\n    ], function ($) {\n        var owl_20 = $(\"#home-content-slider-demo-20\").owlCarousel({\n            items: 1,\n            autoplay: true,\n            autoplayTimeout: 5000,\n            autoplayHoverPause: true,\n            dots: true,\n            navRewind: true,\n            animateIn: \'fadeIn\',\n            animateOut: \'fadeOut\',\n            loop: true,\n            nav: false,\n            navText: [\"<em class=\'porto-icon-chevron-left\'></em>\",\"<em class=\'porto-icon-chevron-right\'></em>\"]\n        });\n    });\n</script>\n{{block class=\"Smartwave\\Porto\\Block\\Template\" template=\"ajaxproducts/ajaxproducts.phtml\"}}\n<div class=\"full-width-image\" style=\"background:url({{media url=\"wysiwyg/smartwave/porto/homepage/20/content/home_golf.jpg\"}}) center no-repeat;background-size: cover;\">\n    <h3>Explore the best of you.</h3>\n    <a href=\"#\">Shop By Golf</a>\n</div>\n<style type=\"text/css\">\n@media (min-width: 768px) {\n    #featured_product {\n        padding: 0 85px;\n    }\n}\nh2.filterproduct-title {\n    background: none;\n    font-size: 40px;\n    font-weight: 600;\n    line-height: 1;\n    text-align: center;\n    color: #000;\n    margin-top: 90px;\n    margin-bottom: 80px;\n}\n.filterproduct-title .content {\n    padding: 0;\n    background-color: transparent;\n}\nbody.layout-fullwidth.cms-index-index .page-main {\n    padding: 0;\n}\n.columns .column.main {\n    padding: 0;\n}\n.owl-theme .owl-controls {\n    margin: 0;\n}\n#home-content-slider-demo-20.owl-theme .owl-dots .owl-dot span {\n    background: transparent;\n    border: 2px solid #fff;\n}\n#home-content-slider-demo-20.owl-theme .owl-dots .owl-dot.active span, #home-content-slider-demo-20 .owl-theme .owl-dots .owl-dot:hover span {\n    background: #fff;\n}\n#home-content-slider-demo-20.owl-bottom-narrow .owl-controls {\n    bottom: 50px;\n}\n.product-item-photo {\n    padding: 0;\n    border: 0;\n}\n.product.actions, .product-item .product-reviews-summary {\n    display: none;\n}\n.product-item-name.product.name > a {\n    font-size: 12px;\n    font-weight: 600;\n    text-transform: uppercase;\n    color: #000;\n}\n.product-item-name.product.name > a:hover {\n    text-decoration: none;\n}\n.page-header.type13 {\n    background: transparent;\n    position: absolute;\n    left: 0;\n    top: 0;\n    width: 100%;\n    border: 0;\n}\n@media (max-width: 767px) {\n    h2.filterproduct-title {\n        margin: 40px 0;\n        font-size: 25px;\n    }\n    #home-content-slider-demo-20.owl-bottom-narrow .owl-controls {\n        bottom: 5px;\n    }\n}\n</style>','2016-10-21 23:35:37','2016-10-21 23:35:37',1,0,'<referenceContainer name=\"page.top\">\n    <block class=\"Magento\\Cms\\Block\\Block\" name=\"home_slider\">\n        <arguments>\n            <argument name=\"block_id\" xsi:type=\"string\">porto_homeslider_20</argument>\n        </arguments>\n    </block>\n</referenceContainer>',NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `cms_page_store` */

DROP TABLE IF EXISTS `cms_page_store`;

CREATE TABLE `cms_page_store` (
  `page_id` smallint(6) NOT NULL COMMENT 'Page ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  PRIMARY KEY (`page_id`,`store_id`),
  KEY `CMS_PAGE_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `CMS_PAGE_STORE_PAGE_ID_CMS_PAGE_PAGE_ID` FOREIGN KEY (`page_id`) REFERENCES `cms_page` (`page_id`) ON DELETE CASCADE,
  CONSTRAINT `CMS_PAGE_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS Page To Store Linkage Table';

/*Data for the table `cms_page_store` */

insert  into `cms_page_store`(`page_id`,`store_id`) values (1,0),(2,0),(3,0),(4,0),(5,0),(6,0),(7,0),(8,0),(9,0),(10,0),(11,0),(12,0),(13,0),(14,0),(15,0),(16,0),(17,0),(18,0),(19,0),(20,0),(21,0),(22,0),(23,0),(24,0),(25,0),(26,0);

/*Table structure for table `core_config_data` */

DROP TABLE IF EXISTS `core_config_data`;

CREATE TABLE `core_config_data` (
  `config_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Config Id',
  `scope` varchar(8) NOT NULL DEFAULT 'default' COMMENT 'Config Scope',
  `scope_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Config Scope Id',
  `path` varchar(255) NOT NULL DEFAULT 'general' COMMENT 'Config Path',
  `value` text COMMENT 'Config Value',
  PRIMARY KEY (`config_id`),
  UNIQUE KEY `CORE_CONFIG_DATA_SCOPE_SCOPE_ID_PATH` (`scope`,`scope_id`,`path`)
) ENGINE=InnoDB AUTO_INCREMENT=270 DEFAULT CHARSET=utf8 COMMENT='Config Data';

/*Data for the table `core_config_data` */

insert  into `core_config_data`(`config_id`,`scope`,`scope_id`,`path`,`value`) values (1,'default',0,'web/seo/use_rewrites','1'),(4,'default',0,'general/locale/code','en_US'),(5,'default',0,'web/secure/use_in_frontend',NULL),(6,'default',0,'web/secure/use_in_adminhtml',NULL),(7,'default',0,'general/locale/timezone','UTC'),(8,'default',0,'currency/options/base','USD'),(9,'default',0,'currency/options/default','USD'),(10,'default',0,'currency/options/allow','USD'),(11,'default',0,'general/region/display_all','1'),(12,'default',0,'general/region/state_required','AT,BR,CA,CH,EE,ES,FI,LT,LV,RO,US'),(13,'default',0,'catalog/category/root_id','2'),(14,'default',0,'web/default/cms_home_page','porto_home_1'),(15,'default',0,'web/default/cms_no_route','no-route-2'),(16,'default',0,'cms/wysiwyg/enabled','hidden'),(17,'default',0,'design/header/logo_width',NULL),(18,'default',0,'design/header/logo_height',NULL),(20,'default',0,'dev/js/merge_files','1'),(21,'default',0,'porto_settings/general/boxed','wide'),(22,'default',0,'porto_settings/general/layout','1170'),(23,'default',0,'porto_settings/general/disable_border_radius','0'),(24,'default',0,'porto_settings/general/show_site_notice','0'),(25,'default',0,'porto_settings/header/header_type','1'),(26,'default',0,'porto_settings/header/static_block','porto_custom_block_for_header'),(27,'default',0,'porto_settings/header/sticky_header_logo',NULL),(28,'default',0,'porto_settings/footer/footer_top','0'),(29,'default',0,'porto_settings/footer/footer_top_block','custom'),(30,'default',0,'porto_settings/footer/footer_top_custom','porto_footer_top_custom_block'),(31,'default',0,'porto_settings/footer/footer_middle','1'),(32,'default',0,'porto_settings/footer/footer_ribbon','1'),(33,'default',0,'porto_settings/footer/footer_ribbon_text','Ribbon Text'),(34,'default',0,'porto_settings/footer/footer_middle_column_1','custom'),(35,'default',0,'porto_settings/footer/footer_middle_column_1_custom','porto_footer_links'),(36,'default',0,'porto_settings/footer/footer_middle_column_1_size','3'),(37,'default',0,'porto_settings/footer/footer_middle_column_2','custom'),(38,'default',0,'porto_settings/footer/footer_middle_column_2_custom','porto_footer_contact_information'),(39,'default',0,'porto_settings/footer/footer_middle_column_2_size','3'),(40,'default',0,'porto_settings/footer/footer_middle_column_3','custom'),(41,'default',0,'porto_settings/footer/footer_middle_column_3_custom','porto_footer_features'),(42,'default',0,'porto_settings/footer/footer_middle_column_3_size','3'),(43,'default',0,'porto_settings/footer/footer_middle_column_4','newsletter'),(44,'default',0,'porto_settings/footer/footer_middle_column_4_custom','porto_footer_links'),(45,'default',0,'porto_settings/footer/footer_middle_column_4_size','3'),(46,'default',0,'porto_settings/footer/footer_middle_2','0'),(47,'default',0,'porto_settings/footer/footer_middle_2_column_1','custom'),(48,'default',0,'porto_settings/footer/footer_middle_2_column_1_custom','porto_footer_links'),(49,'default',0,'porto_settings/footer/footer_middle_2_column_1_size','3'),(50,'default',0,'porto_settings/footer/footer_middle_2_column_2','custom'),(51,'default',0,'porto_settings/footer/footer_middle_2_column_2_custom','porto_footer_links'),(52,'default',0,'porto_settings/footer/footer_middle_2_column_2_size','3'),(53,'default',0,'porto_settings/footer/footer_middle_2_column_3','custom'),(54,'default',0,'porto_settings/footer/footer_middle_2_column_3_custom','porto_footer_links'),(55,'default',0,'porto_settings/footer/footer_middle_2_column_3_size','3'),(56,'default',0,'porto_settings/footer/footer_middle_2_column_4','custom'),(57,'default',0,'porto_settings/footer/footer_middle_2_column_4_custom','porto_footer_links'),(58,'default',0,'porto_settings/footer/footer_middle_2_column_4_size','3'),(59,'default',0,'porto_settings/footer/footer_bottom','1'),(60,'default',0,'porto_settings/footer/footer_store_switcher','0'),(61,'default',0,'porto_settings/footer/footer_logo_src','default/logo_footer.png'),(62,'default',0,'porto_settings/footer/footer_bottom_copyrights','&copy;Copyright 2016 by SW-THEMES. All Rights Reserved.'),(63,'default',0,'porto_settings/footer/footer_bottom_custom_1','porto_footer_bottom_custom_block'),(64,'default',0,'porto_settings/footer/footer_bottom_custom_2',NULL),(65,'default',0,'porto_settings/category/alternative_image','1'),(66,'default',0,'porto_settings/category/aspect_ratio','0'),(67,'default',0,'porto_settings/category/ratio_width','300'),(68,'default',0,'porto_settings/category/ratio_height','400'),(69,'default',0,'porto_settings/category/rating_star','1'),(70,'default',0,'porto_settings/category/product_price','1'),(71,'default',0,'porto_settings/category/actions','1'),(72,'default',0,'porto_settings/category/addtocompare','1'),(73,'default',0,'porto_settings/category/addtowishlist','1'),(74,'default',0,'porto_settings/category/category_description','main_column'),(75,'default',0,'porto_settings/category/side_custom_block','porto_category_side_custom_block'),(76,'default',0,'porto_settings/category_grid/columns','4'),(77,'default',0,'porto_settings/category_grid/move_actions','0'),(78,'default',0,'porto_settings/product/product_image_size','6'),(79,'default',0,'porto_settings/product/side_custom_block','porto_product_side_custom_block'),(80,'default',0,'porto_settings/newsletter/logo_src','default/logo.png'),(81,'default',0,'porto_settings/contacts/enable','1'),(82,'default',0,'porto_settings/contacts/full_width','0'),(83,'default',0,'porto_settings/contacts/address','Porto2 Store'),(84,'default',0,'porto_settings/contacts/latitude','-34.398'),(85,'default',0,'porto_settings/contacts/longitude','150.884'),(86,'default',0,'porto_settings/contacts/zoom','18'),(87,'default',0,'porto_settings/contacts/infoblock','<div class=\"row\">\r\n<div class=\"col-sm-12\">\r\n    <i class=\"porto-icon-phone\"></i>\r\n    <p>0201 203 2032</p>\r\n    <p>0201 203 2032</p>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-sm-12\">\r\n    <i class=\"porto-icon-mobile\"></i>\r\n    <p>201-123-3922</p>\r\n    <p>302-123-3928</p>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-sm-12\">\r\n    <i class=\"porto-icon-mail-alt\"></i>\r\n    <p>porto@gmail.com</p>\r\n    <p>porto@portotemplate.com</p>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-sm-12\">\r\n    <i class=\"porto-icon-skype\"></i>\r\n    <p>porto_skype</p>\r\n    <p>proto_template</p>\r\n</div>\r\n</div>'),(88,'default',0,'porto_settings/contacts/customblock',NULL),(89,'default',0,'porto_settings/custom_settings/custom_style',NULL),(90,'default',0,'porto_settings/custom_settings/custom_style_2',NULL),(91,'default',0,'porto_design/general/theme_color',NULL),(92,'default',0,'porto_design/font/custom','0'),(93,'default',0,'porto_design/font/font_size',NULL),(94,'default',0,'porto_design/font/font_family',NULL),(95,'default',0,'porto_design/font/custom_font_family',NULL),(96,'default',0,'porto_design/font/google_font_family',NULL),(97,'default',0,'porto_design/font/char_latin_ext',NULL),(98,'default',0,'porto_design/font/char_subset',NULL),(99,'default',0,'porto_design/colors/custom','0'),(100,'default',0,'porto_design/colors/text_color',NULL),(101,'default',0,'porto_design/colors/link_color',NULL),(102,'default',0,'porto_design/colors/link_hover_color',NULL),(103,'default',0,'porto_design/colors/button_bg_color',NULL),(104,'default',0,'porto_design/colors/button_text_color',NULL),(105,'default',0,'porto_design/colors/button_hover_bg_color',NULL),(106,'default',0,'porto_design/colors/button_hover_text_color',NULL),(107,'default',0,'porto_design/colors/addtowishlist_color',NULL),(108,'default',0,'porto_design/colors/addtowishlist_hover_color',NULL),(109,'default',0,'porto_design/colors/addtocompare_color',NULL),(110,'default',0,'porto_design/colors/addtocompare_hover_color',NULL),(111,'default',0,'porto_design/colors/breadcrumbs_bg_color',NULL),(112,'default',0,'porto_design/colors/breadcrumbs_color',NULL),(113,'default',0,'porto_design/colors/breadcrumbs_links_color',NULL),(114,'default',0,'porto_design/colors/breadcrumbs_links_hover_color',NULL),(115,'default',0,'porto_design/colors/sale_bg_color',NULL),(116,'default',0,'porto_design/colors/sale_font_color',NULL),(117,'default',0,'porto_design/colors/new_bg_color',NULL),(118,'default',0,'porto_design/colors/new_font_color',NULL),(119,'default',0,'porto_design/header/custom','0'),(120,'default',0,'porto_design/header/header_bgcolor',NULL),(121,'default',0,'porto_design/header/header_bg_image',NULL),(122,'default',0,'porto_design/header/header_bordercolor',NULL),(123,'default',0,'porto_design/header/header_textcolor',NULL),(124,'default',0,'porto_design/header/header_linkcolor',NULL),(125,'default',0,'porto_design/header/header_top_links_bgcolor',NULL),(126,'default',0,'porto_design/header/header_top_links_color',NULL),(127,'default',0,'porto_design/header/header_menu_bgcolor',NULL),(128,'default',0,'porto_design/header/header_menu_color',NULL),(129,'default',0,'porto_design/header/header_menu_hover_bgcolor',NULL),(130,'default',0,'porto_design/header/header_menu_hover_color',NULL),(131,'default',0,'porto_design/header/header_menu_classicmenu_bgcolor',NULL),(132,'default',0,'porto_design/header/header_menu_classicmenu_bordercolor',NULL),(133,'default',0,'porto_design/header/header_menu_classicmenu_color',NULL),(134,'default',0,'porto_design/header/header_menu_classicmenu_hover_bgcolor',NULL),(135,'default',0,'porto_design/header/header_menu_classicmenu_hover_color',NULL),(136,'default',0,'porto_design/header/header_search_bgcolor',NULL),(137,'default',0,'porto_design/header/header_search_text_color',NULL),(138,'default',0,'porto_design/header/header_search_bordercolor',NULL),(139,'default',0,'porto_design/header/header_minicart_bgcolor',NULL),(140,'default',0,'porto_design/header/header_minicart_color',NULL),(141,'default',0,'porto_design/header/header_minicart_icon_color',NULL),(142,'default',0,'porto_design/footer/custom','0'),(143,'default',0,'porto_design/footer/footer_top_bgcolor',NULL),(144,'default',0,'porto_design/footer/footer_top_color',NULL),(145,'default',0,'porto_design/footer/footer_top_link_color',NULL),(146,'default',0,'porto_design/footer/footer_top_link_hover_color',NULL),(147,'default',0,'porto_design/footer/footer_middle_bgcolor',NULL),(148,'default',0,'porto_design/footer/footer_middle_color',NULL),(149,'default',0,'porto_design/footer/footer_middle_link_color',NULL),(150,'default',0,'porto_design/footer/footer_middle_link_hover_color',NULL),(151,'default',0,'porto_design/footer/footer_middle_title_color',NULL),(152,'default',0,'porto_design/footer/footer_middle_links_icon_color',NULL),(153,'default',0,'porto_design/footer/footer_middle_ribbon_bgcolor',NULL),(154,'default',0,'porto_design/footer/footer_middle_ribbon_shadow_color',NULL),(155,'default',0,'porto_design/footer/footer_middle_ribbon_color',NULL),(156,'default',0,'porto_design/footer/footer_middle_2_bgcolor',NULL),(157,'default',0,'porto_design/footer/footer_middle_2_color',NULL),(158,'default',0,'porto_design/footer/footer_middle_2_link_color',NULL),(159,'default',0,'porto_design/footer/footer_middle_2_link_hover_color',NULL),(160,'default',0,'porto_design/footer/footer_middle_2_title_color',NULL),(161,'default',0,'porto_design/footer/footer_middle_2_links_icon_color',NULL),(162,'default',0,'porto_design/footer/footer_bottom_bgcolor',NULL),(163,'default',0,'porto_design/footer/footer_bottom_color',NULL),(164,'default',0,'porto_design/footer/footer_bottom_link_color',NULL),(165,'default',0,'porto_design/footer/footer_bottom_link_hover_color',NULL),(166,'default',0,'porto_design/page/custom','0'),(167,'default',0,'porto_design/page/page_bgcolor',NULL),(168,'default',0,'porto_design/page/page_bg_image',NULL),(169,'default',0,'porto_design/page/page_custom_style',NULL),(170,'default',0,'porto_design/main/custom','0'),(171,'default',0,'porto_design/main/main_bgcolor',NULL),(172,'default',0,'porto_design/main/main_bg_image',NULL),(173,'default',0,'porto_design/main/main_custom_style',NULL),(174,'default',0,'design/head/default_title','Magento Commerce'),(175,'default',0,'design/head/title_prefix',NULL),(176,'default',0,'design/head/title_suffix',NULL),(177,'default',0,'design/head/default_description','Default Description'),(178,'default',0,'design/head/default_keywords','Magento, Varien, E-commerce'),(179,'default',0,'design/head/includes',NULL),(180,'default',0,'design/head/demonotice','0'),(181,'default',0,'design/header/logo_alt','Magento Commerce'),(182,'default',0,'design/header/welcome','Default welcome msg!'),(183,'default',0,'design/footer/copyright','Copyright © 2016 Magento. All rights reserved.'),(184,'default',0,'design/footer/absolute_footer',NULL),(185,'default',0,'design/theme/theme_id','4'),(186,'default',0,'design/pagination/pagination_frame','5'),(187,'default',0,'design/pagination/pagination_frame_skip',NULL),(188,'default',0,'design/pagination/anchor_text_for_previous',NULL),(189,'default',0,'design/pagination/anchor_text_for_next',NULL),(190,'default',0,'design/watermark/image_size',NULL),(191,'default',0,'design/watermark/image_imageOpacity',NULL),(192,'default',0,'design/watermark/image_position','stretch'),(193,'default',0,'design/watermark/small_image_size',NULL),(194,'default',0,'design/watermark/small_image_imageOpacity',NULL),(195,'default',0,'design/watermark/small_image_position','stretch'),(196,'default',0,'design/watermark/thumbnail_size',NULL),(197,'default',0,'design/watermark/thumbnail_imageOpacity',NULL),(198,'default',0,'design/watermark/thumbnail_position','stretch'),(199,'default',0,'design/email/logo_alt',NULL),(200,'default',0,'design/email/logo_width',NULL),(201,'default',0,'design/email/logo_height',NULL),(202,'default',0,'design/email/header_template','design_email_header_template'),(203,'default',0,'design/email/footer_template','design_email_footer_template'),(204,'default',0,'design/watermark/swatch_image_size',NULL),(205,'default',0,'design/watermark/swatch_image_imageOpacity',NULL),(206,'default',0,'design/watermark/swatch_image_position','stretch'),(207,'default',0,'currency/yahoofinance/timeout','100'),(208,'default',0,'currency/fixerio/timeout','100'),(209,'default',0,'currency/import/service','yahoofinance'),(210,'default',0,'crontab/default/jobs/currency_rates_update/schedule/cron_expr','0 0 * * *'),(211,'default',0,'currency/import/time','00,00,00'),(212,'default',0,'currency/import/frequency','D'),(213,'default',0,'currency/import/error_email',NULL),(214,'default',0,'porto_settings/general/boxed_custom_style',NULL),(215,'default',0,'porto_settings/header/sticky_header','0'),(216,'default',0,'porto_settings/header/currency_short','0'),(217,'default',0,'porto_settings/header/language_flag','1'),(218,'default',0,'porto_settings/footer/footer_newsletter_title','Be the First to Know'),(219,'default',0,'porto_settings/footer/footer_newsletter_text','Get all the latest information on Events,<br/>Sales and Offers. Sign up for newsletter today.'),(220,'default',0,'porto_settings/category_grid/xs_one_column','0'),(221,'default',0,'porto_settings/category_grid/flex_grid','0'),(222,'default',0,'porto_settings/product/prev_text','<em class=\"porto-icon-left-open\"></em>'),(223,'default',0,'porto_settings/product/next_text','<em class=\"porto-icon-right-open\"></em>'),(224,'default',0,'porto_settings/product/prev_next','1'),(225,'default',0,'porto_settings/product/enable_addtocart_sticky','0'),(226,'default',0,'porto_settings/product/tab_style',NULL),(227,'default',0,'porto_settings/product/move_tab','0'),(228,'default',0,'porto_settings/product/custom_cms_tabs','a:0:{}'),(229,'default',0,'porto_settings/product/custom_attr_tabs','a:0:{}'),(230,'default',0,'porto_settings/product_label/new_label','1'),(231,'default',0,'porto_settings/product_label/new_label_text','NEW'),(232,'default',0,'porto_settings/product_label/sale_label','1'),(233,'default',0,'porto_settings/product_label/sale_label_percent','1'),(234,'default',0,'porto_settings/newsletter/enable','1'),(235,'default',0,'porto_settings/newsletter/delay','5000'),(236,'default',0,'porto_settings/newsletter/content','<h2>BE THE FIRST TO KNOW</h2><p>Subscribe to the Porto eCommerce newsletter to receive timely updates from your favorite products.</p>'),(237,'default',0,'porto_settings/newsletter/width','700px'),(238,'default',0,'porto_settings/newsletter/height','324px'),(239,'default',0,'porto_settings/newsletter/bg_color',NULL),(240,'default',0,'porto_settings/newsletter/custom_style',NULL),(241,'default',0,'porto_settings/newsletter/bg_image',NULL),(242,'default',0,'porto_settings/contacts/api_key',NULL),(243,'default',0,'porto_settings/install/demo_version','0'),(244,'default',0,'porto_settings/install/overwrite_blocks','0'),(245,'default',0,'porto_settings/install/overwrite_pages','0'),(248,'default',0,'weltpixel_quickview/general/enable_product_listing','1'),(249,'default',0,'weltpixel_quickview/general/remove_product_image','0'),(250,'default',0,'weltpixel_quickview/general/remove_product_image_thumb','1'),(251,'default',0,'weltpixel_quickview/general/remove_short_description','0'),(252,'default',0,'weltpixel_quickview/general/remove_qty_selector','1'),(253,'default',0,'weltpixel_quickview/general/enable_goto_product_button','1'),(254,'default',0,'weltpixel_quickview/general/remove_availability','0'),(255,'default',0,'weltpixel_quickview/general/remove_sku','0'),(256,'default',0,'weltpixel_quickview/general/button_style','v2'),(257,'default',0,'weltpixel_quickview/general/close_quickview','5'),(258,'default',0,'weltpixel_quickview/general/scroll_to_top','1'),(259,'default',0,'weltpixel_quickview/general/enable_shopping_checkout_product_buttons','1'),(260,'default',0,'weltpixel_quickview/general/custom_css',NULL),(261,'default',0,'weltpixel_quickview/general/enable_zoom','false'),(262,'default',0,'weltpixel_quickview/general/zoom_fullscreenzoom',NULL),(263,'default',0,'weltpixel_quickview/general/zoom_top',NULL),(264,'default',0,'weltpixel_quickview/general/zoom_left',NULL),(265,'default',0,'weltpixel_quickview/general/zoom_width',NULL),(266,'default',0,'weltpixel_quickview/general/zoom_height',NULL),(267,'default',0,'weltpixel_quickview/general/zoom_eventtype','hover');

/*Table structure for table `cron_schedule` */

DROP TABLE IF EXISTS `cron_schedule`;

CREATE TABLE `cron_schedule` (
  `schedule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Schedule Id',
  `job_code` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Job Code',
  `status` varchar(7) NOT NULL DEFAULT 'pending' COMMENT 'Status',
  `messages` text COMMENT 'Messages',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `scheduled_at` timestamp NULL DEFAULT NULL COMMENT 'Scheduled At',
  `executed_at` timestamp NULL DEFAULT NULL COMMENT 'Executed At',
  `finished_at` timestamp NULL DEFAULT NULL COMMENT 'Finished At',
  PRIMARY KEY (`schedule_id`),
  KEY `CRON_SCHEDULE_JOB_CODE` (`job_code`),
  KEY `CRON_SCHEDULE_SCHEDULED_AT_STATUS` (`scheduled_at`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cron Schedule';

/*Data for the table `cron_schedule` */

/*Table structure for table `customer_address_entity` */

DROP TABLE IF EXISTS `customer_address_entity`;

CREATE TABLE `customer_address_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Active',
  `city` varchar(255) NOT NULL COMMENT 'City',
  `company` varchar(255) DEFAULT NULL COMMENT 'Company',
  `country_id` varchar(255) NOT NULL COMMENT 'Country',
  `fax` varchar(255) DEFAULT NULL COMMENT 'Fax',
  `firstname` varchar(255) NOT NULL COMMENT 'First Name',
  `lastname` varchar(255) NOT NULL COMMENT 'Last Name',
  `middlename` varchar(255) DEFAULT NULL COMMENT 'Middle Name',
  `postcode` varchar(255) DEFAULT NULL COMMENT 'Zip/Postal Code',
  `prefix` varchar(40) DEFAULT NULL COMMENT 'Prefix',
  `region` varchar(255) DEFAULT NULL COMMENT 'State/Province',
  `region_id` int(10) unsigned DEFAULT NULL COMMENT 'State/Province',
  `street` text NOT NULL COMMENT 'Street Address',
  `suffix` varchar(40) DEFAULT NULL COMMENT 'Suffix',
  `telephone` varchar(255) NOT NULL COMMENT 'Phone Number',
  `vat_id` varchar(255) DEFAULT NULL COMMENT 'VAT number',
  `vat_is_valid` int(10) unsigned DEFAULT NULL COMMENT 'VAT number validity',
  `vat_request_date` varchar(255) DEFAULT NULL COMMENT 'VAT number validation request date',
  `vat_request_id` varchar(255) DEFAULT NULL COMMENT 'VAT number validation request ID',
  `vat_request_success` int(10) unsigned DEFAULT NULL COMMENT 'VAT number validation request success',
  PRIMARY KEY (`entity_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_PARENT_ID` (`parent_id`),
  CONSTRAINT `CUSTOMER_ADDRESS_ENTITY_PARENT_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity';

/*Data for the table `customer_address_entity` */

/*Table structure for table `customer_address_entity_datetime` */

DROP TABLE IF EXISTS `customer_address_entity_datetime`;

CREATE TABLE `customer_address_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` datetime DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ADDRESS_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `CSTR_ADDR_ENTT_DTIME_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CSTR_ADDR_ENTT_DTIME_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Datetime';

/*Data for the table `customer_address_entity_datetime` */

/*Table structure for table `customer_address_entity_decimal` */

DROP TABLE IF EXISTS `customer_address_entity_decimal`;

CREATE TABLE `customer_address_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ADDRESS_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `CSTR_ADDR_ENTT_DEC_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CSTR_ADDR_ENTT_DEC_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Decimal';

/*Data for the table `customer_address_entity_decimal` */

/*Table structure for table `customer_address_entity_int` */

DROP TABLE IF EXISTS `customer_address_entity_int`;

CREATE TABLE `customer_address_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ADDRESS_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `CSTR_ADDR_ENTT_INT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CSTR_ADDR_ENTT_INT_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Int';

/*Data for the table `customer_address_entity_int` */

/*Table structure for table `customer_address_entity_text` */

DROP TABLE IF EXISTS `customer_address_entity_text`;

CREATE TABLE `customer_address_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` text NOT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ADDRESS_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `CSTR_ADDR_ENTT_TEXT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CSTR_ADDR_ENTT_TEXT_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Text';

/*Data for the table `customer_address_entity_text` */

/*Table structure for table `customer_address_entity_varchar` */

DROP TABLE IF EXISTS `customer_address_entity_varchar`;

CREATE TABLE `customer_address_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ADDRESS_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `CSTR_ADDR_ENTT_VCHR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CSTR_ADDR_ENTT_VCHR_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Varchar';

/*Data for the table `customer_address_entity_varchar` */

/*Table structure for table `customer_eav_attribute` */

DROP TABLE IF EXISTS `customer_eav_attribute`;

CREATE TABLE `customer_eav_attribute` (
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  `is_visible` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Visible',
  `input_filter` varchar(255) DEFAULT NULL COMMENT 'Input Filter',
  `multiline_count` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Multiline Count',
  `validate_rules` text COMMENT 'Validate Rules',
  `is_system` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is System',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `data_model` varchar(255) DEFAULT NULL COMMENT 'Data Model',
  `is_used_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used in Grid',
  `is_visible_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible in Grid',
  `is_filterable_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable in Grid',
  `is_searchable_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Searchable in Grid',
  PRIMARY KEY (`attribute_id`),
  CONSTRAINT `CUSTOMER_EAV_ATTRIBUTE_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Eav Attribute';

/*Data for the table `customer_eav_attribute` */

insert  into `customer_eav_attribute`(`attribute_id`,`is_visible`,`input_filter`,`multiline_count`,`validate_rules`,`is_system`,`sort_order`,`data_model`,`is_used_in_grid`,`is_visible_in_grid`,`is_filterable_in_grid`,`is_searchable_in_grid`) values (1,1,NULL,0,NULL,1,10,NULL,1,1,1,0),(2,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(3,1,NULL,0,NULL,1,20,NULL,1,1,0,1),(4,0,NULL,0,NULL,0,30,NULL,0,0,0,0),(5,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,40,NULL,0,0,0,0),(6,0,NULL,0,NULL,0,50,NULL,0,0,0,0),(7,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,60,NULL,0,0,0,0),(8,0,NULL,0,NULL,0,70,NULL,0,0,0,0),(9,1,NULL,0,'a:1:{s:16:\"input_validation\";s:5:\"email\";}',1,80,NULL,1,1,1,1),(10,1,NULL,0,NULL,1,25,NULL,1,1,1,0),(11,0,'date',0,'a:1:{s:16:\"input_validation\";s:4:\"date\";}',0,90,NULL,1,1,1,0),(12,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(13,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(14,0,NULL,0,'a:1:{s:16:\"input_validation\";s:4:\"date\";}',1,0,NULL,0,0,0,0),(15,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(16,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(17,0,NULL,0,'a:1:{s:15:\"max_text_length\";i:255;}',0,100,NULL,1,1,0,1),(18,0,NULL,0,NULL,1,0,NULL,1,1,1,0),(19,0,NULL,0,NULL,0,0,NULL,1,1,1,0),(20,0,NULL,0,'a:0:{}',0,110,NULL,1,1,1,0),(21,1,NULL,0,NULL,1,28,NULL,0,0,0,0),(22,0,NULL,0,NULL,0,10,NULL,0,0,0,0),(23,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,20,NULL,1,0,0,1),(24,0,NULL,0,NULL,0,30,NULL,0,0,0,0),(25,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,40,NULL,1,0,0,1),(26,0,NULL,0,NULL,0,50,NULL,0,0,0,0),(27,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,60,NULL,1,0,0,1),(28,1,NULL,2,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,70,NULL,1,0,0,1),(29,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,80,NULL,1,0,0,1),(30,1,NULL,0,NULL,1,90,NULL,1,1,1,0),(31,1,NULL,0,NULL,1,100,NULL,1,1,0,1),(32,1,NULL,0,NULL,1,100,NULL,0,0,0,0),(33,1,NULL,0,'a:0:{}',1,110,'Magento\\Customer\\Model\\Attribute\\Data\\Postcode',1,1,1,1),(34,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,120,NULL,1,1,1,1),(35,0,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',0,130,NULL,1,0,0,1),(36,1,NULL,0,NULL,1,140,NULL,0,0,0,0),(37,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(38,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(39,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(40,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(41,0,NULL,0,NULL,0,0,NULL,0,0,0,0),(42,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(43,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(44,0,NULL,0,NULL,1,0,NULL,0,0,0,0);

/*Table structure for table `customer_eav_attribute_website` */

DROP TABLE IF EXISTS `customer_eav_attribute_website`;

CREATE TABLE `customer_eav_attribute_website` (
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `is_visible` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Visible',
  `is_required` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Required',
  `default_value` text COMMENT 'Default Value',
  `multiline_count` smallint(5) unsigned DEFAULT NULL COMMENT 'Multiline Count',
  PRIMARY KEY (`attribute_id`,`website_id`),
  KEY `CUSTOMER_EAV_ATTRIBUTE_WEBSITE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `CSTR_EAV_ATTR_WS_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CSTR_EAV_ATTR_WS_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Eav Attribute Website';

/*Data for the table `customer_eav_attribute_website` */

insert  into `customer_eav_attribute_website`(`attribute_id`,`website_id`,`is_visible`,`is_required`,`default_value`,`multiline_count`) values (1,1,NULL,NULL,NULL,NULL),(3,1,NULL,NULL,NULL,NULL),(9,1,NULL,NULL,NULL,NULL),(10,1,NULL,NULL,NULL,NULL),(11,1,NULL,NULL,NULL,NULL),(17,1,NULL,NULL,NULL,NULL),(18,1,NULL,NULL,NULL,NULL),(19,1,NULL,NULL,NULL,NULL),(20,1,NULL,NULL,NULL,NULL),(21,1,NULL,NULL,NULL,NULL),(23,1,NULL,NULL,NULL,NULL),(25,1,NULL,NULL,NULL,NULL),(27,1,NULL,NULL,NULL,NULL),(28,1,NULL,NULL,NULL,NULL),(29,1,NULL,NULL,NULL,NULL),(30,1,NULL,NULL,NULL,NULL),(31,1,NULL,NULL,NULL,NULL),(32,1,NULL,NULL,NULL,NULL),(33,1,NULL,NULL,NULL,NULL),(34,1,NULL,NULL,NULL,NULL),(35,1,NULL,NULL,NULL,NULL),(36,1,NULL,NULL,NULL,NULL);

/*Table structure for table `customer_entity` */

DROP TABLE IF EXISTS `customer_entity`;

CREATE TABLE `customer_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `website_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Website Id',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Group Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `store_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Active',
  `disable_auto_group_change` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Disable automatic group change based on VAT ID',
  `created_in` varchar(255) DEFAULT NULL COMMENT 'Created From',
  `prefix` varchar(40) DEFAULT NULL COMMENT 'Prefix',
  `firstname` varchar(255) DEFAULT NULL COMMENT 'First Name',
  `middlename` varchar(255) DEFAULT NULL COMMENT 'Middle Name/Initial',
  `lastname` varchar(255) DEFAULT NULL COMMENT 'Last Name',
  `suffix` varchar(40) DEFAULT NULL COMMENT 'Suffix',
  `dob` date DEFAULT NULL COMMENT 'Date of Birth',
  `password_hash` varchar(128) DEFAULT NULL COMMENT 'Password_hash',
  `rp_token` varchar(128) DEFAULT NULL COMMENT 'Reset password token',
  `rp_token_created_at` datetime DEFAULT NULL COMMENT 'Reset password token creation time',
  `default_billing` int(10) unsigned DEFAULT NULL COMMENT 'Default Billing Address',
  `default_shipping` int(10) unsigned DEFAULT NULL COMMENT 'Default Shipping Address',
  `taxvat` varchar(50) DEFAULT NULL COMMENT 'Tax/VAT Number',
  `confirmation` varchar(64) DEFAULT NULL COMMENT 'Is Confirmed',
  `gender` smallint(5) unsigned DEFAULT NULL COMMENT 'Gender',
  `failures_num` smallint(6) DEFAULT '0' COMMENT 'Failure Number',
  `first_failure` timestamp NULL DEFAULT NULL COMMENT 'First Failure',
  `lock_expires` timestamp NULL DEFAULT NULL COMMENT 'Lock Expiration Date',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_EMAIL_WEBSITE_ID` (`email`,`website_id`),
  KEY `CUSTOMER_ENTITY_STORE_ID` (`store_id`),
  KEY `CUSTOMER_ENTITY_WEBSITE_ID` (`website_id`),
  KEY `CUSTOMER_ENTITY_FIRSTNAME` (`firstname`),
  KEY `CUSTOMER_ENTITY_LASTNAME` (`lastname`),
  CONSTRAINT `CUSTOMER_ENTITY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL,
  CONSTRAINT `CUSTOMER_ENTITY_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity';

/*Data for the table `customer_entity` */

/*Table structure for table `customer_entity_datetime` */

DROP TABLE IF EXISTS `customer_entity_datetime`;

CREATE TABLE `customer_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` datetime DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `CUSTOMER_ENTITY_DATETIME_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CUSTOMER_ENTITY_DATETIME_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Datetime';

/*Data for the table `customer_entity_datetime` */

/*Table structure for table `customer_entity_decimal` */

DROP TABLE IF EXISTS `customer_entity_decimal`;

CREATE TABLE `customer_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `CUSTOMER_ENTITY_DECIMAL_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CUSTOMER_ENTITY_DECIMAL_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Decimal';

/*Data for the table `customer_entity_decimal` */

/*Table structure for table `customer_entity_int` */

DROP TABLE IF EXISTS `customer_entity_int`;

CREATE TABLE `customer_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `CUSTOMER_ENTITY_INT_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CUSTOMER_ENTITY_INT_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Int';

/*Data for the table `customer_entity_int` */

/*Table structure for table `customer_entity_text` */

DROP TABLE IF EXISTS `customer_entity_text`;

CREATE TABLE `customer_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` text NOT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `CUSTOMER_ENTITY_TEXT_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CUSTOMER_ENTITY_TEXT_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Text';

/*Data for the table `customer_entity_text` */

/*Table structure for table `customer_entity_varchar` */

DROP TABLE IF EXISTS `customer_entity_varchar`;

CREATE TABLE `customer_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `CUSTOMER_ENTITY_VARCHAR_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CUSTOMER_ENTITY_VARCHAR_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Varchar';

/*Data for the table `customer_entity_varchar` */

/*Table structure for table `customer_form_attribute` */

DROP TABLE IF EXISTS `customer_form_attribute`;

CREATE TABLE `customer_form_attribute` (
  `form_code` varchar(32) NOT NULL COMMENT 'Form Code',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  PRIMARY KEY (`form_code`,`attribute_id`),
  KEY `CUSTOMER_FORM_ATTRIBUTE_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `CUSTOMER_FORM_ATTRIBUTE_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Form Attribute';

/*Data for the table `customer_form_attribute` */

insert  into `customer_form_attribute`(`form_code`,`attribute_id`) values ('adminhtml_checkout',9),('adminhtml_checkout',10),('adminhtml_checkout',11),('adminhtml_checkout',17),('adminhtml_checkout',20),('adminhtml_customer',1),('adminhtml_customer',3),('adminhtml_customer',4),('adminhtml_customer',5),('adminhtml_customer',6),('adminhtml_customer',7),('adminhtml_customer',8),('adminhtml_customer',9),('adminhtml_customer',10),('adminhtml_customer',11),('adminhtml_customer',17),('adminhtml_customer',19),('adminhtml_customer',20),('adminhtml_customer',21),('adminhtml_customer_address',22),('adminhtml_customer_address',23),('adminhtml_customer_address',24),('adminhtml_customer_address',25),('adminhtml_customer_address',26),('adminhtml_customer_address',27),('adminhtml_customer_address',28),('adminhtml_customer_address',29),('adminhtml_customer_address',30),('adminhtml_customer_address',31),('adminhtml_customer_address',32),('adminhtml_customer_address',33),('adminhtml_customer_address',34),('adminhtml_customer_address',35),('adminhtml_customer_address',36),('customer_account_create',4),('customer_account_create',5),('customer_account_create',6),('customer_account_create',7),('customer_account_create',8),('customer_account_create',9),('customer_account_create',11),('customer_account_create',17),('customer_account_create',19),('customer_account_create',20),('customer_account_edit',4),('customer_account_edit',5),('customer_account_edit',6),('customer_account_edit',7),('customer_account_edit',8),('customer_account_edit',9),('customer_account_edit',11),('customer_account_edit',17),('customer_account_edit',19),('customer_account_edit',20),('customer_address_edit',22),('customer_address_edit',23),('customer_address_edit',24),('customer_address_edit',25),('customer_address_edit',26),('customer_address_edit',27),('customer_address_edit',28),('customer_address_edit',29),('customer_address_edit',30),('customer_address_edit',31),('customer_address_edit',32),('customer_address_edit',33),('customer_address_edit',34),('customer_address_edit',35),('customer_address_edit',36),('customer_register_address',22),('customer_register_address',23),('customer_register_address',24),('customer_register_address',25),('customer_register_address',26),('customer_register_address',27),('customer_register_address',28),('customer_register_address',29),('customer_register_address',30),('customer_register_address',31),('customer_register_address',32),('customer_register_address',33),('customer_register_address',34),('customer_register_address',35),('customer_register_address',36);

/*Table structure for table `customer_grid_flat` */

DROP TABLE IF EXISTS `customer_grid_flat`;

CREATE TABLE `customer_grid_flat` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `name` text COMMENT 'Name',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `group_id` int(11) DEFAULT NULL COMMENT 'Group_id',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created_at',
  `website_id` int(11) DEFAULT NULL COMMENT 'Website_id',
  `confirmation` varchar(255) DEFAULT NULL COMMENT 'Confirmation',
  `created_in` text COMMENT 'Created_in',
  `dob` date DEFAULT NULL COMMENT 'Dob',
  `gender` int(11) DEFAULT NULL COMMENT 'Gender',
  `taxvat` varchar(255) DEFAULT NULL COMMENT 'Taxvat',
  `lock_expires` timestamp NULL DEFAULT NULL COMMENT 'Lock_expires',
  `billing_full` text COMMENT 'Billing_full',
  `billing_firstname` varchar(255) DEFAULT NULL COMMENT 'Billing_firstname',
  `billing_lastname` varchar(255) DEFAULT NULL COMMENT 'Billing_lastname',
  `billing_telephone` varchar(255) DEFAULT NULL COMMENT 'Billing_telephone',
  `billing_postcode` varchar(255) DEFAULT NULL COMMENT 'Billing_postcode',
  `billing_country_id` varchar(255) DEFAULT NULL COMMENT 'Billing_country_id',
  `billing_region` varchar(255) DEFAULT NULL COMMENT 'Billing_region',
  `billing_street` varchar(255) DEFAULT NULL COMMENT 'Billing_street',
  `billing_city` varchar(255) DEFAULT NULL COMMENT 'Billing_city',
  `billing_fax` varchar(255) DEFAULT NULL COMMENT 'Billing_fax',
  `billing_vat_id` varchar(255) DEFAULT NULL COMMENT 'Billing_vat_id',
  `billing_company` varchar(255) DEFAULT NULL COMMENT 'Billing_company',
  `shipping_full` text COMMENT 'Shipping_full',
  PRIMARY KEY (`entity_id`),
  KEY `CUSTOMER_GRID_FLAT_GROUP_ID` (`group_id`),
  KEY `CUSTOMER_GRID_FLAT_CREATED_AT` (`created_at`),
  KEY `CUSTOMER_GRID_FLAT_WEBSITE_ID` (`website_id`),
  KEY `CUSTOMER_GRID_FLAT_CONFIRMATION` (`confirmation`),
  KEY `CUSTOMER_GRID_FLAT_DOB` (`dob`),
  KEY `CUSTOMER_GRID_FLAT_GENDER` (`gender`),
  KEY `CUSTOMER_GRID_FLAT_BILLING_COUNTRY_ID` (`billing_country_id`),
  FULLTEXT KEY `FTI_B691CA777399890C71AC8A4CDFB8EA99` (`name`,`email`,`created_in`,`taxvat`,`billing_full`,`billing_firstname`,`billing_lastname`,`billing_telephone`,`billing_postcode`,`billing_region`,`billing_city`,`billing_fax`,`billing_company`,`shipping_full`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='customer_grid_flat';

/*Data for the table `customer_grid_flat` */

/*Table structure for table `customer_group` */

DROP TABLE IF EXISTS `customer_group`;

CREATE TABLE `customer_group` (
  `customer_group_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Customer Group Id',
  `customer_group_code` varchar(32) NOT NULL COMMENT 'Customer Group Code',
  `tax_class_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Tax Class Id',
  PRIMARY KEY (`customer_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Customer Group';

/*Data for the table `customer_group` */

insert  into `customer_group`(`customer_group_id`,`customer_group_code`,`tax_class_id`) values (0,'NOT LOGGED IN',3),(1,'General',3),(2,'Wholesale',3),(3,'Retailer',3);

/*Table structure for table `customer_log` */

DROP TABLE IF EXISTS `customer_log`;

CREATE TABLE `customer_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Log ID',
  `customer_id` int(11) NOT NULL COMMENT 'Customer ID',
  `last_login_at` timestamp NULL DEFAULT NULL COMMENT 'Last Login Time',
  `last_logout_at` timestamp NULL DEFAULT NULL COMMENT 'Last Logout Time',
  PRIMARY KEY (`log_id`),
  UNIQUE KEY `CUSTOMER_LOG_CUSTOMER_ID` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Log Table';

/*Data for the table `customer_log` */

/*Table structure for table `customer_visitor` */

DROP TABLE IF EXISTS `customer_visitor`;

CREATE TABLE `customer_visitor` (
  `visitor_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Visitor ID',
  `customer_id` int(11) DEFAULT NULL COMMENT 'Customer Id',
  `session_id` varchar(64) DEFAULT NULL COMMENT 'Session ID',
  `last_visit_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Last Visit Time',
  PRIMARY KEY (`visitor_id`),
  KEY `CUSTOMER_VISITOR_CUSTOMER_ID` (`customer_id`),
  KEY `CUSTOMER_VISITOR_LAST_VISIT_AT` (`last_visit_at`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='Visitor Table';

/*Data for the table `customer_visitor` */

insert  into `customer_visitor`(`visitor_id`,`customer_id`,`session_id`,`last_visit_at`) values (1,NULL,'0g2sjv13gjv2ngg75lnf4dl7r5','2016-08-26 10:48:11'),(2,NULL,'e3a835r4ekpalq6shcpdp27m01','2016-08-26 11:09:43'),(3,NULL,'ovoahe39tqlo2adgddgbjmdua0','2016-08-26 17:32:57'),(4,NULL,'3ikat46mnefd6cu2kl7st5lil3','2016-08-26 23:41:56'),(5,NULL,'edin6gmksh4vre1g6s93i2g6r7','2016-09-05 09:51:24'),(6,NULL,'tdf66995jl8nd07mvhlkfe1q55','2016-09-05 10:04:30'),(7,NULL,'vcvrart1fnv23prnp1na4md5k5','2016-09-05 10:24:52'),(8,NULL,'9p2mjf66j4ic00fqq9mvs3v193','2016-09-05 10:53:03'),(9,NULL,'gva1u7hrc7uqi0b9ere7ooqrr6','2016-09-05 11:02:59'),(10,NULL,'2sljojec658dp6rail5mbgjg11','2016-09-05 11:05:13'),(11,NULL,'ndl5gkrlih811rj1vbrgcjaff1','2016-10-21 23:24:05'),(12,NULL,'b9vd4d0f8v7rubo54s7ic8d4c2','2016-10-21 23:35:58');

/*Table structure for table `design_change` */

DROP TABLE IF EXISTS `design_change`;

CREATE TABLE `design_change` (
  `design_change_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Design Change Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `design` varchar(255) DEFAULT NULL COMMENT 'Design',
  `date_from` date DEFAULT NULL COMMENT 'First Date of Design Activity',
  `date_to` date DEFAULT NULL COMMENT 'Last Date of Design Activity',
  PRIMARY KEY (`design_change_id`),
  KEY `DESIGN_CHANGE_STORE_ID` (`store_id`),
  CONSTRAINT `DESIGN_CHANGE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Design Changes';

/*Data for the table `design_change` */

/*Table structure for table `design_config_grid_flat` */

DROP TABLE IF EXISTS `design_config_grid_flat`;

CREATE TABLE `design_config_grid_flat` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `store_website_id` int(11) DEFAULT NULL COMMENT 'Store_website_id',
  `store_group_id` int(11) DEFAULT NULL COMMENT 'Store_group_id',
  `store_id` int(11) DEFAULT NULL COMMENT 'Store_id',
  `theme_theme_id` varchar(255) DEFAULT NULL COMMENT 'Theme_theme_id',
  PRIMARY KEY (`entity_id`),
  KEY `DESIGN_CONFIG_GRID_FLAT_STORE_WEBSITE_ID` (`store_website_id`),
  KEY `DESIGN_CONFIG_GRID_FLAT_STORE_GROUP_ID` (`store_group_id`),
  KEY `DESIGN_CONFIG_GRID_FLAT_STORE_ID` (`store_id`),
  FULLTEXT KEY `DESIGN_CONFIG_GRID_FLAT_THEME_THEME_ID` (`theme_theme_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='design_config_grid_flat';

/*Data for the table `design_config_grid_flat` */

insert  into `design_config_grid_flat`(`entity_id`,`store_website_id`,`store_group_id`,`store_id`,`theme_theme_id`) values (0,NULL,NULL,NULL,'4'),(1,1,NULL,NULL,'4'),(2,1,1,1,'4'),(3,1,1,2,'4');

/*Table structure for table `directory_country` */

DROP TABLE IF EXISTS `directory_country`;

CREATE TABLE `directory_country` (
  `country_id` varchar(2) NOT NULL COMMENT 'Country Id in ISO-2',
  `iso2_code` varchar(2) DEFAULT NULL COMMENT 'Country ISO-2 format',
  `iso3_code` varchar(3) DEFAULT NULL COMMENT 'Country ISO-3',
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Country';

/*Data for the table `directory_country` */

insert  into `directory_country`(`country_id`,`iso2_code`,`iso3_code`) values ('AD','AD','AND'),('AE','AE','ARE'),('AF','AF','AFG'),('AG','AG','ATG'),('AI','AI','AIA'),('AL','AL','ALB'),('AM','AM','ARM'),('AN','AN','ANT'),('AO','AO','AGO'),('AQ','AQ','ATA'),('AR','AR','ARG'),('AS','AS','ASM'),('AT','AT','AUT'),('AU','AU','AUS'),('AW','AW','ABW'),('AX','AX','ALA'),('AZ','AZ','AZE'),('BA','BA','BIH'),('BB','BB','BRB'),('BD','BD','BGD'),('BE','BE','BEL'),('BF','BF','BFA'),('BG','BG','BGR'),('BH','BH','BHR'),('BI','BI','BDI'),('BJ','BJ','BEN'),('BL','BL','BLM'),('BM','BM','BMU'),('BN','BN','BRN'),('BO','BO','BOL'),('BR','BR','BRA'),('BS','BS','BHS'),('BT','BT','BTN'),('BV','BV','BVT'),('BW','BW','BWA'),('BY','BY','BLR'),('BZ','BZ','BLZ'),('CA','CA','CAN'),('CC','CC','CCK'),('CD','CD','COD'),('CF','CF','CAF'),('CG','CG','COG'),('CH','CH','CHE'),('CI','CI','CIV'),('CK','CK','COK'),('CL','CL','CHL'),('CM','CM','CMR'),('CN','CN','CHN'),('CO','CO','COL'),('CR','CR','CRI'),('CU','CU','CUB'),('CV','CV','CPV'),('CX','CX','CXR'),('CY','CY','CYP'),('CZ','CZ','CZE'),('DE','DE','DEU'),('DJ','DJ','DJI'),('DK','DK','DNK'),('DM','DM','DMA'),('DO','DO','DOM'),('DZ','DZ','DZA'),('EC','EC','ECU'),('EE','EE','EST'),('EG','EG','EGY'),('EH','EH','ESH'),('ER','ER','ERI'),('ES','ES','ESP'),('ET','ET','ETH'),('FI','FI','FIN'),('FJ','FJ','FJI'),('FK','FK','FLK'),('FM','FM','FSM'),('FO','FO','FRO'),('FR','FR','FRA'),('GA','GA','GAB'),('GB','GB','GBR'),('GD','GD','GRD'),('GE','GE','GEO'),('GF','GF','GUF'),('GG','GG','GGY'),('GH','GH','GHA'),('GI','GI','GIB'),('GL','GL','GRL'),('GM','GM','GMB'),('GN','GN','GIN'),('GP','GP','GLP'),('GQ','GQ','GNQ'),('GR','GR','GRC'),('GS','GS','SGS'),('GT','GT','GTM'),('GU','GU','GUM'),('GW','GW','GNB'),('GY','GY','GUY'),('HK','HK','HKG'),('HM','HM','HMD'),('HN','HN','HND'),('HR','HR','HRV'),('HT','HT','HTI'),('HU','HU','HUN'),('ID','ID','IDN'),('IE','IE','IRL'),('IL','IL','ISR'),('IM','IM','IMN'),('IN','IN','IND'),('IO','IO','IOT'),('IQ','IQ','IRQ'),('IR','IR','IRN'),('IS','IS','ISL'),('IT','IT','ITA'),('JE','JE','JEY'),('JM','JM','JAM'),('JO','JO','JOR'),('JP','JP','JPN'),('KE','KE','KEN'),('KG','KG','KGZ'),('KH','KH','KHM'),('KI','KI','KIR'),('KM','KM','COM'),('KN','KN','KNA'),('KP','KP','PRK'),('KR','KR','KOR'),('KW','KW','KWT'),('KY','KY','CYM'),('KZ','KZ','KAZ'),('LA','LA','LAO'),('LB','LB','LBN'),('LC','LC','LCA'),('LI','LI','LIE'),('LK','LK','LKA'),('LR','LR','LBR'),('LS','LS','LSO'),('LT','LT','LTU'),('LU','LU','LUX'),('LV','LV','LVA'),('LY','LY','LBY'),('MA','MA','MAR'),('MC','MC','MCO'),('MD','MD','MDA'),('ME','ME','MNE'),('MF','MF','MAF'),('MG','MG','MDG'),('MH','MH','MHL'),('MK','MK','MKD'),('ML','ML','MLI'),('MM','MM','MMR'),('MN','MN','MNG'),('MO','MO','MAC'),('MP','MP','MNP'),('MQ','MQ','MTQ'),('MR','MR','MRT'),('MS','MS','MSR'),('MT','MT','MLT'),('MU','MU','MUS'),('MV','MV','MDV'),('MW','MW','MWI'),('MX','MX','MEX'),('MY','MY','MYS'),('MZ','MZ','MOZ'),('NA','NA','NAM'),('NC','NC','NCL'),('NE','NE','NER'),('NF','NF','NFK'),('NG','NG','NGA'),('NI','NI','NIC'),('NL','NL','NLD'),('NO','NO','NOR'),('NP','NP','NPL'),('NR','NR','NRU'),('NU','NU','NIU'),('NZ','NZ','NZL'),('OM','OM','OMN'),('PA','PA','PAN'),('PE','PE','PER'),('PF','PF','PYF'),('PG','PG','PNG'),('PH','PH','PHL'),('PK','PK','PAK'),('PL','PL','POL'),('PM','PM','SPM'),('PN','PN','PCN'),('PS','PS','PSE'),('PT','PT','PRT'),('PW','PW','PLW'),('PY','PY','PRY'),('QA','QA','QAT'),('RE','RE','REU'),('RO','RO','ROU'),('RS','RS','SRB'),('RU','RU','RUS'),('RW','RW','RWA'),('SA','SA','SAU'),('SB','SB','SLB'),('SC','SC','SYC'),('SD','SD','SDN'),('SE','SE','SWE'),('SG','SG','SGP'),('SH','SH','SHN'),('SI','SI','SVN'),('SJ','SJ','SJM'),('SK','SK','SVK'),('SL','SL','SLE'),('SM','SM','SMR'),('SN','SN','SEN'),('SO','SO','SOM'),('SR','SR','SUR'),('ST','ST','STP'),('SV','SV','SLV'),('SY','SY','SYR'),('SZ','SZ','SWZ'),('TC','TC','TCA'),('TD','TD','TCD'),('TF','TF','ATF'),('TG','TG','TGO'),('TH','TH','THA'),('TJ','TJ','TJK'),('TK','TK','TKL'),('TL','TL','TLS'),('TM','TM','TKM'),('TN','TN','TUN'),('TO','TO','TON'),('TR','TR','TUR'),('TT','TT','TTO'),('TV','TV','TUV'),('TW','TW','TWN'),('TZ','TZ','TZA'),('UA','UA','UKR'),('UG','UG','UGA'),('UM','UM','UMI'),('US','US','USA'),('UY','UY','URY'),('UZ','UZ','UZB'),('VA','VA','VAT'),('VC','VC','VCT'),('VE','VE','VEN'),('VG','VG','VGB'),('VI','VI','VIR'),('VN','VN','VNM'),('VU','VU','VUT'),('WF','WF','WLF'),('WS','WS','WSM'),('YE','YE','YEM'),('YT','YT','MYT'),('ZA','ZA','ZAF'),('ZM','ZM','ZMB'),('ZW','ZW','ZWE');

/*Table structure for table `directory_country_format` */

DROP TABLE IF EXISTS `directory_country_format`;

CREATE TABLE `directory_country_format` (
  `country_format_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Country Format Id',
  `country_id` varchar(2) DEFAULT NULL COMMENT 'Country Id in ISO-2',
  `type` varchar(30) DEFAULT NULL COMMENT 'Country Format Type',
  `format` text NOT NULL COMMENT 'Country Format',
  PRIMARY KEY (`country_format_id`),
  UNIQUE KEY `DIRECTORY_COUNTRY_FORMAT_COUNTRY_ID_TYPE` (`country_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Country Format';

/*Data for the table `directory_country_format` */

/*Table structure for table `directory_country_region` */

DROP TABLE IF EXISTS `directory_country_region`;

CREATE TABLE `directory_country_region` (
  `region_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Region Id',
  `country_id` varchar(4) NOT NULL DEFAULT '0' COMMENT 'Country Id in ISO-2',
  `code` varchar(32) DEFAULT NULL COMMENT 'Region code',
  `default_name` varchar(255) DEFAULT NULL COMMENT 'Region Name',
  PRIMARY KEY (`region_id`),
  KEY `DIRECTORY_COUNTRY_REGION_COUNTRY_ID` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=512 DEFAULT CHARSET=utf8 COMMENT='Directory Country Region';

/*Data for the table `directory_country_region` */

insert  into `directory_country_region`(`region_id`,`country_id`,`code`,`default_name`) values (1,'US','AL','Alabama'),(2,'US','AK','Alaska'),(3,'US','AS','American Samoa'),(4,'US','AZ','Arizona'),(5,'US','AR','Arkansas'),(6,'US','AE','Armed Forces Africa'),(7,'US','AA','Armed Forces Americas'),(8,'US','AE','Armed Forces Canada'),(9,'US','AE','Armed Forces Europe'),(10,'US','AE','Armed Forces Middle East'),(11,'US','AP','Armed Forces Pacific'),(12,'US','CA','California'),(13,'US','CO','Colorado'),(14,'US','CT','Connecticut'),(15,'US','DE','Delaware'),(16,'US','DC','District of Columbia'),(17,'US','FM','Federated States Of Micronesia'),(18,'US','FL','Florida'),(19,'US','GA','Georgia'),(20,'US','GU','Guam'),(21,'US','HI','Hawaii'),(22,'US','ID','Idaho'),(23,'US','IL','Illinois'),(24,'US','IN','Indiana'),(25,'US','IA','Iowa'),(26,'US','KS','Kansas'),(27,'US','KY','Kentucky'),(28,'US','LA','Louisiana'),(29,'US','ME','Maine'),(30,'US','MH','Marshall Islands'),(31,'US','MD','Maryland'),(32,'US','MA','Massachusetts'),(33,'US','MI','Michigan'),(34,'US','MN','Minnesota'),(35,'US','MS','Mississippi'),(36,'US','MO','Missouri'),(37,'US','MT','Montana'),(38,'US','NE','Nebraska'),(39,'US','NV','Nevada'),(40,'US','NH','New Hampshire'),(41,'US','NJ','New Jersey'),(42,'US','NM','New Mexico'),(43,'US','NY','New York'),(44,'US','NC','North Carolina'),(45,'US','ND','North Dakota'),(46,'US','MP','Northern Mariana Islands'),(47,'US','OH','Ohio'),(48,'US','OK','Oklahoma'),(49,'US','OR','Oregon'),(50,'US','PW','Palau'),(51,'US','PA','Pennsylvania'),(52,'US','PR','Puerto Rico'),(53,'US','RI','Rhode Island'),(54,'US','SC','South Carolina'),(55,'US','SD','South Dakota'),(56,'US','TN','Tennessee'),(57,'US','TX','Texas'),(58,'US','UT','Utah'),(59,'US','VT','Vermont'),(60,'US','VI','Virgin Islands'),(61,'US','VA','Virginia'),(62,'US','WA','Washington'),(63,'US','WV','West Virginia'),(64,'US','WI','Wisconsin'),(65,'US','WY','Wyoming'),(66,'CA','AB','Alberta'),(67,'CA','BC','British Columbia'),(68,'CA','MB','Manitoba'),(69,'CA','NL','Newfoundland and Labrador'),(70,'CA','NB','New Brunswick'),(71,'CA','NS','Nova Scotia'),(72,'CA','NT','Northwest Territories'),(73,'CA','NU','Nunavut'),(74,'CA','ON','Ontario'),(75,'CA','PE','Prince Edward Island'),(76,'CA','QC','Quebec'),(77,'CA','SK','Saskatchewan'),(78,'CA','YT','Yukon Territory'),(79,'DE','NDS','Niedersachsen'),(80,'DE','BAW','Baden-Württemberg'),(81,'DE','BAY','Bayern'),(82,'DE','BER','Berlin'),(83,'DE','BRG','Brandenburg'),(84,'DE','BRE','Bremen'),(85,'DE','HAM','Hamburg'),(86,'DE','HES','Hessen'),(87,'DE','MEC','Mecklenburg-Vorpommern'),(88,'DE','NRW','Nordrhein-Westfalen'),(89,'DE','RHE','Rheinland-Pfalz'),(90,'DE','SAR','Saarland'),(91,'DE','SAS','Sachsen'),(92,'DE','SAC','Sachsen-Anhalt'),(93,'DE','SCN','Schleswig-Holstein'),(94,'DE','THE','Thüringen'),(95,'AT','WI','Wien'),(96,'AT','NO','Niederösterreich'),(97,'AT','OO','Oberösterreich'),(98,'AT','SB','Salzburg'),(99,'AT','KN','Kärnten'),(100,'AT','ST','Steiermark'),(101,'AT','TI','Tirol'),(102,'AT','BL','Burgenland'),(103,'AT','VB','Vorarlberg'),(104,'CH','AG','Aargau'),(105,'CH','AI','Appenzell Innerrhoden'),(106,'CH','AR','Appenzell Ausserrhoden'),(107,'CH','BE','Bern'),(108,'CH','BL','Basel-Landschaft'),(109,'CH','BS','Basel-Stadt'),(110,'CH','FR','Freiburg'),(111,'CH','GE','Genf'),(112,'CH','GL','Glarus'),(113,'CH','GR','Graubünden'),(114,'CH','JU','Jura'),(115,'CH','LU','Luzern'),(116,'CH','NE','Neuenburg'),(117,'CH','NW','Nidwalden'),(118,'CH','OW','Obwalden'),(119,'CH','SG','St. Gallen'),(120,'CH','SH','Schaffhausen'),(121,'CH','SO','Solothurn'),(122,'CH','SZ','Schwyz'),(123,'CH','TG','Thurgau'),(124,'CH','TI','Tessin'),(125,'CH','UR','Uri'),(126,'CH','VD','Waadt'),(127,'CH','VS','Wallis'),(128,'CH','ZG','Zug'),(129,'CH','ZH','Zürich'),(130,'ES','A Coruсa','A Coruña'),(131,'ES','Alava','Alava'),(132,'ES','Albacete','Albacete'),(133,'ES','Alicante','Alicante'),(134,'ES','Almeria','Almeria'),(135,'ES','Asturias','Asturias'),(136,'ES','Avila','Avila'),(137,'ES','Badajoz','Badajoz'),(138,'ES','Baleares','Baleares'),(139,'ES','Barcelona','Barcelona'),(140,'ES','Burgos','Burgos'),(141,'ES','Caceres','Caceres'),(142,'ES','Cadiz','Cadiz'),(143,'ES','Cantabria','Cantabria'),(144,'ES','Castellon','Castellon'),(145,'ES','Ceuta','Ceuta'),(146,'ES','Ciudad Real','Ciudad Real'),(147,'ES','Cordoba','Cordoba'),(148,'ES','Cuenca','Cuenca'),(149,'ES','Girona','Girona'),(150,'ES','Granada','Granada'),(151,'ES','Guadalajara','Guadalajara'),(152,'ES','Guipuzcoa','Guipuzcoa'),(153,'ES','Huelva','Huelva'),(154,'ES','Huesca','Huesca'),(155,'ES','Jaen','Jaen'),(156,'ES','La Rioja','La Rioja'),(157,'ES','Las Palmas','Las Palmas'),(158,'ES','Leon','Leon'),(159,'ES','Lleida','Lleida'),(160,'ES','Lugo','Lugo'),(161,'ES','Madrid','Madrid'),(162,'ES','Malaga','Malaga'),(163,'ES','Melilla','Melilla'),(164,'ES','Murcia','Murcia'),(165,'ES','Navarra','Navarra'),(166,'ES','Ourense','Ourense'),(167,'ES','Palencia','Palencia'),(168,'ES','Pontevedra','Pontevedra'),(169,'ES','Salamanca','Salamanca'),(170,'ES','Santa Cruz de Tenerife','Santa Cruz de Tenerife'),(171,'ES','Segovia','Segovia'),(172,'ES','Sevilla','Sevilla'),(173,'ES','Soria','Soria'),(174,'ES','Tarragona','Tarragona'),(175,'ES','Teruel','Teruel'),(176,'ES','Toledo','Toledo'),(177,'ES','Valencia','Valencia'),(178,'ES','Valladolid','Valladolid'),(179,'ES','Vizcaya','Vizcaya'),(180,'ES','Zamora','Zamora'),(181,'ES','Zaragoza','Zaragoza'),(182,'FR','1','Ain'),(183,'FR','2','Aisne'),(184,'FR','3','Allier'),(185,'FR','4','Alpes-de-Haute-Provence'),(186,'FR','5','Hautes-Alpes'),(187,'FR','6','Alpes-Maritimes'),(188,'FR','7','Ardèche'),(189,'FR','8','Ardennes'),(190,'FR','9','Ariège'),(191,'FR','10','Aube'),(192,'FR','11','Aude'),(193,'FR','12','Aveyron'),(194,'FR','13','Bouches-du-Rhône'),(195,'FR','14','Calvados'),(196,'FR','15','Cantal'),(197,'FR','16','Charente'),(198,'FR','17','Charente-Maritime'),(199,'FR','18','Cher'),(200,'FR','19','Corrèze'),(201,'FR','2A','Corse-du-Sud'),(202,'FR','2B','Haute-Corse'),(203,'FR','21','Côte-d\'Or'),(204,'FR','22','Côtes-d\'Armor'),(205,'FR','23','Creuse'),(206,'FR','24','Dordogne'),(207,'FR','25','Doubs'),(208,'FR','26','Drôme'),(209,'FR','27','Eure'),(210,'FR','28','Eure-et-Loir'),(211,'FR','29','Finistère'),(212,'FR','30','Gard'),(213,'FR','31','Haute-Garonne'),(214,'FR','32','Gers'),(215,'FR','33','Gironde'),(216,'FR','34','Hérault'),(217,'FR','35','Ille-et-Vilaine'),(218,'FR','36','Indre'),(219,'FR','37','Indre-et-Loire'),(220,'FR','38','Isère'),(221,'FR','39','Jura'),(222,'FR','40','Landes'),(223,'FR','41','Loir-et-Cher'),(224,'FR','42','Loire'),(225,'FR','43','Haute-Loire'),(226,'FR','44','Loire-Atlantique'),(227,'FR','45','Loiret'),(228,'FR','46','Lot'),(229,'FR','47','Lot-et-Garonne'),(230,'FR','48','Lozère'),(231,'FR','49','Maine-et-Loire'),(232,'FR','50','Manche'),(233,'FR','51','Marne'),(234,'FR','52','Haute-Marne'),(235,'FR','53','Mayenne'),(236,'FR','54','Meurthe-et-Moselle'),(237,'FR','55','Meuse'),(238,'FR','56','Morbihan'),(239,'FR','57','Moselle'),(240,'FR','58','Nièvre'),(241,'FR','59','Nord'),(242,'FR','60','Oise'),(243,'FR','61','Orne'),(244,'FR','62','Pas-de-Calais'),(245,'FR','63','Puy-de-Dôme'),(246,'FR','64','Pyrénées-Atlantiques'),(247,'FR','65','Hautes-Pyrénées'),(248,'FR','66','Pyrénées-Orientales'),(249,'FR','67','Bas-Rhin'),(250,'FR','68','Haut-Rhin'),(251,'FR','69','Rhône'),(252,'FR','70','Haute-Saône'),(253,'FR','71','Saône-et-Loire'),(254,'FR','72','Sarthe'),(255,'FR','73','Savoie'),(256,'FR','74','Haute-Savoie'),(257,'FR','75','Paris'),(258,'FR','76','Seine-Maritime'),(259,'FR','77','Seine-et-Marne'),(260,'FR','78','Yvelines'),(261,'FR','79','Deux-Sèvres'),(262,'FR','80','Somme'),(263,'FR','81','Tarn'),(264,'FR','82','Tarn-et-Garonne'),(265,'FR','83','Var'),(266,'FR','84','Vaucluse'),(267,'FR','85','Vendée'),(268,'FR','86','Vienne'),(269,'FR','87','Haute-Vienne'),(270,'FR','88','Vosges'),(271,'FR','89','Yonne'),(272,'FR','90','Territoire-de-Belfort'),(273,'FR','91','Essonne'),(274,'FR','92','Hauts-de-Seine'),(275,'FR','93','Seine-Saint-Denis'),(276,'FR','94','Val-de-Marne'),(277,'FR','95','Val-d\'Oise'),(278,'RO','AB','Alba'),(279,'RO','AR','Arad'),(280,'RO','AG','Argeş'),(281,'RO','BC','Bacău'),(282,'RO','BH','Bihor'),(283,'RO','BN','Bistriţa-Năsăud'),(284,'RO','BT','Botoşani'),(285,'RO','BV','Braşov'),(286,'RO','BR','Brăila'),(287,'RO','B','Bucureşti'),(288,'RO','BZ','Buzău'),(289,'RO','CS','Caraş-Severin'),(290,'RO','CL','Călăraşi'),(291,'RO','CJ','Cluj'),(292,'RO','CT','Constanţa'),(293,'RO','CV','Covasna'),(294,'RO','DB','Dâmboviţa'),(295,'RO','DJ','Dolj'),(296,'RO','GL','Galaţi'),(297,'RO','GR','Giurgiu'),(298,'RO','GJ','Gorj'),(299,'RO','HR','Harghita'),(300,'RO','HD','Hunedoara'),(301,'RO','IL','Ialomiţa'),(302,'RO','IS','Iaşi'),(303,'RO','IF','Ilfov'),(304,'RO','MM','Maramureş'),(305,'RO','MH','Mehedinţi'),(306,'RO','MS','Mureş'),(307,'RO','NT','Neamţ'),(308,'RO','OT','Olt'),(309,'RO','PH','Prahova'),(310,'RO','SM','Satu-Mare'),(311,'RO','SJ','Sălaj'),(312,'RO','SB','Sibiu'),(313,'RO','SV','Suceava'),(314,'RO','TR','Teleorman'),(315,'RO','TM','Timiş'),(316,'RO','TL','Tulcea'),(317,'RO','VS','Vaslui'),(318,'RO','VL','Vâlcea'),(319,'RO','VN','Vrancea'),(320,'FI','Lappi','Lappi'),(321,'FI','Pohjois-Pohjanmaa','Pohjois-Pohjanmaa'),(322,'FI','Kainuu','Kainuu'),(323,'FI','Pohjois-Karjala','Pohjois-Karjala'),(324,'FI','Pohjois-Savo','Pohjois-Savo'),(325,'FI','Etelä-Savo','Etelä-Savo'),(326,'FI','Etelä-Pohjanmaa','Etelä-Pohjanmaa'),(327,'FI','Pohjanmaa','Pohjanmaa'),(328,'FI','Pirkanmaa','Pirkanmaa'),(329,'FI','Satakunta','Satakunta'),(330,'FI','Keski-Pohjanmaa','Keski-Pohjanmaa'),(331,'FI','Keski-Suomi','Keski-Suomi'),(332,'FI','Varsinais-Suomi','Varsinais-Suomi'),(333,'FI','Etelä-Karjala','Etelä-Karjala'),(334,'FI','Päijät-Häme','Päijät-Häme'),(335,'FI','Kanta-Häme','Kanta-Häme'),(336,'FI','Uusimaa','Uusimaa'),(337,'FI','Itä-Uusimaa','Itä-Uusimaa'),(338,'FI','Kymenlaakso','Kymenlaakso'),(339,'FI','Ahvenanmaa','Ahvenanmaa'),(340,'EE','EE-37','Harjumaa'),(341,'EE','EE-39','Hiiumaa'),(342,'EE','EE-44','Ida-Virumaa'),(343,'EE','EE-49','Jõgevamaa'),(344,'EE','EE-51','Järvamaa'),(345,'EE','EE-57','Läänemaa'),(346,'EE','EE-59','Lääne-Virumaa'),(347,'EE','EE-65','Põlvamaa'),(348,'EE','EE-67','Pärnumaa'),(349,'EE','EE-70','Raplamaa'),(350,'EE','EE-74','Saaremaa'),(351,'EE','EE-78','Tartumaa'),(352,'EE','EE-82','Valgamaa'),(353,'EE','EE-84','Viljandimaa'),(354,'EE','EE-86','Võrumaa'),(355,'LV','LV-DGV','Daugavpils'),(356,'LV','LV-JEL','Jelgava'),(357,'LV','Jēkabpils','Jēkabpils'),(358,'LV','LV-JUR','Jūrmala'),(359,'LV','LV-LPX','Liepāja'),(360,'LV','LV-LE','Liepājas novads'),(361,'LV','LV-REZ','Rēzekne'),(362,'LV','LV-RIX','Rīga'),(363,'LV','LV-RI','Rīgas novads'),(364,'LV','Valmiera','Valmiera'),(365,'LV','LV-VEN','Ventspils'),(366,'LV','Aglonas novads','Aglonas novads'),(367,'LV','LV-AI','Aizkraukles novads'),(368,'LV','Aizputes novads','Aizputes novads'),(369,'LV','Aknīstes novads','Aknīstes novads'),(370,'LV','Alojas novads','Alojas novads'),(371,'LV','Alsungas novads','Alsungas novads'),(372,'LV','LV-AL','Alūksnes novads'),(373,'LV','Amatas novads','Amatas novads'),(374,'LV','Apes novads','Apes novads'),(375,'LV','Auces novads','Auces novads'),(376,'LV','Babītes novads','Babītes novads'),(377,'LV','Baldones novads','Baldones novads'),(378,'LV','Baltinavas novads','Baltinavas novads'),(379,'LV','LV-BL','Balvu novads'),(380,'LV','LV-BU','Bauskas novads'),(381,'LV','Beverīnas novads','Beverīnas novads'),(382,'LV','Brocēnu novads','Brocēnu novads'),(383,'LV','Burtnieku novads','Burtnieku novads'),(384,'LV','Carnikavas novads','Carnikavas novads'),(385,'LV','Cesvaines novads','Cesvaines novads'),(386,'LV','Ciblas novads','Ciblas novads'),(387,'LV','LV-CE','Cēsu novads'),(388,'LV','Dagdas novads','Dagdas novads'),(389,'LV','LV-DA','Daugavpils novads'),(390,'LV','LV-DO','Dobeles novads'),(391,'LV','Dundagas novads','Dundagas novads'),(392,'LV','Durbes novads','Durbes novads'),(393,'LV','Engures novads','Engures novads'),(394,'LV','Garkalnes novads','Garkalnes novads'),(395,'LV','Grobiņas novads','Grobiņas novads'),(396,'LV','LV-GU','Gulbenes novads'),(397,'LV','Iecavas novads','Iecavas novads'),(398,'LV','Ikšķiles novads','Ikšķiles novads'),(399,'LV','Ilūkstes novads','Ilūkstes novads'),(400,'LV','Inčukalna novads','Inčukalna novads'),(401,'LV','Jaunjelgavas novads','Jaunjelgavas novads'),(402,'LV','Jaunpiebalgas novads','Jaunpiebalgas novads'),(403,'LV','Jaunpils novads','Jaunpils novads'),(404,'LV','LV-JL','Jelgavas novads'),(405,'LV','LV-JK','Jēkabpils novads'),(406,'LV','Kandavas novads','Kandavas novads'),(407,'LV','Kokneses novads','Kokneses novads'),(408,'LV','Krimuldas novads','Krimuldas novads'),(409,'LV','Krustpils novads','Krustpils novads'),(410,'LV','LV-KR','Krāslavas novads'),(411,'LV','LV-KU','Kuldīgas novads'),(412,'LV','Kārsavas novads','Kārsavas novads'),(413,'LV','Lielvārdes novads','Lielvārdes novads'),(414,'LV','LV-LM','Limbažu novads'),(415,'LV','Lubānas novads','Lubānas novads'),(416,'LV','LV-LU','Ludzas novads'),(417,'LV','Līgatnes novads','Līgatnes novads'),(418,'LV','Līvānu novads','Līvānu novads'),(419,'LV','LV-MA','Madonas novads'),(420,'LV','Mazsalacas novads','Mazsalacas novads'),(421,'LV','Mālpils novads','Mālpils novads'),(422,'LV','Mārupes novads','Mārupes novads'),(423,'LV','Naukšēnu novads','Naukšēnu novads'),(424,'LV','Neretas novads','Neretas novads'),(425,'LV','Nīcas novads','Nīcas novads'),(426,'LV','LV-OG','Ogres novads'),(427,'LV','Olaines novads','Olaines novads'),(428,'LV','Ozolnieku novads','Ozolnieku novads'),(429,'LV','LV-PR','Preiļu novads'),(430,'LV','Priekules novads','Priekules novads'),(431,'LV','Priekuļu novads','Priekuļu novads'),(432,'LV','Pārgaujas novads','Pārgaujas novads'),(433,'LV','Pāvilostas novads','Pāvilostas novads'),(434,'LV','Pļaviņu novads','Pļaviņu novads'),(435,'LV','Raunas novads','Raunas novads'),(436,'LV','Riebiņu novads','Riebiņu novads'),(437,'LV','Rojas novads','Rojas novads'),(438,'LV','Ropažu novads','Ropažu novads'),(439,'LV','Rucavas novads','Rucavas novads'),(440,'LV','Rugāju novads','Rugāju novads'),(441,'LV','Rundāles novads','Rundāles novads'),(442,'LV','LV-RE','Rēzeknes novads'),(443,'LV','Rūjienas novads','Rūjienas novads'),(444,'LV','Salacgrīvas novads','Salacgrīvas novads'),(445,'LV','Salas novads','Salas novads'),(446,'LV','Salaspils novads','Salaspils novads'),(447,'LV','LV-SA','Saldus novads'),(448,'LV','Saulkrastu novads','Saulkrastu novads'),(449,'LV','Siguldas novads','Siguldas novads'),(450,'LV','Skrundas novads','Skrundas novads'),(451,'LV','Skrīveru novads','Skrīveru novads'),(452,'LV','Smiltenes novads','Smiltenes novads'),(453,'LV','Stopiņu novads','Stopiņu novads'),(454,'LV','Strenču novads','Strenču novads'),(455,'LV','Sējas novads','Sējas novads'),(456,'LV','LV-TA','Talsu novads'),(457,'LV','LV-TU','Tukuma novads'),(458,'LV','Tērvetes novads','Tērvetes novads'),(459,'LV','Vaiņodes novads','Vaiņodes novads'),(460,'LV','LV-VK','Valkas novads'),(461,'LV','LV-VM','Valmieras novads'),(462,'LV','Varakļānu novads','Varakļānu novads'),(463,'LV','Vecpiebalgas novads','Vecpiebalgas novads'),(464,'LV','Vecumnieku novads','Vecumnieku novads'),(465,'LV','LV-VE','Ventspils novads'),(466,'LV','Viesītes novads','Viesītes novads'),(467,'LV','Viļakas novads','Viļakas novads'),(468,'LV','Viļānu novads','Viļānu novads'),(469,'LV','Vārkavas novads','Vārkavas novads'),(470,'LV','Zilupes novads','Zilupes novads'),(471,'LV','Ādažu novads','Ādažu novads'),(472,'LV','Ērgļu novads','Ērgļu novads'),(473,'LV','Ķeguma novads','Ķeguma novads'),(474,'LV','Ķekavas novads','Ķekavas novads'),(475,'LT','LT-AL','Alytaus Apskritis'),(476,'LT','LT-KU','Kauno Apskritis'),(477,'LT','LT-KL','Klaipėdos Apskritis'),(478,'LT','LT-MR','Marijampolės Apskritis'),(479,'LT','LT-PN','Panevėžio Apskritis'),(480,'LT','LT-SA','Šiaulių Apskritis'),(481,'LT','LT-TA','Tauragės Apskritis'),(482,'LT','LT-TE','Telšių Apskritis'),(483,'LT','LT-UT','Utenos Apskritis'),(484,'LT','LT-VL','Vilniaus Apskritis'),(485,'BR','AC','Acre'),(486,'BR','AL','Alagoas'),(487,'BR','AP','Amapá'),(488,'BR','AM','Amazonas'),(489,'BR','BA','Bahia'),(490,'BR','CE','Ceará'),(491,'BR','ES','Espírito Santo'),(492,'BR','GO','Goiás'),(493,'BR','MA','Maranhão'),(494,'BR','MT','Mato Grosso'),(495,'BR','MS','Mato Grosso do Sul'),(496,'BR','MG','Minas Gerais'),(497,'BR','PA','Pará'),(498,'BR','PB','Paraíba'),(499,'BR','PR','Paraná'),(500,'BR','PE','Pernambuco'),(501,'BR','PI','Piauí'),(502,'BR','RJ','Rio de Janeiro'),(503,'BR','RN','Rio Grande do Norte'),(504,'BR','RS','Rio Grande do Sul'),(505,'BR','RO','Rondônia'),(506,'BR','RR','Roraima'),(507,'BR','SC','Santa Catarina'),(508,'BR','SP','São Paulo'),(509,'BR','SE','Sergipe'),(510,'BR','TO','Tocantins'),(511,'BR','DF','Distrito Federal');

/*Table structure for table `directory_country_region_name` */

DROP TABLE IF EXISTS `directory_country_region_name`;

CREATE TABLE `directory_country_region_name` (
  `locale` varchar(8) NOT NULL COMMENT 'Locale',
  `region_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Region Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Region Name',
  PRIMARY KEY (`locale`,`region_id`),
  KEY `DIRECTORY_COUNTRY_REGION_NAME_REGION_ID` (`region_id`),
  CONSTRAINT `DIR_COUNTRY_REGION_NAME_REGION_ID_DIR_COUNTRY_REGION_REGION_ID` FOREIGN KEY (`region_id`) REFERENCES `directory_country_region` (`region_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Country Region Name';

/*Data for the table `directory_country_region_name` */

insert  into `directory_country_region_name`(`locale`,`region_id`,`name`) values ('en_US',1,'Alabama'),('en_US',2,'Alaska'),('en_US',3,'American Samoa'),('en_US',4,'Arizona'),('en_US',5,'Arkansas'),('en_US',6,'Armed Forces Africa'),('en_US',7,'Armed Forces Americas'),('en_US',8,'Armed Forces Canada'),('en_US',9,'Armed Forces Europe'),('en_US',10,'Armed Forces Middle East'),('en_US',11,'Armed Forces Pacific'),('en_US',12,'California'),('en_US',13,'Colorado'),('en_US',14,'Connecticut'),('en_US',15,'Delaware'),('en_US',16,'District of Columbia'),('en_US',17,'Federated States Of Micronesia'),('en_US',18,'Florida'),('en_US',19,'Georgia'),('en_US',20,'Guam'),('en_US',21,'Hawaii'),('en_US',22,'Idaho'),('en_US',23,'Illinois'),('en_US',24,'Indiana'),('en_US',25,'Iowa'),('en_US',26,'Kansas'),('en_US',27,'Kentucky'),('en_US',28,'Louisiana'),('en_US',29,'Maine'),('en_US',30,'Marshall Islands'),('en_US',31,'Maryland'),('en_US',32,'Massachusetts'),('en_US',33,'Michigan'),('en_US',34,'Minnesota'),('en_US',35,'Mississippi'),('en_US',36,'Missouri'),('en_US',37,'Montana'),('en_US',38,'Nebraska'),('en_US',39,'Nevada'),('en_US',40,'New Hampshire'),('en_US',41,'New Jersey'),('en_US',42,'New Mexico'),('en_US',43,'New York'),('en_US',44,'North Carolina'),('en_US',45,'North Dakota'),('en_US',46,'Northern Mariana Islands'),('en_US',47,'Ohio'),('en_US',48,'Oklahoma'),('en_US',49,'Oregon'),('en_US',50,'Palau'),('en_US',51,'Pennsylvania'),('en_US',52,'Puerto Rico'),('en_US',53,'Rhode Island'),('en_US',54,'South Carolina'),('en_US',55,'South Dakota'),('en_US',56,'Tennessee'),('en_US',57,'Texas'),('en_US',58,'Utah'),('en_US',59,'Vermont'),('en_US',60,'Virgin Islands'),('en_US',61,'Virginia'),('en_US',62,'Washington'),('en_US',63,'West Virginia'),('en_US',64,'Wisconsin'),('en_US',65,'Wyoming'),('en_US',66,'Alberta'),('en_US',67,'British Columbia'),('en_US',68,'Manitoba'),('en_US',69,'Newfoundland and Labrador'),('en_US',70,'New Brunswick'),('en_US',71,'Nova Scotia'),('en_US',72,'Northwest Territories'),('en_US',73,'Nunavut'),('en_US',74,'Ontario'),('en_US',75,'Prince Edward Island'),('en_US',76,'Quebec'),('en_US',77,'Saskatchewan'),('en_US',78,'Yukon Territory'),('en_US',79,'Niedersachsen'),('en_US',80,'Baden-Württemberg'),('en_US',81,'Bayern'),('en_US',82,'Berlin'),('en_US',83,'Brandenburg'),('en_US',84,'Bremen'),('en_US',85,'Hamburg'),('en_US',86,'Hessen'),('en_US',87,'Mecklenburg-Vorpommern'),('en_US',88,'Nordrhein-Westfalen'),('en_US',89,'Rheinland-Pfalz'),('en_US',90,'Saarland'),('en_US',91,'Sachsen'),('en_US',92,'Sachsen-Anhalt'),('en_US',93,'Schleswig-Holstein'),('en_US',94,'Thüringen'),('en_US',95,'Wien'),('en_US',96,'Niederösterreich'),('en_US',97,'Oberösterreich'),('en_US',98,'Salzburg'),('en_US',99,'Kärnten'),('en_US',100,'Steiermark'),('en_US',101,'Tirol'),('en_US',102,'Burgenland'),('en_US',103,'Vorarlberg'),('en_US',104,'Aargau'),('en_US',105,'Appenzell Innerrhoden'),('en_US',106,'Appenzell Ausserrhoden'),('en_US',107,'Bern'),('en_US',108,'Basel-Landschaft'),('en_US',109,'Basel-Stadt'),('en_US',110,'Freiburg'),('en_US',111,'Genf'),('en_US',112,'Glarus'),('en_US',113,'Graubünden'),('en_US',114,'Jura'),('en_US',115,'Luzern'),('en_US',116,'Neuenburg'),('en_US',117,'Nidwalden'),('en_US',118,'Obwalden'),('en_US',119,'St. Gallen'),('en_US',120,'Schaffhausen'),('en_US',121,'Solothurn'),('en_US',122,'Schwyz'),('en_US',123,'Thurgau'),('en_US',124,'Tessin'),('en_US',125,'Uri'),('en_US',126,'Waadt'),('en_US',127,'Wallis'),('en_US',128,'Zug'),('en_US',129,'Zürich'),('en_US',130,'A Coruña'),('en_US',131,'Alava'),('en_US',132,'Albacete'),('en_US',133,'Alicante'),('en_US',134,'Almeria'),('en_US',135,'Asturias'),('en_US',136,'Avila'),('en_US',137,'Badajoz'),('en_US',138,'Baleares'),('en_US',139,'Barcelona'),('en_US',140,'Burgos'),('en_US',141,'Caceres'),('en_US',142,'Cadiz'),('en_US',143,'Cantabria'),('en_US',144,'Castellon'),('en_US',145,'Ceuta'),('en_US',146,'Ciudad Real'),('en_US',147,'Cordoba'),('en_US',148,'Cuenca'),('en_US',149,'Girona'),('en_US',150,'Granada'),('en_US',151,'Guadalajara'),('en_US',152,'Guipuzcoa'),('en_US',153,'Huelva'),('en_US',154,'Huesca'),('en_US',155,'Jaen'),('en_US',156,'La Rioja'),('en_US',157,'Las Palmas'),('en_US',158,'Leon'),('en_US',159,'Lleida'),('en_US',160,'Lugo'),('en_US',161,'Madrid'),('en_US',162,'Malaga'),('en_US',163,'Melilla'),('en_US',164,'Murcia'),('en_US',165,'Navarra'),('en_US',166,'Ourense'),('en_US',167,'Palencia'),('en_US',168,'Pontevedra'),('en_US',169,'Salamanca'),('en_US',170,'Santa Cruz de Tenerife'),('en_US',171,'Segovia'),('en_US',172,'Sevilla'),('en_US',173,'Soria'),('en_US',174,'Tarragona'),('en_US',175,'Teruel'),('en_US',176,'Toledo'),('en_US',177,'Valencia'),('en_US',178,'Valladolid'),('en_US',179,'Vizcaya'),('en_US',180,'Zamora'),('en_US',181,'Zaragoza'),('en_US',182,'Ain'),('en_US',183,'Aisne'),('en_US',184,'Allier'),('en_US',185,'Alpes-de-Haute-Provence'),('en_US',186,'Hautes-Alpes'),('en_US',187,'Alpes-Maritimes'),('en_US',188,'Ardèche'),('en_US',189,'Ardennes'),('en_US',190,'Ariège'),('en_US',191,'Aube'),('en_US',192,'Aude'),('en_US',193,'Aveyron'),('en_US',194,'Bouches-du-Rhône'),('en_US',195,'Calvados'),('en_US',196,'Cantal'),('en_US',197,'Charente'),('en_US',198,'Charente-Maritime'),('en_US',199,'Cher'),('en_US',200,'Corrèze'),('en_US',201,'Corse-du-Sud'),('en_US',202,'Haute-Corse'),('en_US',203,'Côte-d\'Or'),('en_US',204,'Côtes-d\'Armor'),('en_US',205,'Creuse'),('en_US',206,'Dordogne'),('en_US',207,'Doubs'),('en_US',208,'Drôme'),('en_US',209,'Eure'),('en_US',210,'Eure-et-Loir'),('en_US',211,'Finistère'),('en_US',212,'Gard'),('en_US',213,'Haute-Garonne'),('en_US',214,'Gers'),('en_US',215,'Gironde'),('en_US',216,'Hérault'),('en_US',217,'Ille-et-Vilaine'),('en_US',218,'Indre'),('en_US',219,'Indre-et-Loire'),('en_US',220,'Isère'),('en_US',221,'Jura'),('en_US',222,'Landes'),('en_US',223,'Loir-et-Cher'),('en_US',224,'Loire'),('en_US',225,'Haute-Loire'),('en_US',226,'Loire-Atlantique'),('en_US',227,'Loiret'),('en_US',228,'Lot'),('en_US',229,'Lot-et-Garonne'),('en_US',230,'Lozère'),('en_US',231,'Maine-et-Loire'),('en_US',232,'Manche'),('en_US',233,'Marne'),('en_US',234,'Haute-Marne'),('en_US',235,'Mayenne'),('en_US',236,'Meurthe-et-Moselle'),('en_US',237,'Meuse'),('en_US',238,'Morbihan'),('en_US',239,'Moselle'),('en_US',240,'Nièvre'),('en_US',241,'Nord'),('en_US',242,'Oise'),('en_US',243,'Orne'),('en_US',244,'Pas-de-Calais'),('en_US',245,'Puy-de-Dôme'),('en_US',246,'Pyrénées-Atlantiques'),('en_US',247,'Hautes-Pyrénées'),('en_US',248,'Pyrénées-Orientales'),('en_US',249,'Bas-Rhin'),('en_US',250,'Haut-Rhin'),('en_US',251,'Rhône'),('en_US',252,'Haute-Saône'),('en_US',253,'Saône-et-Loire'),('en_US',254,'Sarthe'),('en_US',255,'Savoie'),('en_US',256,'Haute-Savoie'),('en_US',257,'Paris'),('en_US',258,'Seine-Maritime'),('en_US',259,'Seine-et-Marne'),('en_US',260,'Yvelines'),('en_US',261,'Deux-Sèvres'),('en_US',262,'Somme'),('en_US',263,'Tarn'),('en_US',264,'Tarn-et-Garonne'),('en_US',265,'Var'),('en_US',266,'Vaucluse'),('en_US',267,'Vendée'),('en_US',268,'Vienne'),('en_US',269,'Haute-Vienne'),('en_US',270,'Vosges'),('en_US',271,'Yonne'),('en_US',272,'Territoire-de-Belfort'),('en_US',273,'Essonne'),('en_US',274,'Hauts-de-Seine'),('en_US',275,'Seine-Saint-Denis'),('en_US',276,'Val-de-Marne'),('en_US',277,'Val-d\'Oise'),('en_US',278,'Alba'),('en_US',279,'Arad'),('en_US',280,'Argeş'),('en_US',281,'Bacău'),('en_US',282,'Bihor'),('en_US',283,'Bistriţa-Năsăud'),('en_US',284,'Botoşani'),('en_US',285,'Braşov'),('en_US',286,'Brăila'),('en_US',287,'Bucureşti'),('en_US',288,'Buzău'),('en_US',289,'Caraş-Severin'),('en_US',290,'Călăraşi'),('en_US',291,'Cluj'),('en_US',292,'Constanţa'),('en_US',293,'Covasna'),('en_US',294,'Dâmboviţa'),('en_US',295,'Dolj'),('en_US',296,'Galaţi'),('en_US',297,'Giurgiu'),('en_US',298,'Gorj'),('en_US',299,'Harghita'),('en_US',300,'Hunedoara'),('en_US',301,'Ialomiţa'),('en_US',302,'Iaşi'),('en_US',303,'Ilfov'),('en_US',304,'Maramureş'),('en_US',305,'Mehedinţi'),('en_US',306,'Mureş'),('en_US',307,'Neamţ'),('en_US',308,'Olt'),('en_US',309,'Prahova'),('en_US',310,'Satu-Mare'),('en_US',311,'Sălaj'),('en_US',312,'Sibiu'),('en_US',313,'Suceava'),('en_US',314,'Teleorman'),('en_US',315,'Timiş'),('en_US',316,'Tulcea'),('en_US',317,'Vaslui'),('en_US',318,'Vâlcea'),('en_US',319,'Vrancea'),('en_US',320,'Lappi'),('en_US',321,'Pohjois-Pohjanmaa'),('en_US',322,'Kainuu'),('en_US',323,'Pohjois-Karjala'),('en_US',324,'Pohjois-Savo'),('en_US',325,'Etelä-Savo'),('en_US',326,'Etelä-Pohjanmaa'),('en_US',327,'Pohjanmaa'),('en_US',328,'Pirkanmaa'),('en_US',329,'Satakunta'),('en_US',330,'Keski-Pohjanmaa'),('en_US',331,'Keski-Suomi'),('en_US',332,'Varsinais-Suomi'),('en_US',333,'Etelä-Karjala'),('en_US',334,'Päijät-Häme'),('en_US',335,'Kanta-Häme'),('en_US',336,'Uusimaa'),('en_US',337,'Itä-Uusimaa'),('en_US',338,'Kymenlaakso'),('en_US',339,'Ahvenanmaa'),('en_US',340,'Harjumaa'),('en_US',341,'Hiiumaa'),('en_US',342,'Ida-Virumaa'),('en_US',343,'Jõgevamaa'),('en_US',344,'Järvamaa'),('en_US',345,'Läänemaa'),('en_US',346,'Lääne-Virumaa'),('en_US',347,'Põlvamaa'),('en_US',348,'Pärnumaa'),('en_US',349,'Raplamaa'),('en_US',350,'Saaremaa'),('en_US',351,'Tartumaa'),('en_US',352,'Valgamaa'),('en_US',353,'Viljandimaa'),('en_US',354,'Võrumaa'),('en_US',355,'Daugavpils'),('en_US',356,'Jelgava'),('en_US',357,'Jēkabpils'),('en_US',358,'Jūrmala'),('en_US',359,'Liepāja'),('en_US',360,'Liepājas novads'),('en_US',361,'Rēzekne'),('en_US',362,'Rīga'),('en_US',363,'Rīgas novads'),('en_US',364,'Valmiera'),('en_US',365,'Ventspils'),('en_US',366,'Aglonas novads'),('en_US',367,'Aizkraukles novads'),('en_US',368,'Aizputes novads'),('en_US',369,'Aknīstes novads'),('en_US',370,'Alojas novads'),('en_US',371,'Alsungas novads'),('en_US',372,'Alūksnes novads'),('en_US',373,'Amatas novads'),('en_US',374,'Apes novads'),('en_US',375,'Auces novads'),('en_US',376,'Babītes novads'),('en_US',377,'Baldones novads'),('en_US',378,'Baltinavas novads'),('en_US',379,'Balvu novads'),('en_US',380,'Bauskas novads'),('en_US',381,'Beverīnas novads'),('en_US',382,'Brocēnu novads'),('en_US',383,'Burtnieku novads'),('en_US',384,'Carnikavas novads'),('en_US',385,'Cesvaines novads'),('en_US',386,'Ciblas novads'),('en_US',387,'Cēsu novads'),('en_US',388,'Dagdas novads'),('en_US',389,'Daugavpils novads'),('en_US',390,'Dobeles novads'),('en_US',391,'Dundagas novads'),('en_US',392,'Durbes novads'),('en_US',393,'Engures novads'),('en_US',394,'Garkalnes novads'),('en_US',395,'Grobiņas novads'),('en_US',396,'Gulbenes novads'),('en_US',397,'Iecavas novads'),('en_US',398,'Ikšķiles novads'),('en_US',399,'Ilūkstes novads'),('en_US',400,'Inčukalna novads'),('en_US',401,'Jaunjelgavas novads'),('en_US',402,'Jaunpiebalgas novads'),('en_US',403,'Jaunpils novads'),('en_US',404,'Jelgavas novads'),('en_US',405,'Jēkabpils novads'),('en_US',406,'Kandavas novads'),('en_US',407,'Kokneses novads'),('en_US',408,'Krimuldas novads'),('en_US',409,'Krustpils novads'),('en_US',410,'Krāslavas novads'),('en_US',411,'Kuldīgas novads'),('en_US',412,'Kārsavas novads'),('en_US',413,'Lielvārdes novads'),('en_US',414,'Limbažu novads'),('en_US',415,'Lubānas novads'),('en_US',416,'Ludzas novads'),('en_US',417,'Līgatnes novads'),('en_US',418,'Līvānu novads'),('en_US',419,'Madonas novads'),('en_US',420,'Mazsalacas novads'),('en_US',421,'Mālpils novads'),('en_US',422,'Mārupes novads'),('en_US',423,'Naukšēnu novads'),('en_US',424,'Neretas novads'),('en_US',425,'Nīcas novads'),('en_US',426,'Ogres novads'),('en_US',427,'Olaines novads'),('en_US',428,'Ozolnieku novads'),('en_US',429,'Preiļu novads'),('en_US',430,'Priekules novads'),('en_US',431,'Priekuļu novads'),('en_US',432,'Pārgaujas novads'),('en_US',433,'Pāvilostas novads'),('en_US',434,'Pļaviņu novads'),('en_US',435,'Raunas novads'),('en_US',436,'Riebiņu novads'),('en_US',437,'Rojas novads'),('en_US',438,'Ropažu novads'),('en_US',439,'Rucavas novads'),('en_US',440,'Rugāju novads'),('en_US',441,'Rundāles novads'),('en_US',442,'Rēzeknes novads'),('en_US',443,'Rūjienas novads'),('en_US',444,'Salacgrīvas novads'),('en_US',445,'Salas novads'),('en_US',446,'Salaspils novads'),('en_US',447,'Saldus novads'),('en_US',448,'Saulkrastu novads'),('en_US',449,'Siguldas novads'),('en_US',450,'Skrundas novads'),('en_US',451,'Skrīveru novads'),('en_US',452,'Smiltenes novads'),('en_US',453,'Stopiņu novads'),('en_US',454,'Strenču novads'),('en_US',455,'Sējas novads'),('en_US',456,'Talsu novads'),('en_US',457,'Tukuma novads'),('en_US',458,'Tērvetes novads'),('en_US',459,'Vaiņodes novads'),('en_US',460,'Valkas novads'),('en_US',461,'Valmieras novads'),('en_US',462,'Varakļānu novads'),('en_US',463,'Vecpiebalgas novads'),('en_US',464,'Vecumnieku novads'),('en_US',465,'Ventspils novads'),('en_US',466,'Viesītes novads'),('en_US',467,'Viļakas novads'),('en_US',468,'Viļānu novads'),('en_US',469,'Vārkavas novads'),('en_US',470,'Zilupes novads'),('en_US',471,'Ādažu novads'),('en_US',472,'Ērgļu novads'),('en_US',473,'Ķeguma novads'),('en_US',474,'Ķekavas novads'),('en_US',475,'Alytaus Apskritis'),('en_US',476,'Kauno Apskritis'),('en_US',477,'Klaipėdos Apskritis'),('en_US',478,'Marijampolės Apskritis'),('en_US',479,'Panevėžio Apskritis'),('en_US',480,'Šiaulių Apskritis'),('en_US',481,'Tauragės Apskritis'),('en_US',482,'Telšių Apskritis'),('en_US',483,'Utenos Apskritis'),('en_US',484,'Vilniaus Apskritis'),('en_US',485,'Acre'),('en_US',486,'Alagoas'),('en_US',487,'Amapá'),('en_US',488,'Amazonas'),('en_US',489,'Bahia'),('en_US',490,'Ceará'),('en_US',491,'Espírito Santo'),('en_US',492,'Goiás'),('en_US',493,'Maranhão'),('en_US',494,'Mato Grosso'),('en_US',495,'Mato Grosso do Sul'),('en_US',496,'Minas Gerais'),('en_US',497,'Pará'),('en_US',498,'Paraíba'),('en_US',499,'Paraná'),('en_US',500,'Pernambuco'),('en_US',501,'Piauí'),('en_US',502,'Rio de Janeiro'),('en_US',503,'Rio Grande do Norte'),('en_US',504,'Rio Grande do Sul'),('en_US',505,'Rondônia'),('en_US',506,'Roraima'),('en_US',507,'Santa Catarina'),('en_US',508,'São Paulo'),('en_US',509,'Sergipe'),('en_US',510,'Tocantins'),('en_US',511,'Distrito Federal');

/*Table structure for table `directory_currency_rate` */

DROP TABLE IF EXISTS `directory_currency_rate`;

CREATE TABLE `directory_currency_rate` (
  `currency_from` varchar(3) NOT NULL COMMENT 'Currency Code Convert From',
  `currency_to` varchar(3) NOT NULL COMMENT 'Currency Code Convert To',
  `rate` decimal(24,12) NOT NULL DEFAULT '0.000000000000' COMMENT 'Currency Conversion Rate',
  PRIMARY KEY (`currency_from`,`currency_to`),
  KEY `DIRECTORY_CURRENCY_RATE_CURRENCY_TO` (`currency_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Currency Rate';

/*Data for the table `directory_currency_rate` */

insert  into `directory_currency_rate`(`currency_from`,`currency_to`,`rate`) values ('EUR','EUR','1.000000000000'),('EUR','USD','1.415000000000'),('USD','EUR','0.885600000000'),('USD','USD','1.000000000000');

/*Table structure for table `downloadable_link` */

DROP TABLE IF EXISTS `downloadable_link`;

CREATE TABLE `downloadable_link` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort order',
  `number_of_downloads` int(11) DEFAULT NULL COMMENT 'Number of downloads',
  `is_shareable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Shareable flag',
  `link_url` varchar(255) DEFAULT NULL COMMENT 'Link Url',
  `link_file` varchar(255) DEFAULT NULL COMMENT 'Link File',
  `link_type` varchar(20) DEFAULT NULL COMMENT 'Link Type',
  `sample_url` varchar(255) DEFAULT NULL COMMENT 'Sample Url',
  `sample_file` varchar(255) DEFAULT NULL COMMENT 'Sample File',
  `sample_type` varchar(20) DEFAULT NULL COMMENT 'Sample Type',
  PRIMARY KEY (`link_id`),
  KEY `DOWNLOADABLE_LINK_PRODUCT_ID_SORT_ORDER` (`product_id`,`sort_order`),
  CONSTRAINT `DOWNLOADABLE_LINK_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Link Table';

/*Data for the table `downloadable_link` */

/*Table structure for table `downloadable_link_price` */

DROP TABLE IF EXISTS `downloadable_link_price`;

CREATE TABLE `downloadable_link_price` (
  `price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Price ID',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Link ID',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website ID',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  PRIMARY KEY (`price_id`),
  KEY `DOWNLOADABLE_LINK_PRICE_LINK_ID` (`link_id`),
  KEY `DOWNLOADABLE_LINK_PRICE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `DOWNLOADABLE_LINK_PRICE_LINK_ID_DOWNLOADABLE_LINK_LINK_ID` FOREIGN KEY (`link_id`) REFERENCES `downloadable_link` (`link_id`) ON DELETE CASCADE,
  CONSTRAINT `DOWNLOADABLE_LINK_PRICE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Link Price Table';

/*Data for the table `downloadable_link_price` */

/*Table structure for table `downloadable_link_purchased` */

DROP TABLE IF EXISTS `downloadable_link_purchased`;

CREATE TABLE `downloadable_link_purchased` (
  `purchased_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Purchased ID',
  `order_id` int(10) unsigned DEFAULT '0' COMMENT 'Order ID',
  `order_increment_id` varchar(50) DEFAULT NULL COMMENT 'Order Increment ID',
  `order_item_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Order Item ID',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date of creation',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of modification',
  `customer_id` int(10) unsigned DEFAULT '0' COMMENT 'Customer ID',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product name',
  `product_sku` varchar(255) DEFAULT NULL COMMENT 'Product sku',
  `link_section_title` varchar(255) DEFAULT NULL COMMENT 'Link_section_title',
  PRIMARY KEY (`purchased_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ORDER_ID` (`order_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ORDER_ITEM_ID` (`order_item_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_CUSTOMER_ID` (`customer_id`),
  CONSTRAINT `DL_LNK_PURCHASED_CSTR_ID_CSTR_ENTT_ENTT_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL,
  CONSTRAINT `DOWNLOADABLE_LINK_PURCHASED_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Link Purchased Table';

/*Data for the table `downloadable_link_purchased` */

/*Table structure for table `downloadable_link_purchased_item` */

DROP TABLE IF EXISTS `downloadable_link_purchased_item`;

CREATE TABLE `downloadable_link_purchased_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item ID',
  `purchased_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Purchased ID',
  `order_item_id` int(10) unsigned DEFAULT '0' COMMENT 'Order Item ID',
  `product_id` int(10) unsigned DEFAULT '0' COMMENT 'Product ID',
  `link_hash` varchar(255) DEFAULT NULL COMMENT 'Link hash',
  `number_of_downloads_bought` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of downloads bought',
  `number_of_downloads_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of downloads used',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Link ID',
  `link_title` varchar(255) DEFAULT NULL COMMENT 'Link Title',
  `is_shareable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Shareable Flag',
  `link_url` varchar(255) DEFAULT NULL COMMENT 'Link Url',
  `link_file` varchar(255) DEFAULT NULL COMMENT 'Link File',
  `link_type` varchar(255) DEFAULT NULL COMMENT 'Link Type',
  `status` varchar(50) DEFAULT NULL COMMENT 'Status',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation Time',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
  PRIMARY KEY (`item_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ITEM_LINK_HASH` (`link_hash`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ITEM_ORDER_ITEM_ID` (`order_item_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ITEM_PURCHASED_ID` (`purchased_id`),
  CONSTRAINT `DL_LNK_PURCHASED_ITEM_ORDER_ITEM_ID_SALES_ORDER_ITEM_ITEM_ID` FOREIGN KEY (`order_item_id`) REFERENCES `sales_order_item` (`item_id`) ON DELETE SET NULL,
  CONSTRAINT `DL_LNK_PURCHASED_ITEM_PURCHASED_ID_DL_LNK_PURCHASED_PURCHASED_ID` FOREIGN KEY (`purchased_id`) REFERENCES `downloadable_link_purchased` (`purchased_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Link Purchased Item Table';

/*Data for the table `downloadable_link_purchased_item` */

/*Table structure for table `downloadable_link_title` */

DROP TABLE IF EXISTS `downloadable_link_title`;

CREATE TABLE `downloadable_link_title` (
  `title_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Title ID',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Link ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`title_id`),
  UNIQUE KEY `DOWNLOADABLE_LINK_TITLE_LINK_ID_STORE_ID` (`link_id`,`store_id`),
  KEY `DOWNLOADABLE_LINK_TITLE_STORE_ID` (`store_id`),
  CONSTRAINT `DOWNLOADABLE_LINK_TITLE_LINK_ID_DOWNLOADABLE_LINK_LINK_ID` FOREIGN KEY (`link_id`) REFERENCES `downloadable_link` (`link_id`) ON DELETE CASCADE,
  CONSTRAINT `DOWNLOADABLE_LINK_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Link Title Table';

/*Data for the table `downloadable_link_title` */

/*Table structure for table `downloadable_sample` */

DROP TABLE IF EXISTS `downloadable_sample`;

CREATE TABLE `downloadable_sample` (
  `sample_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Sample ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `sample_url` varchar(255) DEFAULT NULL COMMENT 'Sample URL',
  `sample_file` varchar(255) DEFAULT NULL COMMENT 'Sample file',
  `sample_type` varchar(20) DEFAULT NULL COMMENT 'Sample Type',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`sample_id`),
  KEY `DOWNLOADABLE_SAMPLE_PRODUCT_ID` (`product_id`),
  CONSTRAINT `DOWNLOADABLE_SAMPLE_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Sample Table';

/*Data for the table `downloadable_sample` */

/*Table structure for table `downloadable_sample_title` */

DROP TABLE IF EXISTS `downloadable_sample_title`;

CREATE TABLE `downloadable_sample_title` (
  `title_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Title ID',
  `sample_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sample ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`title_id`),
  UNIQUE KEY `DOWNLOADABLE_SAMPLE_TITLE_SAMPLE_ID_STORE_ID` (`sample_id`,`store_id`),
  KEY `DOWNLOADABLE_SAMPLE_TITLE_STORE_ID` (`store_id`),
  CONSTRAINT `DL_SAMPLE_TTL_SAMPLE_ID_DL_SAMPLE_SAMPLE_ID` FOREIGN KEY (`sample_id`) REFERENCES `downloadable_sample` (`sample_id`) ON DELETE CASCADE,
  CONSTRAINT `DOWNLOADABLE_SAMPLE_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Sample Title Table';

/*Data for the table `downloadable_sample_title` */

/*Table structure for table `eav_attribute` */

DROP TABLE IF EXISTS `eav_attribute`;

CREATE TABLE `eav_attribute` (
  `attribute_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_code` varchar(255) DEFAULT NULL COMMENT 'Attribute Code',
  `attribute_model` varchar(255) DEFAULT NULL COMMENT 'Attribute Model',
  `backend_model` varchar(255) DEFAULT NULL COMMENT 'Backend Model',
  `backend_type` varchar(8) NOT NULL DEFAULT 'static' COMMENT 'Backend Type',
  `backend_table` varchar(255) DEFAULT NULL COMMENT 'Backend Table',
  `frontend_model` varchar(255) DEFAULT NULL COMMENT 'Frontend Model',
  `frontend_input` varchar(50) DEFAULT NULL COMMENT 'Frontend Input',
  `frontend_label` varchar(255) DEFAULT NULL COMMENT 'Frontend Label',
  `frontend_class` varchar(255) DEFAULT NULL COMMENT 'Frontend Class',
  `source_model` varchar(255) DEFAULT NULL COMMENT 'Source Model',
  `is_required` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Defines Is Required',
  `is_user_defined` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Defines Is User Defined',
  `default_value` text COMMENT 'Default Value',
  `is_unique` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Defines Is Unique',
  `note` varchar(255) DEFAULT NULL COMMENT 'Note',
  PRIMARY KEY (`attribute_id`),
  UNIQUE KEY `EAV_ATTRIBUTE_ENTITY_TYPE_ID_ATTRIBUTE_CODE` (`entity_type_id`,`attribute_code`),
  CONSTRAINT `EAV_ATTRIBUTE_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute';

/*Data for the table `eav_attribute` */

insert  into `eav_attribute`(`attribute_id`,`entity_type_id`,`attribute_code`,`attribute_model`,`backend_model`,`backend_type`,`backend_table`,`frontend_model`,`frontend_input`,`frontend_label`,`frontend_class`,`source_model`,`is_required`,`is_user_defined`,`default_value`,`is_unique`,`note`) values (1,1,'website_id',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Backend\\Website','static',NULL,NULL,'select','Associate to Website',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Source\\Website',1,0,NULL,0,NULL),(2,1,'store_id',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Backend\\Store','static',NULL,NULL,'select','Create In',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Source\\Store',1,0,NULL,0,NULL),(3,1,'created_in',NULL,NULL,'static',NULL,NULL,'text','Created From',NULL,NULL,0,0,NULL,0,NULL),(4,1,'prefix',NULL,NULL,'static',NULL,NULL,'text','Prefix',NULL,NULL,0,0,NULL,0,NULL),(5,1,'firstname',NULL,NULL,'static',NULL,NULL,'text','First Name',NULL,NULL,1,0,NULL,0,NULL),(6,1,'middlename',NULL,NULL,'static',NULL,NULL,'text','Middle Name/Initial',NULL,NULL,0,0,NULL,0,NULL),(7,1,'lastname',NULL,NULL,'static',NULL,NULL,'text','Last Name',NULL,NULL,1,0,NULL,0,NULL),(8,1,'suffix',NULL,NULL,'static',NULL,NULL,'text','Suffix',NULL,NULL,0,0,NULL,0,NULL),(9,1,'email',NULL,NULL,'static',NULL,NULL,'text','Email',NULL,NULL,1,0,NULL,0,NULL),(10,1,'group_id',NULL,NULL,'static',NULL,NULL,'select','Group',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Source\\Group',1,0,NULL,0,NULL),(11,1,'dob',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\Datetime','static',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Frontend\\Datetime','date','Date of Birth',NULL,NULL,0,0,NULL,0,NULL),(12,1,'password_hash',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Backend\\Password','static',NULL,NULL,'hidden',NULL,NULL,NULL,0,0,NULL,0,NULL),(13,1,'rp_token',NULL,NULL,'static',NULL,NULL,'hidden',NULL,NULL,NULL,0,0,NULL,0,NULL),(14,1,'rp_token_created_at',NULL,NULL,'static',NULL,NULL,'date',NULL,NULL,NULL,0,0,NULL,0,NULL),(15,1,'default_billing',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Backend\\Billing','static',NULL,NULL,'text','Default Billing Address',NULL,NULL,0,0,NULL,0,NULL),(16,1,'default_shipping',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Backend\\Shipping','static',NULL,NULL,'text','Default Shipping Address',NULL,NULL,0,0,NULL,0,NULL),(17,1,'taxvat',NULL,NULL,'static',NULL,NULL,'text','Tax/VAT Number',NULL,NULL,0,0,NULL,0,NULL),(18,1,'confirmation',NULL,NULL,'static',NULL,NULL,'text','Is Confirmed',NULL,NULL,0,0,NULL,0,NULL),(19,1,'created_at',NULL,NULL,'static',NULL,NULL,'date','Created At',NULL,NULL,0,0,NULL,0,NULL),(20,1,'gender',NULL,NULL,'static',NULL,NULL,'select','Gender',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,0,NULL,0,NULL),(21,1,'disable_auto_group_change',NULL,'Magento\\Customer\\Model\\Attribute\\Backend\\Data\\Boolean','static',NULL,NULL,'boolean','Disable Automatic Group Change Based on VAT ID',NULL,NULL,0,0,NULL,0,NULL),(22,2,'prefix',NULL,NULL,'static',NULL,NULL,'text','Prefix',NULL,NULL,0,0,NULL,0,NULL),(23,2,'firstname',NULL,NULL,'static',NULL,NULL,'text','First Name',NULL,NULL,1,0,NULL,0,NULL),(24,2,'middlename',NULL,NULL,'static',NULL,NULL,'text','Middle Name/Initial',NULL,NULL,0,0,NULL,0,NULL),(25,2,'lastname',NULL,NULL,'static',NULL,NULL,'text','Last Name',NULL,NULL,1,0,NULL,0,NULL),(26,2,'suffix',NULL,NULL,'static',NULL,NULL,'text','Suffix',NULL,NULL,0,0,NULL,0,NULL),(27,2,'company',NULL,NULL,'static',NULL,NULL,'text','Company',NULL,NULL,0,0,NULL,0,NULL),(28,2,'street',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\DefaultBackend','static',NULL,NULL,'multiline','Street Address',NULL,NULL,1,0,NULL,0,NULL),(29,2,'city',NULL,NULL,'static',NULL,NULL,'text','City',NULL,NULL,1,0,NULL,0,NULL),(30,2,'country_id',NULL,NULL,'static',NULL,NULL,'select','Country',NULL,'Magento\\Customer\\Model\\ResourceModel\\Address\\Attribute\\Source\\Country',1,0,NULL,0,NULL),(31,2,'region',NULL,'Magento\\Customer\\Model\\ResourceModel\\Address\\Attribute\\Backend\\Region','static',NULL,NULL,'text','State/Province',NULL,NULL,0,0,NULL,0,NULL),(32,2,'region_id',NULL,NULL,'static',NULL,NULL,'hidden','State/Province',NULL,'Magento\\Customer\\Model\\ResourceModel\\Address\\Attribute\\Source\\Region',0,0,NULL,0,NULL),(33,2,'postcode',NULL,NULL,'static',NULL,NULL,'text','Zip/Postal Code',NULL,NULL,0,0,NULL,0,NULL),(34,2,'telephone',NULL,NULL,'static',NULL,NULL,'text','Phone Number',NULL,NULL,1,0,NULL,0,NULL),(35,2,'fax',NULL,NULL,'static',NULL,NULL,'text','Fax',NULL,NULL,0,0,NULL,0,NULL),(36,2,'vat_id',NULL,NULL,'static',NULL,NULL,'text','VAT number',NULL,NULL,0,0,NULL,0,NULL),(37,2,'vat_is_valid',NULL,NULL,'static',NULL,NULL,'text','VAT number validity',NULL,NULL,0,0,NULL,0,NULL),(38,2,'vat_request_id',NULL,NULL,'static',NULL,NULL,'text','VAT number validation request ID',NULL,NULL,0,0,NULL,0,NULL),(39,2,'vat_request_date',NULL,NULL,'static',NULL,NULL,'text','VAT number validation request date',NULL,NULL,0,0,NULL,0,NULL),(40,2,'vat_request_success',NULL,NULL,'static',NULL,NULL,'text','VAT number validation request success',NULL,NULL,0,0,NULL,0,NULL),(41,1,'updated_at',NULL,NULL,'static',NULL,NULL,'date','Updated At',NULL,NULL,0,0,NULL,0,NULL),(42,1,'failures_num',NULL,NULL,'static',NULL,NULL,'hidden','Failures Number',NULL,NULL,0,0,NULL,0,NULL),(43,1,'first_failure',NULL,NULL,'static',NULL,NULL,'date','First Failure Date',NULL,NULL,0,0,NULL,0,NULL),(44,1,'lock_expires',NULL,NULL,'static',NULL,NULL,'date','Failures Number',NULL,NULL,0,0,NULL,0,NULL),(45,3,'name',NULL,NULL,'varchar',NULL,NULL,'text','Name',NULL,NULL,1,0,NULL,0,NULL),(46,3,'is_active',NULL,NULL,'int',NULL,NULL,'select','Is Active',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',1,0,NULL,0,NULL),(47,3,'description',NULL,NULL,'text',NULL,NULL,'textarea','Description',NULL,NULL,0,0,NULL,0,NULL),(48,3,'image',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Backend\\Image','varchar',NULL,NULL,'image','Image',NULL,NULL,0,0,NULL,0,NULL),(49,3,'meta_title',NULL,NULL,'varchar',NULL,NULL,'text','Page Title',NULL,NULL,0,0,NULL,0,NULL),(50,3,'meta_keywords',NULL,NULL,'text',NULL,NULL,'textarea','Meta Keywords',NULL,NULL,0,0,NULL,0,NULL),(51,3,'meta_description',NULL,NULL,'text',NULL,NULL,'textarea','Meta Description',NULL,NULL,0,0,NULL,0,NULL),(52,3,'display_mode',NULL,NULL,'varchar',NULL,NULL,'select','Display Mode',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Source\\Mode',0,0,NULL,0,NULL),(53,3,'landing_page',NULL,NULL,'int',NULL,NULL,'select','CMS Block',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Source\\Page',0,0,NULL,0,NULL),(54,3,'is_anchor',NULL,NULL,'int',NULL,NULL,'select','Is Anchor',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',0,0,'1',0,NULL),(55,3,'path',NULL,NULL,'static',NULL,NULL,'text','Path',NULL,NULL,0,0,NULL,0,NULL),(56,3,'position',NULL,NULL,'static',NULL,NULL,'text','Position',NULL,NULL,0,0,NULL,0,NULL),(57,3,'all_children',NULL,NULL,'text',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(58,3,'path_in_store',NULL,NULL,'text',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(59,3,'children',NULL,NULL,'text',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(60,3,'custom_design',NULL,NULL,'varchar',NULL,NULL,'select','Custom Design',NULL,'Magento\\Theme\\Model\\Theme\\Source\\Theme',0,0,NULL,0,NULL),(61,3,'custom_design_from','Magento\\Catalog\\Model\\ResourceModel\\Eav\\Attribute','Magento\\Catalog\\Model\\Attribute\\Backend\\Startdate','datetime',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Frontend\\Datetime','date','Active From',NULL,NULL,0,0,NULL,0,NULL),(62,3,'custom_design_to',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\Datetime','datetime',NULL,NULL,'date','Active To',NULL,NULL,0,0,NULL,0,NULL),(63,3,'page_layout',NULL,NULL,'varchar',NULL,NULL,'select','Page Layout',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Source\\Layout',0,0,NULL,0,NULL),(64,3,'custom_layout_update',NULL,'Magento\\Catalog\\Model\\Attribute\\Backend\\Customlayoutupdate','text',NULL,NULL,'textarea','Custom Layout Update',NULL,NULL,0,0,NULL,0,NULL),(65,3,'level',NULL,NULL,'static',NULL,NULL,'text','Level',NULL,NULL,0,0,NULL,0,NULL),(66,3,'children_count',NULL,NULL,'static',NULL,NULL,'text','Children Count',NULL,NULL,0,0,NULL,0,NULL),(67,3,'available_sort_by',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Backend\\Sortby','text',NULL,NULL,'multiselect','Available Product Listing Sort By',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Source\\Sortby',1,0,NULL,0,NULL),(68,3,'default_sort_by',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Backend\\Sortby','varchar',NULL,NULL,'select','Default Product Listing Sort By',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Source\\Sortby',1,0,NULL,0,NULL),(69,3,'include_in_menu',NULL,NULL,'int',NULL,NULL,'select','Include in Navigation Menu',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',1,0,'1',0,NULL),(70,3,'custom_use_parent_settings',NULL,NULL,'int',NULL,NULL,'select','Use Parent Category Settings',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',0,0,NULL,0,NULL),(71,3,'custom_apply_to_products',NULL,NULL,'int',NULL,NULL,'select','Apply To Products',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',0,0,NULL,0,NULL),(72,3,'filter_price_range',NULL,NULL,'decimal',NULL,NULL,'text','Layered Navigation Price Step',NULL,NULL,0,0,NULL,0,NULL),(73,4,'name',NULL,NULL,'varchar',NULL,NULL,'text','Product Name','validate-length maximum-length-255',NULL,1,0,NULL,0,NULL),(74,4,'sku',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Sku','static',NULL,NULL,'text','SKU','validate-length maximum-length-64',NULL,1,0,NULL,1,NULL),(75,4,'description',NULL,NULL,'text',NULL,NULL,'textarea','Description',NULL,NULL,0,0,NULL,0,NULL),(76,4,'short_description',NULL,NULL,'text',NULL,NULL,'textarea','Short Description',NULL,NULL,0,0,NULL,0,NULL),(77,4,'price',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Price','decimal',NULL,NULL,'price','Price',NULL,NULL,1,0,NULL,0,NULL),(78,4,'special_price',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Price','decimal',NULL,NULL,'price','Special Price',NULL,NULL,0,0,NULL,0,NULL),(79,4,'special_from_date',NULL,'Magento\\Catalog\\Model\\Attribute\\Backend\\Startdate','datetime',NULL,NULL,'date','Special Price From Date',NULL,NULL,0,0,NULL,0,NULL),(80,4,'special_to_date',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\Datetime','datetime',NULL,NULL,'date','Special Price To Date',NULL,NULL,0,0,NULL,0,NULL),(81,4,'cost',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Price','decimal',NULL,NULL,'price','Cost',NULL,NULL,0,1,NULL,0,NULL),(82,4,'weight',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Weight','decimal',NULL,NULL,'weight','Weight',NULL,NULL,0,0,NULL,0,NULL),(83,4,'manufacturer',NULL,NULL,'int',NULL,NULL,'select','Manufacturer',NULL,NULL,0,1,NULL,0,NULL),(84,4,'meta_title',NULL,NULL,'varchar',NULL,NULL,'text','Meta Title',NULL,NULL,0,0,NULL,0,NULL),(85,4,'meta_keyword',NULL,NULL,'text',NULL,NULL,'textarea','Meta Keywords',NULL,NULL,0,0,NULL,0,NULL),(86,4,'meta_description',NULL,NULL,'varchar',NULL,NULL,'textarea','Meta Description',NULL,NULL,0,0,NULL,0,'Maximum 255 chars. Meta Description should optimally be between 150-160 characters'),(87,4,'image',NULL,NULL,'varchar',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Frontend\\Image','media_image','Base',NULL,NULL,0,0,NULL,0,NULL),(88,4,'small_image',NULL,NULL,'varchar',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Frontend\\Image','media_image','Small',NULL,NULL,0,0,NULL,0,NULL),(89,4,'thumbnail',NULL,NULL,'varchar',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Frontend\\Image','media_image','Thumbnail',NULL,NULL,0,0,NULL,0,NULL),(90,4,'media_gallery',NULL,NULL,'static',NULL,NULL,'gallery','Media Gallery',NULL,NULL,0,0,NULL,0,NULL),(91,4,'old_id',NULL,NULL,'int',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(92,4,'tier_price',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Tierprice','decimal',NULL,NULL,'text','Tier Price',NULL,NULL,0,0,NULL,0,NULL),(93,4,'color',NULL,NULL,'int',NULL,NULL,'select','Color',NULL,NULL,0,1,'',0,NULL),(94,4,'news_from_date',NULL,'Magento\\Catalog\\Model\\Attribute\\Backend\\Startdate','datetime',NULL,NULL,'date','Set Product as New from Date',NULL,NULL,0,0,NULL,0,NULL),(95,4,'news_to_date',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\Datetime','datetime',NULL,NULL,'date','Set Product as New to Date',NULL,NULL,0,0,NULL,0,NULL),(96,4,'gallery',NULL,NULL,'varchar',NULL,NULL,'gallery','Image Gallery',NULL,NULL,0,0,NULL,0,NULL),(97,4,'status',NULL,NULL,'int',NULL,NULL,'select','Enable Product',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Status',0,0,'1',0,NULL),(98,4,'minimal_price',NULL,NULL,'decimal',NULL,NULL,'price','Minimal Price',NULL,NULL,0,0,NULL,0,NULL),(99,4,'visibility',NULL,NULL,'int',NULL,NULL,'select','Visibility',NULL,'Magento\\Catalog\\Model\\Product\\Visibility',0,0,'4',0,NULL),(100,4,'custom_design',NULL,NULL,'varchar',NULL,NULL,'select','New Theme',NULL,'Magento\\Theme\\Model\\Theme\\Source\\Theme',0,0,NULL,0,NULL),(101,4,'custom_design_from',NULL,'Magento\\Catalog\\Model\\Attribute\\Backend\\Startdate','datetime',NULL,NULL,'date','Active From',NULL,NULL,0,0,NULL,0,NULL),(102,4,'custom_design_to',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\Datetime','datetime',NULL,NULL,'date','Active To',NULL,NULL,0,0,NULL,0,NULL),(103,4,'custom_layout_update',NULL,'Magento\\Catalog\\Model\\Attribute\\Backend\\Customlayoutupdate','text',NULL,NULL,'textarea','Layout Update XML',NULL,NULL,0,0,NULL,0,NULL),(104,4,'page_layout',NULL,NULL,'varchar',NULL,NULL,'select','Layout',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Layout',0,0,NULL,0,NULL),(105,4,'category_ids',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Category','static',NULL,NULL,'text','Categories',NULL,NULL,0,0,NULL,0,NULL),(106,4,'options_container',NULL,NULL,'varchar',NULL,NULL,'select','Display Product Options In',NULL,'Magento\\Catalog\\Model\\Entity\\Product\\Attribute\\Design\\Options\\Container',0,0,'container2',0,NULL),(107,4,'required_options',NULL,NULL,'static',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(108,4,'has_options',NULL,NULL,'static',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(109,4,'image_label',NULL,NULL,'varchar',NULL,NULL,'text','Image Label',NULL,NULL,0,0,NULL,0,NULL),(110,4,'small_image_label',NULL,NULL,'varchar',NULL,NULL,'text','Small Image Label',NULL,NULL,0,0,NULL,0,NULL),(111,4,'thumbnail_label',NULL,NULL,'varchar',NULL,NULL,'text','Thumbnail Label',NULL,NULL,0,0,NULL,0,NULL),(112,4,'created_at',NULL,NULL,'static',NULL,NULL,'date',NULL,NULL,NULL,1,0,NULL,0,NULL),(113,4,'updated_at',NULL,NULL,'static',NULL,NULL,'date',NULL,NULL,NULL,1,0,NULL,0,NULL),(114,4,'country_of_manufacture',NULL,NULL,'varchar',NULL,NULL,'select','Country of Manufacture',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Countryofmanufacture',0,0,NULL,0,NULL),(115,4,'quantity_and_stock_status',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Stock','int',NULL,NULL,'select','Quantity',NULL,'Magento\\CatalogInventory\\Model\\Source\\Stock',0,0,'1',0,NULL),(116,4,'custom_layout',NULL,NULL,'varchar',NULL,NULL,'select','New Layout',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Layout',0,0,NULL,0,NULL),(117,3,'url_key',NULL,NULL,'varchar',NULL,NULL,'text','URL Key',NULL,NULL,0,0,NULL,0,NULL),(118,3,'url_path',NULL,NULL,'varchar',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(119,4,'url_key',NULL,NULL,'varchar',NULL,NULL,'text','URL Key',NULL,NULL,0,0,NULL,0,NULL),(120,4,'url_path',NULL,NULL,'varchar',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(121,4,'msrp',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Price','decimal',NULL,NULL,'price','Manufacturer\'s Suggested Retail Price',NULL,NULL,0,0,NULL,0,NULL),(122,4,'msrp_display_actual_price_type',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Boolean','varchar',NULL,NULL,'select','Display Actual Price',NULL,'Magento\\Msrp\\Model\\Product\\Attribute\\Source\\Type\\Price',0,0,'0',0,NULL),(123,4,'price_type',NULL,NULL,'int',NULL,NULL,'boolean','Dynamic Price',NULL,NULL,1,0,'0',0,NULL),(124,4,'sku_type',NULL,NULL,'int',NULL,NULL,'boolean','Dynamic SKU',NULL,NULL,1,0,'0',0,NULL),(125,4,'weight_type',NULL,NULL,'int',NULL,NULL,'boolean','Dynamic Weight',NULL,NULL,1,0,'0',0,NULL),(126,4,'price_view',NULL,NULL,'int',NULL,NULL,'select','Price View',NULL,'Magento\\Bundle\\Model\\Product\\Attribute\\Source\\Price\\View',1,0,NULL,0,NULL),(127,4,'shipment_type',NULL,NULL,'int',NULL,NULL,'select','Ship Bundle Items',NULL,'Magento\\Bundle\\Model\\Product\\Attribute\\Source\\Shipment\\Type',1,0,'0',0,NULL),(128,4,'links_purchased_separately',NULL,NULL,'int',NULL,NULL,NULL,'Links can be purchased separately',NULL,NULL,1,0,NULL,0,NULL),(129,4,'samples_title',NULL,NULL,'varchar',NULL,NULL,NULL,'Samples title',NULL,NULL,1,0,NULL,0,NULL),(130,4,'links_title',NULL,NULL,'varchar',NULL,NULL,NULL,'Links title',NULL,NULL,1,0,NULL,0,NULL),(131,4,'links_exist',NULL,NULL,'int',NULL,NULL,NULL,NULL,NULL,NULL,0,0,'0',0,NULL),(132,4,'swatch_image',NULL,NULL,'varchar',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Frontend\\Image','media_image','Swatch',NULL,NULL,0,0,NULL,0,NULL),(133,4,'tax_class_id',NULL,NULL,'int',NULL,NULL,'select','Tax Class',NULL,'Magento\\Tax\\Model\\TaxClass\\Source\\Product',0,0,'2',0,NULL),(134,4,'gift_message_available',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Boolean','varchar',NULL,NULL,'select','Allow Gift Message',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Boolean',0,0,NULL,0,NULL),(135,4,'sw_featured',NULL,NULL,'int',NULL,NULL,'boolean','Is Featured',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',0,1,NULL,0,NULL),(136,3,'sw_menu_hide_item',NULL,NULL,'int',NULL,NULL,'select','Hide This Menu Item',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',0,0,NULL,0,NULL),(137,3,'sw_menu_type',NULL,NULL,'varchar',NULL,NULL,'select','Menu Type',NULL,'Smartwave\\Megamenu\\Model\\Attribute\\Menutype',0,0,NULL,0,NULL),(138,3,'sw_menu_static_width',NULL,NULL,'varchar',NULL,NULL,'text','Static Width',NULL,NULL,0,0,NULL,0,NULL),(139,3,'sw_menu_cat_columns',NULL,NULL,'varchar',NULL,NULL,'select','Sub Category Columns',NULL,'Smartwave\\Megamenu\\Model\\Attribute\\Subcatcolumns',0,0,NULL,0,NULL),(140,3,'sw_menu_float_type',NULL,NULL,'varchar',NULL,NULL,'select','Float',NULL,'Smartwave\\Megamenu\\Model\\Attribute\\Floattype',0,0,NULL,0,NULL),(141,3,'sw_menu_cat_label',NULL,NULL,'varchar',NULL,NULL,'select','Category Label',NULL,'Smartwave\\Megamenu\\Model\\Attribute\\Categorylabel',0,0,NULL,0,NULL),(142,3,'sw_menu_icon_img',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Backend\\Image','varchar',NULL,NULL,'image','Icon Image',NULL,NULL,0,0,NULL,0,NULL),(143,3,'sw_menu_font_icon',NULL,NULL,'varchar',NULL,NULL,'text','Font Icon Class',NULL,NULL,0,0,NULL,0,NULL),(144,3,'sw_menu_block_top_content',NULL,NULL,'text',NULL,NULL,'textarea','Top Block',NULL,NULL,0,0,NULL,0,NULL),(145,3,'sw_menu_block_left_width',NULL,NULL,'varchar',NULL,NULL,'select','Left Block Width',NULL,'Smartwave\\Megamenu\\Model\\Attribute\\Width',0,0,NULL,0,NULL),(146,3,'sw_menu_block_left_content',NULL,NULL,'text',NULL,NULL,'textarea','Left Block',NULL,NULL,0,0,NULL,0,NULL),(147,3,'sw_menu_block_right_width',NULL,NULL,'varchar',NULL,NULL,'select','Right Block Width',NULL,'Smartwave\\Megamenu\\Model\\Attribute\\Width',0,0,NULL,0,NULL),(148,3,'sw_menu_block_right_content',NULL,NULL,'text',NULL,NULL,'textarea','Right Block',NULL,NULL,0,0,NULL,0,NULL),(149,3,'sw_menu_block_bottom_content',NULL,NULL,'text',NULL,NULL,'textarea','Bottom Block',NULL,NULL,0,0,NULL,0,NULL),(150,3,'sw_ocat_hide_this_item',NULL,NULL,'int',NULL,NULL,'select','Hide This Category',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',0,0,NULL,0,NULL),(151,3,'sw_ocat_category_icon_image',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Backend\\Image','varchar',NULL,NULL,'image','Icon Image',NULL,NULL,0,0,NULL,0,NULL),(152,3,'sw_ocat_category_font_icon',NULL,NULL,'varchar',NULL,NULL,'text','Font Icon Class',NULL,NULL,0,0,NULL,0,'If this category has no \"Icon Image\", font icon will be shown. example to input: icon-dollar'),(153,3,'sw_ocat_category_hoverbgcolor',NULL,NULL,'varchar',NULL,NULL,'text','Category Hover Background Color',NULL,NULL,0,0,NULL,0,'eg: #00d59d'),(154,3,'sw_ocat_additional_content',NULL,NULL,'text',NULL,NULL,'textarea','Additional Content',NULL,NULL,0,0,NULL,0,NULL);

/*Table structure for table `eav_attribute_group` */

DROP TABLE IF EXISTS `eav_attribute_group`;

CREATE TABLE `eav_attribute_group` (
  `attribute_group_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Group Id',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set Id',
  `attribute_group_name` varchar(255) DEFAULT NULL COMMENT 'Attribute Group Name',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `default_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Default Id',
  `attribute_group_code` varchar(255) NOT NULL COMMENT 'Attribute Group Code',
  `tab_group_code` varchar(255) DEFAULT NULL COMMENT 'Tab Group Code',
  PRIMARY KEY (`attribute_group_id`),
  UNIQUE KEY `EAV_ATTRIBUTE_GROUP_ATTRIBUTE_SET_ID_ATTRIBUTE_GROUP_NAME` (`attribute_set_id`,`attribute_group_name`),
  KEY `EAV_ATTRIBUTE_GROUP_ATTRIBUTE_SET_ID_SORT_ORDER` (`attribute_set_id`,`sort_order`),
  CONSTRAINT `EAV_ATTR_GROUP_ATTR_SET_ID_EAV_ATTR_SET_ATTR_SET_ID` FOREIGN KEY (`attribute_set_id`) REFERENCES `eav_attribute_set` (`attribute_set_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Group';

/*Data for the table `eav_attribute_group` */

insert  into `eav_attribute_group`(`attribute_group_id`,`attribute_set_id`,`attribute_group_name`,`sort_order`,`default_id`,`attribute_group_code`,`tab_group_code`) values (1,1,'General',1,1,'general',NULL),(2,2,'General',1,1,'general',NULL),(3,3,'General',10,1,'general',NULL),(4,3,'General Information',2,0,'general-information',NULL),(5,3,'Display Settings',20,0,'display-settings',NULL),(6,3,'Custom Design',30,0,'custom-design',NULL),(7,4,'Product Details',10,1,'product-details','basic'),(8,4,'Advanced Pricing',40,0,'advanced-pricing','advanced'),(9,4,'Search Engine Optimization',30,0,'search-engine-optimization','basic'),(10,4,'Images',20,0,'image-management','basic'),(11,4,'Design',50,0,'design','advanced'),(12,4,'Autosettings',60,0,'autosettings','advanced'),(13,4,'Content',15,0,'content','basic'),(14,4,'Schedule Design Update',55,0,'schedule-design-update','advanced'),(15,4,'Bundle Items',16,0,'bundle-items',NULL),(16,5,'General',1,1,'general',NULL),(17,6,'General',1,1,'general',NULL),(18,7,'General',1,1,'general',NULL),(19,8,'General',1,1,'general',NULL),(20,4,'Gift Options',61,0,'gift-options',NULL),(21,3,'SW Menu',31,0,'sw-menu',NULL),(22,3,'Onepage Category',32,0,'onepage-category',NULL),(23,9,'Gift Options',61,0,'gift-options',NULL),(24,9,'Autosettings',60,0,'autosettings','advanced'),(25,9,'Schedule Design Update',55,0,'schedule-design-update','advanced'),(26,9,'Design',50,0,'design','advanced'),(27,9,'Advanced Pricing',40,0,'advanced-pricing','advanced'),(28,9,'Search Engine Optimization',30,0,'search-engine-optimization','basic'),(29,9,'Images',20,0,'image-management','basic'),(30,9,'Bundle Items',16,0,'bundle-items',NULL),(31,9,'Content',15,0,'content','basic'),(32,9,'Product Details',10,1,'product-details','basic');

/*Table structure for table `eav_attribute_label` */

DROP TABLE IF EXISTS `eav_attribute_label`;

CREATE TABLE `eav_attribute_label` (
  `attribute_label_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Label Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`attribute_label_id`),
  KEY `EAV_ATTRIBUTE_LABEL_STORE_ID` (`store_id`),
  KEY `EAV_ATTRIBUTE_LABEL_ATTRIBUTE_ID_STORE_ID` (`attribute_id`,`store_id`),
  CONSTRAINT `EAV_ATTRIBUTE_LABEL_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ATTRIBUTE_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Label';

/*Data for the table `eav_attribute_label` */

/*Table structure for table `eav_attribute_option` */

DROP TABLE IF EXISTS `eav_attribute_option`;

CREATE TABLE `eav_attribute_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`option_id`),
  KEY `EAV_ATTRIBUTE_OPTION_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `EAV_ATTRIBUTE_OPTION_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Option';

/*Data for the table `eav_attribute_option` */

insert  into `eav_attribute_option`(`option_id`,`attribute_id`,`sort_order`) values (1,20,0),(2,20,1),(3,20,3),(4,93,1),(5,93,2),(6,93,3),(7,93,4),(8,93,5);

/*Table structure for table `eav_attribute_option_swatch` */

DROP TABLE IF EXISTS `eav_attribute_option_swatch`;

CREATE TABLE `eav_attribute_option_swatch` (
  `swatch_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Swatch ID',
  `option_id` int(10) unsigned NOT NULL COMMENT 'Option ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `type` smallint(5) unsigned NOT NULL COMMENT 'Swatch type: 0 - text, 1 - visual color, 2 - visual image',
  `value` varchar(255) DEFAULT NULL COMMENT 'Swatch Value',
  PRIMARY KEY (`swatch_id`),
  UNIQUE KEY `EAV_ATTRIBUTE_OPTION_SWATCH_STORE_ID_OPTION_ID` (`store_id`,`option_id`),
  KEY `EAV_ATTRIBUTE_OPTION_SWATCH_SWATCH_ID` (`swatch_id`),
  KEY `EAV_ATTR_OPT_SWATCH_OPT_ID_EAV_ATTR_OPT_OPT_ID` (`option_id`),
  CONSTRAINT `EAV_ATTRIBUTE_OPTION_SWATCH_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ATTR_OPT_SWATCH_OPT_ID_EAV_ATTR_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `eav_attribute_option` (`option_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Magento Swatches table';

/*Data for the table `eav_attribute_option_swatch` */

/*Table structure for table `eav_attribute_option_value` */

DROP TABLE IF EXISTS `eav_attribute_option_value`;

CREATE TABLE `eav_attribute_option_value` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  KEY `EAV_ATTRIBUTE_OPTION_VALUE_OPTION_ID` (`option_id`),
  KEY `EAV_ATTRIBUTE_OPTION_VALUE_STORE_ID` (`store_id`),
  CONSTRAINT `EAV_ATTRIBUTE_OPTION_VALUE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ATTR_OPT_VAL_OPT_ID_EAV_ATTR_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `eav_attribute_option` (`option_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Option Value';

/*Data for the table `eav_attribute_option_value` */

insert  into `eav_attribute_option_value`(`value_id`,`option_id`,`store_id`,`value`) values (1,1,0,'Male'),(2,2,0,'Female'),(3,3,0,'Not Specified'),(4,4,0,'White'),(5,5,0,'Black'),(6,6,0,'Red'),(7,7,0,'Blue'),(8,8,0,'Green');

/*Table structure for table `eav_attribute_set` */

DROP TABLE IF EXISTS `eav_attribute_set`;

CREATE TABLE `eav_attribute_set` (
  `attribute_set_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Set Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_set_name` varchar(255) DEFAULT NULL COMMENT 'Attribute Set Name',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`attribute_set_id`),
  UNIQUE KEY `EAV_ATTRIBUTE_SET_ENTITY_TYPE_ID_ATTRIBUTE_SET_NAME` (`entity_type_id`,`attribute_set_name`),
  KEY `EAV_ATTRIBUTE_SET_ENTITY_TYPE_ID_SORT_ORDER` (`entity_type_id`,`sort_order`),
  CONSTRAINT `EAV_ATTRIBUTE_SET_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Set';

/*Data for the table `eav_attribute_set` */

insert  into `eav_attribute_set`(`attribute_set_id`,`entity_type_id`,`attribute_set_name`,`sort_order`) values (1,1,'Default',2),(2,2,'Default',2),(3,3,'Default',1),(4,4,'Default',1),(5,5,'Default',1),(6,6,'Default',1),(7,7,'Default',1),(8,8,'Default',1),(9,4,'Configurable',0);

/*Table structure for table `eav_entity` */

DROP TABLE IF EXISTS `eav_entity`;

CREATE TABLE `eav_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Defines Is Entity Active',
  PRIMARY KEY (`entity_id`),
  KEY `EAV_ENTITY_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `EAV_ENTITY_STORE_ID` (`store_id`),
  CONSTRAINT `EAV_ENTITY_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity';

/*Data for the table `eav_entity` */

/*Table structure for table `eav_entity_attribute` */

DROP TABLE IF EXISTS `eav_entity_attribute`;

CREATE TABLE `eav_entity_attribute` (
  `entity_attribute_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Attribute Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set Id',
  `attribute_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Group Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`entity_attribute_id`),
  UNIQUE KEY `EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_SET_ID_ATTRIBUTE_ID` (`attribute_set_id`,`attribute_id`),
  UNIQUE KEY `EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_GROUP_ID_ATTRIBUTE_ID` (`attribute_group_id`,`attribute_id`),
  KEY `EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_SET_ID_SORT_ORDER` (`attribute_set_id`,`sort_order`),
  KEY `EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTT_ATTR_ATTR_GROUP_ID_EAV_ATTR_GROUP_ATTR_GROUP_ID` FOREIGN KEY (`attribute_group_id`) REFERENCES `eav_attribute_group` (`attribute_group_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=273 DEFAULT CHARSET=utf8 COMMENT='Eav Entity Attributes';

/*Data for the table `eav_entity_attribute` */

insert  into `eav_entity_attribute`(`entity_attribute_id`,`entity_type_id`,`attribute_set_id`,`attribute_group_id`,`attribute_id`,`sort_order`) values (1,1,1,1,1,10),(2,1,1,1,2,20),(3,1,1,1,3,20),(4,1,1,1,4,30),(5,1,1,1,5,40),(6,1,1,1,6,50),(7,1,1,1,7,60),(8,1,1,1,8,70),(9,1,1,1,9,80),(10,1,1,1,10,25),(11,1,1,1,11,90),(12,1,1,1,12,81),(13,1,1,1,13,115),(14,1,1,1,14,120),(15,1,1,1,15,82),(16,1,1,1,16,83),(17,1,1,1,17,100),(18,1,1,1,18,85),(19,1,1,1,19,86),(20,1,1,1,20,110),(21,1,1,1,21,121),(22,2,2,2,22,10),(23,2,2,2,23,20),(24,2,2,2,24,30),(25,2,2,2,25,40),(26,2,2,2,26,50),(27,2,2,2,27,60),(28,2,2,2,28,70),(29,2,2,2,29,80),(30,2,2,2,30,90),(31,2,2,2,31,100),(32,2,2,2,32,100),(33,2,2,2,33,110),(34,2,2,2,34,120),(35,2,2,2,35,130),(36,2,2,2,36,131),(37,2,2,2,37,132),(38,2,2,2,38,133),(39,2,2,2,39,134),(40,2,2,2,40,135),(41,1,1,1,41,87),(42,1,1,1,42,100),(43,1,1,1,43,110),(44,1,1,1,44,120),(45,3,3,4,45,1),(46,3,3,4,46,2),(47,3,3,4,47,4),(48,3,3,4,48,5),(49,3,3,4,49,6),(50,3,3,4,50,7),(51,3,3,4,51,8),(52,3,3,5,52,10),(53,3,3,5,53,20),(54,3,3,5,54,30),(55,3,3,4,55,12),(56,3,3,4,56,13),(57,3,3,4,57,14),(58,3,3,4,58,15),(59,3,3,4,59,16),(60,3,3,6,60,10),(61,3,3,6,61,30),(62,3,3,6,62,40),(63,3,3,6,63,50),(64,3,3,6,64,60),(65,3,3,4,65,24),(66,3,3,4,66,25),(67,3,3,5,67,40),(68,3,3,5,68,50),(69,3,3,4,69,10),(70,3,3,6,70,5),(71,3,3,6,71,6),(72,3,3,5,72,51),(73,4,4,7,73,10),(74,4,4,7,74,20),(75,4,4,13,75,90),(76,4,4,13,76,100),(77,4,4,7,77,30),(78,4,4,8,78,3),(79,4,4,8,79,4),(80,4,4,8,80,5),(81,4,4,8,81,6),(82,4,4,7,82,70),(83,4,4,9,84,20),(84,4,4,9,85,30),(85,4,4,9,86,40),(86,4,4,10,87,1),(87,4,4,10,88,2),(88,4,4,10,89,3),(89,4,4,10,90,4),(90,4,4,7,91,6),(91,4,4,8,92,7),(92,4,4,7,94,90),(93,4,4,7,95,100),(94,4,4,10,96,5),(95,4,4,7,97,5),(96,4,4,8,98,8),(97,4,4,7,99,80),(98,4,4,14,100,40),(99,4,4,14,101,20),(100,4,4,14,102,30),(101,4,4,11,103,10),(102,4,4,11,104,5),(103,4,4,7,105,80),(104,4,4,11,106,6),(105,4,4,7,107,14),(106,4,4,7,108,15),(107,4,4,7,109,16),(108,4,4,7,110,17),(109,4,4,7,111,18),(110,4,4,7,112,19),(111,4,4,7,113,20),(112,4,4,7,114,110),(113,4,4,7,115,60),(114,4,4,14,116,50),(115,3,3,4,117,3),(116,3,3,4,118,17),(117,4,4,9,119,10),(118,4,4,7,120,11),(119,4,4,8,121,9),(120,4,4,8,122,10),(121,4,4,7,123,31),(122,4,4,7,124,21),(123,4,4,7,125,71),(124,4,4,8,126,11),(125,4,4,15,127,1),(126,4,4,7,128,111),(127,4,4,7,129,112),(128,4,4,7,130,113),(129,4,4,7,131,114),(130,4,4,10,132,3),(131,4,4,7,133,40),(132,4,4,20,134,10),(133,4,4,7,135,102),(134,3,3,21,136,10),(135,3,3,21,137,20),(136,3,3,21,138,30),(137,3,3,21,139,40),(138,3,3,21,140,50),(139,3,3,21,141,60),(140,3,3,21,142,70),(141,3,3,21,143,80),(142,3,3,21,144,90),(143,3,3,21,145,100),(144,3,3,21,146,110),(145,3,3,21,147,120),(146,3,3,21,148,130),(147,3,3,21,149,140),(148,3,3,22,150,10),(149,3,3,22,151,20),(150,3,3,22,152,30),(151,3,3,22,153,40),(152,3,3,22,154,50),(154,4,9,23,134,10),(156,4,9,25,101,20),(158,4,9,25,102,30),(160,4,9,25,100,40),(162,4,9,25,116,50),(164,4,9,26,104,5),(166,4,9,26,106,6),(168,4,9,26,103,10),(170,4,9,27,78,3),(172,4,9,27,79,4),(174,4,9,27,80,5),(176,4,9,27,81,6),(178,4,9,27,92,7),(180,4,9,27,98,8),(182,4,9,27,121,9),(184,4,9,27,122,10),(186,4,9,27,126,11),(188,4,9,28,119,10),(190,4,9,28,84,20),(192,4,9,28,85,30),(194,4,9,28,86,40),(196,4,9,29,87,1),(198,4,9,29,88,2),(200,4,9,29,89,3),(202,4,9,29,132,3),(204,4,9,29,90,4),(206,4,9,29,96,5),(208,4,9,30,127,1),(210,4,9,31,75,90),(212,4,9,31,76,100),(214,4,9,32,97,5),(216,4,9,32,91,6),(218,4,9,32,73,10),(220,4,9,32,120,11),(222,4,9,32,107,14),(224,4,9,32,108,15),(226,4,9,32,109,16),(228,4,9,32,110,17),(230,4,9,32,111,18),(232,4,9,32,112,19),(234,4,9,32,74,20),(236,4,9,32,113,20),(238,4,9,32,124,21),(240,4,9,32,77,30),(242,4,9,32,123,31),(244,4,9,32,133,40),(246,4,9,32,115,60),(248,4,9,32,82,70),(250,4,9,32,125,71),(252,4,9,32,105,80),(254,4,9,32,99,80),(256,4,9,32,94,90),(258,4,9,32,95,100),(260,4,9,32,135,102),(262,4,9,32,114,110),(264,4,9,32,128,111),(266,4,9,32,129,112),(268,4,9,32,130,113),(270,4,9,32,131,114),(272,4,9,32,93,116);

/*Table structure for table `eav_entity_datetime` */

DROP TABLE IF EXISTS `eav_entity_datetime`;

CREATE TABLE `eav_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` datetime DEFAULT NULL COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `EAV_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `EAV_ENTITY_DATETIME_STORE_ID` (`store_id`),
  KEY `EAV_ENTITY_DATETIME_ATTRIBUTE_ID_VALUE` (`attribute_id`,`value`),
  KEY `EAV_ENTITY_DATETIME_ENTITY_TYPE_ID_VALUE` (`entity_type_id`,`value`),
  CONSTRAINT `EAV_ENTITY_DATETIME_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_DATETIME_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTT_DTIME_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';

/*Data for the table `eav_entity_datetime` */

/*Table structure for table `eav_entity_decimal` */

DROP TABLE IF EXISTS `eav_entity_decimal`;

CREATE TABLE `eav_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `EAV_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `EAV_ENTITY_DECIMAL_STORE_ID` (`store_id`),
  KEY `EAV_ENTITY_DECIMAL_ATTRIBUTE_ID_VALUE` (`attribute_id`,`value`),
  KEY `EAV_ENTITY_DECIMAL_ENTITY_TYPE_ID_VALUE` (`entity_type_id`,`value`),
  CONSTRAINT `EAV_ENTITY_DECIMAL_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_DECIMAL_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_DECIMAL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';

/*Data for the table `eav_entity_decimal` */

/*Table structure for table `eav_entity_int` */

DROP TABLE IF EXISTS `eav_entity_int`;

CREATE TABLE `eav_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `EAV_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `EAV_ENTITY_INT_STORE_ID` (`store_id`),
  KEY `EAV_ENTITY_INT_ATTRIBUTE_ID_VALUE` (`attribute_id`,`value`),
  KEY `EAV_ENTITY_INT_ENTITY_TYPE_ID_VALUE` (`entity_type_id`,`value`),
  CONSTRAINT `EAV_ENTITY_INT_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_INT_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_INT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';

/*Data for the table `eav_entity_int` */

/*Table structure for table `eav_entity_store` */

DROP TABLE IF EXISTS `eav_entity_store`;

CREATE TABLE `eav_entity_store` (
  `entity_store_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Store Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `increment_prefix` varchar(20) DEFAULT NULL COMMENT 'Increment Prefix',
  `increment_last_id` varchar(50) DEFAULT NULL COMMENT 'Last Incremented Id',
  PRIMARY KEY (`entity_store_id`),
  KEY `EAV_ENTITY_STORE_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `EAV_ENTITY_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `EAV_ENTITY_STORE_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Store';

/*Data for the table `eav_entity_store` */

/*Table structure for table `eav_entity_text` */

DROP TABLE IF EXISTS `eav_entity_text`;

CREATE TABLE `eav_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` text NOT NULL COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `EAV_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `EAV_ENTITY_TEXT_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `EAV_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `EAV_ENTITY_TEXT_STORE_ID` (`store_id`),
  CONSTRAINT `EAV_ENTITY_TEXT_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_TEXT_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_TEXT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';

/*Data for the table `eav_entity_text` */

/*Table structure for table `eav_entity_type` */

DROP TABLE IF EXISTS `eav_entity_type`;

CREATE TABLE `eav_entity_type` (
  `entity_type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Type Id',
  `entity_type_code` varchar(50) NOT NULL COMMENT 'Entity Type Code',
  `entity_model` varchar(255) NOT NULL COMMENT 'Entity Model',
  `attribute_model` varchar(255) DEFAULT NULL COMMENT 'Attribute Model',
  `entity_table` varchar(255) DEFAULT NULL COMMENT 'Entity Table',
  `value_table_prefix` varchar(255) DEFAULT NULL COMMENT 'Value Table Prefix',
  `entity_id_field` varchar(255) DEFAULT NULL COMMENT 'Entity Id Field',
  `is_data_sharing` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Defines Is Data Sharing',
  `data_sharing_key` varchar(100) DEFAULT 'default' COMMENT 'Data Sharing Key',
  `default_attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Default Attribute Set Id',
  `increment_model` varchar(255) DEFAULT NULL COMMENT 'Increment Model',
  `increment_per_store` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Increment Per Store',
  `increment_pad_length` smallint(5) unsigned NOT NULL DEFAULT '8' COMMENT 'Increment Pad Length',
  `increment_pad_char` varchar(1) NOT NULL DEFAULT '0' COMMENT 'Increment Pad Char',
  `additional_attribute_table` varchar(255) DEFAULT NULL COMMENT 'Additional Attribute Table',
  `entity_attribute_collection` varchar(255) DEFAULT NULL COMMENT 'Entity Attribute Collection',
  PRIMARY KEY (`entity_type_id`),
  KEY `EAV_ENTITY_TYPE_ENTITY_TYPE_CODE` (`entity_type_code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Eav Entity Type';

/*Data for the table `eav_entity_type` */

insert  into `eav_entity_type`(`entity_type_id`,`entity_type_code`,`entity_model`,`attribute_model`,`entity_table`,`value_table_prefix`,`entity_id_field`,`is_data_sharing`,`data_sharing_key`,`default_attribute_set_id`,`increment_model`,`increment_per_store`,`increment_pad_length`,`increment_pad_char`,`additional_attribute_table`,`entity_attribute_collection`) values (1,'customer','Magento\\Customer\\Model\\ResourceModel\\Customer','Magento\\Customer\\Model\\Attribute','customer_entity',NULL,NULL,1,'default',1,'Magento\\Eav\\Model\\Entity\\Increment\\NumericValue',0,8,'0','customer_eav_attribute','Magento\\Customer\\Model\\ResourceModel\\Attribute\\Collection'),(2,'customer_address','Magento\\Customer\\Model\\ResourceModel\\Address','Magento\\Customer\\Model\\Attribute','customer_address_entity',NULL,NULL,1,'default',2,NULL,0,8,'0','customer_eav_attribute','Magento\\Customer\\Model\\ResourceModel\\Address\\Attribute\\Collection'),(3,'catalog_category','Magento\\Catalog\\Model\\ResourceModel\\Category','Magento\\Catalog\\Model\\ResourceModel\\Eav\\Attribute','catalog_category_entity',NULL,NULL,1,'default',3,NULL,0,8,'0','catalog_eav_attribute','Magento\\Catalog\\Model\\ResourceModel\\Category\\Attribute\\Collection'),(4,'catalog_product','Magento\\Catalog\\Model\\ResourceModel\\Product','Magento\\Catalog\\Model\\ResourceModel\\Eav\\Attribute','catalog_product_entity',NULL,NULL,1,'default',4,NULL,0,8,'0','catalog_eav_attribute','Magento\\Catalog\\Model\\ResourceModel\\Product\\Attribute\\Collection'),(5,'order','Magento\\Sales\\Model\\ResourceModel\\Order',NULL,'sales_order',NULL,NULL,1,'default',5,'Magento\\Eav\\Model\\Entity\\Increment\\NumericValue',1,8,'0',NULL,NULL),(6,'invoice','Magento\\Sales\\Model\\ResourceModel\\Order',NULL,'sales_invoice',NULL,NULL,1,'default',6,'Magento\\Eav\\Model\\Entity\\Increment\\NumericValue',1,8,'0',NULL,NULL),(7,'creditmemo','Magento\\Sales\\Model\\ResourceModel\\Order\\Creditmemo',NULL,'sales_creditmemo',NULL,NULL,1,'default',7,'Magento\\Eav\\Model\\Entity\\Increment\\NumericValue',1,8,'0',NULL,NULL),(8,'shipment','Magento\\Sales\\Model\\ResourceModel\\Order\\Shipment',NULL,'sales_shipment',NULL,NULL,1,'default',8,'Magento\\Eav\\Model\\Entity\\Increment\\NumericValue',1,8,'0',NULL,NULL);

/*Table structure for table `eav_entity_varchar` */

DROP TABLE IF EXISTS `eav_entity_varchar`;

CREATE TABLE `eav_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `EAV_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `EAV_ENTITY_VARCHAR_STORE_ID` (`store_id`),
  KEY `EAV_ENTITY_VARCHAR_ATTRIBUTE_ID_VALUE` (`attribute_id`,`value`),
  KEY `EAV_ENTITY_VARCHAR_ENTITY_TYPE_ID_VALUE` (`entity_type_id`,`value`),
  CONSTRAINT `EAV_ENTITY_VARCHAR_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_VARCHAR_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_VARCHAR_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';

/*Data for the table `eav_entity_varchar` */

/*Table structure for table `eav_form_element` */

DROP TABLE IF EXISTS `eav_form_element`;

CREATE TABLE `eav_form_element` (
  `element_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Element Id',
  `type_id` smallint(5) unsigned NOT NULL COMMENT 'Type Id',
  `fieldset_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Fieldset Id',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  `sort_order` int(11) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`element_id`),
  UNIQUE KEY `EAV_FORM_ELEMENT_TYPE_ID_ATTRIBUTE_ID` (`type_id`,`attribute_id`),
  KEY `EAV_FORM_ELEMENT_FIELDSET_ID` (`fieldset_id`),
  KEY `EAV_FORM_ELEMENT_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `EAV_FORM_ELEMENT_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_FORM_ELEMENT_FIELDSET_ID_EAV_FORM_FIELDSET_FIELDSET_ID` FOREIGN KEY (`fieldset_id`) REFERENCES `eav_form_fieldset` (`fieldset_id`) ON DELETE SET NULL,
  CONSTRAINT `EAV_FORM_ELEMENT_TYPE_ID_EAV_FORM_TYPE_TYPE_ID` FOREIGN KEY (`type_id`) REFERENCES `eav_form_type` (`type_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='Eav Form Element';

/*Data for the table `eav_form_element` */

insert  into `eav_form_element`(`element_id`,`type_id`,`fieldset_id`,`attribute_id`,`sort_order`) values (1,1,NULL,23,0),(2,1,NULL,25,1),(3,1,NULL,27,2),(4,1,NULL,9,3),(5,1,NULL,28,4),(6,1,NULL,29,5),(7,1,NULL,31,6),(8,1,NULL,33,7),(9,1,NULL,30,8),(10,1,NULL,34,9),(11,1,NULL,35,10),(12,2,NULL,23,0),(13,2,NULL,25,1),(14,2,NULL,27,2),(15,2,NULL,9,3),(16,2,NULL,28,4),(17,2,NULL,29,5),(18,2,NULL,31,6),(19,2,NULL,33,7),(20,2,NULL,30,8),(21,2,NULL,34,9),(22,2,NULL,35,10),(23,3,NULL,23,0),(24,3,NULL,25,1),(25,3,NULL,27,2),(26,3,NULL,28,3),(27,3,NULL,29,4),(28,3,NULL,31,5),(29,3,NULL,33,6),(30,3,NULL,30,7),(31,3,NULL,34,8),(32,3,NULL,35,9),(33,4,NULL,23,0),(34,4,NULL,25,1),(35,4,NULL,27,2),(36,4,NULL,28,3),(37,4,NULL,29,4),(38,4,NULL,31,5),(39,4,NULL,33,6),(40,4,NULL,30,7),(41,4,NULL,34,8),(42,4,NULL,35,9);

/*Table structure for table `eav_form_fieldset` */

DROP TABLE IF EXISTS `eav_form_fieldset`;

CREATE TABLE `eav_form_fieldset` (
  `fieldset_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Fieldset Id',
  `type_id` smallint(5) unsigned NOT NULL COMMENT 'Type Id',
  `code` varchar(64) NOT NULL COMMENT 'Code',
  `sort_order` int(11) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`fieldset_id`),
  UNIQUE KEY `EAV_FORM_FIELDSET_TYPE_ID_CODE` (`type_id`,`code`),
  CONSTRAINT `EAV_FORM_FIELDSET_TYPE_ID_EAV_FORM_TYPE_TYPE_ID` FOREIGN KEY (`type_id`) REFERENCES `eav_form_type` (`type_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Form Fieldset';

/*Data for the table `eav_form_fieldset` */

/*Table structure for table `eav_form_fieldset_label` */

DROP TABLE IF EXISTS `eav_form_fieldset_label`;

CREATE TABLE `eav_form_fieldset_label` (
  `fieldset_id` smallint(5) unsigned NOT NULL COMMENT 'Fieldset Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `label` varchar(255) NOT NULL COMMENT 'Label',
  PRIMARY KEY (`fieldset_id`,`store_id`),
  KEY `EAV_FORM_FIELDSET_LABEL_STORE_ID` (`store_id`),
  CONSTRAINT `EAV_FORM_FIELDSET_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_FORM_FSET_LBL_FSET_ID_EAV_FORM_FSET_FSET_ID` FOREIGN KEY (`fieldset_id`) REFERENCES `eav_form_fieldset` (`fieldset_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Form Fieldset Label';

/*Data for the table `eav_form_fieldset_label` */

/*Table structure for table `eav_form_type` */

DROP TABLE IF EXISTS `eav_form_type`;

CREATE TABLE `eav_form_type` (
  `type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Type Id',
  `code` varchar(64) NOT NULL COMMENT 'Code',
  `label` varchar(255) NOT NULL COMMENT 'Label',
  `is_system` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is System',
  `theme` varchar(64) DEFAULT NULL COMMENT 'Theme',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `EAV_FORM_TYPE_CODE_THEME_STORE_ID` (`code`,`theme`,`store_id`),
  KEY `EAV_FORM_TYPE_STORE_ID` (`store_id`),
  CONSTRAINT `EAV_FORM_TYPE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Eav Form Type';

/*Data for the table `eav_form_type` */

insert  into `eav_form_type`(`type_id`,`code`,`label`,`is_system`,`theme`,`store_id`) values (1,'checkout_onepage_register','checkout_onepage_register',1,'',0),(2,'checkout_onepage_register_guest','checkout_onepage_register_guest',1,'',0),(3,'checkout_onepage_billing_address','checkout_onepage_billing_address',1,'',0),(4,'checkout_onepage_shipping_address','checkout_onepage_shipping_address',1,'',0);

/*Table structure for table `eav_form_type_entity` */

DROP TABLE IF EXISTS `eav_form_type_entity`;

CREATE TABLE `eav_form_type_entity` (
  `type_id` smallint(5) unsigned NOT NULL COMMENT 'Type Id',
  `entity_type_id` smallint(5) unsigned NOT NULL COMMENT 'Entity Type Id',
  PRIMARY KEY (`type_id`,`entity_type_id`),
  KEY `EAV_FORM_TYPE_ENTITY_ENTITY_TYPE_ID` (`entity_type_id`),
  CONSTRAINT `EAV_FORM_TYPE_ENTITY_TYPE_ID_EAV_FORM_TYPE_TYPE_ID` FOREIGN KEY (`type_id`) REFERENCES `eav_form_type` (`type_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_FORM_TYPE_ENTT_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Form Type Entity';

/*Data for the table `eav_form_type_entity` */

insert  into `eav_form_type_entity`(`type_id`,`entity_type_id`) values (1,1),(1,2),(2,1),(2,2),(3,2),(4,2);

/*Table structure for table `email_template` */

DROP TABLE IF EXISTS `email_template`;

CREATE TABLE `email_template` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Template ID',
  `template_code` varchar(150) NOT NULL COMMENT 'Template Name',
  `template_text` text NOT NULL COMMENT 'Template Content',
  `template_styles` text COMMENT 'Templste Styles',
  `template_type` int(10) unsigned DEFAULT NULL COMMENT 'Template Type',
  `template_subject` varchar(200) NOT NULL COMMENT 'Template Subject',
  `template_sender_name` varchar(200) DEFAULT NULL COMMENT 'Template Sender Name',
  `template_sender_email` varchar(200) DEFAULT NULL COMMENT 'Template Sender Email',
  `added_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date of Template Creation',
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of Template Modification',
  `orig_template_code` varchar(200) DEFAULT NULL COMMENT 'Original Template Code',
  `orig_template_variables` text COMMENT 'Original Template Variables',
  PRIMARY KEY (`template_id`),
  UNIQUE KEY `EMAIL_TEMPLATE_TEMPLATE_CODE` (`template_code`),
  KEY `EMAIL_TEMPLATE_ADDED_AT` (`added_at`),
  KEY `EMAIL_TEMPLATE_MODIFIED_AT` (`modified_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Email Templates';

/*Data for the table `email_template` */

/*Table structure for table `flag` */

DROP TABLE IF EXISTS `flag`;

CREATE TABLE `flag` (
  `flag_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Flag Id',
  `flag_code` varchar(255) NOT NULL COMMENT 'Flag Code',
  `state` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Flag State',
  `flag_data` text COMMENT 'Flag Data',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of Last Flag Update',
  PRIMARY KEY (`flag_id`),
  KEY `FLAG_LAST_UPDATE` (`last_update`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Flag';

/*Data for the table `flag` */

/*Table structure for table `gift_message` */

DROP TABLE IF EXISTS `gift_message`;

CREATE TABLE `gift_message` (
  `gift_message_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'GiftMessage Id',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer id',
  `sender` varchar(255) DEFAULT NULL COMMENT 'Sender',
  `recipient` varchar(255) DEFAULT NULL COMMENT 'Registrant',
  `message` text COMMENT 'Message',
  PRIMARY KEY (`gift_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Gift Message';

/*Data for the table `gift_message` */

/*Table structure for table `googleoptimizer_code` */

DROP TABLE IF EXISTS `googleoptimizer_code`;

CREATE TABLE `googleoptimizer_code` (
  `code_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Google experiment code id',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Optimized entity id product id or catalog id',
  `entity_type` varchar(50) DEFAULT NULL COMMENT 'Optimized entity type',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store id',
  `experiment_script` text COMMENT 'Google experiment script',
  PRIMARY KEY (`code_id`),
  UNIQUE KEY `GOOGLEOPTIMIZER_CODE_STORE_ID_ENTITY_ID_ENTITY_TYPE` (`store_id`,`entity_id`,`entity_type`),
  CONSTRAINT `GOOGLEOPTIMIZER_CODE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Google Experiment code';

/*Data for the table `googleoptimizer_code` */

/*Table structure for table `import_history` */

DROP TABLE IF EXISTS `import_history`;

CREATE TABLE `import_history` (
  `history_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'History record Id',
  `started_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Started at',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'User ID',
  `imported_file` varchar(255) DEFAULT NULL COMMENT 'Imported file',
  `execution_time` varchar(255) DEFAULT NULL COMMENT 'Execution time',
  `summary` varchar(255) DEFAULT NULL COMMENT 'Summary',
  `error_file` varchar(255) NOT NULL COMMENT 'Imported file with errors',
  PRIMARY KEY (`history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Import history table';

/*Data for the table `import_history` */

/*Table structure for table `importexport_importdata` */

DROP TABLE IF EXISTS `importexport_importdata`;

CREATE TABLE `importexport_importdata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `entity` varchar(50) NOT NULL COMMENT 'Entity',
  `behavior` varchar(10) NOT NULL DEFAULT 'append' COMMENT 'Behavior',
  `data` longtext COMMENT 'Data',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Import Data Table';

/*Data for the table `importexport_importdata` */

/*Table structure for table `indexer_state` */

DROP TABLE IF EXISTS `indexer_state`;

CREATE TABLE `indexer_state` (
  `state_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Indexer State Id',
  `indexer_id` varchar(255) DEFAULT NULL COMMENT 'Indexer Id',
  `status` varchar(16) DEFAULT 'invalid' COMMENT 'Indexer Status',
  `updated` datetime DEFAULT NULL COMMENT 'Indexer Status',
  `hash_config` varchar(32) NOT NULL COMMENT 'Hash of indexer config',
  PRIMARY KEY (`state_id`),
  KEY `INDEXER_STATE_INDEXER_ID` (`indexer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='Indexer State';

/*Data for the table `indexer_state` */

insert  into `indexer_state`(`state_id`,`indexer_id`,`status`,`updated`,`hash_config`) values (1,'design_config_grid','valid','2016-09-05 03:09:24','27baa8fe6a5369f52c8b7cbd54a3c3c4'),(2,'customer_grid','valid','2016-09-05 03:09:28','a1bbcab4c6368d654719ccf6cf0e55a8'),(3,'catalog_category_product','valid','2016-09-05 03:09:30','57b48d3cf1fcd64abe6b01dea3173d02'),(4,'catalog_product_category','valid','2016-08-26 09:21:50','9957f66909342cc58ff2703dcd268bf4'),(5,'catalog_product_price','valid','2016-09-05 03:09:33','15a819a577a149220cd0722c291de721'),(6,'catalog_product_attribute','valid','2016-09-05 03:09:33','77eed0bf72b16099d299d0ab47b74910'),(7,'cataloginventory_stock','valid','2016-09-05 03:09:35','78a405fd852458c326c85096099d7d5e'),(8,'catalogrule_rule','valid','2016-09-05 03:09:35','5afe3cacdcb52ec3a7e68dc245679021'),(9,'catalogrule_product','valid','2016-08-26 09:21:54','0ebee9e52ed424273132e8227fe646f3'),(10,'catalogsearch_fulltext','valid','2016-09-05 03:09:42','4486b57e2021aa78b526c68c9af2dcab');

/*Table structure for table `integration` */

DROP TABLE IF EXISTS `integration`;

CREATE TABLE `integration` (
  `integration_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Integration ID',
  `name` varchar(255) NOT NULL COMMENT 'Integration name is displayed in the admin interface',
  `email` varchar(255) NOT NULL COMMENT 'Email address of the contact person',
  `endpoint` varchar(255) DEFAULT NULL COMMENT 'Endpoint for posting consumer credentials',
  `status` smallint(5) unsigned NOT NULL COMMENT 'Integration status',
  `consumer_id` int(10) unsigned DEFAULT NULL COMMENT 'Oauth consumer',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation Time',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
  `setup_type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Integration type - manual or config file',
  `identity_link_url` varchar(255) DEFAULT NULL COMMENT 'Identity linking Url',
  PRIMARY KEY (`integration_id`),
  UNIQUE KEY `INTEGRATION_NAME` (`name`),
  UNIQUE KEY `INTEGRATION_CONSUMER_ID` (`consumer_id`),
  CONSTRAINT `INTEGRATION_CONSUMER_ID_OAUTH_CONSUMER_ENTITY_ID` FOREIGN KEY (`consumer_id`) REFERENCES `oauth_consumer` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='integration';

/*Data for the table `integration` */

/*Table structure for table `layout_link` */

DROP TABLE IF EXISTS `layout_link`;

CREATE TABLE `layout_link` (
  `layout_link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `theme_id` int(10) unsigned NOT NULL COMMENT 'Theme id',
  `layout_update_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Layout Update Id',
  `is_temporary` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Defines whether Layout Update is Temporary',
  PRIMARY KEY (`layout_link_id`),
  KEY `LAYOUT_LINK_LAYOUT_UPDATE_ID` (`layout_update_id`),
  KEY `LAYOUT_LINK_STORE_ID_THEME_ID_LAYOUT_UPDATE_ID_IS_TEMPORARY` (`store_id`,`theme_id`,`layout_update_id`,`is_temporary`),
  KEY `LAYOUT_LINK_THEME_ID_THEME_THEME_ID` (`theme_id`),
  CONSTRAINT `LAYOUT_LINK_LAYOUT_UPDATE_ID_LAYOUT_UPDATE_LAYOUT_UPDATE_ID` FOREIGN KEY (`layout_update_id`) REFERENCES `layout_update` (`layout_update_id`) ON DELETE CASCADE,
  CONSTRAINT `LAYOUT_LINK_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `LAYOUT_LINK_THEME_ID_THEME_THEME_ID` FOREIGN KEY (`theme_id`) REFERENCES `theme` (`theme_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Layout Link';

/*Data for the table `layout_link` */

/*Table structure for table `layout_update` */

DROP TABLE IF EXISTS `layout_update`;

CREATE TABLE `layout_update` (
  `layout_update_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Layout Update Id',
  `handle` varchar(255) DEFAULT NULL COMMENT 'Handle',
  `xml` text COMMENT 'Xml',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Last Update Timestamp',
  PRIMARY KEY (`layout_update_id`),
  KEY `LAYOUT_UPDATE_HANDLE` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Layout Updates';

/*Data for the table `layout_update` */

/*Table structure for table `magefan_blog_category` */

DROP TABLE IF EXISTS `magefan_blog_category`;

CREATE TABLE `magefan_blog_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Category ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Category Title',
  `meta_keywords` text COMMENT 'Category Meta Keywords',
  `meta_description` text COMMENT 'Category Meta Description',
  `identifier` varchar(100) DEFAULT NULL COMMENT 'Category String Identifier',
  `content_heading` varchar(255) DEFAULT NULL COMMENT 'Category Content Heading',
  `content` mediumtext COMMENT 'Category Content',
  `path` varchar(255) DEFAULT NULL COMMENT 'Category Path',
  `position` smallint(6) NOT NULL COMMENT 'Category Position',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Category Active',
  PRIMARY KEY (`category_id`),
  KEY `MAGEFAN_BLOG_CATEGORY_IDENTIFIER` (`identifier`),
  FULLTEXT KEY `FTI_B76AD11A0A06CC3327BA3A86A016134F` (`title`,`meta_keywords`,`meta_description`,`identifier`,`content`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Magefan Blog Category Table';

/*Data for the table `magefan_blog_category` */

/*Table structure for table `magefan_blog_category_store` */

DROP TABLE IF EXISTS `magefan_blog_category_store`;

CREATE TABLE `magefan_blog_category_store` (
  `category_id` int(11) NOT NULL COMMENT 'Category ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  PRIMARY KEY (`category_id`,`store_id`),
  KEY `MAGEFAN_BLOG_CATEGORY_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `MAGEFAN_BLOG_CATEGORY_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `MAGEFAN_BLOG_CTGR_STORE_CTGR_ID_MAGEFAN_BLOG_CTGR_CTGR_ID` FOREIGN KEY (`category_id`) REFERENCES `magefan_blog_category` (`category_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Magefan Blog Category To Store Linkage Table';

/*Data for the table `magefan_blog_category_store` */

/*Table structure for table `magefan_blog_post` */

DROP TABLE IF EXISTS `magefan_blog_post`;

CREATE TABLE `magefan_blog_post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Post ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Post Title',
  `meta_keywords` text COMMENT 'Post Meta Keywords',
  `meta_description` text COMMENT 'Post Meta Description',
  `identifier` varchar(100) DEFAULT NULL COMMENT 'Post String Identifier',
  `content_heading` varchar(255) DEFAULT NULL COMMENT 'Post Content Heading',
  `content` mediumtext COMMENT 'Post Content',
  `creation_time` timestamp NULL DEFAULT NULL COMMENT 'Post Creation Time',
  `update_time` timestamp NULL DEFAULT NULL COMMENT 'Post Modification Time',
  `publish_time` timestamp NULL DEFAULT NULL COMMENT 'Post Publish Time',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Post Active',
  `featured_img` varchar(255) DEFAULT NULL COMMENT 'Thumbnail Image',
  PRIMARY KEY (`post_id`),
  KEY `MAGEFAN_BLOG_POST_IDENTIFIER` (`identifier`),
  FULLTEXT KEY `FTI_A5DB7871E89B230F74EDFBECEB4659FA` (`title`,`meta_keywords`,`meta_description`,`identifier`,`content`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Magefan Blog Post Table';

/*Data for the table `magefan_blog_post` */

insert  into `magefan_blog_post`(`post_id`,`title`,`meta_keywords`,`meta_description`,`identifier`,`content_heading`,`content`,`creation_time`,`update_time`,`publish_time`,`is_active`,`featured_img`) values (1,'Hello world!','magento 2 blog','Magento 2 blog default post.','hello-world','Hello world!','Welcome to <a target=\"_blank\" href=\"http://magefan.com/\" title=\"Magefan - solutions for Magento 2\">Magefan</a> blog extension for Magento&reg; 2. This is your first post. Edit or delete it, then start blogging!','2016-09-05 09:55:09','2016-09-05 09:55:09','2016-09-05 09:55:09',1,NULL);

/*Table structure for table `magefan_blog_post_category` */

DROP TABLE IF EXISTS `magefan_blog_post_category`;

CREATE TABLE `magefan_blog_post_category` (
  `post_id` int(11) NOT NULL COMMENT 'Post ID',
  `category_id` int(11) NOT NULL COMMENT 'Category ID',
  PRIMARY KEY (`post_id`,`category_id`),
  KEY `MAGEFAN_BLOG_POST_CATEGORY_CATEGORY_ID` (`category_id`),
  CONSTRAINT `MAGEFAN_BLOG_POST_CATEGORY_POST_ID_MAGEFAN_BLOG_POST_POST_ID` FOREIGN KEY (`post_id`) REFERENCES `magefan_blog_post` (`post_id`) ON DELETE CASCADE,
  CONSTRAINT `MAGEFAN_BLOG_POST_CTGR_CTGR_ID_MAGEFAN_BLOG_CTGR_CTGR_ID` FOREIGN KEY (`category_id`) REFERENCES `magefan_blog_category` (`category_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Magefan Blog Post To Category Linkage Table';

/*Data for the table `magefan_blog_post_category` */

/*Table structure for table `magefan_blog_post_relatedpost` */

DROP TABLE IF EXISTS `magefan_blog_post_relatedpost`;

CREATE TABLE `magefan_blog_post_relatedpost` (
  `post_id` int(11) NOT NULL COMMENT 'Post ID',
  `related_id` int(11) NOT NULL COMMENT 'Related Post ID',
  `position` int(11) NOT NULL COMMENT 'Position',
  PRIMARY KEY (`post_id`,`related_id`),
  KEY `MAGEFAN_BLOG_POST_RELATEDPRODUCT_RELATED_ID` (`related_id`),
  CONSTRAINT `FK_9F26E983FE1D7D7063B61D3F3C8D1104` FOREIGN KEY (`post_id`) REFERENCES `magefan_blog_post` (`post_id`) ON DELETE CASCADE,
  CONSTRAINT `MAGEFAN_BLOG_POST_RELATEDPRD1_POST_ID_MAGEFAN_BLOG_POST_POST_ID` FOREIGN KEY (`post_id`) REFERENCES `magefan_blog_post` (`post_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Magefan Blog Post To Post Linkage Table';

/*Data for the table `magefan_blog_post_relatedpost` */

/*Table structure for table `magefan_blog_post_relatedproduct` */

DROP TABLE IF EXISTS `magefan_blog_post_relatedproduct`;

CREATE TABLE `magefan_blog_post_relatedproduct` (
  `post_id` int(11) NOT NULL COMMENT 'Post ID',
  `related_id` int(10) unsigned NOT NULL COMMENT 'Related Product ID',
  `position` int(11) NOT NULL COMMENT 'Position',
  PRIMARY KEY (`post_id`,`related_id`),
  KEY `MAGEFAN_BLOG_POST_RELATEDPRODUCT_RELATED_ID` (`related_id`),
  CONSTRAINT `MAGEFAN_BLOG_POST_RELATEDPRD_POST_ID_MAGEFAN_BLOG_POST_POST_ID` FOREIGN KEY (`post_id`) REFERENCES `magefan_blog_post` (`post_id`) ON DELETE CASCADE,
  CONSTRAINT `MAGEFAN_BLOG_POST_RELATEDPRD_RELATED_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`related_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Magefan Blog Post To Product Linkage Table';

/*Data for the table `magefan_blog_post_relatedproduct` */

/*Table structure for table `magefan_blog_post_store` */

DROP TABLE IF EXISTS `magefan_blog_post_store`;

CREATE TABLE `magefan_blog_post_store` (
  `post_id` int(11) NOT NULL COMMENT 'Post ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  PRIMARY KEY (`post_id`,`store_id`),
  KEY `MAGEFAN_BLOG_POST_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `MAGEFAN_BLOG_POST_STORE_POST_ID_MAGEFAN_BLOG_POST_POST_ID` FOREIGN KEY (`post_id`) REFERENCES `magefan_blog_post` (`post_id`) ON DELETE CASCADE,
  CONSTRAINT `MAGEFAN_BLOG_POST_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Magefan Blog Post To Store Linkage Table';

/*Data for the table `magefan_blog_post_store` */

insert  into `magefan_blog_post_store`(`post_id`,`store_id`) values (1,0);

/*Table structure for table `mview_state` */

DROP TABLE IF EXISTS `mview_state`;

CREATE TABLE `mview_state` (
  `state_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'View State Id',
  `view_id` varchar(255) DEFAULT NULL COMMENT 'View Id',
  `mode` varchar(16) DEFAULT 'disabled' COMMENT 'View Mode',
  `status` varchar(16) DEFAULT 'idle' COMMENT 'View Status',
  `updated` datetime DEFAULT NULL COMMENT 'View updated time',
  `version_id` int(10) unsigned DEFAULT NULL COMMENT 'View Version Id',
  PRIMARY KEY (`state_id`),
  KEY `MVIEW_STATE_VIEW_ID` (`view_id`),
  KEY `MVIEW_STATE_MODE` (`mode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='View State';

/*Data for the table `mview_state` */

/*Table structure for table `newsletter_problem` */

DROP TABLE IF EXISTS `newsletter_problem`;

CREATE TABLE `newsletter_problem` (
  `problem_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Problem Id',
  `subscriber_id` int(10) unsigned DEFAULT NULL COMMENT 'Subscriber Id',
  `queue_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Queue Id',
  `problem_error_code` int(10) unsigned DEFAULT '0' COMMENT 'Problem Error Code',
  `problem_error_text` varchar(200) DEFAULT NULL COMMENT 'Problem Error Text',
  PRIMARY KEY (`problem_id`),
  KEY `NEWSLETTER_PROBLEM_SUBSCRIBER_ID` (`subscriber_id`),
  KEY `NEWSLETTER_PROBLEM_QUEUE_ID` (`queue_id`),
  CONSTRAINT `NEWSLETTER_PROBLEM_QUEUE_ID_NEWSLETTER_QUEUE_QUEUE_ID` FOREIGN KEY (`queue_id`) REFERENCES `newsletter_queue` (`queue_id`) ON DELETE CASCADE,
  CONSTRAINT `NLTTR_PROBLEM_SUBSCRIBER_ID_NLTTR_SUBSCRIBER_SUBSCRIBER_ID` FOREIGN KEY (`subscriber_id`) REFERENCES `newsletter_subscriber` (`subscriber_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Problems';

/*Data for the table `newsletter_problem` */

/*Table structure for table `newsletter_queue` */

DROP TABLE IF EXISTS `newsletter_queue`;

CREATE TABLE `newsletter_queue` (
  `queue_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Queue Id',
  `template_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Template ID',
  `newsletter_type` int(11) DEFAULT NULL COMMENT 'Newsletter Type',
  `newsletter_text` text COMMENT 'Newsletter Text',
  `newsletter_styles` text COMMENT 'Newsletter Styles',
  `newsletter_subject` varchar(200) DEFAULT NULL COMMENT 'Newsletter Subject',
  `newsletter_sender_name` varchar(200) DEFAULT NULL COMMENT 'Newsletter Sender Name',
  `newsletter_sender_email` varchar(200) DEFAULT NULL COMMENT 'Newsletter Sender Email',
  `queue_status` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Queue Status',
  `queue_start_at` timestamp NULL DEFAULT NULL COMMENT 'Queue Start At',
  `queue_finish_at` timestamp NULL DEFAULT NULL COMMENT 'Queue Finish At',
  PRIMARY KEY (`queue_id`),
  KEY `NEWSLETTER_QUEUE_TEMPLATE_ID` (`template_id`),
  CONSTRAINT `NEWSLETTER_QUEUE_TEMPLATE_ID_NEWSLETTER_TEMPLATE_TEMPLATE_ID` FOREIGN KEY (`template_id`) REFERENCES `newsletter_template` (`template_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Queue';

/*Data for the table `newsletter_queue` */

/*Table structure for table `newsletter_queue_link` */

DROP TABLE IF EXISTS `newsletter_queue_link`;

CREATE TABLE `newsletter_queue_link` (
  `queue_link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Queue Link Id',
  `queue_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Queue Id',
  `subscriber_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Subscriber Id',
  `letter_sent_at` timestamp NULL DEFAULT NULL COMMENT 'Letter Sent At',
  PRIMARY KEY (`queue_link_id`),
  KEY `NEWSLETTER_QUEUE_LINK_SUBSCRIBER_ID` (`subscriber_id`),
  KEY `NEWSLETTER_QUEUE_LINK_QUEUE_ID_LETTER_SENT_AT` (`queue_id`,`letter_sent_at`),
  CONSTRAINT `NEWSLETTER_QUEUE_LINK_QUEUE_ID_NEWSLETTER_QUEUE_QUEUE_ID` FOREIGN KEY (`queue_id`) REFERENCES `newsletter_queue` (`queue_id`) ON DELETE CASCADE,
  CONSTRAINT `NLTTR_QUEUE_LNK_SUBSCRIBER_ID_NLTTR_SUBSCRIBER_SUBSCRIBER_ID` FOREIGN KEY (`subscriber_id`) REFERENCES `newsletter_subscriber` (`subscriber_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Queue Link';

/*Data for the table `newsletter_queue_link` */

/*Table structure for table `newsletter_queue_store_link` */

DROP TABLE IF EXISTS `newsletter_queue_store_link`;

CREATE TABLE `newsletter_queue_store_link` (
  `queue_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Queue Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  PRIMARY KEY (`queue_id`,`store_id`),
  KEY `NEWSLETTER_QUEUE_STORE_LINK_STORE_ID` (`store_id`),
  CONSTRAINT `NEWSLETTER_QUEUE_STORE_LINK_QUEUE_ID_NEWSLETTER_QUEUE_QUEUE_ID` FOREIGN KEY (`queue_id`) REFERENCES `newsletter_queue` (`queue_id`) ON DELETE CASCADE,
  CONSTRAINT `NEWSLETTER_QUEUE_STORE_LINK_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Queue Store Link';

/*Data for the table `newsletter_queue_store_link` */

/*Table structure for table `newsletter_subscriber` */

DROP TABLE IF EXISTS `newsletter_subscriber`;

CREATE TABLE `newsletter_subscriber` (
  `subscriber_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Subscriber Id',
  `store_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Store Id',
  `change_status_at` timestamp NULL DEFAULT NULL COMMENT 'Change Status At',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Id',
  `subscriber_email` varchar(150) DEFAULT NULL COMMENT 'Subscriber Email',
  `subscriber_status` int(11) NOT NULL DEFAULT '0' COMMENT 'Subscriber Status',
  `subscriber_confirm_code` varchar(32) DEFAULT 'NULL' COMMENT 'Subscriber Confirm Code',
  PRIMARY KEY (`subscriber_id`),
  KEY `NEWSLETTER_SUBSCRIBER_CUSTOMER_ID` (`customer_id`),
  KEY `NEWSLETTER_SUBSCRIBER_STORE_ID` (`store_id`),
  CONSTRAINT `NEWSLETTER_SUBSCRIBER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Subscriber';

/*Data for the table `newsletter_subscriber` */

/*Table structure for table `newsletter_template` */

DROP TABLE IF EXISTS `newsletter_template`;

CREATE TABLE `newsletter_template` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Template ID',
  `template_code` varchar(150) DEFAULT NULL COMMENT 'Template Code',
  `template_text` text COMMENT 'Template Text',
  `template_styles` text COMMENT 'Template Styles',
  `template_type` int(10) unsigned DEFAULT NULL COMMENT 'Template Type',
  `template_subject` varchar(200) DEFAULT NULL COMMENT 'Template Subject',
  `template_sender_name` varchar(200) DEFAULT NULL COMMENT 'Template Sender Name',
  `template_sender_email` varchar(200) DEFAULT NULL COMMENT 'Template Sender Email',
  `template_actual` smallint(5) unsigned DEFAULT '1' COMMENT 'Template Actual',
  `added_at` timestamp NULL DEFAULT NULL COMMENT 'Added At',
  `modified_at` timestamp NULL DEFAULT NULL COMMENT 'Modified At',
  PRIMARY KEY (`template_id`),
  KEY `NEWSLETTER_TEMPLATE_TEMPLATE_ACTUAL` (`template_actual`),
  KEY `NEWSLETTER_TEMPLATE_ADDED_AT` (`added_at`),
  KEY `NEWSLETTER_TEMPLATE_MODIFIED_AT` (`modified_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Template';

/*Data for the table `newsletter_template` */

/*Table structure for table `oauth_consumer` */

DROP TABLE IF EXISTS `oauth_consumer`;

CREATE TABLE `oauth_consumer` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `name` varchar(255) NOT NULL COMMENT 'Name of consumer',
  `key` varchar(32) NOT NULL COMMENT 'Key code',
  `secret` varchar(32) NOT NULL COMMENT 'Secret code',
  `callback_url` text COMMENT 'Callback URL',
  `rejected_callback_url` text NOT NULL COMMENT 'Rejected callback URL',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `OAUTH_CONSUMER_KEY` (`key`),
  UNIQUE KEY `OAUTH_CONSUMER_SECRET` (`secret`),
  KEY `OAUTH_CONSUMER_CREATED_AT` (`created_at`),
  KEY `OAUTH_CONSUMER_UPDATED_AT` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='OAuth Consumers';

/*Data for the table `oauth_consumer` */

/*Table structure for table `oauth_nonce` */

DROP TABLE IF EXISTS `oauth_nonce`;

CREATE TABLE `oauth_nonce` (
  `nonce` varchar(32) NOT NULL COMMENT 'Nonce String',
  `timestamp` int(10) unsigned NOT NULL COMMENT 'Nonce Timestamp',
  `consumer_id` int(10) unsigned NOT NULL COMMENT 'Consumer ID',
  UNIQUE KEY `OAUTH_NONCE_NONCE_CONSUMER_ID` (`nonce`,`consumer_id`),
  KEY `OAUTH_NONCE_CONSUMER_ID_OAUTH_CONSUMER_ENTITY_ID` (`consumer_id`),
  CONSTRAINT `OAUTH_NONCE_CONSUMER_ID_OAUTH_CONSUMER_ENTITY_ID` FOREIGN KEY (`consumer_id`) REFERENCES `oauth_consumer` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='OAuth Nonce';

/*Data for the table `oauth_nonce` */

/*Table structure for table `oauth_token` */

DROP TABLE IF EXISTS `oauth_token`;

CREATE TABLE `oauth_token` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `consumer_id` int(10) unsigned DEFAULT NULL COMMENT 'Oauth Consumer ID',
  `admin_id` int(10) unsigned DEFAULT NULL COMMENT 'Admin user ID',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer user ID',
  `type` varchar(16) NOT NULL COMMENT 'Token Type',
  `token` varchar(32) NOT NULL COMMENT 'Token',
  `secret` varchar(32) NOT NULL COMMENT 'Token Secret',
  `verifier` varchar(32) DEFAULT NULL COMMENT 'Token Verifier',
  `callback_url` text NOT NULL COMMENT 'Token Callback URL',
  `revoked` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Token revoked',
  `authorized` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Token authorized',
  `user_type` int(11) DEFAULT NULL COMMENT 'User type',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Token creation timestamp',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `OAUTH_TOKEN_TOKEN` (`token`),
  KEY `OAUTH_TOKEN_CONSUMER_ID` (`consumer_id`),
  KEY `OAUTH_TOKEN_ADMIN_ID_ADMIN_USER_USER_ID` (`admin_id`),
  KEY `OAUTH_TOKEN_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` (`customer_id`),
  CONSTRAINT `OAUTH_TOKEN_ADMIN_ID_ADMIN_USER_USER_ID` FOREIGN KEY (`admin_id`) REFERENCES `admin_user` (`user_id`) ON DELETE CASCADE,
  CONSTRAINT `OAUTH_TOKEN_CONSUMER_ID_OAUTH_CONSUMER_ENTITY_ID` FOREIGN KEY (`consumer_id`) REFERENCES `oauth_consumer` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `OAUTH_TOKEN_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='OAuth Tokens';

/*Data for the table `oauth_token` */

/*Table structure for table `oauth_token_request_log` */

DROP TABLE IF EXISTS `oauth_token_request_log`;

CREATE TABLE `oauth_token_request_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Log Id',
  `user_name` varchar(255) NOT NULL COMMENT 'Customer email or admin login',
  `user_type` smallint(5) unsigned NOT NULL COMMENT 'User type (admin or customer)',
  `failures_count` smallint(5) unsigned DEFAULT '0' COMMENT 'Number of failed authentication attempts in a row',
  `lock_expires_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Lock expiration time',
  PRIMARY KEY (`log_id`),
  UNIQUE KEY `OAUTH_TOKEN_REQUEST_LOG_USER_NAME_USER_TYPE` (`user_name`,`user_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Log of token request authentication failures.';

/*Data for the table `oauth_token_request_log` */

/*Table structure for table `password_reset_request_event` */

DROP TABLE IF EXISTS `password_reset_request_event`;

CREATE TABLE `password_reset_request_event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `request_type` smallint(5) unsigned NOT NULL COMMENT 'Type of the event under a security control',
  `account_reference` varchar(255) DEFAULT NULL COMMENT 'An identifier for existing account or another target',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Timestamp when the event occurs',
  `ip` varchar(15) NOT NULL COMMENT 'Remote user IP',
  PRIMARY KEY (`id`),
  KEY `PASSWORD_RESET_REQUEST_EVENT_ACCOUNT_REFERENCE` (`account_reference`),
  KEY `PASSWORD_RESET_REQUEST_EVENT_CREATED_AT` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Password Reset Request Event under a security control';

/*Data for the table `password_reset_request_event` */

/*Table structure for table `paypal_billing_agreement` */

DROP TABLE IF EXISTS `paypal_billing_agreement`;

CREATE TABLE `paypal_billing_agreement` (
  `agreement_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Agreement Id',
  `customer_id` int(10) unsigned NOT NULL COMMENT 'Customer Id',
  `method_code` varchar(32) NOT NULL COMMENT 'Method Code',
  `reference_id` varchar(32) NOT NULL COMMENT 'Reference Id',
  `status` varchar(20) NOT NULL COMMENT 'Status',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `agreement_label` varchar(255) DEFAULT NULL COMMENT 'Agreement Label',
  PRIMARY KEY (`agreement_id`),
  KEY `PAYPAL_BILLING_AGREEMENT_CUSTOMER_ID` (`customer_id`),
  KEY `PAYPAL_BILLING_AGREEMENT_STORE_ID` (`store_id`),
  CONSTRAINT `PAYPAL_BILLING_AGREEMENT_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `PAYPAL_BILLING_AGREEMENT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Billing Agreement';

/*Data for the table `paypal_billing_agreement` */

/*Table structure for table `paypal_billing_agreement_order` */

DROP TABLE IF EXISTS `paypal_billing_agreement_order`;

CREATE TABLE `paypal_billing_agreement_order` (
  `agreement_id` int(10) unsigned NOT NULL COMMENT 'Agreement Id',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  PRIMARY KEY (`agreement_id`,`order_id`),
  KEY `PAYPAL_BILLING_AGREEMENT_ORDER_ORDER_ID` (`order_id`),
  CONSTRAINT `PAYPAL_BILLING_AGREEMENT_ORDER_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `PAYPAL_BILLING_AGRT_ORDER_AGRT_ID_PAYPAL_BILLING_AGRT_AGRT_ID` FOREIGN KEY (`agreement_id`) REFERENCES `paypal_billing_agreement` (`agreement_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Billing Agreement Order';

/*Data for the table `paypal_billing_agreement_order` */

/*Table structure for table `paypal_cert` */

DROP TABLE IF EXISTS `paypal_cert`;

CREATE TABLE `paypal_cert` (
  `cert_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Cert Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `content` text COMMENT 'Content',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  PRIMARY KEY (`cert_id`),
  KEY `PAYPAL_CERT_WEBSITE_ID` (`website_id`),
  CONSTRAINT `PAYPAL_CERT_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Paypal Certificate Table';

/*Data for the table `paypal_cert` */

/*Table structure for table `paypal_payment_transaction` */

DROP TABLE IF EXISTS `paypal_payment_transaction`;

CREATE TABLE `paypal_payment_transaction` (
  `transaction_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `txn_id` varchar(100) DEFAULT NULL COMMENT 'Txn Id',
  `additional_information` blob COMMENT 'Additional Information',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  PRIMARY KEY (`transaction_id`),
  UNIQUE KEY `PAYPAL_PAYMENT_TRANSACTION_TXN_ID` (`txn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='PayPal Payflow Link Payment Transaction';

/*Data for the table `paypal_payment_transaction` */

/*Table structure for table `paypal_settlement_report` */

DROP TABLE IF EXISTS `paypal_settlement_report`;

CREATE TABLE `paypal_settlement_report` (
  `report_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Report Id',
  `report_date` timestamp NULL DEFAULT NULL COMMENT 'Report Date',
  `account_id` varchar(64) DEFAULT NULL COMMENT 'Account Id',
  `filename` varchar(24) DEFAULT NULL COMMENT 'Filename',
  `last_modified` timestamp NULL DEFAULT NULL COMMENT 'Last Modified',
  PRIMARY KEY (`report_id`),
  UNIQUE KEY `PAYPAL_SETTLEMENT_REPORT_REPORT_DATE_ACCOUNT_ID` (`report_date`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Paypal Settlement Report Table';

/*Data for the table `paypal_settlement_report` */

/*Table structure for table `paypal_settlement_report_row` */

DROP TABLE IF EXISTS `paypal_settlement_report_row`;

CREATE TABLE `paypal_settlement_report_row` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Row Id',
  `report_id` int(10) unsigned NOT NULL COMMENT 'Report Id',
  `transaction_id` varchar(19) DEFAULT NULL COMMENT 'Transaction Id',
  `invoice_id` varchar(127) DEFAULT NULL COMMENT 'Invoice Id',
  `paypal_reference_id` varchar(19) DEFAULT NULL COMMENT 'Paypal Reference Id',
  `paypal_reference_id_type` varchar(3) DEFAULT NULL COMMENT 'Paypal Reference Id Type',
  `transaction_event_code` varchar(5) DEFAULT NULL COMMENT 'Transaction Event Code',
  `transaction_initiation_date` timestamp NULL DEFAULT NULL COMMENT 'Transaction Initiation Date',
  `transaction_completion_date` timestamp NULL DEFAULT NULL COMMENT 'Transaction Completion Date',
  `transaction_debit_or_credit` varchar(2) NOT NULL DEFAULT 'CR' COMMENT 'Transaction Debit Or Credit',
  `gross_transaction_amount` decimal(20,6) NOT NULL DEFAULT '0.000000' COMMENT 'Gross Transaction Amount',
  `gross_transaction_currency` varchar(3) DEFAULT NULL COMMENT 'Gross Transaction Currency',
  `fee_debit_or_credit` varchar(2) DEFAULT NULL COMMENT 'Fee Debit Or Credit',
  `fee_amount` decimal(20,6) NOT NULL DEFAULT '0.000000' COMMENT 'Fee Amount',
  `fee_currency` varchar(3) DEFAULT NULL COMMENT 'Fee Currency',
  `custom_field` varchar(255) DEFAULT NULL COMMENT 'Custom Field',
  `consumer_id` varchar(127) DEFAULT NULL COMMENT 'Consumer Id',
  `payment_tracking_id` varchar(255) DEFAULT NULL COMMENT 'Payment Tracking ID',
  `store_id` varchar(50) DEFAULT NULL COMMENT 'Store ID',
  PRIMARY KEY (`row_id`),
  KEY `PAYPAL_SETTLEMENT_REPORT_ROW_REPORT_ID` (`report_id`),
  CONSTRAINT `FK_E183E488F593E0DE10C6EBFFEBAC9B55` FOREIGN KEY (`report_id`) REFERENCES `paypal_settlement_report` (`report_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Paypal Settlement Report Row Table';

/*Data for the table `paypal_settlement_report_row` */

/*Table structure for table `persistent_session` */

DROP TABLE IF EXISTS `persistent_session`;

CREATE TABLE `persistent_session` (
  `persistent_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Session id',
  `key` varchar(50) NOT NULL COMMENT 'Unique cookie key',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website ID',
  `info` text COMMENT 'Session Data',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`persistent_id`),
  UNIQUE KEY `PERSISTENT_SESSION_KEY` (`key`),
  UNIQUE KEY `PERSISTENT_SESSION_CUSTOMER_ID` (`customer_id`),
  KEY `PERSISTENT_SESSION_UPDATED_AT` (`updated_at`),
  KEY `PERSISTENT_SESSION_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `PERSISTENT_SESSION_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `PERSISTENT_SESSION_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Persistent Session';

/*Data for the table `persistent_session` */

/*Table structure for table `product_alert_price` */

DROP TABLE IF EXISTS `product_alert_price`;

CREATE TABLE `product_alert_price` (
  `alert_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product alert price id',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price amount',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website id',
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Product alert add date',
  `last_send_date` timestamp NULL DEFAULT NULL COMMENT 'Product alert last send date',
  `send_count` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Product alert send count',
  `status` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Product alert status',
  PRIMARY KEY (`alert_price_id`),
  KEY `PRODUCT_ALERT_PRICE_CUSTOMER_ID` (`customer_id`),
  KEY `PRODUCT_ALERT_PRICE_PRODUCT_ID` (`product_id`),
  KEY `PRODUCT_ALERT_PRICE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `PRODUCT_ALERT_PRICE_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `PRODUCT_ALERT_PRICE_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `PRODUCT_ALERT_PRICE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Product Alert Price';

/*Data for the table `product_alert_price` */

/*Table structure for table `product_alert_stock` */

DROP TABLE IF EXISTS `product_alert_stock`;

CREATE TABLE `product_alert_stock` (
  `alert_stock_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product alert stock id',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website id',
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Product alert add date',
  `send_date` timestamp NULL DEFAULT NULL COMMENT 'Product alert send date',
  `send_count` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Send Count',
  `status` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Product alert status',
  PRIMARY KEY (`alert_stock_id`),
  KEY `PRODUCT_ALERT_STOCK_CUSTOMER_ID` (`customer_id`),
  KEY `PRODUCT_ALERT_STOCK_PRODUCT_ID` (`product_id`),
  KEY `PRODUCT_ALERT_STOCK_WEBSITE_ID` (`website_id`),
  CONSTRAINT `PRODUCT_ALERT_STOCK_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `PRODUCT_ALERT_STOCK_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `PRODUCT_ALERT_STOCK_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Product Alert Stock';

/*Data for the table `product_alert_stock` */

/*Table structure for table `quote` */

DROP TABLE IF EXISTS `quote`;

CREATE TABLE `quote` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `converted_at` timestamp NULL DEFAULT NULL COMMENT 'Converted At',
  `is_active` smallint(5) unsigned DEFAULT '1' COMMENT 'Is Active',
  `is_virtual` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Virtual',
  `is_multi_shipping` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Multi Shipping',
  `items_count` int(10) unsigned DEFAULT '0' COMMENT 'Items Count',
  `items_qty` decimal(12,4) DEFAULT '0.0000' COMMENT 'Items Qty',
  `orig_order_id` int(10) unsigned DEFAULT '0' COMMENT 'Orig Order Id',
  `store_to_base_rate` decimal(12,4) DEFAULT '0.0000' COMMENT 'Store To Base Rate',
  `store_to_quote_rate` decimal(12,4) DEFAULT '0.0000' COMMENT 'Store To Quote Rate',
  `base_currency_code` varchar(255) DEFAULT NULL COMMENT 'Base Currency Code',
  `store_currency_code` varchar(255) DEFAULT NULL COMMENT 'Store Currency Code',
  `quote_currency_code` varchar(255) DEFAULT NULL COMMENT 'Quote Currency Code',
  `grand_total` decimal(12,4) DEFAULT '0.0000' COMMENT 'Grand Total',
  `base_grand_total` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Grand Total',
  `checkout_method` varchar(255) DEFAULT NULL COMMENT 'Checkout Method',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `customer_tax_class_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Tax Class Id',
  `customer_group_id` int(10) unsigned DEFAULT '0' COMMENT 'Customer Group Id',
  `customer_email` varchar(255) DEFAULT NULL COMMENT 'Customer Email',
  `customer_prefix` varchar(40) DEFAULT NULL COMMENT 'Customer Prefix',
  `customer_firstname` varchar(255) DEFAULT NULL COMMENT 'Customer Firstname',
  `customer_middlename` varchar(40) DEFAULT NULL COMMENT 'Customer Middlename',
  `customer_lastname` varchar(255) DEFAULT NULL COMMENT 'Customer Lastname',
  `customer_suffix` varchar(40) DEFAULT NULL COMMENT 'Customer Suffix',
  `customer_dob` datetime DEFAULT NULL COMMENT 'Customer Dob',
  `customer_note` varchar(255) DEFAULT NULL COMMENT 'Customer Note',
  `customer_note_notify` smallint(5) unsigned DEFAULT '1' COMMENT 'Customer Note Notify',
  `customer_is_guest` smallint(5) unsigned DEFAULT '0' COMMENT 'Customer Is Guest',
  `remote_ip` varchar(32) DEFAULT NULL COMMENT 'Remote Ip',
  `applied_rule_ids` varchar(255) DEFAULT NULL COMMENT 'Applied Rule Ids',
  `reserved_order_id` varchar(64) DEFAULT NULL COMMENT 'Reserved Order Id',
  `password_hash` varchar(255) DEFAULT NULL COMMENT 'Password Hash',
  `coupon_code` varchar(255) DEFAULT NULL COMMENT 'Coupon Code',
  `global_currency_code` varchar(255) DEFAULT NULL COMMENT 'Global Currency Code',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `base_to_quote_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Quote Rate',
  `customer_taxvat` varchar(255) DEFAULT NULL COMMENT 'Customer Taxvat',
  `customer_gender` varchar(255) DEFAULT NULL COMMENT 'Customer Gender',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `subtotal_with_discount` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal With Discount',
  `base_subtotal_with_discount` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal With Discount',
  `is_changed` int(10) unsigned DEFAULT NULL COMMENT 'Is Changed',
  `trigger_recollect` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Trigger Recollect',
  `ext_shipping_info` text COMMENT 'Ext Shipping Info',
  `is_persistent` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Quote Persistent',
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  PRIMARY KEY (`entity_id`),
  KEY `QUOTE_CUSTOMER_ID_STORE_ID_IS_ACTIVE` (`customer_id`,`store_id`,`is_active`),
  KEY `QUOTE_STORE_ID` (`store_id`),
  CONSTRAINT `QUOTE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote';

/*Data for the table `quote` */

/*Table structure for table `quote_address` */

DROP TABLE IF EXISTS `quote_address`;

CREATE TABLE `quote_address` (
  `address_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Address Id',
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `save_in_address_book` smallint(6) DEFAULT '0' COMMENT 'Save In Address Book',
  `customer_address_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Address Id',
  `address_type` varchar(10) DEFAULT NULL COMMENT 'Address Type',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `prefix` varchar(40) DEFAULT NULL COMMENT 'Prefix',
  `firstname` varchar(20) DEFAULT NULL COMMENT 'Firstname',
  `middlename` varchar(20) DEFAULT NULL COMMENT 'Middlename',
  `lastname` varchar(20) DEFAULT NULL COMMENT 'Lastname',
  `suffix` varchar(40) DEFAULT NULL COMMENT 'Suffix',
  `company` varchar(255) DEFAULT NULL COMMENT 'Company',
  `street` varchar(255) DEFAULT NULL COMMENT 'Street',
  `city` varchar(40) DEFAULT NULL COMMENT 'City',
  `region` varchar(40) DEFAULT NULL COMMENT 'Region',
  `region_id` int(10) unsigned DEFAULT NULL COMMENT 'Region Id',
  `postcode` varchar(20) DEFAULT NULL COMMENT 'Postcode',
  `country_id` varchar(30) DEFAULT NULL COMMENT 'Country Id',
  `telephone` varchar(20) DEFAULT NULL COMMENT 'Phone Number',
  `fax` varchar(20) DEFAULT NULL COMMENT 'Fax',
  `same_as_billing` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Same As Billing',
  `collect_shipping_rates` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Collect Shipping Rates',
  `shipping_method` varchar(40) DEFAULT NULL COMMENT 'Shipping Method',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `weight` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Weight',
  `subtotal` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal',
  `base_subtotal` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Subtotal',
  `subtotal_with_discount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal With Discount',
  `base_subtotal_with_discount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Subtotal With Discount',
  `tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Shipping Amount',
  `base_shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Shipping Amount',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `base_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `grand_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Grand Total',
  `base_grand_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Grand Total',
  `customer_notes` text COMMENT 'Customer Notes',
  `applied_taxes` text COMMENT 'Applied Taxes',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Amount',
  `base_shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `base_subtotal_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Total Incl Tax',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
  `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `free_shipping` smallint(6) DEFAULT NULL,
  `vat_id` text COMMENT 'Vat Id',
  `vat_is_valid` smallint(6) DEFAULT NULL COMMENT 'Vat Is Valid',
  `vat_request_id` text COMMENT 'Vat Request Id',
  `vat_request_date` text COMMENT 'Vat Request Date',
  `vat_request_success` smallint(6) DEFAULT NULL COMMENT 'Vat Request Success',
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  PRIMARY KEY (`address_id`),
  KEY `QUOTE_ADDRESS_QUOTE_ID` (`quote_id`),
  CONSTRAINT `QUOTE_ADDRESS_QUOTE_ID_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `quote` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Address';

/*Data for the table `quote_address` */

/*Table structure for table `quote_address_item` */

DROP TABLE IF EXISTS `quote_address_item`;

CREATE TABLE `quote_address_item` (
  `address_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Address Item Id',
  `parent_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Item Id',
  `quote_address_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Address Id',
  `quote_item_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Item Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `applied_rule_ids` text COMMENT 'Applied Rule Ids',
  `additional_data` text COMMENT 'Additional Data',
  `weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Weight',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Amount',
  `tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Amount',
  `row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Total',
  `base_row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Total',
  `row_total_with_discount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Total With Discount',
  `base_discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `base_tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `row_weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Weight',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `super_product_id` int(10) unsigned DEFAULT NULL COMMENT 'Super Product Id',
  `parent_product_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Product Id',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `image` varchar(255) DEFAULT NULL COMMENT 'Image',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `is_qty_decimal` int(10) unsigned DEFAULT NULL COMMENT 'Is Qty Decimal',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `discount_percent` decimal(12,4) DEFAULT NULL COMMENT 'Discount Percent',
  `no_discount` int(10) unsigned DEFAULT NULL COMMENT 'No Discount',
  `tax_percent` decimal(12,4) DEFAULT NULL COMMENT 'Tax Percent',
  `base_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Price',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `free_shipping` int(11) DEFAULT NULL,
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  PRIMARY KEY (`address_item_id`),
  KEY `QUOTE_ADDRESS_ITEM_QUOTE_ADDRESS_ID` (`quote_address_id`),
  KEY `QUOTE_ADDRESS_ITEM_PARENT_ITEM_ID` (`parent_item_id`),
  KEY `QUOTE_ADDRESS_ITEM_QUOTE_ITEM_ID` (`quote_item_id`),
  CONSTRAINT `QUOTE_ADDRESS_ITEM_QUOTE_ADDRESS_ID_QUOTE_ADDRESS_ADDRESS_ID` FOREIGN KEY (`quote_address_id`) REFERENCES `quote_address` (`address_id`) ON DELETE CASCADE,
  CONSTRAINT `QUOTE_ADDRESS_ITEM_QUOTE_ITEM_ID_QUOTE_ITEM_ITEM_ID` FOREIGN KEY (`quote_item_id`) REFERENCES `quote_item` (`item_id`) ON DELETE CASCADE,
  CONSTRAINT `QUOTE_ADDR_ITEM_PARENT_ITEM_ID_QUOTE_ADDR_ITEM_ADDR_ITEM_ID` FOREIGN KEY (`parent_item_id`) REFERENCES `quote_address_item` (`address_item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Address Item';

/*Data for the table `quote_address_item` */

/*Table structure for table `quote_id_mask` */

DROP TABLE IF EXISTS `quote_id_mask`;

CREATE TABLE `quote_id_mask` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `quote_id` int(10) unsigned NOT NULL COMMENT 'Quote ID',
  `masked_id` varchar(32) DEFAULT NULL COMMENT 'Masked ID',
  PRIMARY KEY (`entity_id`,`quote_id`),
  KEY `QUOTE_ID_MASK_QUOTE_ID` (`quote_id`),
  KEY `QUOTE_ID_MASK_MASKED_ID` (`masked_id`),
  CONSTRAINT `QUOTE_ID_MASK_QUOTE_ID_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `quote` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Quote ID and masked ID mapping';

/*Data for the table `quote_id_mask` */

/*Table structure for table `quote_item` */

DROP TABLE IF EXISTS `quote_item`;

CREATE TABLE `quote_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item Id',
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `parent_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Item Id',
  `is_virtual` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Virtual',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `applied_rule_ids` text COMMENT 'Applied Rule Ids',
  `additional_data` text COMMENT 'Additional Data',
  `is_qty_decimal` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Qty Decimal',
  `no_discount` smallint(5) unsigned DEFAULT '0' COMMENT 'No Discount',
  `weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Weight',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `base_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Price',
  `custom_price` decimal(12,4) DEFAULT NULL COMMENT 'Custom Price',
  `discount_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Percent',
  `discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Amount',
  `base_discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `tax_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Percent',
  `tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Total',
  `base_row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Total',
  `row_total_with_discount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Total With Discount',
  `row_weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Weight',
  `product_type` varchar(255) DEFAULT NULL COMMENT 'Product Type',
  `base_tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Before Discount',
  `tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Before Discount',
  `original_custom_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Custom Price',
  `redirect_url` varchar(255) DEFAULT NULL COMMENT 'Redirect Url',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `free_shipping` smallint(6) DEFAULT NULL,
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  PRIMARY KEY (`item_id`),
  KEY `QUOTE_ITEM_PARENT_ITEM_ID` (`parent_item_id`),
  KEY `QUOTE_ITEM_PRODUCT_ID` (`product_id`),
  KEY `QUOTE_ITEM_QUOTE_ID` (`quote_id`),
  KEY `QUOTE_ITEM_STORE_ID` (`store_id`),
  CONSTRAINT `QUOTE_ITEM_PARENT_ITEM_ID_QUOTE_ITEM_ITEM_ID` FOREIGN KEY (`parent_item_id`) REFERENCES `quote_item` (`item_id`) ON DELETE CASCADE,
  CONSTRAINT `QUOTE_ITEM_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `QUOTE_ITEM_QUOTE_ID_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `quote` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `QUOTE_ITEM_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Item';

/*Data for the table `quote_item` */

/*Table structure for table `quote_item_option` */

DROP TABLE IF EXISTS `quote_item_option`;

CREATE TABLE `quote_item_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
  `item_id` int(10) unsigned NOT NULL COMMENT 'Item Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `value` text COMMENT 'Value',
  PRIMARY KEY (`option_id`),
  KEY `QUOTE_ITEM_OPTION_ITEM_ID` (`item_id`),
  CONSTRAINT `QUOTE_ITEM_OPTION_ITEM_ID_QUOTE_ITEM_ITEM_ID` FOREIGN KEY (`item_id`) REFERENCES `quote_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Item Option';

/*Data for the table `quote_item_option` */

/*Table structure for table `quote_payment` */

DROP TABLE IF EXISTS `quote_payment`;

CREATE TABLE `quote_payment` (
  `payment_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Payment Id',
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `method` varchar(255) DEFAULT NULL COMMENT 'Method',
  `cc_type` varchar(255) DEFAULT NULL COMMENT 'Cc Type',
  `cc_number_enc` varchar(255) DEFAULT NULL COMMENT 'Cc Number Enc',
  `cc_last_4` varchar(255) DEFAULT NULL COMMENT 'Cc Last 4',
  `cc_cid_enc` varchar(255) DEFAULT NULL COMMENT 'Cc Cid Enc',
  `cc_owner` varchar(255) DEFAULT NULL COMMENT 'Cc Owner',
  `cc_exp_month` varchar(255) DEFAULT NULL COMMENT 'Cc Exp Month',
  `cc_exp_year` smallint(5) unsigned DEFAULT '0' COMMENT 'Cc Exp Year',
  `cc_ss_owner` varchar(255) DEFAULT NULL COMMENT 'Cc Ss Owner',
  `cc_ss_start_month` smallint(5) unsigned DEFAULT '0' COMMENT 'Cc Ss Start Month',
  `cc_ss_start_year` smallint(5) unsigned DEFAULT '0' COMMENT 'Cc Ss Start Year',
  `po_number` varchar(255) DEFAULT NULL COMMENT 'Po Number',
  `additional_data` text COMMENT 'Additional Data',
  `cc_ss_issue` varchar(255) DEFAULT NULL COMMENT 'Cc Ss Issue',
  `additional_information` text COMMENT 'Additional Information',
  `paypal_payer_id` varchar(255) DEFAULT NULL COMMENT 'Paypal Payer Id',
  `paypal_payer_status` varchar(255) DEFAULT NULL COMMENT 'Paypal Payer Status',
  `paypal_correlation_id` varchar(255) DEFAULT NULL COMMENT 'Paypal Correlation Id',
  PRIMARY KEY (`payment_id`),
  KEY `QUOTE_PAYMENT_QUOTE_ID` (`quote_id`),
  CONSTRAINT `QUOTE_PAYMENT_QUOTE_ID_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `quote` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Payment';

/*Data for the table `quote_payment` */

/*Table structure for table `quote_shipping_rate` */

DROP TABLE IF EXISTS `quote_shipping_rate`;

CREATE TABLE `quote_shipping_rate` (
  `rate_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rate Id',
  `address_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Address Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `carrier` varchar(255) DEFAULT NULL COMMENT 'Carrier',
  `carrier_title` varchar(255) DEFAULT NULL COMMENT 'Carrier Title',
  `code` varchar(255) DEFAULT NULL COMMENT 'Code',
  `method` varchar(255) DEFAULT NULL COMMENT 'Method',
  `method_description` text COMMENT 'Method Description',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `error_message` text COMMENT 'Error Message',
  `method_title` text COMMENT 'Method Title',
  PRIMARY KEY (`rate_id`),
  KEY `QUOTE_SHIPPING_RATE_ADDRESS_ID` (`address_id`),
  CONSTRAINT `QUOTE_SHIPPING_RATE_ADDRESS_ID_QUOTE_ADDRESS_ADDRESS_ID` FOREIGN KEY (`address_id`) REFERENCES `quote_address` (`address_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Shipping Rate';

/*Data for the table `quote_shipping_rate` */

/*Table structure for table `rating` */

DROP TABLE IF EXISTS `rating`;

CREATE TABLE `rating` (
  `rating_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rating Id',
  `entity_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `rating_code` varchar(64) NOT NULL COMMENT 'Rating Code',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Position On Storefront',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Rating is active.',
  PRIMARY KEY (`rating_id`),
  UNIQUE KEY `RATING_RATING_CODE` (`rating_code`),
  KEY `RATING_ENTITY_ID` (`entity_id`),
  CONSTRAINT `RATING_ENTITY_ID_RATING_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `rating_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Ratings';

/*Data for the table `rating` */

insert  into `rating`(`rating_id`,`entity_id`,`rating_code`,`position`,`is_active`) values (1,1,'Quality',0,1),(2,1,'Value',0,1),(3,1,'Price',0,1);

/*Table structure for table `rating_entity` */

DROP TABLE IF EXISTS `rating_entity`;

CREATE TABLE `rating_entity` (
  `entity_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `entity_code` varchar(64) NOT NULL COMMENT 'Entity Code',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `RATING_ENTITY_ENTITY_CODE` (`entity_code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Rating entities';

/*Data for the table `rating_entity` */

insert  into `rating_entity`(`entity_id`,`entity_code`) values (1,'product'),(2,'product_review'),(3,'review');

/*Table structure for table `rating_option` */

DROP TABLE IF EXISTS `rating_option`;

CREATE TABLE `rating_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rating Option Id',
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Id',
  `code` varchar(32) NOT NULL COMMENT 'Rating Option Code',
  `value` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Option Value',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Ration option position on Storefront',
  PRIMARY KEY (`option_id`),
  KEY `RATING_OPTION_RATING_ID` (`rating_id`),
  CONSTRAINT `RATING_OPTION_RATING_ID_RATING_RATING_ID` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='Rating options';

/*Data for the table `rating_option` */

insert  into `rating_option`(`option_id`,`rating_id`,`code`,`value`,`position`) values (1,1,'1',1,1),(2,1,'2',2,2),(3,1,'3',3,3),(4,1,'4',4,4),(5,1,'5',5,5),(6,2,'1',1,1),(7,2,'2',2,2),(8,2,'3',3,3),(9,2,'4',4,4),(10,2,'5',5,5),(11,3,'1',1,1),(12,3,'2',2,2),(13,3,'3',3,3),(14,3,'4',4,4),(15,3,'5',5,5);

/*Table structure for table `rating_option_vote` */

DROP TABLE IF EXISTS `rating_option_vote`;

CREATE TABLE `rating_option_vote` (
  `vote_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Vote id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Vote option id',
  `remote_ip` varchar(16) NOT NULL COMMENT 'Customer IP',
  `remote_ip_long` bigint(20) NOT NULL DEFAULT '0' COMMENT 'Customer IP converted to long integer format',
  `customer_id` int(10) unsigned DEFAULT '0' COMMENT 'Customer Id',
  `entity_pk_value` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating id',
  `review_id` bigint(20) unsigned DEFAULT NULL COMMENT 'Review id',
  `percent` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Percent amount',
  `value` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Vote option value',
  PRIMARY KEY (`vote_id`),
  KEY `RATING_OPTION_VOTE_OPTION_ID` (`option_id`),
  KEY `RATING_OPTION_VOTE_REVIEW_ID_REVIEW_REVIEW_ID` (`review_id`),
  CONSTRAINT `RATING_OPTION_VOTE_OPTION_ID_RATING_OPTION_OPTION_ID` FOREIGN KEY (`option_id`) REFERENCES `rating_option` (`option_id`) ON DELETE CASCADE,
  CONSTRAINT `RATING_OPTION_VOTE_REVIEW_ID_REVIEW_REVIEW_ID` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rating option values';

/*Data for the table `rating_option_vote` */

/*Table structure for table `rating_option_vote_aggregated` */

DROP TABLE IF EXISTS `rating_option_vote_aggregated`;

CREATE TABLE `rating_option_vote_aggregated` (
  `primary_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Vote aggregation id',
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating id',
  `entity_pk_value` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `vote_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Vote dty',
  `vote_value_sum` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'General vote sum',
  `percent` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Vote percent',
  `percent_approved` smallint(6) DEFAULT '0' COMMENT 'Vote percent approved by admin',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  PRIMARY KEY (`primary_id`),
  KEY `RATING_OPTION_VOTE_AGGREGATED_RATING_ID` (`rating_id`),
  KEY `RATING_OPTION_VOTE_AGGREGATED_STORE_ID` (`store_id`),
  CONSTRAINT `RATING_OPTION_VOTE_AGGREGATED_RATING_ID_RATING_RATING_ID` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE,
  CONSTRAINT `RATING_OPTION_VOTE_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rating vote aggregated';

/*Data for the table `rating_option_vote_aggregated` */

/*Table structure for table `rating_store` */

DROP TABLE IF EXISTS `rating_store`;

CREATE TABLE `rating_store` (
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store id',
  PRIMARY KEY (`rating_id`,`store_id`),
  KEY `RATING_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `RATING_STORE_RATING_ID_RATING_RATING_ID` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE,
  CONSTRAINT `RATING_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rating Store';

/*Data for the table `rating_store` */

/*Table structure for table `rating_title` */

DROP TABLE IF EXISTS `rating_title`;

CREATE TABLE `rating_title` (
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `value` varchar(255) NOT NULL COMMENT 'Rating Label',
  PRIMARY KEY (`rating_id`,`store_id`),
  KEY `RATING_TITLE_STORE_ID` (`store_id`),
  CONSTRAINT `RATING_TITLE_RATING_ID_RATING_RATING_ID` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE,
  CONSTRAINT `RATING_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rating Title';

/*Data for the table `rating_title` */

/*Table structure for table `report_compared_product_index` */

DROP TABLE IF EXISTS `report_compared_product_index`;

CREATE TABLE `report_compared_product_index` (
  `index_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Index Id',
  `visitor_id` int(10) unsigned DEFAULT NULL COMMENT 'Visitor Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `added_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Added At',
  PRIMARY KEY (`index_id`),
  UNIQUE KEY `REPORT_COMPARED_PRODUCT_INDEX_VISITOR_ID_PRODUCT_ID` (`visitor_id`,`product_id`),
  UNIQUE KEY `REPORT_COMPARED_PRODUCT_INDEX_CUSTOMER_ID_PRODUCT_ID` (`customer_id`,`product_id`),
  KEY `REPORT_COMPARED_PRODUCT_INDEX_STORE_ID` (`store_id`),
  KEY `REPORT_COMPARED_PRODUCT_INDEX_ADDED_AT` (`added_at`),
  KEY `REPORT_COMPARED_PRODUCT_INDEX_PRODUCT_ID` (`product_id`),
  CONSTRAINT `REPORT_CMPD_PRD_IDX_CSTR_ID_CSTR_ENTT_ENTT_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `REPORT_CMPD_PRD_IDX_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `REPORT_COMPARED_PRODUCT_INDEX_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reports Compared Product Index Table';

/*Data for the table `report_compared_product_index` */

/*Table structure for table `report_event` */

DROP TABLE IF EXISTS `report_event`;

CREATE TABLE `report_event` (
  `event_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Event Id',
  `logged_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Logged At',
  `event_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Event Type Id',
  `object_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Object Id',
  `subject_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Subject Id',
  `subtype` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Subtype',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  PRIMARY KEY (`event_id`),
  KEY `REPORT_EVENT_EVENT_TYPE_ID` (`event_type_id`),
  KEY `REPORT_EVENT_SUBJECT_ID` (`subject_id`),
  KEY `REPORT_EVENT_OBJECT_ID` (`object_id`),
  KEY `REPORT_EVENT_SUBTYPE` (`subtype`),
  KEY `REPORT_EVENT_STORE_ID` (`store_id`),
  CONSTRAINT `REPORT_EVENT_EVENT_TYPE_ID_REPORT_EVENT_TYPES_EVENT_TYPE_ID` FOREIGN KEY (`event_type_id`) REFERENCES `report_event_types` (`event_type_id`) ON DELETE CASCADE,
  CONSTRAINT `REPORT_EVENT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Reports Event Table';

/*Data for the table `report_event` */

insert  into `report_event`(`event_id`,`logged_at`,`event_type_id`,`object_id`,`subject_id`,`subtype`,`store_id`) values (1,'2016-08-26 23:05:30',1,15,4,1,1),(2,'2016-08-26 23:07:05',1,9,4,1,1),(3,'2016-08-26 23:10:49',1,9,4,1,1),(4,'2016-08-26 23:13:37',1,15,4,1,1);

/*Table structure for table `report_event_types` */

DROP TABLE IF EXISTS `report_event_types`;

CREATE TABLE `report_event_types` (
  `event_type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Event Type Id',
  `event_name` varchar(64) NOT NULL COMMENT 'Event Name',
  `customer_login` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Login',
  PRIMARY KEY (`event_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Reports Event Type Table';

/*Data for the table `report_event_types` */

insert  into `report_event_types`(`event_type_id`,`event_name`,`customer_login`) values (1,'catalog_product_view',0),(2,'sendfriend_product',0),(3,'catalog_product_compare_add_product',0),(4,'checkout_cart_add_product',0),(5,'wishlist_add_product',0),(6,'wishlist_share',0);

/*Table structure for table `report_viewed_product_aggregated_daily` */

DROP TABLE IF EXISTS `report_viewed_product_aggregated_daily`;

CREATE TABLE `report_viewed_product_aggregated_daily` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `views_num` int(11) NOT NULL DEFAULT '0' COMMENT 'Number of Views',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `REPORT_VIEWED_PRD_AGGRED_DAILY_PERIOD_STORE_ID_PRD_ID` (`period`,`store_id`,`product_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_DAILY_STORE_ID` (`store_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_DAILY_PRODUCT_ID` (`product_id`),
  CONSTRAINT `REPORT_VIEWED_PRD_AGGRED_DAILY_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `REPORT_VIEWED_PRODUCT_AGGREGATED_DAILY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Most Viewed Products Aggregated Daily';

/*Data for the table `report_viewed_product_aggregated_daily` */

/*Table structure for table `report_viewed_product_aggregated_monthly` */

DROP TABLE IF EXISTS `report_viewed_product_aggregated_monthly`;

CREATE TABLE `report_viewed_product_aggregated_monthly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `views_num` int(11) NOT NULL DEFAULT '0' COMMENT 'Number of Views',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `REPORT_VIEWED_PRD_AGGRED_MONTHLY_PERIOD_STORE_ID_PRD_ID` (`period`,`store_id`,`product_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_MONTHLY_STORE_ID` (`store_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_MONTHLY_PRODUCT_ID` (`product_id`),
  CONSTRAINT `REPORT_VIEWED_PRD_AGGRED_MONTHLY_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `REPORT_VIEWED_PRODUCT_AGGREGATED_MONTHLY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Most Viewed Products Aggregated Monthly';

/*Data for the table `report_viewed_product_aggregated_monthly` */

/*Table structure for table `report_viewed_product_aggregated_yearly` */

DROP TABLE IF EXISTS `report_viewed_product_aggregated_yearly`;

CREATE TABLE `report_viewed_product_aggregated_yearly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `views_num` int(11) NOT NULL DEFAULT '0' COMMENT 'Number of Views',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `REPORT_VIEWED_PRD_AGGRED_YEARLY_PERIOD_STORE_ID_PRD_ID` (`period`,`store_id`,`product_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_YEARLY_STORE_ID` (`store_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_YEARLY_PRODUCT_ID` (`product_id`),
  CONSTRAINT `REPORT_VIEWED_PRD_AGGRED_YEARLY_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `REPORT_VIEWED_PRODUCT_AGGREGATED_YEARLY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Most Viewed Products Aggregated Yearly';

/*Data for the table `report_viewed_product_aggregated_yearly` */

/*Table structure for table `report_viewed_product_index` */

DROP TABLE IF EXISTS `report_viewed_product_index`;

CREATE TABLE `report_viewed_product_index` (
  `index_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Index Id',
  `visitor_id` int(10) unsigned DEFAULT NULL COMMENT 'Visitor Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `added_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Added At',
  PRIMARY KEY (`index_id`),
  UNIQUE KEY `REPORT_VIEWED_PRODUCT_INDEX_VISITOR_ID_PRODUCT_ID` (`visitor_id`,`product_id`),
  UNIQUE KEY `REPORT_VIEWED_PRODUCT_INDEX_CUSTOMER_ID_PRODUCT_ID` (`customer_id`,`product_id`),
  KEY `REPORT_VIEWED_PRODUCT_INDEX_STORE_ID` (`store_id`),
  KEY `REPORT_VIEWED_PRODUCT_INDEX_ADDED_AT` (`added_at`),
  KEY `REPORT_VIEWED_PRODUCT_INDEX_PRODUCT_ID` (`product_id`),
  CONSTRAINT `REPORT_VIEWED_PRD_IDX_CSTR_ID_CSTR_ENTT_ENTT_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `REPORT_VIEWED_PRD_IDX_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `REPORT_VIEWED_PRODUCT_INDEX_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Reports Viewed Product Index Table';

/*Data for the table `report_viewed_product_index` */

insert  into `report_viewed_product_index`(`index_id`,`visitor_id`,`customer_id`,`product_id`,`store_id`,`added_at`) values (1,4,NULL,15,1,'2016-08-26 23:05:30'),(2,4,NULL,9,1,'2016-08-26 23:07:05');

/*Table structure for table `reporting_counts` */

DROP TABLE IF EXISTS `reporting_counts`;

CREATE TABLE `reporting_counts` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `type` varchar(255) DEFAULT NULL COMMENT 'Item Reported',
  `count` int(10) unsigned DEFAULT NULL COMMENT 'Count Value',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reporting for all count related events generated via the cron job';

/*Data for the table `reporting_counts` */

/*Table structure for table `reporting_module_status` */

DROP TABLE IF EXISTS `reporting_module_status`;

CREATE TABLE `reporting_module_status` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Module Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Module Name',
  `active` varchar(255) DEFAULT NULL COMMENT 'Module Active Status',
  `setup_version` varchar(255) DEFAULT NULL COMMENT 'Module Version',
  `state` varchar(255) DEFAULT NULL COMMENT 'Module State',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Module Status Table';

/*Data for the table `reporting_module_status` */

/*Table structure for table `reporting_orders` */

DROP TABLE IF EXISTS `reporting_orders`;

CREATE TABLE `reporting_orders` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `total` decimal(20,2) DEFAULT NULL COMMENT 'Total From Store',
  `total_base` decimal(20,2) DEFAULT NULL COMMENT 'Total From Base Currency',
  `item_count` int(10) unsigned NOT NULL COMMENT 'Line Item Count',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reporting for all orders';

/*Data for the table `reporting_orders` */

/*Table structure for table `reporting_system_updates` */

DROP TABLE IF EXISTS `reporting_system_updates`;

CREATE TABLE `reporting_system_updates` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `type` varchar(255) DEFAULT NULL COMMENT 'Update Type',
  `action` varchar(255) DEFAULT NULL COMMENT 'Action Performed',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reporting for system updates';

/*Data for the table `reporting_system_updates` */

/*Table structure for table `reporting_users` */

DROP TABLE IF EXISTS `reporting_users`;

CREATE TABLE `reporting_users` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `type` varchar(255) DEFAULT NULL COMMENT 'User Type',
  `action` varchar(255) DEFAULT NULL COMMENT 'Action Performed',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reporting for user actions';

/*Data for the table `reporting_users` */

/*Table structure for table `review` */

DROP TABLE IF EXISTS `review`;

CREATE TABLE `review` (
  `review_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Review id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Review create date',
  `entity_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity id',
  `entity_pk_value` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `status_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Status code',
  PRIMARY KEY (`review_id`),
  KEY `REVIEW_ENTITY_ID` (`entity_id`),
  KEY `REVIEW_STATUS_ID` (`status_id`),
  KEY `REVIEW_ENTITY_PK_VALUE` (`entity_pk_value`),
  CONSTRAINT `REVIEW_ENTITY_ID_REVIEW_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `review_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `REVIEW_STATUS_ID_REVIEW_STATUS_STATUS_ID` FOREIGN KEY (`status_id`) REFERENCES `review_status` (`status_id`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review base information';

/*Data for the table `review` */

/*Table structure for table `review_detail` */

DROP TABLE IF EXISTS `review_detail`;

CREATE TABLE `review_detail` (
  `detail_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Review detail id',
  `review_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Review id',
  `store_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Store id',
  `title` varchar(255) NOT NULL COMMENT 'Title',
  `detail` text NOT NULL COMMENT 'Detail description',
  `nickname` varchar(128) NOT NULL COMMENT 'User nickname',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  PRIMARY KEY (`detail_id`),
  KEY `REVIEW_DETAIL_REVIEW_ID` (`review_id`),
  KEY `REVIEW_DETAIL_STORE_ID` (`store_id`),
  KEY `REVIEW_DETAIL_CUSTOMER_ID` (`customer_id`),
  CONSTRAINT `REVIEW_DETAIL_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL,
  CONSTRAINT `REVIEW_DETAIL_REVIEW_ID_REVIEW_REVIEW_ID` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE CASCADE,
  CONSTRAINT `REVIEW_DETAIL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review detail information';

/*Data for the table `review_detail` */

/*Table structure for table `review_entity` */

DROP TABLE IF EXISTS `review_entity`;

CREATE TABLE `review_entity` (
  `entity_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Review entity id',
  `entity_code` varchar(32) NOT NULL COMMENT 'Review entity code',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Review entities';

/*Data for the table `review_entity` */

insert  into `review_entity`(`entity_id`,`entity_code`) values (1,'product'),(2,'customer'),(3,'category');

/*Table structure for table `review_entity_summary` */

DROP TABLE IF EXISTS `review_entity_summary`;

CREATE TABLE `review_entity_summary` (
  `primary_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Summary review entity id',
  `entity_pk_value` bigint(20) NOT NULL DEFAULT '0' COMMENT 'Product id',
  `entity_type` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Entity type id',
  `reviews_count` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Qty of reviews',
  `rating_summary` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Summarized rating',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store id',
  PRIMARY KEY (`primary_id`),
  KEY `REVIEW_ENTITY_SUMMARY_STORE_ID` (`store_id`),
  CONSTRAINT `REVIEW_ENTITY_SUMMARY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review aggregates';

/*Data for the table `review_entity_summary` */

/*Table structure for table `review_status` */

DROP TABLE IF EXISTS `review_status`;

CREATE TABLE `review_status` (
  `status_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Status id',
  `status_code` varchar(32) NOT NULL COMMENT 'Status code',
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Review statuses';

/*Data for the table `review_status` */

insert  into `review_status`(`status_id`,`status_code`) values (1,'Approved'),(2,'Pending'),(3,'Not Approved');

/*Table structure for table `review_store` */

DROP TABLE IF EXISTS `review_store`;

CREATE TABLE `review_store` (
  `review_id` bigint(20) unsigned NOT NULL COMMENT 'Review Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  PRIMARY KEY (`review_id`,`store_id`),
  KEY `REVIEW_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `REVIEW_STORE_REVIEW_ID_REVIEW_REVIEW_ID` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE CASCADE,
  CONSTRAINT `REVIEW_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review Store';

/*Data for the table `review_store` */

/*Table structure for table `sales_bestsellers_aggregated_daily` */

DROP TABLE IF EXISTS `sales_bestsellers_aggregated_daily`;

CREATE TABLE `sales_bestsellers_aggregated_daily` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_BESTSELLERS_AGGREGATED_DAILY_PERIOD_STORE_ID_PRODUCT_ID` (`period`,`store_id`,`product_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_DAILY_STORE_ID` (`store_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_DAILY_PRODUCT_ID` (`product_id`),
  CONSTRAINT `SALES_BESTSELLERS_AGGREGATED_DAILY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Bestsellers Aggregated Daily';

/*Data for the table `sales_bestsellers_aggregated_daily` */

/*Table structure for table `sales_bestsellers_aggregated_monthly` */

DROP TABLE IF EXISTS `sales_bestsellers_aggregated_monthly`;

CREATE TABLE `sales_bestsellers_aggregated_monthly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_BESTSELLERS_AGGREGATED_MONTHLY_PERIOD_STORE_ID_PRODUCT_ID` (`period`,`store_id`,`product_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_MONTHLY_STORE_ID` (`store_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_MONTHLY_PRODUCT_ID` (`product_id`),
  CONSTRAINT `SALES_BESTSELLERS_AGGREGATED_MONTHLY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Bestsellers Aggregated Monthly';

/*Data for the table `sales_bestsellers_aggregated_monthly` */

/*Table structure for table `sales_bestsellers_aggregated_yearly` */

DROP TABLE IF EXISTS `sales_bestsellers_aggregated_yearly`;

CREATE TABLE `sales_bestsellers_aggregated_yearly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_BESTSELLERS_AGGREGATED_YEARLY_PERIOD_STORE_ID_PRODUCT_ID` (`period`,`store_id`,`product_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_YEARLY_STORE_ID` (`store_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_YEARLY_PRODUCT_ID` (`product_id`),
  CONSTRAINT `SALES_BESTSELLERS_AGGREGATED_YEARLY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Bestsellers Aggregated Yearly';

/*Data for the table `sales_bestsellers_aggregated_yearly` */

/*Table structure for table `sales_creditmemo` */

DROP TABLE IF EXISTS `sales_creditmemo`;

CREATE TABLE `sales_creditmemo` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Positive',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `base_adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Negative',
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Incl Tax',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Negative',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `base_adjustment` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `adjustment` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `base_adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Positive',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `send_email` smallint(5) unsigned DEFAULT NULL COMMENT 'Send Email',
  `creditmemo_status` int(11) DEFAULT NULL COMMENT 'Creditmemo Status',
  `state` int(11) DEFAULT NULL COMMENT 'State',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `invoice_id` int(11) DEFAULT NULL COMMENT 'Invoice Id',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `transaction_id` varchar(255) DEFAULT NULL COMMENT 'Transaction Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
  `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `customer_note` text COMMENT 'Customer Note',
  `customer_note_notify` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Note Notify',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_CREDITMEMO_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_CREDITMEMO_STORE_ID` (`store_id`),
  KEY `SALES_CREDITMEMO_ORDER_ID` (`order_id`),
  KEY `SALES_CREDITMEMO_CREDITMEMO_STATUS` (`creditmemo_status`),
  KEY `SALES_CREDITMEMO_STATE` (`state`),
  KEY `SALES_CREDITMEMO_CREATED_AT` (`created_at`),
  KEY `SALES_CREDITMEMO_UPDATED_AT` (`updated_at`),
  KEY `SALES_CREDITMEMO_SEND_EMAIL` (`send_email`),
  KEY `SALES_CREDITMEMO_EMAIL_SENT` (`email_sent`),
  CONSTRAINT `SALES_CREDITMEMO_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `SALES_CREDITMEMO_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo';

/*Data for the table `sales_creditmemo` */

/*Table structure for table `sales_creditmemo_comment` */

DROP TABLE IF EXISTS `sales_creditmemo_comment`;

CREATE TABLE `sales_creditmemo_comment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` int(11) DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_CREDITMEMO_COMMENT_CREATED_AT` (`created_at`),
  KEY `SALES_CREDITMEMO_COMMENT_PARENT_ID` (`parent_id`),
  CONSTRAINT `SALES_CREDITMEMO_COMMENT_PARENT_ID_SALES_CREDITMEMO_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_creditmemo` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo Comment';

/*Data for the table `sales_creditmemo_comment` */

/*Table structure for table `sales_creditmemo_grid` */

DROP TABLE IF EXISTS `sales_creditmemo_grid`;

CREATE TABLE `sales_creditmemo_grid` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `order_increment_id` varchar(50) DEFAULT NULL COMMENT 'Order Increment Id',
  `order_created_at` timestamp NULL DEFAULT NULL COMMENT 'Order Created At',
  `billing_name` varchar(255) DEFAULT NULL COMMENT 'Billing Name',
  `state` int(11) DEFAULT NULL COMMENT 'Status',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `order_status` varchar(32) DEFAULT NULL COMMENT 'Order Status',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `customer_name` varchar(128) NOT NULL COMMENT 'Customer Name',
  `customer_email` varchar(128) DEFAULT NULL COMMENT 'Customer Email',
  `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
  `payment_method` varchar(32) DEFAULT NULL COMMENT 'Payment Method',
  `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `shipping_and_handling` decimal(12,4) DEFAULT NULL COMMENT 'Shipping and handling amount',
  `adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Positive',
  `adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Negative',
  `order_base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Order Grand Total',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_CREDITMEMO_GRID_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_CREDITMEMO_GRID_ORDER_INCREMENT_ID` (`order_increment_id`),
  KEY `SALES_CREDITMEMO_GRID_CREATED_AT` (`created_at`),
  KEY `SALES_CREDITMEMO_GRID_UPDATED_AT` (`updated_at`),
  KEY `SALES_CREDITMEMO_GRID_ORDER_CREATED_AT` (`order_created_at`),
  KEY `SALES_CREDITMEMO_GRID_STATE` (`state`),
  KEY `SALES_CREDITMEMO_GRID_BILLING_NAME` (`billing_name`),
  KEY `SALES_CREDITMEMO_GRID_ORDER_STATUS` (`order_status`),
  KEY `SALES_CREDITMEMO_GRID_BASE_GRAND_TOTAL` (`base_grand_total`),
  KEY `SALES_CREDITMEMO_GRID_STORE_ID` (`store_id`),
  KEY `SALES_CREDITMEMO_GRID_ORDER_BASE_GRAND_TOTAL` (`order_base_grand_total`),
  KEY `SALES_CREDITMEMO_GRID_ORDER_ID` (`order_id`),
  FULLTEXT KEY `FTI_32B7BA885941A8254EE84AE650ABDC86` (`increment_id`,`order_increment_id`,`billing_name`,`billing_address`,`shipping_address`,`customer_name`,`customer_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo Grid';

/*Data for the table `sales_creditmemo_grid` */

/*Table structure for table `sales_creditmemo_item` */

DROP TABLE IF EXISTS `sales_creditmemo_item`;

CREATE TABLE `sales_creditmemo_item` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `base_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Price',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `base_row_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `row_total` decimal(12,4) DEFAULT NULL COMMENT 'Row Total',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `product_id` int(11) DEFAULT NULL COMMENT 'Product Id',
  `order_item_id` int(11) DEFAULT NULL COMMENT 'Order Item Id',
  `additional_data` text COMMENT 'Additional Data',
  `description` text COMMENT 'Description',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `tax_ratio` text COMMENT 'Ratio of tax in the creditmemo item over tax of the order item',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_CREDITMEMO_ITEM_PARENT_ID` (`parent_id`),
  CONSTRAINT `SALES_CREDITMEMO_ITEM_PARENT_ID_SALES_CREDITMEMO_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_creditmemo` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo Item';

/*Data for the table `sales_creditmemo_item` */

/*Table structure for table `sales_invoice` */

DROP TABLE IF EXISTS `sales_invoice`;

CREATE TABLE `sales_invoice` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Incl Tax',
  `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `is_used_for_refund` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Used For Refund',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `send_email` smallint(5) unsigned DEFAULT NULL COMMENT 'Send Email',
  `can_void_flag` smallint(5) unsigned DEFAULT NULL COMMENT 'Can Void Flag',
  `state` int(11) DEFAULT NULL COMMENT 'State',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `transaction_id` varchar(255) DEFAULT NULL COMMENT 'Transaction Id',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
  `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `base_total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Refunded',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `customer_note` text COMMENT 'Customer Note',
  `customer_note_notify` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Note Notify',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_INVOICE_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_INVOICE_STORE_ID` (`store_id`),
  KEY `SALES_INVOICE_GRAND_TOTAL` (`grand_total`),
  KEY `SALES_INVOICE_ORDER_ID` (`order_id`),
  KEY `SALES_INVOICE_STATE` (`state`),
  KEY `SALES_INVOICE_CREATED_AT` (`created_at`),
  KEY `SALES_INVOICE_UPDATED_AT` (`updated_at`),
  KEY `SALES_INVOICE_SEND_EMAIL` (`send_email`),
  KEY `SALES_INVOICE_EMAIL_SENT` (`email_sent`),
  CONSTRAINT `SALES_INVOICE_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `SALES_INVOICE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice';

/*Data for the table `sales_invoice` */

/*Table structure for table `sales_invoice_comment` */

DROP TABLE IF EXISTS `sales_invoice_comment`;

CREATE TABLE `sales_invoice_comment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_INVOICE_COMMENT_CREATED_AT` (`created_at`),
  KEY `SALES_INVOICE_COMMENT_PARENT_ID` (`parent_id`),
  CONSTRAINT `SALES_INVOICE_COMMENT_PARENT_ID_SALES_INVOICE_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_invoice` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice Comment';

/*Data for the table `sales_invoice_comment` */

/*Table structure for table `sales_invoice_grid` */

DROP TABLE IF EXISTS `sales_invoice_grid`;

CREATE TABLE `sales_invoice_grid` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `state` int(11) DEFAULT NULL COMMENT 'State',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `store_name` varchar(255) DEFAULT NULL COMMENT 'Store Name',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `order_increment_id` varchar(50) DEFAULT NULL COMMENT 'Order Increment Id',
  `order_created_at` timestamp NULL DEFAULT NULL COMMENT 'Order Created At',
  `customer_name` varchar(255) DEFAULT NULL COMMENT 'Customer Name',
  `customer_email` varchar(255) DEFAULT NULL COMMENT 'Customer Email',
  `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
  `payment_method` varchar(128) DEFAULT NULL COMMENT 'Payment Method',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `billing_name` varchar(255) DEFAULT NULL COMMENT 'Billing Name',
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `shipping_and_handling` decimal(12,4) DEFAULT NULL COMMENT 'Shipping and handling amount',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_INVOICE_GRID_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_INVOICE_GRID_STORE_ID` (`store_id`),
  KEY `SALES_INVOICE_GRID_GRAND_TOTAL` (`grand_total`),
  KEY `SALES_INVOICE_GRID_BASE_GRAND_TOTAL` (`base_grand_total`),
  KEY `SALES_INVOICE_GRID_ORDER_ID` (`order_id`),
  KEY `SALES_INVOICE_GRID_STATE` (`state`),
  KEY `SALES_INVOICE_GRID_ORDER_INCREMENT_ID` (`order_increment_id`),
  KEY `SALES_INVOICE_GRID_CREATED_AT` (`created_at`),
  KEY `SALES_INVOICE_GRID_UPDATED_AT` (`updated_at`),
  KEY `SALES_INVOICE_GRID_ORDER_CREATED_AT` (`order_created_at`),
  KEY `SALES_INVOICE_GRID_BILLING_NAME` (`billing_name`),
  FULLTEXT KEY `FTI_95D9C924DD6A8734EB8B5D01D60F90DE` (`increment_id`,`order_increment_id`,`billing_name`,`billing_address`,`shipping_address`,`customer_name`,`customer_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice Grid';

/*Data for the table `sales_invoice_grid` */

/*Table structure for table `sales_invoice_item` */

DROP TABLE IF EXISTS `sales_invoice_item`;

CREATE TABLE `sales_invoice_item` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `base_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Price',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `base_row_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `row_total` decimal(12,4) DEFAULT NULL COMMENT 'Row Total',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `product_id` int(11) DEFAULT NULL COMMENT 'Product Id',
  `order_item_id` int(11) DEFAULT NULL COMMENT 'Order Item Id',
  `additional_data` text COMMENT 'Additional Data',
  `description` text COMMENT 'Description',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `tax_ratio` text COMMENT 'Ratio of tax invoiced over tax of the order item',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_INVOICE_ITEM_PARENT_ID` (`parent_id`),
  CONSTRAINT `SALES_INVOICE_ITEM_PARENT_ID_SALES_INVOICE_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_invoice` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice Item';

/*Data for the table `sales_invoice_item` */

/*Table structure for table `sales_invoiced_aggregated` */

DROP TABLE IF EXISTS `sales_invoiced_aggregated`;

CREATE TABLE `sales_invoiced_aggregated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `orders_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Orders Invoiced',
  `invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced',
  `invoiced_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Captured',
  `invoiced_not_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Not Captured',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_INVOICED_AGGREGATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_INVOICED_AGGREGATED_STORE_ID` (`store_id`),
  CONSTRAINT `SALES_INVOICED_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Invoiced Aggregated';

/*Data for the table `sales_invoiced_aggregated` */

/*Table structure for table `sales_invoiced_aggregated_order` */

DROP TABLE IF EXISTS `sales_invoiced_aggregated_order`;

CREATE TABLE `sales_invoiced_aggregated_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `orders_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Orders Invoiced',
  `invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced',
  `invoiced_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Captured',
  `invoiced_not_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Not Captured',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_INVOICED_AGGREGATED_ORDER_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_INVOICED_AGGREGATED_ORDER_STORE_ID` (`store_id`),
  CONSTRAINT `SALES_INVOICED_AGGREGATED_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Invoiced Aggregated Order';

/*Data for the table `sales_invoiced_aggregated_order` */

/*Table structure for table `sales_order` */

DROP TABLE IF EXISTS `sales_order`;

CREATE TABLE `sales_order` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `state` varchar(32) DEFAULT NULL COMMENT 'State',
  `status` varchar(32) DEFAULT NULL COMMENT 'Status',
  `coupon_code` varchar(255) DEFAULT NULL COMMENT 'Coupon Code',
  `protect_code` varchar(255) DEFAULT NULL COMMENT 'Protect Code',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `is_virtual` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Virtual',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `base_discount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Canceled',
  `base_discount_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Invoiced',
  `base_discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Refunded',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `base_shipping_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Canceled',
  `base_shipping_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Invoiced',
  `base_shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Refunded',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `base_shipping_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Refunded',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `base_subtotal_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Canceled',
  `base_subtotal_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Invoiced',
  `base_subtotal_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Refunded',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `base_tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Canceled',
  `base_tax_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Invoiced',
  `base_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Refunded',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
  `base_total_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Canceled',
  `base_total_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Invoiced',
  `base_total_invoiced_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Invoiced Cost',
  `base_total_offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Offline Refunded',
  `base_total_online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Online Refunded',
  `base_total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Paid',
  `base_total_qty_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Qty Ordered',
  `base_total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Refunded',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `discount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Discount Canceled',
  `discount_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Discount Invoiced',
  `discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Refunded',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `shipping_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Canceled',
  `shipping_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Invoiced',
  `shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Refunded',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `shipping_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Refunded',
  `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
  `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `subtotal_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Canceled',
  `subtotal_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Invoiced',
  `subtotal_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Refunded',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Tax Canceled',
  `tax_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Tax Invoiced',
  `tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Tax Refunded',
  `total_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Total Canceled',
  `total_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Total Invoiced',
  `total_offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Offline Refunded',
  `total_online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Online Refunded',
  `total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Total Paid',
  `total_qty_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty Ordered',
  `total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Refunded',
  `can_ship_partially` smallint(5) unsigned DEFAULT NULL COMMENT 'Can Ship Partially',
  `can_ship_partially_item` smallint(5) unsigned DEFAULT NULL COMMENT 'Can Ship Partially Item',
  `customer_is_guest` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Is Guest',
  `customer_note_notify` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Note Notify',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
  `edit_increment` int(11) DEFAULT NULL COMMENT 'Edit Increment',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `send_email` smallint(5) unsigned DEFAULT NULL COMMENT 'Send Email',
  `forced_shipment_with_invoice` smallint(5) unsigned DEFAULT NULL COMMENT 'Forced Do Shipment With Invoice',
  `payment_auth_expiration` int(11) DEFAULT NULL COMMENT 'Payment Authorization Expiration',
  `quote_address_id` int(11) DEFAULT NULL COMMENT 'Quote Address Id',
  `quote_id` int(11) DEFAULT NULL COMMENT 'Quote Id',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Negative',
  `adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Positive',
  `base_adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Negative',
  `base_adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Positive',
  `base_shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Amount',
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Incl Tax',
  `base_total_due` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Due',
  `payment_authorization_amount` decimal(12,4) DEFAULT NULL COMMENT 'Payment Authorization Amount',
  `shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `total_due` decimal(12,4) DEFAULT NULL COMMENT 'Total Due',
  `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
  `customer_dob` datetime DEFAULT NULL COMMENT 'Customer Dob',
  `increment_id` varchar(32) DEFAULT NULL COMMENT 'Increment Id',
  `applied_rule_ids` varchar(128) DEFAULT NULL COMMENT 'Applied Rule Ids',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `customer_email` varchar(128) DEFAULT NULL COMMENT 'Customer Email',
  `customer_firstname` varchar(128) DEFAULT NULL COMMENT 'Customer Firstname',
  `customer_lastname` varchar(128) DEFAULT NULL COMMENT 'Customer Lastname',
  `customer_middlename` varchar(128) DEFAULT NULL COMMENT 'Customer Middlename',
  `customer_prefix` varchar(32) DEFAULT NULL COMMENT 'Customer Prefix',
  `customer_suffix` varchar(32) DEFAULT NULL COMMENT 'Customer Suffix',
  `customer_taxvat` varchar(32) DEFAULT NULL COMMENT 'Customer Taxvat',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `ext_customer_id` varchar(32) DEFAULT NULL COMMENT 'Ext Customer Id',
  `ext_order_id` varchar(32) DEFAULT NULL COMMENT 'Ext Order Id',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `hold_before_state` varchar(32) DEFAULT NULL COMMENT 'Hold Before State',
  `hold_before_status` varchar(32) DEFAULT NULL COMMENT 'Hold Before Status',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `original_increment_id` varchar(32) DEFAULT NULL COMMENT 'Original Increment Id',
  `relation_child_id` varchar(32) DEFAULT NULL COMMENT 'Relation Child Id',
  `relation_child_real_id` varchar(32) DEFAULT NULL COMMENT 'Relation Child Real Id',
  `relation_parent_id` varchar(32) DEFAULT NULL COMMENT 'Relation Parent Id',
  `relation_parent_real_id` varchar(32) DEFAULT NULL COMMENT 'Relation Parent Real Id',
  `remote_ip` varchar(32) DEFAULT NULL COMMENT 'Remote Ip',
  `shipping_method` varchar(32) DEFAULT NULL COMMENT 'Shipping Method',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `store_name` varchar(32) DEFAULT NULL COMMENT 'Store Name',
  `x_forwarded_for` varchar(32) DEFAULT NULL COMMENT 'X Forwarded For',
  `customer_note` text COMMENT 'Customer Note',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `total_item_count` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Total Item Count',
  `customer_gender` int(11) DEFAULT NULL COMMENT 'Customer Gender',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
  `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
  `discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Invoiced',
  `base_discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Invoiced',
  `discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Refunded',
  `base_discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Refunded',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `coupon_rule_name` varchar(255) DEFAULT NULL COMMENT 'Coupon Sales Rule Name',
  `paypal_ipn_customer_notified` int(11) DEFAULT '0' COMMENT 'Paypal Ipn Customer Notified',
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_ORDER_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_ORDER_STATUS` (`status`),
  KEY `SALES_ORDER_STATE` (`state`),
  KEY `SALES_ORDER_STORE_ID` (`store_id`),
  KEY `SALES_ORDER_CREATED_AT` (`created_at`),
  KEY `SALES_ORDER_CUSTOMER_ID` (`customer_id`),
  KEY `SALES_ORDER_EXT_ORDER_ID` (`ext_order_id`),
  KEY `SALES_ORDER_QUOTE_ID` (`quote_id`),
  KEY `SALES_ORDER_UPDATED_AT` (`updated_at`),
  KEY `SALES_ORDER_SEND_EMAIL` (`send_email`),
  KEY `SALES_ORDER_EMAIL_SENT` (`email_sent`),
  CONSTRAINT `SALES_ORDER_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL,
  CONSTRAINT `SALES_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order';

/*Data for the table `sales_order` */

/*Table structure for table `sales_order_address` */

DROP TABLE IF EXISTS `sales_order_address`;

CREATE TABLE `sales_order_address` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Id',
  `customer_address_id` int(11) DEFAULT NULL COMMENT 'Customer Address Id',
  `quote_address_id` int(11) DEFAULT NULL COMMENT 'Quote Address Id',
  `region_id` int(11) DEFAULT NULL COMMENT 'Region Id',
  `customer_id` int(11) DEFAULT NULL COMMENT 'Customer Id',
  `fax` varchar(255) DEFAULT NULL COMMENT 'Fax',
  `region` varchar(255) DEFAULT NULL COMMENT 'Region',
  `postcode` varchar(255) DEFAULT NULL COMMENT 'Postcode',
  `lastname` varchar(255) DEFAULT NULL COMMENT 'Lastname',
  `street` varchar(255) DEFAULT NULL COMMENT 'Street',
  `city` varchar(255) DEFAULT NULL COMMENT 'City',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `telephone` varchar(255) DEFAULT NULL COMMENT 'Phone Number',
  `country_id` varchar(2) DEFAULT NULL COMMENT 'Country Id',
  `firstname` varchar(255) DEFAULT NULL COMMENT 'Firstname',
  `address_type` varchar(255) DEFAULT NULL COMMENT 'Address Type',
  `prefix` varchar(255) DEFAULT NULL COMMENT 'Prefix',
  `middlename` varchar(255) DEFAULT NULL COMMENT 'Middlename',
  `suffix` varchar(255) DEFAULT NULL COMMENT 'Suffix',
  `company` varchar(255) DEFAULT NULL COMMENT 'Company',
  `vat_id` text COMMENT 'Vat Id',
  `vat_is_valid` smallint(6) DEFAULT NULL COMMENT 'Vat Is Valid',
  `vat_request_id` text COMMENT 'Vat Request Id',
  `vat_request_date` text COMMENT 'Vat Request Date',
  `vat_request_success` smallint(6) DEFAULT NULL COMMENT 'Vat Request Success',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_ORDER_ADDRESS_PARENT_ID` (`parent_id`),
  CONSTRAINT `SALES_ORDER_ADDRESS_PARENT_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Address';

/*Data for the table `sales_order_address` */

/*Table structure for table `sales_order_aggregated_created` */

DROP TABLE IF EXISTS `sales_order_aggregated_created`;

CREATE TABLE `sales_order_aggregated_created` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Ordered',
  `total_qty_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Invoiced',
  `total_income_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Income Amount',
  `total_revenue_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Revenue Amount',
  `total_profit_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Profit Amount',
  `total_invoiced_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Invoiced Amount',
  `total_canceled_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Canceled Amount',
  `total_paid_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Paid Amount',
  `total_refunded_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Refunded Amount',
  `total_tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount',
  `total_tax_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount Actual',
  `total_shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount',
  `total_shipping_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount Actual',
  `total_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount',
  `total_discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount Actual',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_ORDER_AGGREGATED_CREATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_ORDER_AGGREGATED_CREATED_STORE_ID` (`store_id`),
  CONSTRAINT `SALES_ORDER_AGGREGATED_CREATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Aggregated Created';

/*Data for the table `sales_order_aggregated_created` */

/*Table structure for table `sales_order_aggregated_updated` */

DROP TABLE IF EXISTS `sales_order_aggregated_updated`;

CREATE TABLE `sales_order_aggregated_updated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Ordered',
  `total_qty_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Invoiced',
  `total_income_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Income Amount',
  `total_revenue_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Revenue Amount',
  `total_profit_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Profit Amount',
  `total_invoiced_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Invoiced Amount',
  `total_canceled_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Canceled Amount',
  `total_paid_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Paid Amount',
  `total_refunded_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Refunded Amount',
  `total_tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount',
  `total_tax_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount Actual',
  `total_shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount',
  `total_shipping_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount Actual',
  `total_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount',
  `total_discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount Actual',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_ORDER_AGGREGATED_UPDATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_ORDER_AGGREGATED_UPDATED_STORE_ID` (`store_id`),
  CONSTRAINT `SALES_ORDER_AGGREGATED_UPDATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Aggregated Updated';

/*Data for the table `sales_order_aggregated_updated` */

/*Table structure for table `sales_order_grid` */

DROP TABLE IF EXISTS `sales_order_grid`;

CREATE TABLE `sales_order_grid` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `status` varchar(32) DEFAULT NULL COMMENT 'Status',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `store_name` varchar(255) DEFAULT NULL COMMENT 'Store Name',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `base_total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Paid',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Total Paid',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `order_currency_code` varchar(255) DEFAULT NULL COMMENT 'Order Currency Code',
  `shipping_name` varchar(255) DEFAULT NULL COMMENT 'Shipping Name',
  `billing_name` varchar(255) DEFAULT NULL COMMENT 'Billing Name',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
  `customer_email` varchar(255) DEFAULT NULL COMMENT 'Customer Email',
  `customer_group` varchar(255) DEFAULT NULL COMMENT 'Customer Group',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `shipping_and_handling` decimal(12,4) DEFAULT NULL COMMENT 'Shipping and handling amount',
  `customer_name` varchar(255) DEFAULT NULL COMMENT 'Customer Name',
  `payment_method` varchar(255) DEFAULT NULL COMMENT 'Payment Method',
  `total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Refunded',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_ORDER_GRID_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_ORDER_GRID_STATUS` (`status`),
  KEY `SALES_ORDER_GRID_STORE_ID` (`store_id`),
  KEY `SALES_ORDER_GRID_BASE_GRAND_TOTAL` (`base_grand_total`),
  KEY `SALES_ORDER_GRID_BASE_TOTAL_PAID` (`base_total_paid`),
  KEY `SALES_ORDER_GRID_GRAND_TOTAL` (`grand_total`),
  KEY `SALES_ORDER_GRID_TOTAL_PAID` (`total_paid`),
  KEY `SALES_ORDER_GRID_SHIPPING_NAME` (`shipping_name`),
  KEY `SALES_ORDER_GRID_BILLING_NAME` (`billing_name`),
  KEY `SALES_ORDER_GRID_CREATED_AT` (`created_at`),
  KEY `SALES_ORDER_GRID_CUSTOMER_ID` (`customer_id`),
  KEY `SALES_ORDER_GRID_UPDATED_AT` (`updated_at`),
  FULLTEXT KEY `FTI_65B9E9925EC58F0C7C2E2F6379C233E7` (`increment_id`,`billing_name`,`shipping_name`,`shipping_address`,`billing_address`,`customer_name`,`customer_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Grid';

/*Data for the table `sales_order_grid` */

/*Table structure for table `sales_order_item` */

DROP TABLE IF EXISTS `sales_order_item`;

CREATE TABLE `sales_order_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item Id',
  `order_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Order Id',
  `parent_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Item Id',
  `quote_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Quote Item Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_type` varchar(255) DEFAULT NULL COMMENT 'Product Type',
  `product_options` text COMMENT 'Product Options',
  `weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Weight',
  `is_virtual` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Virtual',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `applied_rule_ids` text COMMENT 'Applied Rule Ids',
  `additional_data` text COMMENT 'Additional Data',
  `is_qty_decimal` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Qty Decimal',
  `no_discount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'No Discount',
  `qty_backordered` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Backordered',
  `qty_canceled` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Canceled',
  `qty_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Invoiced',
  `qty_ordered` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `qty_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Refunded',
  `qty_shipped` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Shipped',
  `base_cost` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Cost',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `base_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Price',
  `original_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Price',
  `base_original_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Original Price',
  `tax_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Percent',
  `tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `tax_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Invoiced',
  `base_tax_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Invoiced',
  `discount_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Percent',
  `discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Amount',
  `base_discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `discount_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Invoiced',
  `base_discount_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Invoiced',
  `amount_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Amount Refunded',
  `base_amount_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Amount Refunded',
  `row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Total',
  `base_row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Total',
  `row_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Invoiced',
  `base_row_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Invoiced',
  `row_weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Weight',
  `base_tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Before Discount',
  `tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Before Discount',
  `ext_order_item_id` varchar(255) DEFAULT NULL COMMENT 'Ext Order Item Id',
  `locked_do_invoice` smallint(5) unsigned DEFAULT NULL COMMENT 'Locked Do Invoice',
  `locked_do_ship` smallint(5) unsigned DEFAULT NULL COMMENT 'Locked Do Ship',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Invoiced',
  `base_discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Invoiced',
  `discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Refunded',
  `base_discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Refunded',
  `tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Tax Canceled',
  `discount_tax_compensation_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Canceled',
  `tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Tax Refunded',
  `base_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Refunded',
  `discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Refunded',
  `base_discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Refunded',
  `free_shipping` smallint(6) DEFAULT NULL,
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  `gift_message_available` int(11) DEFAULT NULL COMMENT 'Gift Message Available',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  PRIMARY KEY (`item_id`),
  KEY `SALES_ORDER_ITEM_ORDER_ID` (`order_id`),
  KEY `SALES_ORDER_ITEM_STORE_ID` (`store_id`),
  CONSTRAINT `SALES_ORDER_ITEM_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `SALES_ORDER_ITEM_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Item';

/*Data for the table `sales_order_item` */

/*Table structure for table `sales_order_payment` */

DROP TABLE IF EXISTS `sales_order_payment`;

CREATE TABLE `sales_order_payment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `base_shipping_captured` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Captured',
  `shipping_captured` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Captured',
  `amount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Amount Refunded',
  `base_amount_paid` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Paid',
  `amount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Amount Canceled',
  `base_amount_authorized` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Authorized',
  `base_amount_paid_online` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Paid Online',
  `base_amount_refunded_online` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Refunded Online',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `amount_paid` decimal(12,4) DEFAULT NULL COMMENT 'Amount Paid',
  `amount_authorized` decimal(12,4) DEFAULT NULL COMMENT 'Amount Authorized',
  `base_amount_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Ordered',
  `base_shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Refunded',
  `shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Refunded',
  `base_amount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Refunded',
  `amount_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Amount Ordered',
  `base_amount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Canceled',
  `quote_payment_id` int(11) DEFAULT NULL COMMENT 'Quote Payment Id',
  `additional_data` text COMMENT 'Additional Data',
  `cc_exp_month` varchar(12) DEFAULT NULL COMMENT 'Cc Exp Month',
  `cc_ss_start_year` varchar(12) DEFAULT NULL COMMENT 'Cc Ss Start Year',
  `echeck_bank_name` varchar(128) DEFAULT NULL COMMENT 'Echeck Bank Name',
  `method` varchar(128) DEFAULT NULL COMMENT 'Method',
  `cc_debug_request_body` varchar(32) DEFAULT NULL COMMENT 'Cc Debug Request Body',
  `cc_secure_verify` varchar(32) DEFAULT NULL COMMENT 'Cc Secure Verify',
  `protection_eligibility` varchar(32) DEFAULT NULL COMMENT 'Protection Eligibility',
  `cc_approval` varchar(32) DEFAULT NULL COMMENT 'Cc Approval',
  `cc_last_4` varchar(100) DEFAULT NULL COMMENT 'Cc Last 4',
  `cc_status_description` varchar(32) DEFAULT NULL COMMENT 'Cc Status Description',
  `echeck_type` varchar(32) DEFAULT NULL COMMENT 'Echeck Type',
  `cc_debug_response_serialized` varchar(32) DEFAULT NULL COMMENT 'Cc Debug Response Serialized',
  `cc_ss_start_month` varchar(128) DEFAULT NULL COMMENT 'Cc Ss Start Month',
  `echeck_account_type` varchar(255) DEFAULT NULL COMMENT 'Echeck Account Type',
  `last_trans_id` varchar(32) DEFAULT NULL COMMENT 'Last Trans Id',
  `cc_cid_status` varchar(32) DEFAULT NULL COMMENT 'Cc Cid Status',
  `cc_owner` varchar(128) DEFAULT NULL COMMENT 'Cc Owner',
  `cc_type` varchar(32) DEFAULT NULL COMMENT 'Cc Type',
  `po_number` varchar(32) DEFAULT NULL COMMENT 'Po Number',
  `cc_exp_year` varchar(4) DEFAULT NULL COMMENT 'Cc Exp Year',
  `cc_status` varchar(4) DEFAULT NULL COMMENT 'Cc Status',
  `echeck_routing_number` varchar(32) DEFAULT NULL COMMENT 'Echeck Routing Number',
  `account_status` varchar(32) DEFAULT NULL COMMENT 'Account Status',
  `anet_trans_method` varchar(32) DEFAULT NULL COMMENT 'Anet Trans Method',
  `cc_debug_response_body` varchar(32) DEFAULT NULL COMMENT 'Cc Debug Response Body',
  `cc_ss_issue` varchar(32) DEFAULT NULL COMMENT 'Cc Ss Issue',
  `echeck_account_name` varchar(32) DEFAULT NULL COMMENT 'Echeck Account Name',
  `cc_avs_status` varchar(32) DEFAULT NULL COMMENT 'Cc Avs Status',
  `cc_number_enc` varchar(32) DEFAULT NULL COMMENT 'Cc Number Enc',
  `cc_trans_id` varchar(32) DEFAULT NULL COMMENT 'Cc Trans Id',
  `address_status` varchar(32) DEFAULT NULL COMMENT 'Address Status',
  `additional_information` text COMMENT 'Additional Information',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_ORDER_PAYMENT_PARENT_ID` (`parent_id`),
  CONSTRAINT `SALES_ORDER_PAYMENT_PARENT_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Payment';

/*Data for the table `sales_order_payment` */

/*Table structure for table `sales_order_status` */

DROP TABLE IF EXISTS `sales_order_status`;

CREATE TABLE `sales_order_status` (
  `status` varchar(32) NOT NULL COMMENT 'Status',
  `label` varchar(128) NOT NULL COMMENT 'Label',
  PRIMARY KEY (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Status Table';

/*Data for the table `sales_order_status` */

insert  into `sales_order_status`(`status`,`label`) values ('canceled','Canceled'),('closed','Closed'),('complete','Complete'),('fraud','Suspected Fraud'),('holded','On Hold'),('payment_review','Payment Review'),('paypal_canceled_reversal','PayPal Canceled Reversal'),('paypal_reversed','PayPal Reversed'),('pending','Pending'),('pending_payment','Pending Payment'),('pending_paypal','Pending PayPal'),('processing','Processing');

/*Table structure for table `sales_order_status_history` */

DROP TABLE IF EXISTS `sales_order_status_history`;

CREATE TABLE `sales_order_status_history` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` int(11) DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `status` varchar(32) DEFAULT NULL COMMENT 'Status',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `entity_name` varchar(32) DEFAULT NULL COMMENT 'Shows what entity history is bind to.',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_ORDER_STATUS_HISTORY_PARENT_ID` (`parent_id`),
  KEY `SALES_ORDER_STATUS_HISTORY_CREATED_AT` (`created_at`),
  CONSTRAINT `SALES_ORDER_STATUS_HISTORY_PARENT_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Status History';

/*Data for the table `sales_order_status_history` */

/*Table structure for table `sales_order_status_label` */

DROP TABLE IF EXISTS `sales_order_status_label`;

CREATE TABLE `sales_order_status_label` (
  `status` varchar(32) NOT NULL COMMENT 'Status',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `label` varchar(128) NOT NULL COMMENT 'Label',
  PRIMARY KEY (`status`,`store_id`),
  KEY `SALES_ORDER_STATUS_LABEL_STORE_ID` (`store_id`),
  CONSTRAINT `SALES_ORDER_STATUS_LABEL_STATUS_SALES_ORDER_STATUS_STATUS` FOREIGN KEY (`status`) REFERENCES `sales_order_status` (`status`) ON DELETE CASCADE,
  CONSTRAINT `SALES_ORDER_STATUS_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Status Label Table';

/*Data for the table `sales_order_status_label` */

/*Table structure for table `sales_order_status_state` */

DROP TABLE IF EXISTS `sales_order_status_state`;

CREATE TABLE `sales_order_status_state` (
  `status` varchar(32) NOT NULL COMMENT 'Status',
  `state` varchar(32) NOT NULL COMMENT 'Label',
  `is_default` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Default',
  `visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Visible on front',
  PRIMARY KEY (`status`,`state`),
  CONSTRAINT `SALES_ORDER_STATUS_STATE_STATUS_SALES_ORDER_STATUS_STATUS` FOREIGN KEY (`status`) REFERENCES `sales_order_status` (`status`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Status Table';

/*Data for the table `sales_order_status_state` */

insert  into `sales_order_status_state`(`status`,`state`,`is_default`,`visible_on_front`) values ('canceled','canceled',1,1),('closed','closed',1,1),('complete','complete',1,1),('fraud','payment_review',0,1),('fraud','processing',0,1),('holded','holded',1,1),('payment_review','payment_review',1,1),('pending','new',1,1),('pending_payment','pending_payment',1,0),('processing','processing',1,1);

/*Table structure for table `sales_order_tax` */

DROP TABLE IF EXISTS `sales_order_tax`;

CREATE TABLE `sales_order_tax` (
  `tax_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Tax Id',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `code` varchar(255) DEFAULT NULL COMMENT 'Code',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `percent` decimal(12,4) DEFAULT NULL COMMENT 'Percent',
  `amount` decimal(12,4) DEFAULT NULL COMMENT 'Amount',
  `priority` int(11) NOT NULL COMMENT 'Priority',
  `position` int(11) NOT NULL COMMENT 'Position',
  `base_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount',
  `process` smallint(6) NOT NULL COMMENT 'Process',
  `base_real_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Real Amount',
  PRIMARY KEY (`tax_id`),
  KEY `SALES_ORDER_TAX_ORDER_ID_PRIORITY_POSITION` (`order_id`,`priority`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Tax Table';

/*Data for the table `sales_order_tax` */

/*Table structure for table `sales_order_tax_item` */

DROP TABLE IF EXISTS `sales_order_tax_item`;

CREATE TABLE `sales_order_tax_item` (
  `tax_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Tax Item Id',
  `tax_id` int(10) unsigned NOT NULL COMMENT 'Tax Id',
  `item_id` int(10) unsigned DEFAULT NULL COMMENT 'Item Id',
  `tax_percent` decimal(12,4) NOT NULL COMMENT 'Real Tax Percent For Item',
  `amount` decimal(12,4) NOT NULL COMMENT 'Tax amount for the item and tax rate',
  `base_amount` decimal(12,4) NOT NULL COMMENT 'Base tax amount for the item and tax rate',
  `real_amount` decimal(12,4) NOT NULL COMMENT 'Real tax amount for the item and tax rate',
  `real_base_amount` decimal(12,4) NOT NULL COMMENT 'Real base tax amount for the item and tax rate',
  `associated_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Id of the associated item',
  `taxable_item_type` varchar(32) NOT NULL COMMENT 'Type of the taxable item',
  PRIMARY KEY (`tax_item_id`),
  UNIQUE KEY `SALES_ORDER_TAX_ITEM_TAX_ID_ITEM_ID` (`tax_id`,`item_id`),
  KEY `SALES_ORDER_TAX_ITEM_ITEM_ID` (`item_id`),
  KEY `SALES_ORDER_TAX_ITEM_ASSOCIATED_ITEM_ID_SALES_ORDER_ITEM_ITEM_ID` (`associated_item_id`),
  CONSTRAINT `SALES_ORDER_TAX_ITEM_ASSOCIATED_ITEM_ID_SALES_ORDER_ITEM_ITEM_ID` FOREIGN KEY (`associated_item_id`) REFERENCES `sales_order_item` (`item_id`) ON DELETE CASCADE,
  CONSTRAINT `SALES_ORDER_TAX_ITEM_ITEM_ID_SALES_ORDER_ITEM_ITEM_ID` FOREIGN KEY (`item_id`) REFERENCES `sales_order_item` (`item_id`) ON DELETE CASCADE,
  CONSTRAINT `SALES_ORDER_TAX_ITEM_TAX_ID_SALES_ORDER_TAX_TAX_ID` FOREIGN KEY (`tax_id`) REFERENCES `sales_order_tax` (`tax_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Tax Item';

/*Data for the table `sales_order_tax_item` */

/*Table structure for table `sales_payment_transaction` */

DROP TABLE IF EXISTS `sales_payment_transaction`;

CREATE TABLE `sales_payment_transaction` (
  `transaction_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Transaction Id',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Id',
  `order_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Order Id',
  `payment_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Payment Id',
  `txn_id` varchar(100) DEFAULT NULL COMMENT 'Txn Id',
  `parent_txn_id` varchar(100) DEFAULT NULL COMMENT 'Parent Txn Id',
  `txn_type` varchar(15) DEFAULT NULL COMMENT 'Txn Type',
  `is_closed` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Closed',
  `additional_information` blob COMMENT 'Additional Information',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  PRIMARY KEY (`transaction_id`),
  UNIQUE KEY `SALES_PAYMENT_TRANSACTION_ORDER_ID_PAYMENT_ID_TXN_ID` (`order_id`,`payment_id`,`txn_id`),
  KEY `SALES_PAYMENT_TRANSACTION_PARENT_ID` (`parent_id`),
  KEY `SALES_PAYMENT_TRANSACTION_PAYMENT_ID` (`payment_id`),
  CONSTRAINT `FK_B99FF1A06402D725EBDB0F3A7ECD47A2` FOREIGN KEY (`parent_id`) REFERENCES `sales_payment_transaction` (`transaction_id`) ON DELETE CASCADE,
  CONSTRAINT `SALES_PAYMENT_TRANSACTION_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `SALES_PAYMENT_TRANSACTION_PAYMENT_ID_SALES_ORDER_PAYMENT_ENTT_ID` FOREIGN KEY (`payment_id`) REFERENCES `sales_order_payment` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Payment Transaction';

/*Data for the table `sales_payment_transaction` */

/*Table structure for table `sales_refunded_aggregated` */

DROP TABLE IF EXISTS `sales_refunded_aggregated`;

CREATE TABLE `sales_refunded_aggregated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `refunded` decimal(12,4) DEFAULT NULL COMMENT 'Refunded',
  `online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Online Refunded',
  `offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Offline Refunded',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_REFUNDED_AGGREGATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_REFUNDED_AGGREGATED_STORE_ID` (`store_id`),
  CONSTRAINT `SALES_REFUNDED_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Refunded Aggregated';

/*Data for the table `sales_refunded_aggregated` */

/*Table structure for table `sales_refunded_aggregated_order` */

DROP TABLE IF EXISTS `sales_refunded_aggregated_order`;

CREATE TABLE `sales_refunded_aggregated_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `refunded` decimal(12,4) DEFAULT NULL COMMENT 'Refunded',
  `online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Online Refunded',
  `offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Offline Refunded',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_REFUNDED_AGGREGATED_ORDER_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_REFUNDED_AGGREGATED_ORDER_STORE_ID` (`store_id`),
  CONSTRAINT `SALES_REFUNDED_AGGREGATED_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Refunded Aggregated Order';

/*Data for the table `sales_refunded_aggregated_order` */

/*Table structure for table `sales_sequence_meta` */

DROP TABLE IF EXISTS `sales_sequence_meta`;

CREATE TABLE `sales_sequence_meta` (
  `meta_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `entity_type` varchar(32) NOT NULL COMMENT 'Prefix',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `sequence_table` varchar(32) NOT NULL COMMENT 'table for sequence',
  PRIMARY KEY (`meta_id`),
  UNIQUE KEY `SALES_SEQUENCE_META_ENTITY_TYPE_STORE_ID` (`entity_type`,`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='sales_sequence_meta';

/*Data for the table `sales_sequence_meta` */

insert  into `sales_sequence_meta`(`meta_id`,`entity_type`,`store_id`,`sequence_table`) values (1,'order',0,'sequence_order_0'),(2,'invoice',0,'sequence_invoice_0'),(3,'creditmemo',0,'sequence_creditmemo_0'),(4,'shipment',0,'sequence_shipment_0'),(5,'order',1,'sequence_order_1'),(6,'invoice',1,'sequence_invoice_1'),(7,'creditmemo',1,'sequence_creditmemo_1'),(8,'shipment',1,'sequence_shipment_1'),(9,'order',2,'sequence_order_2'),(10,'invoice',2,'sequence_invoice_2'),(11,'creditmemo',2,'sequence_creditmemo_2'),(12,'shipment',2,'sequence_shipment_2');

/*Table structure for table `sales_sequence_profile` */

DROP TABLE IF EXISTS `sales_sequence_profile`;

CREATE TABLE `sales_sequence_profile` (
  `profile_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `meta_id` int(10) unsigned NOT NULL COMMENT 'Meta_id',
  `prefix` varchar(32) DEFAULT NULL COMMENT 'Prefix',
  `suffix` varchar(32) DEFAULT NULL COMMENT 'Suffix',
  `start_value` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Start value for sequence',
  `step` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Step for sequence',
  `max_value` int(10) unsigned NOT NULL COMMENT 'MaxValue for sequence',
  `warning_value` int(10) unsigned NOT NULL COMMENT 'WarningValue for sequence',
  `is_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'isActive flag',
  PRIMARY KEY (`profile_id`),
  UNIQUE KEY `SALES_SEQUENCE_PROFILE_META_ID_PREFIX_SUFFIX` (`meta_id`,`prefix`,`suffix`),
  CONSTRAINT `SALES_SEQUENCE_PROFILE_META_ID_SALES_SEQUENCE_META_META_ID` FOREIGN KEY (`meta_id`) REFERENCES `sales_sequence_meta` (`meta_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='sales_sequence_profile';

/*Data for the table `sales_sequence_profile` */

insert  into `sales_sequence_profile`(`profile_id`,`meta_id`,`prefix`,`suffix`,`start_value`,`step`,`max_value`,`warning_value`,`is_active`) values (1,1,NULL,NULL,1,1,0,0,1),(2,2,NULL,NULL,1,1,0,0,1),(3,3,NULL,NULL,1,1,0,0,1),(4,4,NULL,NULL,1,1,0,0,1),(5,5,NULL,NULL,1,1,0,0,1),(6,6,NULL,NULL,1,1,0,0,1),(7,7,NULL,NULL,1,1,0,0,1),(8,8,NULL,NULL,1,1,0,0,1),(9,9,'2',NULL,1,1,0,0,1),(10,10,'2',NULL,1,1,0,0,1),(11,11,'2',NULL,1,1,0,0,1),(12,12,'2',NULL,1,1,0,0,1);

/*Table structure for table `sales_shipment` */

DROP TABLE IF EXISTS `sales_shipment`;

CREATE TABLE `sales_shipment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `total_weight` decimal(12,4) DEFAULT NULL COMMENT 'Total Weight',
  `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `send_email` smallint(5) unsigned DEFAULT NULL COMMENT 'Send Email',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `customer_id` int(11) DEFAULT NULL COMMENT 'Customer Id',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `shipment_status` int(11) DEFAULT NULL COMMENT 'Shipment Status',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `packages` text COMMENT 'Packed Products in Packages',
  `shipping_label` mediumblob COMMENT 'Shipping Label Content',
  `customer_note` text COMMENT 'Customer Note',
  `customer_note_notify` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Note Notify',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_SHIPMENT_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_SHIPMENT_STORE_ID` (`store_id`),
  KEY `SALES_SHIPMENT_TOTAL_QTY` (`total_qty`),
  KEY `SALES_SHIPMENT_ORDER_ID` (`order_id`),
  KEY `SALES_SHIPMENT_CREATED_AT` (`created_at`),
  KEY `SALES_SHIPMENT_UPDATED_AT` (`updated_at`),
  KEY `SALES_SHIPMENT_SEND_EMAIL` (`send_email`),
  KEY `SALES_SHIPMENT_EMAIL_SENT` (`email_sent`),
  CONSTRAINT `SALES_SHIPMENT_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `SALES_SHIPMENT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment';

/*Data for the table `sales_shipment` */

/*Table structure for table `sales_shipment_comment` */

DROP TABLE IF EXISTS `sales_shipment_comment`;

CREATE TABLE `sales_shipment_comment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` int(11) DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_SHIPMENT_COMMENT_CREATED_AT` (`created_at`),
  KEY `SALES_SHIPMENT_COMMENT_PARENT_ID` (`parent_id`),
  CONSTRAINT `SALES_SHIPMENT_COMMENT_PARENT_ID_SALES_SHIPMENT_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_shipment` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Comment';

/*Data for the table `sales_shipment_comment` */

/*Table structure for table `sales_shipment_grid` */

DROP TABLE IF EXISTS `sales_shipment_grid`;

CREATE TABLE `sales_shipment_grid` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_increment_id` varchar(32) NOT NULL COMMENT 'Order Increment Id',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `order_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Order Increment Id',
  `customer_name` varchar(128) NOT NULL COMMENT 'Customer Name',
  `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
  `shipment_status` int(11) DEFAULT NULL COMMENT 'Shipment Status',
  `order_status` varchar(32) DEFAULT NULL COMMENT 'Order',
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `billing_name` varchar(128) DEFAULT NULL COMMENT 'Billing Name',
  `shipping_name` varchar(128) DEFAULT NULL COMMENT 'Shipping Name',
  `customer_email` varchar(128) DEFAULT NULL COMMENT 'Customer Email',
  `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
  `payment_method` varchar(32) DEFAULT NULL COMMENT 'Payment Method',
  `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_SHIPMENT_GRID_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_SHIPMENT_GRID_STORE_ID` (`store_id`),
  KEY `SALES_SHIPMENT_GRID_TOTAL_QTY` (`total_qty`),
  KEY `SALES_SHIPMENT_GRID_ORDER_INCREMENT_ID` (`order_increment_id`),
  KEY `SALES_SHIPMENT_GRID_SHIPMENT_STATUS` (`shipment_status`),
  KEY `SALES_SHIPMENT_GRID_ORDER_STATUS` (`order_status`),
  KEY `SALES_SHIPMENT_GRID_CREATED_AT` (`created_at`),
  KEY `SALES_SHIPMENT_GRID_UPDATED_AT` (`updated_at`),
  KEY `SALES_SHIPMENT_GRID_ORDER_CREATED_AT` (`order_created_at`),
  KEY `SALES_SHIPMENT_GRID_SHIPPING_NAME` (`shipping_name`),
  KEY `SALES_SHIPMENT_GRID_BILLING_NAME` (`billing_name`),
  FULLTEXT KEY `FTI_086B40C8955F167B8EA76653437879B4` (`increment_id`,`order_increment_id`,`shipping_name`,`customer_name`,`customer_email`,`billing_address`,`shipping_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Grid';

/*Data for the table `sales_shipment_grid` */

/*Table structure for table `sales_shipment_item` */

DROP TABLE IF EXISTS `sales_shipment_item`;

CREATE TABLE `sales_shipment_item` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `row_total` decimal(12,4) DEFAULT NULL COMMENT 'Row Total',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `product_id` int(11) DEFAULT NULL COMMENT 'Product Id',
  `order_item_id` int(11) DEFAULT NULL COMMENT 'Order Item Id',
  `additional_data` text COMMENT 'Additional Data',
  `description` text COMMENT 'Description',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_SHIPMENT_ITEM_PARENT_ID` (`parent_id`),
  CONSTRAINT `SALES_SHIPMENT_ITEM_PARENT_ID_SALES_SHIPMENT_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_shipment` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Item';

/*Data for the table `sales_shipment_item` */

/*Table structure for table `sales_shipment_track` */

DROP TABLE IF EXISTS `sales_shipment_track`;

CREATE TABLE `sales_shipment_track` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `track_number` text COMMENT 'Number',
  `description` text COMMENT 'Description',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `carrier_code` varchar(32) DEFAULT NULL COMMENT 'Carrier Code',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_SHIPMENT_TRACK_PARENT_ID` (`parent_id`),
  KEY `SALES_SHIPMENT_TRACK_ORDER_ID` (`order_id`),
  KEY `SALES_SHIPMENT_TRACK_CREATED_AT` (`created_at`),
  CONSTRAINT `SALES_SHIPMENT_TRACK_PARENT_ID_SALES_SHIPMENT_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_shipment` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Track';

/*Data for the table `sales_shipment_track` */

/*Table structure for table `sales_shipping_aggregated` */

DROP TABLE IF EXISTS `sales_shipping_aggregated`;

CREATE TABLE `sales_shipping_aggregated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_shipping` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping',
  `total_shipping_actual` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping Actual',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_SHPP_AGGRED_PERIOD_STORE_ID_ORDER_STS_SHPP_DESCRIPTION` (`period`,`store_id`,`order_status`,`shipping_description`),
  KEY `SALES_SHIPPING_AGGREGATED_STORE_ID` (`store_id`),
  CONSTRAINT `SALES_SHIPPING_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Shipping Aggregated';

/*Data for the table `sales_shipping_aggregated` */

/*Table structure for table `sales_shipping_aggregated_order` */

DROP TABLE IF EXISTS `sales_shipping_aggregated_order`;

CREATE TABLE `sales_shipping_aggregated_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_shipping` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping',
  `total_shipping_actual` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping Actual',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_C05FAE47282EEA68654D0924E946761F` (`period`,`store_id`,`order_status`,`shipping_description`),
  KEY `SALES_SHIPPING_AGGREGATED_ORDER_STORE_ID` (`store_id`),
  CONSTRAINT `SALES_SHIPPING_AGGREGATED_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Shipping Aggregated Order';

/*Data for the table `sales_shipping_aggregated_order` */

/*Table structure for table `salesrule` */

DROP TABLE IF EXISTS `salesrule`;

CREATE TABLE `salesrule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `from_date` date DEFAULT NULL COMMENT 'From',
  `to_date` date DEFAULT NULL COMMENT 'To',
  `uses_per_customer` int(11) NOT NULL DEFAULT '0' COMMENT 'Uses Per Customer',
  `is_active` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Active',
  `conditions_serialized` mediumtext COMMENT 'Conditions Serialized',
  `actions_serialized` mediumtext COMMENT 'Actions Serialized',
  `stop_rules_processing` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Stop Rules Processing',
  `is_advanced` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Advanced',
  `product_ids` text COMMENT 'Product Ids',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `simple_action` varchar(32) DEFAULT NULL COMMENT 'Simple Action',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `discount_qty` decimal(12,4) DEFAULT NULL COMMENT 'Discount Qty',
  `discount_step` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Discount Step',
  `apply_to_shipping` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Apply To Shipping',
  `times_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  `is_rss` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Rss',
  `coupon_type` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Coupon Type',
  `use_auto_generation` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Use Auto Generation',
  `uses_per_coupon` int(11) NOT NULL DEFAULT '0' COMMENT 'User Per Coupon',
  `simple_free_shipping` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`rule_id`),
  KEY `SALESRULE_IS_ACTIVE_SORT_ORDER_TO_DATE_FROM_DATE` (`is_active`,`sort_order`,`to_date`,`from_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule';

/*Data for the table `salesrule` */

/*Table structure for table `salesrule_coupon` */

DROP TABLE IF EXISTS `salesrule_coupon`;

CREATE TABLE `salesrule_coupon` (
  `coupon_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Coupon Id',
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `code` varchar(255) DEFAULT NULL COMMENT 'Code',
  `usage_limit` int(10) unsigned DEFAULT NULL COMMENT 'Usage Limit',
  `usage_per_customer` int(10) unsigned DEFAULT NULL COMMENT 'Usage Per Customer',
  `times_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  `expiration_date` timestamp NULL DEFAULT NULL COMMENT 'Expiration Date',
  `is_primary` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Primary',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Coupon Code Creation Date',
  `type` smallint(6) DEFAULT '0' COMMENT 'Coupon Code Type',
  PRIMARY KEY (`coupon_id`),
  UNIQUE KEY `SALESRULE_COUPON_CODE` (`code`),
  UNIQUE KEY `SALESRULE_COUPON_RULE_ID_IS_PRIMARY` (`rule_id`,`is_primary`),
  KEY `SALESRULE_COUPON_RULE_ID` (`rule_id`),
  CONSTRAINT `SALESRULE_COUPON_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Coupon';

/*Data for the table `salesrule_coupon` */

/*Table structure for table `salesrule_coupon_aggregated` */

DROP TABLE IF EXISTS `salesrule_coupon_aggregated`;

CREATE TABLE `salesrule_coupon_aggregated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date NOT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `coupon_code` varchar(50) DEFAULT NULL COMMENT 'Coupon Code',
  `coupon_uses` int(11) NOT NULL DEFAULT '0' COMMENT 'Coupon Uses',
  `subtotal_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `total_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount',
  `subtotal_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount Actual',
  `discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount Actual',
  `total_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount Actual',
  `rule_name` varchar(255) DEFAULT NULL COMMENT 'Rule Name',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALESRULE_COUPON_AGGRED_PERIOD_STORE_ID_ORDER_STS_COUPON_CODE` (`period`,`store_id`,`order_status`,`coupon_code`),
  KEY `SALESRULE_COUPON_AGGREGATED_STORE_ID` (`store_id`),
  KEY `SALESRULE_COUPON_AGGREGATED_RULE_NAME` (`rule_name`),
  CONSTRAINT `SALESRULE_COUPON_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Coupon Aggregated';

/*Data for the table `salesrule_coupon_aggregated` */

/*Table structure for table `salesrule_coupon_aggregated_order` */

DROP TABLE IF EXISTS `salesrule_coupon_aggregated_order`;

CREATE TABLE `salesrule_coupon_aggregated_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date NOT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `coupon_code` varchar(50) DEFAULT NULL COMMENT 'Coupon Code',
  `coupon_uses` int(11) NOT NULL DEFAULT '0' COMMENT 'Coupon Uses',
  `subtotal_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `total_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount',
  `rule_name` varchar(255) DEFAULT NULL COMMENT 'Rule Name',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_1094D1FBBCBB11704A29DEF3ACC37D2B` (`period`,`store_id`,`order_status`,`coupon_code`),
  KEY `SALESRULE_COUPON_AGGREGATED_ORDER_STORE_ID` (`store_id`),
  KEY `SALESRULE_COUPON_AGGREGATED_ORDER_RULE_NAME` (`rule_name`),
  CONSTRAINT `SALESRULE_COUPON_AGGREGATED_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Coupon Aggregated Order';

/*Data for the table `salesrule_coupon_aggregated_order` */

/*Table structure for table `salesrule_coupon_aggregated_updated` */

DROP TABLE IF EXISTS `salesrule_coupon_aggregated_updated`;

CREATE TABLE `salesrule_coupon_aggregated_updated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date NOT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `coupon_code` varchar(50) DEFAULT NULL COMMENT 'Coupon Code',
  `coupon_uses` int(11) NOT NULL DEFAULT '0' COMMENT 'Coupon Uses',
  `subtotal_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `total_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount',
  `subtotal_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount Actual',
  `discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount Actual',
  `total_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount Actual',
  `rule_name` varchar(255) DEFAULT NULL COMMENT 'Rule Name',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_7196FA120A4F0F84E1B66605E87E213E` (`period`,`store_id`,`order_status`,`coupon_code`),
  KEY `SALESRULE_COUPON_AGGREGATED_UPDATED_STORE_ID` (`store_id`),
  KEY `SALESRULE_COUPON_AGGREGATED_UPDATED_RULE_NAME` (`rule_name`),
  CONSTRAINT `SALESRULE_COUPON_AGGREGATED_UPDATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Coupon Aggregated Updated';

/*Data for the table `salesrule_coupon_aggregated_updated` */

/*Table structure for table `salesrule_coupon_usage` */

DROP TABLE IF EXISTS `salesrule_coupon_usage`;

CREATE TABLE `salesrule_coupon_usage` (
  `coupon_id` int(10) unsigned NOT NULL COMMENT 'Coupon Id',
  `customer_id` int(10) unsigned NOT NULL COMMENT 'Customer Id',
  `times_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  PRIMARY KEY (`coupon_id`,`customer_id`),
  KEY `SALESRULE_COUPON_USAGE_CUSTOMER_ID` (`customer_id`),
  CONSTRAINT `SALESRULE_COUPON_USAGE_COUPON_ID_SALESRULE_COUPON_COUPON_ID` FOREIGN KEY (`coupon_id`) REFERENCES `salesrule_coupon` (`coupon_id`) ON DELETE CASCADE,
  CONSTRAINT `SALESRULE_COUPON_USAGE_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Coupon Usage';

/*Data for the table `salesrule_coupon_usage` */

/*Table structure for table `salesrule_customer` */

DROP TABLE IF EXISTS `salesrule_customer`;

CREATE TABLE `salesrule_customer` (
  `rule_customer_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Customer Id',
  `rule_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Rule Id',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Id',
  `times_used` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  PRIMARY KEY (`rule_customer_id`),
  KEY `SALESRULE_CUSTOMER_RULE_ID_CUSTOMER_ID` (`rule_id`,`customer_id`),
  KEY `SALESRULE_CUSTOMER_CUSTOMER_ID_RULE_ID` (`customer_id`,`rule_id`),
  CONSTRAINT `SALESRULE_CUSTOMER_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `SALESRULE_CUSTOMER_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Customer';

/*Data for the table `salesrule_customer` */

/*Table structure for table `salesrule_customer_group` */

DROP TABLE IF EXISTS `salesrule_customer_group`;

CREATE TABLE `salesrule_customer_group` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  PRIMARY KEY (`rule_id`,`customer_group_id`),
  KEY `SALESRULE_CUSTOMER_GROUP_CUSTOMER_GROUP_ID` (`customer_group_id`),
  CONSTRAINT `SALESRULE_CSTR_GROUP_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  CONSTRAINT `SALESRULE_CUSTOMER_GROUP_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Rules To Customer Groups Relations';

/*Data for the table `salesrule_customer_group` */

/*Table structure for table `salesrule_label` */

DROP TABLE IF EXISTS `salesrule_label`;

CREATE TABLE `salesrule_label` (
  `label_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Label Id',
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `label` varchar(255) DEFAULT NULL COMMENT 'Label',
  PRIMARY KEY (`label_id`),
  UNIQUE KEY `SALESRULE_LABEL_RULE_ID_STORE_ID` (`rule_id`,`store_id`),
  KEY `SALESRULE_LABEL_STORE_ID` (`store_id`),
  CONSTRAINT `SALESRULE_LABEL_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE,
  CONSTRAINT `SALESRULE_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Label';

/*Data for the table `salesrule_label` */

/*Table structure for table `salesrule_product_attribute` */

DROP TABLE IF EXISTS `salesrule_product_attribute`;

CREATE TABLE `salesrule_product_attribute` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  PRIMARY KEY (`rule_id`,`website_id`,`customer_group_id`,`attribute_id`),
  KEY `SALESRULE_PRODUCT_ATTRIBUTE_WEBSITE_ID` (`website_id`),
  KEY `SALESRULE_PRODUCT_ATTRIBUTE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `SALESRULE_PRODUCT_ATTRIBUTE_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `SALESRULE_PRD_ATTR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `SALESRULE_PRD_ATTR_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  CONSTRAINT `SALESRULE_PRODUCT_ATTRIBUTE_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE,
  CONSTRAINT `SALESRULE_PRODUCT_ATTRIBUTE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Product Attribute';

/*Data for the table `salesrule_product_attribute` */

/*Table structure for table `salesrule_website` */

DROP TABLE IF EXISTS `salesrule_website`;

CREATE TABLE `salesrule_website` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  PRIMARY KEY (`rule_id`,`website_id`),
  KEY `SALESRULE_WEBSITE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `SALESRULE_WEBSITE_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE,
  CONSTRAINT `SALESRULE_WEBSITE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Rules To Websites Relations';

/*Data for the table `salesrule_website` */

/*Table structure for table `search_query` */

DROP TABLE IF EXISTS `search_query`;

CREATE TABLE `search_query` (
  `query_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Query ID',
  `query_text` varchar(255) DEFAULT NULL COMMENT 'Query text',
  `num_results` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Num results',
  `popularity` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Popularity',
  `redirect` varchar(255) DEFAULT NULL COMMENT 'Redirect',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `display_in_terms` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Display in terms',
  `is_active` smallint(6) DEFAULT '1' COMMENT 'Active status',
  `is_processed` smallint(6) DEFAULT '0' COMMENT 'Processed status',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated at',
  PRIMARY KEY (`query_id`),
  UNIQUE KEY `SEARCH_QUERY_QUERY_TEXT_STORE_ID` (`query_text`,`store_id`),
  KEY `SEARCH_QUERY_QUERY_TEXT_STORE_ID_POPULARITY` (`query_text`,`store_id`,`popularity`),
  KEY `SEARCH_QUERY_STORE_ID` (`store_id`),
  KEY `SEARCH_QUERY_IS_PROCESSED` (`is_processed`),
  CONSTRAINT `SEARCH_QUERY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Search query table';

/*Data for the table `search_query` */

/*Table structure for table `search_synonyms` */

DROP TABLE IF EXISTS `search_synonyms`;

CREATE TABLE `search_synonyms` (
  `group_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Synonyms Group Id',
  `synonyms` text NOT NULL COMMENT 'list of synonyms making up this group',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id - identifies the store view these synonyms belong to',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id - identifies the website id these synonyms belong to',
  PRIMARY KEY (`group_id`),
  KEY `SEARCH_SYNONYMS_STORE_ID` (`store_id`),
  KEY `SEARCH_SYNONYMS_WEBSITE_ID` (`website_id`),
  FULLTEXT KEY `SEARCH_SYNONYMS_SYNONYMS` (`synonyms`),
  CONSTRAINT `SEARCH_SYNONYMS_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `SEARCH_SYNONYMS_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table storing various synonyms groups';

/*Data for the table `search_synonyms` */

/*Table structure for table `sendfriend_log` */

DROP TABLE IF EXISTS `sendfriend_log`;

CREATE TABLE `sendfriend_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Log ID',
  `ip` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer IP address',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Log time',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website ID',
  PRIMARY KEY (`log_id`),
  KEY `SENDFRIEND_LOG_IP` (`ip`),
  KEY `SENDFRIEND_LOG_TIME` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Send to friend function log storage table';

/*Data for the table `sendfriend_log` */

/*Table structure for table `sequence_creditmemo_0` */

DROP TABLE IF EXISTS `sequence_creditmemo_0`;

CREATE TABLE `sequence_creditmemo_0` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sequence_creditmemo_0` */

/*Table structure for table `sequence_creditmemo_1` */

DROP TABLE IF EXISTS `sequence_creditmemo_1`;

CREATE TABLE `sequence_creditmemo_1` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sequence_creditmemo_1` */

/*Table structure for table `sequence_creditmemo_2` */

DROP TABLE IF EXISTS `sequence_creditmemo_2`;

CREATE TABLE `sequence_creditmemo_2` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sequence_creditmemo_2` */

/*Table structure for table `sequence_invoice_0` */

DROP TABLE IF EXISTS `sequence_invoice_0`;

CREATE TABLE `sequence_invoice_0` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sequence_invoice_0` */

/*Table structure for table `sequence_invoice_1` */

DROP TABLE IF EXISTS `sequence_invoice_1`;

CREATE TABLE `sequence_invoice_1` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sequence_invoice_1` */

/*Table structure for table `sequence_invoice_2` */

DROP TABLE IF EXISTS `sequence_invoice_2`;

CREATE TABLE `sequence_invoice_2` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sequence_invoice_2` */

/*Table structure for table `sequence_order_0` */

DROP TABLE IF EXISTS `sequence_order_0`;

CREATE TABLE `sequence_order_0` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sequence_order_0` */

/*Table structure for table `sequence_order_1` */

DROP TABLE IF EXISTS `sequence_order_1`;

CREATE TABLE `sequence_order_1` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sequence_order_1` */

/*Table structure for table `sequence_order_2` */

DROP TABLE IF EXISTS `sequence_order_2`;

CREATE TABLE `sequence_order_2` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sequence_order_2` */

/*Table structure for table `sequence_shipment_0` */

DROP TABLE IF EXISTS `sequence_shipment_0`;

CREATE TABLE `sequence_shipment_0` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sequence_shipment_0` */

/*Table structure for table `sequence_shipment_1` */

DROP TABLE IF EXISTS `sequence_shipment_1`;

CREATE TABLE `sequence_shipment_1` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sequence_shipment_1` */

/*Table structure for table `sequence_shipment_2` */

DROP TABLE IF EXISTS `sequence_shipment_2`;

CREATE TABLE `sequence_shipment_2` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sequence_shipment_2` */

/*Table structure for table `session` */

DROP TABLE IF EXISTS `session`;

CREATE TABLE `session` (
  `session_id` varchar(255) NOT NULL COMMENT 'Session Id',
  `session_expires` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Date of Session Expiration',
  `session_data` mediumblob NOT NULL COMMENT 'Session Data',
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Database Sessions Storage';

/*Data for the table `session` */

/*Table structure for table `setup_module` */

DROP TABLE IF EXISTS `setup_module`;

CREATE TABLE `setup_module` (
  `module` varchar(50) NOT NULL COMMENT 'Module',
  `schema_version` varchar(50) DEFAULT NULL COMMENT 'Schema Version',
  `data_version` varchar(50) DEFAULT NULL COMMENT 'Data Version',
  PRIMARY KEY (`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Module versions registry';

/*Data for the table `setup_module` */

insert  into `setup_module`(`module`,`schema_version`,`data_version`) values ('Magefan_Blog','2.0.2','2.0.2'),('Magento_AdminNotification','2.0.0','2.0.0'),('Magento_AdvancedPricingImportExport','2.0.0','2.0.0'),('Magento_Authorization','2.0.0','2.0.0'),('Magento_Authorizenet','2.0.0','2.0.0'),('Magento_Backend','2.0.0','2.0.0'),('Magento_Backup','2.0.0','2.0.0'),('Magento_Braintree','2.0.0','2.0.0'),('Magento_Bundle','2.0.2','2.0.2'),('Magento_BundleImportExport','2.0.0','2.0.0'),('Magento_CacheInvalidate','2.0.0','2.0.0'),('Magento_Captcha','2.0.0','2.0.0'),('Magento_Catalog','2.0.7','2.0.7'),('Magento_CatalogImportExport','2.0.0','2.0.0'),('Magento_CatalogInventory','2.0.0','2.0.0'),('Magento_CatalogRule','2.0.1','2.0.1'),('Magento_CatalogRuleConfigurable','2.0.0','2.0.0'),('Magento_CatalogSearch','2.0.0','2.0.0'),('Magento_CatalogUrlRewrite','2.0.0','2.0.0'),('Magento_CatalogWidget','2.0.0','2.0.0'),('Magento_Checkout','2.0.0','2.0.0'),('Magento_CheckoutAgreements','2.0.1','2.0.1'),('Magento_Cms','2.0.1','2.0.1'),('Magento_CmsUrlRewrite','2.0.0','2.0.0'),('Magento_Config','2.0.0','2.0.0'),('Magento_ConfigurableImportExport','2.0.0','2.0.0'),('Magento_ConfigurableProduct','2.0.0','2.0.0'),('Magento_Contact','2.0.0','2.0.0'),('Magento_Cookie','2.0.0','2.0.0'),('Magento_Cron','2.0.0','2.0.0'),('Magento_CurrencySymbol','2.0.0','2.0.0'),('Magento_Customer','2.0.7','2.0.7'),('Magento_CustomerImportExport','2.0.0','2.0.0'),('Magento_Deploy','2.0.0','2.0.0'),('Magento_Developer','2.0.0','2.0.0'),('Magento_Dhl','2.0.0','2.0.0'),('Magento_Directory','2.0.0','2.0.0'),('Magento_Downloadable','2.0.1','2.0.1'),('Magento_DownloadableImportExport','2.0.0','2.0.0'),('Magento_Eav','2.0.0','2.0.0'),('Magento_Email','2.0.0','2.0.0'),('Magento_EncryptionKey','2.0.0','2.0.0'),('Magento_Fedex','2.0.0','2.0.0'),('Magento_GiftMessage','2.0.1','2.0.1'),('Magento_GoogleAdwords','2.0.0','2.0.0'),('Magento_GoogleAnalytics','2.0.0','2.0.0'),('Magento_GoogleOptimizer','2.0.0','2.0.0'),('Magento_GroupedImportExport','2.0.0','2.0.0'),('Magento_GroupedProduct','2.0.1','2.0.1'),('Magento_ImportExport','2.0.1','2.0.1'),('Magento_Indexer','2.0.0','2.0.0'),('Magento_Integration','2.0.1','2.0.1'),('Magento_LayeredNavigation','2.0.0','2.0.0'),('Magento_Marketplace','1.0.0','1.0.0'),('Magento_MediaStorage','2.0.0','2.0.0'),('Magento_Msrp','2.0.0','2.0.0'),('Magento_Multishipping','2.0.0','2.0.0'),('Magento_NewRelicReporting','2.0.0','2.0.0'),('Magento_Newsletter','2.0.0','2.0.0'),('Magento_OfflinePayments','2.0.0','2.0.0'),('Magento_OfflineShipping','2.0.0','2.0.0'),('Magento_PageCache','2.0.0','2.0.0'),('Magento_Payment','2.0.0','2.0.0'),('Magento_Paypal','2.0.0','2.0.0'),('Magento_Persistent','2.0.0','2.0.0'),('Magento_ProductAlert','2.0.0','2.0.0'),('Magento_ProductVideo','2.0.0.2','2.0.0.2'),('Magento_Quote','2.0.2','2.0.2'),('Magento_Reports','2.0.0','2.0.0'),('Magento_RequireJs','2.0.0','2.0.0'),('Magento_Review','2.0.0','2.0.0'),('Magento_Rss','2.0.0','2.0.0'),('Magento_Rule','2.0.0','2.0.0'),('Magento_Sales','2.0.3','2.0.3'),('Magento_SalesRule','2.0.1','2.0.1'),('Magento_SalesSequence','2.0.0','2.0.0'),('Magento_SampleData','2.0.0','2.0.0'),('Magento_Search','2.0.4','2.0.4'),('Magento_Security','2.0.1','2.0.1'),('Magento_SendFriend','2.0.0','2.0.0'),('Magento_Shipping','2.0.0','2.0.0'),('Magento_Sitemap','2.0.0','2.0.0'),('Magento_Store','2.0.0','2.0.0'),('Magento_Swagger','2.0.0','2.0.0'),('Magento_Swatches','2.0.1','2.0.1'),('Magento_SwatchesLayeredNavigation','2.0.0','2.0.0'),('Magento_Tax','2.0.1','2.0.1'),('Magento_TaxImportExport','2.0.0','2.0.0'),('Magento_Theme','2.0.1','2.0.1'),('Magento_Translation','2.0.0','2.0.0'),('Magento_Ui','2.0.0','2.0.0'),('Magento_Ups','2.0.0','2.0.0'),('Magento_UrlRewrite','2.0.0','2.0.0'),('Magento_User','2.0.1','2.0.1'),('Magento_Usps','2.0.0','2.0.0'),('Magento_Variable','2.0.0','2.0.0'),('Magento_Vault','2.0.0','2.0.0'),('Magento_Version','2.0.0','2.0.0'),('Magento_Webapi','2.0.0','2.0.0'),('Magento_WebapiSecurity','2.0.0','2.0.0'),('Magento_Weee','2.0.0','2.0.0'),('Magento_Widget','2.0.0','2.0.0'),('Magento_Wishlist','2.0.0','2.0.0'),('Mageplaza_Core','1.0.0','1.0.0'),('Mageplaza_LayeredNavigation','1.0.0','1.0.0'),('Smartwave_Core','2.0.0','2.0.0'),('Smartwave_Filterproducts','2.0.0','2.0.0'),('Smartwave_Megamenu','2.1.0','2.1.0'),('Smartwave_Porto','2.4.0','2.4.0'),('Smartwave_Socialfeeds','2.0.0','2.0.0'),('WeltPixel_Backend','1.0.2','1.0.2'),('WeltPixel_Quickview','1.0.1','1.0.1');

/*Table structure for table `shipping_tablerate` */

DROP TABLE IF EXISTS `shipping_tablerate`;

CREATE TABLE `shipping_tablerate` (
  `pk` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `website_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `dest_country_id` varchar(4) NOT NULL DEFAULT '0' COMMENT 'Destination coutry ISO/2 or ISO/3 code',
  `dest_region_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Destination Region Id',
  `dest_zip` varchar(10) NOT NULL DEFAULT '*' COMMENT 'Destination Post Code (Zip)',
  `condition_name` varchar(20) NOT NULL COMMENT 'Rate Condition name',
  `condition_value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Rate condition value',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `cost` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Cost',
  PRIMARY KEY (`pk`),
  UNIQUE KEY `UNQ_D60821CDB2AFACEE1566CFC02D0D4CAA` (`website_id`,`dest_country_id`,`dest_region_id`,`dest_zip`,`condition_name`,`condition_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Shipping Tablerate';

/*Data for the table `shipping_tablerate` */

/*Table structure for table `sitemap` */

DROP TABLE IF EXISTS `sitemap`;

CREATE TABLE `sitemap` (
  `sitemap_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Sitemap Id',
  `sitemap_type` varchar(32) DEFAULT NULL COMMENT 'Sitemap Type',
  `sitemap_filename` varchar(32) DEFAULT NULL COMMENT 'Sitemap Filename',
  `sitemap_path` varchar(255) DEFAULT NULL COMMENT 'Sitemap Path',
  `sitemap_time` timestamp NULL DEFAULT NULL COMMENT 'Sitemap Time',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store id',
  PRIMARY KEY (`sitemap_id`),
  KEY `SITEMAP_STORE_ID` (`store_id`),
  CONSTRAINT `SITEMAP_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='XML Sitemap';

/*Data for the table `sitemap` */

/*Table structure for table `store` */

DROP TABLE IF EXISTS `store`;

CREATE TABLE `store` (
  `store_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Store Id',
  `code` varchar(32) DEFAULT NULL COMMENT 'Code',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Group Id',
  `name` varchar(255) NOT NULL COMMENT 'Store Name',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Sort Order',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Activity',
  PRIMARY KEY (`store_id`),
  UNIQUE KEY `STORE_CODE` (`code`),
  KEY `STORE_WEBSITE_ID` (`website_id`),
  KEY `STORE_IS_ACTIVE_SORT_ORDER` (`is_active`,`sort_order`),
  KEY `STORE_GROUP_ID` (`group_id`),
  CONSTRAINT `STORE_GROUP_ID_STORE_GROUP_GROUP_ID` FOREIGN KEY (`group_id`) REFERENCES `store_group` (`group_id`) ON DELETE CASCADE,
  CONSTRAINT `STORE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Stores';

/*Data for the table `store` */

insert  into `store`(`store_id`,`code`,`website_id`,`group_id`,`name`,`sort_order`,`is_active`) values (0,'admin',0,0,'Admin',0,1),(1,'english',1,1,'English',1,1),(2,'french',1,1,'French',2,1);

/*Table structure for table `store_group` */

DROP TABLE IF EXISTS `store_group`;

CREATE TABLE `store_group` (
  `group_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Group Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `name` varchar(255) NOT NULL COMMENT 'Store Group Name',
  `root_category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Root Category Id',
  `default_store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Default Store Id',
  PRIMARY KEY (`group_id`),
  KEY `STORE_GROUP_WEBSITE_ID` (`website_id`),
  KEY `STORE_GROUP_DEFAULT_STORE_ID` (`default_store_id`),
  CONSTRAINT `STORE_GROUP_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Store Groups';

/*Data for the table `store_group` */

insert  into `store_group`(`group_id`,`website_id`,`name`,`root_category_id`,`default_store_id`) values (0,0,'Default',0,0),(1,1,'Main Website Store',2,1);

/*Table structure for table `store_website` */

DROP TABLE IF EXISTS `store_website`;

CREATE TABLE `store_website` (
  `website_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Website Id',
  `code` varchar(32) DEFAULT NULL COMMENT 'Code',
  `name` varchar(64) DEFAULT NULL COMMENT 'Website Name',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `default_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Default Group Id',
  `is_default` smallint(5) unsigned DEFAULT '0' COMMENT 'Defines Is Website Default',
  PRIMARY KEY (`website_id`),
  UNIQUE KEY `STORE_WEBSITE_CODE` (`code`),
  KEY `STORE_WEBSITE_SORT_ORDER` (`sort_order`),
  KEY `STORE_WEBSITE_DEFAULT_GROUP_ID` (`default_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Websites';

/*Data for the table `store_website` */

insert  into `store_website`(`website_id`,`code`,`name`,`sort_order`,`default_group_id`,`is_default`) values (0,'admin','Admin',0,0,0),(1,'base','Main Website',0,1,1);

/*Table structure for table `tax_calculation` */

DROP TABLE IF EXISTS `tax_calculation`;

CREATE TABLE `tax_calculation` (
  `tax_calculation_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Id',
  `tax_calculation_rate_id` int(11) NOT NULL COMMENT 'Tax Calculation Rate Id',
  `tax_calculation_rule_id` int(11) NOT NULL COMMENT 'Tax Calculation Rule Id',
  `customer_tax_class_id` smallint(6) NOT NULL COMMENT 'Customer Tax Class Id',
  `product_tax_class_id` smallint(6) NOT NULL COMMENT 'Product Tax Class Id',
  PRIMARY KEY (`tax_calculation_id`),
  KEY `TAX_CALCULATION_TAX_CALCULATION_RULE_ID` (`tax_calculation_rule_id`),
  KEY `TAX_CALCULATION_CUSTOMER_TAX_CLASS_ID` (`customer_tax_class_id`),
  KEY `TAX_CALCULATION_PRODUCT_TAX_CLASS_ID` (`product_tax_class_id`),
  KEY `TAX_CALC_TAX_CALC_RATE_ID_CSTR_TAX_CLASS_ID_PRD_TAX_CLASS_ID` (`tax_calculation_rate_id`,`customer_tax_class_id`,`product_tax_class_id`),
  CONSTRAINT `TAX_CALCULATION_CUSTOMER_TAX_CLASS_ID_TAX_CLASS_CLASS_ID` FOREIGN KEY (`customer_tax_class_id`) REFERENCES `tax_class` (`class_id`) ON DELETE CASCADE,
  CONSTRAINT `TAX_CALCULATION_PRODUCT_TAX_CLASS_ID_TAX_CLASS_CLASS_ID` FOREIGN KEY (`product_tax_class_id`) REFERENCES `tax_class` (`class_id`) ON DELETE CASCADE,
  CONSTRAINT `TAX_CALC_TAX_CALC_RATE_ID_TAX_CALC_RATE_TAX_CALC_RATE_ID` FOREIGN KEY (`tax_calculation_rate_id`) REFERENCES `tax_calculation_rate` (`tax_calculation_rate_id`) ON DELETE CASCADE,
  CONSTRAINT `TAX_CALC_TAX_CALC_RULE_ID_TAX_CALC_RULE_TAX_CALC_RULE_ID` FOREIGN KEY (`tax_calculation_rule_id`) REFERENCES `tax_calculation_rule` (`tax_calculation_rule_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Calculation';

/*Data for the table `tax_calculation` */

/*Table structure for table `tax_calculation_rate` */

DROP TABLE IF EXISTS `tax_calculation_rate`;

CREATE TABLE `tax_calculation_rate` (
  `tax_calculation_rate_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Rate Id',
  `tax_country_id` varchar(2) NOT NULL COMMENT 'Tax Country Id',
  `tax_region_id` int(11) NOT NULL COMMENT 'Tax Region Id',
  `tax_postcode` varchar(21) DEFAULT NULL COMMENT 'Tax Postcode',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `rate` decimal(12,4) NOT NULL COMMENT 'Rate',
  `zip_is_range` smallint(6) DEFAULT NULL COMMENT 'Zip Is Range',
  `zip_from` int(10) unsigned DEFAULT NULL COMMENT 'Zip From',
  `zip_to` int(10) unsigned DEFAULT NULL COMMENT 'Zip To',
  PRIMARY KEY (`tax_calculation_rate_id`),
  KEY `TAX_CALCULATION_RATE_TAX_COUNTRY_ID_TAX_REGION_ID_TAX_POSTCODE` (`tax_country_id`,`tax_region_id`,`tax_postcode`),
  KEY `TAX_CALCULATION_RATE_CODE` (`code`),
  KEY `IDX_CA799F1E2CB843495F601E56C84A626D` (`tax_calculation_rate_id`,`tax_country_id`,`tax_region_id`,`zip_is_range`,`tax_postcode`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Tax Calculation Rate';

/*Data for the table `tax_calculation_rate` */

insert  into `tax_calculation_rate`(`tax_calculation_rate_id`,`tax_country_id`,`tax_region_id`,`tax_postcode`,`code`,`rate`,`zip_is_range`,`zip_from`,`zip_to`) values (1,'US',12,'*','US-CA-*-Rate 1','8.2500',NULL,NULL,NULL),(2,'US',43,'*','US-NY-*-Rate 1','8.3750',NULL,NULL,NULL);

/*Table structure for table `tax_calculation_rate_title` */

DROP TABLE IF EXISTS `tax_calculation_rate_title`;

CREATE TABLE `tax_calculation_rate_title` (
  `tax_calculation_rate_title_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Rate Title Id',
  `tax_calculation_rate_id` int(11) NOT NULL COMMENT 'Tax Calculation Rate Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `value` varchar(255) NOT NULL COMMENT 'Value',
  PRIMARY KEY (`tax_calculation_rate_title_id`),
  KEY `TAX_CALCULATION_RATE_TITLE_TAX_CALCULATION_RATE_ID_STORE_ID` (`tax_calculation_rate_id`,`store_id`),
  KEY `TAX_CALCULATION_RATE_TITLE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_37FB965F786AD5897BB3AE90470C42AB` FOREIGN KEY (`tax_calculation_rate_id`) REFERENCES `tax_calculation_rate` (`tax_calculation_rate_id`) ON DELETE CASCADE,
  CONSTRAINT `TAX_CALCULATION_RATE_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Calculation Rate Title';

/*Data for the table `tax_calculation_rate_title` */

/*Table structure for table `tax_calculation_rule` */

DROP TABLE IF EXISTS `tax_calculation_rule`;

CREATE TABLE `tax_calculation_rule` (
  `tax_calculation_rule_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Rule Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `priority` int(11) NOT NULL COMMENT 'Priority',
  `position` int(11) NOT NULL COMMENT 'Position',
  `calculate_subtotal` int(11) NOT NULL COMMENT 'Calculate off subtotal option',
  PRIMARY KEY (`tax_calculation_rule_id`),
  KEY `TAX_CALCULATION_RULE_PRIORITY_POSITION` (`priority`,`position`),
  KEY `TAX_CALCULATION_RULE_CODE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Calculation Rule';

/*Data for the table `tax_calculation_rule` */

/*Table structure for table `tax_class` */

DROP TABLE IF EXISTS `tax_class`;

CREATE TABLE `tax_class` (
  `class_id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Class Id',
  `class_name` varchar(255) NOT NULL COMMENT 'Class Name',
  `class_type` varchar(8) NOT NULL DEFAULT 'CUSTOMER' COMMENT 'Class Type',
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Tax Class';

/*Data for the table `tax_class` */

insert  into `tax_class`(`class_id`,`class_name`,`class_type`) values (2,'Taxable Goods','PRODUCT'),(3,'Retail Customer','CUSTOMER');

/*Table structure for table `tax_order_aggregated_created` */

DROP TABLE IF EXISTS `tax_order_aggregated_created`;

CREATE TABLE `tax_order_aggregated_created` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `percent` float DEFAULT NULL COMMENT 'Percent',
  `orders_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `tax_base_amount_sum` float DEFAULT NULL COMMENT 'Tax Base Amount Sum',
  PRIMARY KEY (`id`),
  UNIQUE KEY `TAX_ORDER_AGGRED_CREATED_PERIOD_STORE_ID_CODE_PERCENT_ORDER_STS` (`period`,`store_id`,`code`,`percent`,`order_status`),
  KEY `TAX_ORDER_AGGREGATED_CREATED_STORE_ID` (`store_id`),
  CONSTRAINT `TAX_ORDER_AGGREGATED_CREATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Order Aggregation';

/*Data for the table `tax_order_aggregated_created` */

/*Table structure for table `tax_order_aggregated_updated` */

DROP TABLE IF EXISTS `tax_order_aggregated_updated`;

CREATE TABLE `tax_order_aggregated_updated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `percent` float DEFAULT NULL COMMENT 'Percent',
  `orders_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `tax_base_amount_sum` float DEFAULT NULL COMMENT 'Tax Base Amount Sum',
  PRIMARY KEY (`id`),
  UNIQUE KEY `TAX_ORDER_AGGRED_UPDATED_PERIOD_STORE_ID_CODE_PERCENT_ORDER_STS` (`period`,`store_id`,`code`,`percent`,`order_status`),
  KEY `TAX_ORDER_AGGREGATED_UPDATED_STORE_ID` (`store_id`),
  CONSTRAINT `TAX_ORDER_AGGREGATED_UPDATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Order Aggregated Updated';

/*Data for the table `tax_order_aggregated_updated` */

/*Table structure for table `theme` */

DROP TABLE IF EXISTS `theme`;

CREATE TABLE `theme` (
  `theme_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Theme identifier',
  `parent_id` int(11) DEFAULT NULL COMMENT 'Parent Id',
  `theme_path` varchar(255) DEFAULT NULL COMMENT 'Theme Path',
  `theme_title` varchar(255) NOT NULL COMMENT 'Theme Title',
  `preview_image` varchar(255) DEFAULT NULL COMMENT 'Preview Image',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Is Theme Featured',
  `area` varchar(255) NOT NULL COMMENT 'Theme Area',
  `type` smallint(6) NOT NULL COMMENT 'Theme type: 0:physical, 1:virtual, 2:staging',
  `code` text COMMENT 'Full theme code, including package',
  PRIMARY KEY (`theme_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Core theme';

/*Data for the table `theme` */

insert  into `theme`(`theme_id`,`parent_id`,`theme_path`,`theme_title`,`preview_image`,`is_featured`,`area`,`type`,`code`) values (1,NULL,'Magento/blank','Magento Blank','preview_image_57bfad569bb8a.jpeg',0,'frontend',0,'Magento/blank'),(2,1,'Magento/luma','Magento Luma','preview_image_57bfad56e6304.jpeg',0,'frontend',0,'Magento/luma'),(3,NULL,'Magento/backend','Magento 2 backend',NULL,0,'adminhtml',0,'Magento/backend'),(4,1,'Smartwave/porto','Smartwave Porto','preview_image_57bfae9d14a1a.png',0,'frontend',0,'Smartwave/porto'),(5,4,'Smartwave/porto_rtl','Smartwave Porto RTL','preview_image_57bfae9d75511.png',0,'frontend',0,'Smartwave/porto_rtl');

/*Table structure for table `theme_file` */

DROP TABLE IF EXISTS `theme_file`;

CREATE TABLE `theme_file` (
  `theme_files_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Theme files identifier',
  `theme_id` int(10) unsigned NOT NULL COMMENT 'Theme Id',
  `file_path` varchar(255) DEFAULT NULL COMMENT 'Relative path to file',
  `file_type` varchar(32) NOT NULL COMMENT 'File Type',
  `content` longtext NOT NULL COMMENT 'File Content',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `is_temporary` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Is Temporary File',
  PRIMARY KEY (`theme_files_id`),
  KEY `THEME_FILE_THEME_ID_THEME_THEME_ID` (`theme_id`),
  CONSTRAINT `THEME_FILE_THEME_ID_THEME_THEME_ID` FOREIGN KEY (`theme_id`) REFERENCES `theme` (`theme_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Core theme files';

/*Data for the table `theme_file` */

/*Table structure for table `translation` */

DROP TABLE IF EXISTS `translation`;

CREATE TABLE `translation` (
  `key_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Key Id of Translation',
  `string` varchar(255) NOT NULL DEFAULT 'Translate String' COMMENT 'Translation String',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `translate` varchar(255) DEFAULT NULL COMMENT 'Translate',
  `locale` varchar(20) NOT NULL DEFAULT 'en_US' COMMENT 'Locale',
  `crc_string` bigint(20) NOT NULL DEFAULT '1591228201' COMMENT 'Translation String CRC32 Hash',
  PRIMARY KEY (`key_id`),
  UNIQUE KEY `TRANSLATION_STORE_ID_LOCALE_CRC_STRING_STRING` (`store_id`,`locale`,`crc_string`,`string`),
  CONSTRAINT `TRANSLATION_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Translations';

/*Data for the table `translation` */

/*Table structure for table `ui_bookmark` */

DROP TABLE IF EXISTS `ui_bookmark`;

CREATE TABLE `ui_bookmark` (
  `bookmark_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Bookmark identifier',
  `user_id` int(10) unsigned NOT NULL COMMENT 'User Id',
  `namespace` varchar(255) NOT NULL COMMENT 'Bookmark namespace',
  `identifier` varchar(255) NOT NULL COMMENT 'Bookmark Identifier',
  `current` smallint(6) NOT NULL COMMENT 'Mark current bookmark per user and identifier',
  `title` varchar(255) DEFAULT NULL COMMENT 'Bookmark title',
  `config` longtext COMMENT 'Bookmark config',
  `created_at` datetime NOT NULL COMMENT 'Bookmark created at',
  `updated_at` datetime NOT NULL COMMENT 'Bookmark updated at',
  PRIMARY KEY (`bookmark_id`),
  KEY `UI_BOOKMARK_USER_ID_NAMESPACE_IDENTIFIER` (`user_id`,`namespace`,`identifier`),
  CONSTRAINT `UI_BOOKMARK_USER_ID_ADMIN_USER_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `admin_user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bookmark';

/*Data for the table `ui_bookmark` */

/*Table structure for table `url_rewrite` */

DROP TABLE IF EXISTS `url_rewrite`;

CREATE TABLE `url_rewrite` (
  `url_rewrite_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rewrite Id',
  `entity_type` varchar(32) NOT NULL COMMENT 'Entity type code',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `request_path` varchar(255) DEFAULT NULL COMMENT 'Request Path',
  `target_path` varchar(255) DEFAULT NULL COMMENT 'Target Path',
  `redirect_type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Redirect Type',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `description` varchar(255) DEFAULT NULL COMMENT 'Description',
  `is_autogenerated` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is rewrite generated automatically flag',
  `metadata` varchar(255) DEFAULT NULL COMMENT 'Meta data for url rewrite',
  PRIMARY KEY (`url_rewrite_id`),
  UNIQUE KEY `URL_REWRITE_REQUEST_PATH_STORE_ID` (`request_path`,`store_id`),
  KEY `URL_REWRITE_TARGET_PATH` (`target_path`),
  KEY `URL_REWRITE_STORE_ID_ENTITY_ID` (`store_id`,`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=246 DEFAULT CHARSET=utf8 COMMENT='Url Rewrites';

/*Data for the table `url_rewrite` */

insert  into `url_rewrite`(`url_rewrite_id`,`entity_type`,`entity_id`,`request_path`,`target_path`,`redirect_type`,`store_id`,`description`,`is_autogenerated`,`metadata`) values (1,'cms-page',1,'no-route','cms/page/view/page_id/1',0,1,NULL,1,NULL),(2,'cms-page',2,'home','cms/page/view/page_id/2',0,1,NULL,1,NULL),(3,'cms-page',3,'enable-cookies','cms/page/view/page_id/3',0,1,NULL,1,NULL),(4,'cms-page',4,'privacy-policy-cookie-restriction-mode','cms/page/view/page_id/4',0,1,NULL,1,NULL),(5,'cms-page',5,'porto_home_1','cms/page/view/page_id/5',0,1,NULL,1,NULL),(6,'cms-page',6,'about-porto','cms/page/view/page_id/6',0,1,NULL,1,NULL),(7,'cms-page',7,'no-route-2','cms/page/view/page_id/7',0,1,NULL,1,NULL),(8,'category',3,'fashion.html','catalog/category/view/id/3',0,1,NULL,1,NULL),(9,'category',4,'electronics.html','catalog/category/view/id/4',0,1,NULL,1,NULL),(10,'category',5,'gear.html','catalog/category/view/id/5',0,1,NULL,1,NULL),(11,'category',6,'fashion/women.html','catalog/category/view/id/6',0,1,NULL,1,NULL),(12,'category',7,'fashion/men.html','catalog/category/view/id/7',0,1,NULL,1,NULL),(13,'category',8,'fashion/jewellery.html','catalog/category/view/id/8',0,1,NULL,1,NULL),(14,'category',9,'fashion/kids-fashion.html','catalog/category/view/id/9',0,1,NULL,1,NULL),(15,'category',10,'fashion/women/tops-blouses.html','catalog/category/view/id/10',0,1,NULL,1,NULL),(16,'category',11,'fashion/women/accessories.html','catalog/category/view/id/11',0,1,NULL,1,NULL),(17,'category',12,'fashion/women/bottoms-skirts.html','catalog/category/view/id/12',0,1,NULL,1,NULL),(18,'category',13,'fashion/women/shoes-boots.html','catalog/category/view/id/13',0,1,NULL,1,NULL),(19,'category',14,'fashion/men/accessories.html','catalog/category/view/id/14',0,1,NULL,1,NULL),(20,'category',15,'fashion/men/watch-fashion.html','catalog/category/view/id/15',0,1,NULL,1,NULL),(21,'category',16,'fashion/men/tees-knits-polos.html','catalog/category/view/id/16',0,1,NULL,1,NULL),(22,'category',17,'fashion/men/pants-denim.html','catalog/category/view/id/17',0,1,NULL,1,NULL),(23,'category',18,'fashion/jewellery/rings.html','catalog/category/view/id/18',0,1,NULL,1,NULL),(24,'category',19,'fashion/jewellery/earrings.html','catalog/category/view/id/19',0,1,NULL,1,NULL),(25,'category',20,'fashion/jewellery/pendants-necklaces.html','catalog/category/view/id/20',0,1,NULL,1,NULL),(26,'category',21,'fashion/kids-fashion/casual-shoes.html','catalog/category/view/id/21',0,1,NULL,1,NULL),(27,'category',22,'fashion/kids-fashion/spring-autumn.html','catalog/category/view/id/22',0,1,NULL,1,NULL),(28,'category',23,'fashion/kids-fashion/winter-sneakers.html','catalog/category/view/id/23',0,1,NULL,1,NULL),(50,'category',45,'gear/bags.html','catalog/category/view/id/45',0,1,NULL,1,NULL),(51,'category',46,'gear/fitness-equipment.html','catalog/category/view/id/46',0,1,NULL,1,NULL),(52,'category',47,'gear/watches.html','catalog/category/view/id/47',0,1,NULL,1,NULL),(53,'category',48,'gear/training.html','catalog/category/view/id/48',0,1,NULL,1,NULL),(54,'category',49,'gear/training/video-download.html','catalog/category/view/id/49',0,1,NULL,1,NULL),(55,'category',24,'electronics/smart-tvs.html','catalog/category/view/id/24',0,1,NULL,1,NULL),(56,'category',27,'electronics/smart-tvs/tv-audio.html','catalog/category/view/id/27',0,1,NULL,1,NULL),(57,'category',28,'electronics/smart-tvs/computers-tablets.html','catalog/category/view/id/28',0,1,NULL,1,NULL),(58,'category',29,'electronics/smart-tvs/home-office-equipments.html','catalog/category/view/id/29',0,1,NULL,1,NULL),(59,'category',30,'electronics/smart-tvs/gps-navigation.html','catalog/category/view/id/30',0,1,NULL,1,NULL),(60,'category',31,'electronics/smart-tvs/car-audio-video-gps.html','catalog/category/view/id/31',0,1,NULL,1,NULL),(61,'category',32,'electronics/smart-tvs/radios-clock-radios.html','catalog/category/view/id/32',0,1,NULL,1,NULL),(62,'category',25,'electronics/cameras.html','catalog/category/view/id/25',0,1,NULL,1,NULL),(63,'category',33,'electronics/cameras/cell-phones-accessories.html','catalog/category/view/id/33',0,1,NULL,1,NULL),(64,'category',34,'electronics/cameras/cameras-photo.html','catalog/category/view/id/34',0,1,NULL,1,NULL),(65,'category',35,'electronics/cameras/photo-accessories.html','catalog/category/view/id/35',0,1,NULL,1,NULL),(66,'category',36,'electronics/cameras/ip-phones.html','catalog/category/view/id/36',0,1,NULL,1,NULL),(67,'category',37,'electronics/cameras/samsung-galaxy-phones.html','catalog/category/view/id/37',0,1,NULL,1,NULL),(68,'category',38,'electronics/cameras/ipad-android-tablets.html','catalog/category/view/id/38',0,1,NULL,1,NULL),(76,'category',26,'electronics/games.html','catalog/category/view/id/26',0,1,NULL,1,NULL),(77,'category',39,'electronics/games/e-book-readers.html','catalog/category/view/id/39',0,1,NULL,1,NULL),(78,'category',40,'electronics/games/video-games-consolers.html','catalog/category/view/id/40',0,1,NULL,1,NULL),(79,'category',41,'electronics/games/printers-scanners.html','catalog/category/view/id/41',0,1,NULL,1,NULL),(80,'category',42,'electronics/games/digital-picture-frames.html','catalog/category/view/id/42',0,1,NULL,1,NULL),(81,'category',43,'electronics/games/3d-fashion-games.html','catalog/category/view/id/43',0,1,NULL,1,NULL),(82,'category',44,'electronics/games/game-machine-devices.html','catalog/category/view/id/44',0,1,NULL,1,NULL),(83,'product',1,'women-blouse.html','catalog/product/view/id/1',0,1,NULL,1,NULL),(84,'product',1,'fashion/women-blouse.html','catalog/product/view/id/1/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(85,'product',1,'fashion/women/women-blouse.html','catalog/product/view/id/1/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),(86,'product',1,'fashion/women/tops-blouses/women-blouse.html','catalog/product/view/id/1/category/10',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"10\";}'),(87,'product',2,'women-blouse-1.html','catalog/product/view/id/2',0,1,NULL,1,NULL),(88,'product',2,'fashion/women-blouse-1.html','catalog/product/view/id/2/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(89,'product',2,'fashion/women/women-blouse-1.html','catalog/product/view/id/2/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),(90,'product',2,'fashion/women/tops-blouses/women-blouse-1.html','catalog/product/view/id/2/category/10',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"10\";}'),(91,'product',3,'women-blouse-2.html','catalog/product/view/id/3',0,1,NULL,1,NULL),(92,'product',3,'fashion/women-blouse-2.html','catalog/product/view/id/3/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(93,'product',3,'fashion/women/women-blouse-2.html','catalog/product/view/id/3/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),(94,'product',3,'fashion/women/tops-blouses/women-blouse-2.html','catalog/product/view/id/3/category/10',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"10\";}'),(95,'product',4,'women-blouse-3.html','catalog/product/view/id/4',0,1,NULL,1,NULL),(96,'product',4,'fashion/women-blouse-3.html','catalog/product/view/id/4/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(97,'product',4,'fashion/women/women-blouse-3.html','catalog/product/view/id/4/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),(98,'product',4,'fashion/women/tops-blouses/women-blouse-3.html','catalog/product/view/id/4/category/10',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"10\";}'),(99,'product',5,'women-blouse-4.html','catalog/product/view/id/5',0,1,NULL,1,NULL),(100,'product',5,'fashion/women-blouse-4.html','catalog/product/view/id/5/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(101,'product',5,'fashion/women/women-blouse-4.html','catalog/product/view/id/5/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),(102,'product',5,'fashion/women/tops-blouses/women-blouse-4.html','catalog/product/view/id/5/category/10',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"10\";}'),(103,'product',6,'women-blouse-5.html','catalog/product/view/id/6',0,1,NULL,1,NULL),(104,'product',6,'fashion/women-blouse-5.html','catalog/product/view/id/6/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(105,'product',6,'fashion/women/women-blouse-5.html','catalog/product/view/id/6/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),(106,'product',6,'fashion/women/tops-blouses/women-blouse-5.html','catalog/product/view/id/6/category/10',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"10\";}'),(107,'product',7,'women-blouse-6.html','catalog/product/view/id/7',0,1,NULL,1,NULL),(108,'product',7,'fashion/women-blouse-6.html','catalog/product/view/id/7/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(109,'product',7,'fashion/women/women-blouse-6.html','catalog/product/view/id/7/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),(110,'product',7,'fashion/women/tops-blouses/women-blouse-6.html','catalog/product/view/id/7/category/10',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"10\";}'),(111,'product',8,'women-blouse-7.html','catalog/product/view/id/8',0,1,NULL,1,NULL),(112,'product',8,'fashion/women-blouse-7.html','catalog/product/view/id/8/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(113,'product',8,'fashion/women/women-blouse-7.html','catalog/product/view/id/8/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),(114,'product',8,'fashion/women/tops-blouses/women-blouse-7.html','catalog/product/view/id/8/category/10',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"10\";}'),(115,'product',9,'women-blouse-8.html','catalog/product/view/id/9',0,1,NULL,1,NULL),(116,'product',9,'fashion/women-blouse-8.html','catalog/product/view/id/9/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(117,'product',9,'fashion/women/women-blouse-8.html','catalog/product/view/id/9/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),(118,'product',9,'fashion/women/tops-blouses/women-blouse-8.html','catalog/product/view/id/9/category/10',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"10\";}'),(119,'product',15,'sample-configurable.html','catalog/product/view/id/15',0,1,NULL,1,NULL),(120,'product',15,'fashion/sample-configurable.html','catalog/product/view/id/15/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(121,'product',15,'fashion/women/sample-configurable.html','catalog/product/view/id/15/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),(122,'category',3,'fashion.html','catalog/category/view/id/3',0,2,NULL,1,NULL),(123,'category',6,'fashion/women.html','catalog/category/view/id/6',0,2,NULL,1,NULL),(124,'category',10,'fashion/women/tops-blouses.html','catalog/category/view/id/10',0,2,NULL,1,NULL),(125,'category',11,'fashion/women/accessories.html','catalog/category/view/id/11',0,2,NULL,1,NULL),(126,'category',12,'fashion/women/bottoms-skirts.html','catalog/category/view/id/12',0,2,NULL,1,NULL),(127,'category',13,'fashion/women/shoes-boots.html','catalog/category/view/id/13',0,2,NULL,1,NULL),(128,'category',7,'fashion/men.html','catalog/category/view/id/7',0,2,NULL,1,NULL),(129,'category',14,'fashion/men/accessories.html','catalog/category/view/id/14',0,2,NULL,1,NULL),(130,'category',15,'fashion/men/watch-fashion.html','catalog/category/view/id/15',0,2,NULL,1,NULL),(131,'category',16,'fashion/men/tees-knits-polos.html','catalog/category/view/id/16',0,2,NULL,1,NULL),(132,'category',17,'fashion/men/pants-denim.html','catalog/category/view/id/17',0,2,NULL,1,NULL),(133,'category',8,'fashion/jewellery.html','catalog/category/view/id/8',0,2,NULL,1,NULL),(134,'category',18,'fashion/jewellery/rings.html','catalog/category/view/id/18',0,2,NULL,1,NULL),(135,'category',19,'fashion/jewellery/earrings.html','catalog/category/view/id/19',0,2,NULL,1,NULL),(136,'category',20,'fashion/jewellery/pendants-necklaces.html','catalog/category/view/id/20',0,2,NULL,1,NULL),(137,'category',9,'fashion/kids-fashion.html','catalog/category/view/id/9',0,2,NULL,1,NULL),(138,'category',21,'fashion/kids-fashion/casual-shoes.html','catalog/category/view/id/21',0,2,NULL,1,NULL),(139,'category',22,'fashion/kids-fashion/spring-autumn.html','catalog/category/view/id/22',0,2,NULL,1,NULL),(140,'category',23,'fashion/kids-fashion/winter-sneakers.html','catalog/category/view/id/23',0,2,NULL,1,NULL),(141,'category',4,'electronics.html','catalog/category/view/id/4',0,2,NULL,1,NULL),(142,'category',24,'electronics/smart-tvs.html','catalog/category/view/id/24',0,2,NULL,1,NULL),(143,'category',27,'electronics/smart-tvs/tv-audio.html','catalog/category/view/id/27',0,2,NULL,1,NULL),(144,'category',28,'electronics/smart-tvs/computers-tablets.html','catalog/category/view/id/28',0,2,NULL,1,NULL),(145,'category',29,'electronics/smart-tvs/home-office-equipments.html','catalog/category/view/id/29',0,2,NULL,1,NULL),(146,'category',30,'electronics/smart-tvs/gps-navigation.html','catalog/category/view/id/30',0,2,NULL,1,NULL),(147,'category',31,'electronics/smart-tvs/car-audio-video-gps.html','catalog/category/view/id/31',0,2,NULL,1,NULL),(148,'category',32,'electronics/smart-tvs/radios-clock-radios.html','catalog/category/view/id/32',0,2,NULL,1,NULL),(149,'category',25,'electronics/cameras.html','catalog/category/view/id/25',0,2,NULL,1,NULL),(150,'category',33,'electronics/cameras/cell-phones-accessories.html','catalog/category/view/id/33',0,2,NULL,1,NULL),(151,'category',34,'electronics/cameras/cameras-photo.html','catalog/category/view/id/34',0,2,NULL,1,NULL),(152,'category',35,'electronics/cameras/photo-accessories.html','catalog/category/view/id/35',0,2,NULL,1,NULL),(153,'category',36,'electronics/cameras/ip-phones.html','catalog/category/view/id/36',0,2,NULL,1,NULL),(154,'category',37,'electronics/cameras/samsung-galaxy-phones.html','catalog/category/view/id/37',0,2,NULL,1,NULL),(155,'category',38,'electronics/cameras/ipad-android-tablets.html','catalog/category/view/id/38',0,2,NULL,1,NULL),(156,'category',26,'electronics/games.html','catalog/category/view/id/26',0,2,NULL,1,NULL),(157,'category',39,'electronics/games/e-book-readers.html','catalog/category/view/id/39',0,2,NULL,1,NULL),(158,'category',40,'electronics/games/video-games-consolers.html','catalog/category/view/id/40',0,2,NULL,1,NULL),(159,'category',41,'electronics/games/printers-scanners.html','catalog/category/view/id/41',0,2,NULL,1,NULL),(160,'category',42,'electronics/games/digital-picture-frames.html','catalog/category/view/id/42',0,2,NULL,1,NULL),(161,'category',43,'electronics/games/3d-fashion-games.html','catalog/category/view/id/43',0,2,NULL,1,NULL),(162,'category',44,'electronics/games/game-machine-devices.html','catalog/category/view/id/44',0,2,NULL,1,NULL),(163,'category',5,'gear.html','catalog/category/view/id/5',0,2,NULL,1,NULL),(164,'category',45,'gear/bags.html','catalog/category/view/id/45',0,2,NULL,1,NULL),(165,'category',46,'gear/fitness-equipment.html','catalog/category/view/id/46',0,2,NULL,1,NULL),(166,'category',47,'gear/watches.html','catalog/category/view/id/47',0,2,NULL,1,NULL),(167,'category',48,'gear/training.html','catalog/category/view/id/48',0,2,NULL,1,NULL),(168,'category',49,'gear/training/video-download.html','catalog/category/view/id/49',0,2,NULL,1,NULL),(169,'product',1,'women-blouse.html','catalog/product/view/id/1',0,2,NULL,1,NULL),(170,'product',1,'fashion/women-blouse.html','catalog/product/view/id/1/category/3',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(171,'product',1,'fashion/women/women-blouse.html','catalog/product/view/id/1/category/6',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),(172,'product',1,'fashion/women/tops-blouses/women-blouse.html','catalog/product/view/id/1/category/10',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"10\";}'),(173,'product',2,'women-blouse-1.html','catalog/product/view/id/2',0,2,NULL,1,NULL),(174,'product',2,'fashion/women-blouse-1.html','catalog/product/view/id/2/category/3',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(175,'product',2,'fashion/women/women-blouse-1.html','catalog/product/view/id/2/category/6',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),(176,'product',2,'fashion/women/tops-blouses/women-blouse-1.html','catalog/product/view/id/2/category/10',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"10\";}'),(177,'product',3,'women-blouse-2.html','catalog/product/view/id/3',0,2,NULL,1,NULL),(178,'product',3,'fashion/women-blouse-2.html','catalog/product/view/id/3/category/3',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(179,'product',3,'fashion/women/women-blouse-2.html','catalog/product/view/id/3/category/6',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),(180,'product',3,'fashion/women/tops-blouses/women-blouse-2.html','catalog/product/view/id/3/category/10',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"10\";}'),(181,'product',4,'women-blouse-3.html','catalog/product/view/id/4',0,2,NULL,1,NULL),(182,'product',4,'fashion/women-blouse-3.html','catalog/product/view/id/4/category/3',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(183,'product',4,'fashion/women/women-blouse-3.html','catalog/product/view/id/4/category/6',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),(184,'product',4,'fashion/women/tops-blouses/women-blouse-3.html','catalog/product/view/id/4/category/10',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"10\";}'),(185,'product',5,'women-blouse-4.html','catalog/product/view/id/5',0,2,NULL,1,NULL),(186,'product',5,'fashion/women-blouse-4.html','catalog/product/view/id/5/category/3',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(187,'product',5,'fashion/women/women-blouse-4.html','catalog/product/view/id/5/category/6',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),(188,'product',5,'fashion/women/tops-blouses/women-blouse-4.html','catalog/product/view/id/5/category/10',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"10\";}'),(189,'product',6,'women-blouse-5.html','catalog/product/view/id/6',0,2,NULL,1,NULL),(190,'product',6,'fashion/women-blouse-5.html','catalog/product/view/id/6/category/3',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(191,'product',6,'fashion/women/women-blouse-5.html','catalog/product/view/id/6/category/6',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),(192,'product',6,'fashion/women/tops-blouses/women-blouse-5.html','catalog/product/view/id/6/category/10',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"10\";}'),(193,'product',7,'women-blouse-6.html','catalog/product/view/id/7',0,2,NULL,1,NULL),(194,'product',7,'fashion/women-blouse-6.html','catalog/product/view/id/7/category/3',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(195,'product',7,'fashion/women/women-blouse-6.html','catalog/product/view/id/7/category/6',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),(196,'product',7,'fashion/women/tops-blouses/women-blouse-6.html','catalog/product/view/id/7/category/10',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"10\";}'),(197,'product',8,'women-blouse-7.html','catalog/product/view/id/8',0,2,NULL,1,NULL),(198,'product',8,'fashion/women-blouse-7.html','catalog/product/view/id/8/category/3',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(199,'product',8,'fashion/women/women-blouse-7.html','catalog/product/view/id/8/category/6',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),(200,'product',8,'fashion/women/tops-blouses/women-blouse-7.html','catalog/product/view/id/8/category/10',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"10\";}'),(201,'product',9,'women-blouse-8.html','catalog/product/view/id/9',0,2,NULL,1,NULL),(202,'product',9,'fashion/women-blouse-8.html','catalog/product/view/id/9/category/3',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(203,'product',9,'fashion/women/women-blouse-8.html','catalog/product/view/id/9/category/6',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),(204,'product',9,'fashion/women/tops-blouses/women-blouse-8.html','catalog/product/view/id/9/category/10',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"10\";}'),(205,'product',15,'sample-configurable.html','catalog/product/view/id/15',0,2,NULL,1,NULL),(206,'product',15,'fashion/sample-configurable.html','catalog/product/view/id/15/category/3',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(207,'product',15,'fashion/women/sample-configurable.html','catalog/product/view/id/15/category/6',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),(208,'cms-page',8,'porto_home_2','cms/page/view/page_id/8',0,1,NULL,1,NULL),(209,'cms-page',8,'porto_home_2','cms/page/view/page_id/8',0,2,NULL,1,NULL),(210,'cms-page',9,'porto_home_3','cms/page/view/page_id/9',0,1,NULL,1,NULL),(211,'cms-page',9,'porto_home_3','cms/page/view/page_id/9',0,2,NULL,1,NULL),(212,'cms-page',10,'porto_home_4','cms/page/view/page_id/10',0,1,NULL,1,NULL),(213,'cms-page',10,'porto_home_4','cms/page/view/page_id/10',0,2,NULL,1,NULL),(214,'cms-page',11,'porto_home_5','cms/page/view/page_id/11',0,1,NULL,1,NULL),(215,'cms-page',11,'porto_home_5','cms/page/view/page_id/11',0,2,NULL,1,NULL),(216,'cms-page',12,'porto_home_6','cms/page/view/page_id/12',0,1,NULL,1,NULL),(217,'cms-page',12,'porto_home_6','cms/page/view/page_id/12',0,2,NULL,1,NULL),(218,'cms-page',13,'porto_home_7','cms/page/view/page_id/13',0,1,NULL,1,NULL),(219,'cms-page',13,'porto_home_7','cms/page/view/page_id/13',0,2,NULL,1,NULL),(220,'cms-page',14,'porto_home_8','cms/page/view/page_id/14',0,1,NULL,1,NULL),(221,'cms-page',14,'porto_home_8','cms/page/view/page_id/14',0,2,NULL,1,NULL),(222,'cms-page',15,'porto_home_9','cms/page/view/page_id/15',0,1,NULL,1,NULL),(223,'cms-page',15,'porto_home_9','cms/page/view/page_id/15',0,2,NULL,1,NULL),(224,'cms-page',16,'porto_home_10','cms/page/view/page_id/16',0,1,NULL,1,NULL),(225,'cms-page',16,'porto_home_10','cms/page/view/page_id/16',0,2,NULL,1,NULL),(226,'cms-page',17,'porto_home_11','cms/page/view/page_id/17',0,1,NULL,1,NULL),(227,'cms-page',17,'porto_home_11','cms/page/view/page_id/17',0,2,NULL,1,NULL),(228,'cms-page',18,'porto_home_12','cms/page/view/page_id/18',0,1,NULL,1,NULL),(229,'cms-page',18,'porto_home_12','cms/page/view/page_id/18',0,2,NULL,1,NULL),(230,'cms-page',19,'porto_home_13','cms/page/view/page_id/19',0,1,NULL,1,NULL),(231,'cms-page',19,'porto_home_13','cms/page/view/page_id/19',0,2,NULL,1,NULL),(232,'cms-page',20,'porto_home_14','cms/page/view/page_id/20',0,1,NULL,1,NULL),(233,'cms-page',20,'porto_home_14','cms/page/view/page_id/20',0,2,NULL,1,NULL),(234,'cms-page',21,'porto_home_15','cms/page/view/page_id/21',0,1,NULL,1,NULL),(235,'cms-page',21,'porto_home_15','cms/page/view/page_id/21',0,2,NULL,1,NULL),(236,'cms-page',22,'porto_home_16','cms/page/view/page_id/22',0,1,NULL,1,NULL),(237,'cms-page',22,'porto_home_16','cms/page/view/page_id/22',0,2,NULL,1,NULL),(238,'cms-page',23,'porto_home_17','cms/page/view/page_id/23',0,1,NULL,1,NULL),(239,'cms-page',23,'porto_home_17','cms/page/view/page_id/23',0,2,NULL,1,NULL),(240,'cms-page',24,'porto_home_18','cms/page/view/page_id/24',0,1,NULL,1,NULL),(241,'cms-page',24,'porto_home_18','cms/page/view/page_id/24',0,2,NULL,1,NULL),(242,'cms-page',25,'porto_home_19','cms/page/view/page_id/25',0,1,NULL,1,NULL),(243,'cms-page',25,'porto_home_19','cms/page/view/page_id/25',0,2,NULL,1,NULL),(244,'cms-page',26,'porto_home_20','cms/page/view/page_id/26',0,1,NULL,1,NULL),(245,'cms-page',26,'porto_home_20','cms/page/view/page_id/26',0,2,NULL,1,NULL);

/*Table structure for table `variable` */

DROP TABLE IF EXISTS `variable`;

CREATE TABLE `variable` (
  `variable_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Variable Id',
  `code` varchar(255) DEFAULT NULL COMMENT 'Variable Code',
  `name` varchar(255) DEFAULT NULL COMMENT 'Variable Name',
  PRIMARY KEY (`variable_id`),
  UNIQUE KEY `VARIABLE_CODE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Variables';

/*Data for the table `variable` */

/*Table structure for table `variable_value` */

DROP TABLE IF EXISTS `variable_value`;

CREATE TABLE `variable_value` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Variable Value Id',
  `variable_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Variable Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `plain_value` text COMMENT 'Plain Text Value',
  `html_value` text COMMENT 'Html Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `VARIABLE_VALUE_VARIABLE_ID_STORE_ID` (`variable_id`,`store_id`),
  KEY `VARIABLE_VALUE_STORE_ID` (`store_id`),
  CONSTRAINT `VARIABLE_VALUE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `VARIABLE_VALUE_VARIABLE_ID_VARIABLE_VARIABLE_ID` FOREIGN KEY (`variable_id`) REFERENCES `variable` (`variable_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Variable Value';

/*Data for the table `variable_value` */

/*Table structure for table `vault_payment_token` */

DROP TABLE IF EXISTS `vault_payment_token`;

CREATE TABLE `vault_payment_token` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `public_hash` varchar(128) NOT NULL COMMENT 'Hash code for using on frontend',
  `payment_method_code` varchar(128) NOT NULL COMMENT 'Payment method code',
  `type` varchar(128) NOT NULL COMMENT 'Type',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `expires_at` timestamp NULL DEFAULT NULL COMMENT 'Expires At',
  `gateway_token` varchar(255) NOT NULL COMMENT 'Gateway Token',
  `details` text COMMENT 'Details',
  `is_active` tinyint(1) NOT NULL COMMENT 'Is active flag',
  `is_visible` tinyint(1) NOT NULL COMMENT 'Is visible flag',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `VAULT_PAYMENT_TOKEN_HASH_UNIQUE_INDEX_PUBLIC_HASH` (`public_hash`),
  UNIQUE KEY `UNQ_54DCE14AEAEA03B587F9EF723EB10A10` (`payment_method_code`,`customer_id`,`gateway_token`),
  KEY `VAULT_PAYMENT_TOKEN_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` (`customer_id`),
  CONSTRAINT `VAULT_PAYMENT_TOKEN_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Vault tokens of payment';

/*Data for the table `vault_payment_token` */

/*Table structure for table `vault_payment_token_order_payment_link` */

DROP TABLE IF EXISTS `vault_payment_token_order_payment_link`;

CREATE TABLE `vault_payment_token_order_payment_link` (
  `order_payment_id` int(10) unsigned NOT NULL COMMENT 'Order payment Id',
  `payment_token_id` int(10) unsigned NOT NULL COMMENT 'Payment token Id',
  PRIMARY KEY (`order_payment_id`,`payment_token_id`),
  KEY `FK_4ED894655446D385894580BECA993862` (`payment_token_id`),
  CONSTRAINT `FK_4ED894655446D385894580BECA993862` FOREIGN KEY (`payment_token_id`) REFERENCES `vault_payment_token` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_CF37B9D854256534BE23C818F6291CA2` FOREIGN KEY (`order_payment_id`) REFERENCES `sales_order_payment` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Order payments to vault token';

/*Data for the table `vault_payment_token_order_payment_link` */

/*Table structure for table `weee_tax` */

DROP TABLE IF EXISTS `weee_tax`;

CREATE TABLE `weee_tax` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `country` varchar(2) DEFAULT NULL COMMENT 'Country',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  `state` int(11) NOT NULL DEFAULT '0' COMMENT 'State',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  PRIMARY KEY (`value_id`),
  KEY `WEEE_TAX_WEBSITE_ID` (`website_id`),
  KEY `WEEE_TAX_ENTITY_ID` (`entity_id`),
  KEY `WEEE_TAX_COUNTRY` (`country`),
  KEY `WEEE_TAX_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `WEEE_TAX_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `WEEE_TAX_COUNTRY_DIRECTORY_COUNTRY_COUNTRY_ID` FOREIGN KEY (`country`) REFERENCES `directory_country` (`country_id`) ON DELETE CASCADE,
  CONSTRAINT `WEEE_TAX_ENTITY_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `WEEE_TAX_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Weee Tax';

/*Data for the table `weee_tax` */

/*Table structure for table `widget` */

DROP TABLE IF EXISTS `widget`;

CREATE TABLE `widget` (
  `widget_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Widget Id',
  `widget_code` varchar(255) DEFAULT NULL COMMENT 'Widget code for template directive',
  `widget_type` varchar(255) DEFAULT NULL COMMENT 'Widget Type',
  `parameters` text COMMENT 'Parameters',
  PRIMARY KEY (`widget_id`),
  KEY `WIDGET_WIDGET_CODE` (`widget_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Preconfigured Widgets';

/*Data for the table `widget` */

/*Table structure for table `widget_instance` */

DROP TABLE IF EXISTS `widget_instance`;

CREATE TABLE `widget_instance` (
  `instance_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Instance Id',
  `instance_type` varchar(255) DEFAULT NULL COMMENT 'Instance Type',
  `theme_id` int(10) unsigned NOT NULL COMMENT 'Theme id',
  `title` varchar(255) DEFAULT NULL COMMENT 'Widget Title',
  `store_ids` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Store ids',
  `widget_parameters` text COMMENT 'Widget parameters',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort order',
  PRIMARY KEY (`instance_id`),
  KEY `WIDGET_INSTANCE_THEME_ID_THEME_THEME_ID` (`theme_id`),
  CONSTRAINT `WIDGET_INSTANCE_THEME_ID_THEME_THEME_ID` FOREIGN KEY (`theme_id`) REFERENCES `theme` (`theme_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Instances of Widget for Package Theme';

/*Data for the table `widget_instance` */

/*Table structure for table `widget_instance_page` */

DROP TABLE IF EXISTS `widget_instance_page`;

CREATE TABLE `widget_instance_page` (
  `page_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Page Id',
  `instance_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Instance Id',
  `page_group` varchar(25) DEFAULT NULL COMMENT 'Block Group Type',
  `layout_handle` varchar(255) DEFAULT NULL COMMENT 'Layout Handle',
  `block_reference` varchar(255) DEFAULT NULL COMMENT 'Container',
  `page_for` varchar(25) DEFAULT NULL COMMENT 'For instance entities',
  `entities` text COMMENT 'Catalog entities (comma separated)',
  `page_template` varchar(255) DEFAULT NULL COMMENT 'Path to widget template',
  PRIMARY KEY (`page_id`),
  KEY `WIDGET_INSTANCE_PAGE_INSTANCE_ID` (`instance_id`),
  CONSTRAINT `WIDGET_INSTANCE_PAGE_INSTANCE_ID_WIDGET_INSTANCE_INSTANCE_ID` FOREIGN KEY (`instance_id`) REFERENCES `widget_instance` (`instance_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Instance of Widget on Page';

/*Data for the table `widget_instance_page` */

/*Table structure for table `widget_instance_page_layout` */

DROP TABLE IF EXISTS `widget_instance_page_layout`;

CREATE TABLE `widget_instance_page_layout` (
  `page_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Page Id',
  `layout_update_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Layout Update Id',
  UNIQUE KEY `WIDGET_INSTANCE_PAGE_LAYOUT_LAYOUT_UPDATE_ID_PAGE_ID` (`layout_update_id`,`page_id`),
  KEY `WIDGET_INSTANCE_PAGE_LAYOUT_PAGE_ID` (`page_id`),
  CONSTRAINT `WIDGET_INSTANCE_PAGE_LAYOUT_PAGE_ID_WIDGET_INSTANCE_PAGE_PAGE_ID` FOREIGN KEY (`page_id`) REFERENCES `widget_instance_page` (`page_id`) ON DELETE CASCADE,
  CONSTRAINT `WIDGET_INSTANCE_PAGE_LYT_LYT_UPDATE_ID_LYT_UPDATE_LYT_UPDATE_ID` FOREIGN KEY (`layout_update_id`) REFERENCES `layout_update` (`layout_update_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Layout updates';

/*Data for the table `widget_instance_page_layout` */

/*Table structure for table `wishlist` */

DROP TABLE IF EXISTS `wishlist`;

CREATE TABLE `wishlist` (
  `wishlist_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Wishlist ID',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer ID',
  `shared` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sharing flag (0 or 1)',
  `sharing_code` varchar(32) DEFAULT NULL COMMENT 'Sharing encrypted code',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Last updated date',
  PRIMARY KEY (`wishlist_id`),
  UNIQUE KEY `WISHLIST_CUSTOMER_ID` (`customer_id`),
  KEY `WISHLIST_SHARED` (`shared`),
  CONSTRAINT `WISHLIST_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Wishlist main Table';

/*Data for the table `wishlist` */

/*Table structure for table `wishlist_item` */

DROP TABLE IF EXISTS `wishlist_item`;

CREATE TABLE `wishlist_item` (
  `wishlist_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Wishlist item ID',
  `wishlist_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Wishlist ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store ID',
  `added_at` timestamp NULL DEFAULT NULL COMMENT 'Add date and time',
  `description` text COMMENT 'Short description of wish list item',
  `qty` decimal(12,4) NOT NULL COMMENT 'Qty',
  PRIMARY KEY (`wishlist_item_id`),
  KEY `WISHLIST_ITEM_WISHLIST_ID` (`wishlist_id`),
  KEY `WISHLIST_ITEM_PRODUCT_ID` (`product_id`),
  KEY `WISHLIST_ITEM_STORE_ID` (`store_id`),
  CONSTRAINT `WISHLIST_ITEM_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `WISHLIST_ITEM_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL,
  CONSTRAINT `WISHLIST_ITEM_WISHLIST_ID_WISHLIST_WISHLIST_ID` FOREIGN KEY (`wishlist_id`) REFERENCES `wishlist` (`wishlist_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Wishlist items';

/*Data for the table `wishlist_item` */

/*Table structure for table `wishlist_item_option` */

DROP TABLE IF EXISTS `wishlist_item_option`;

CREATE TABLE `wishlist_item_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
  `wishlist_item_id` int(10) unsigned NOT NULL COMMENT 'Wishlist Item Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `value` text COMMENT 'Value',
  PRIMARY KEY (`option_id`),
  KEY `FK_A014B30B04B72DD0EAB3EECD779728D6` (`wishlist_item_id`),
  CONSTRAINT `FK_A014B30B04B72DD0EAB3EECD779728D6` FOREIGN KEY (`wishlist_item_id`) REFERENCES `wishlist_item` (`wishlist_item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Wishlist Item Option Table';

/*Data for the table `wishlist_item_option` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
