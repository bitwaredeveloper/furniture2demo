<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Marketplace\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Upgrade Data script
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        /**
         * insert marketplace controller's data
         */
        $data = [
            [
                'module_name' => 'Webkul_Marketplace',
                'controller_path' => 'marketplace/account/dashboard',
                'label' => 'Marketplace Dashboard',
                'is_child' => '0',
                'parent_id' => '0',
            ],
            [
                'module_name' => 'Webkul_Marketplace',
                'controller_path' => 'marketplace/account/editprofile',
                'label' => 'Seller Profile',
                'is_child' => '0',
                'parent_id' => '0',
            ],
            [
                'module_name' => 'Webkul_Marketplace',
                'controller_path' => 'marketplace/product_attribute/new',
                'label' => 'Create Attribute',
                'is_child' => '0',
                'parent_id' => '0',
            ],
            [
                'module_name' => 'Webkul_Marketplace',
                'controller_path' => 'marketplace/product/add',
                'label' => 'New Products',
                'is_child' => '0',
                'parent_id' => '0',
            ],
            [
                'module_name' => 'Webkul_Marketplace',
                'controller_path' => 'marketplace/product/productlist',
                'label' => 'My Products List',
                'is_child' => '0',
                'parent_id' => '0',
            ],
            [
                'module_name' => 'Webkul_Marketplace',
                'controller_path' => 'marketplace/transaction/history',
                'label' => 'My Transaction List',
                'is_child' => '0',
                'parent_id' => '0',
            ],
            [
                'module_name' => 'Webkul_Marketplace',
                'controller_path' => 'marketplace/order/shipping',
                'label' => 'Manage Print PDF Header Info',
                'is_child' => '0',
                'parent_id' => '0',
            ],
            [
                'module_name' => 'Webkul_Marketplace',
                'controller_path' => 'marketplace/order/history',
                'label' => 'My Order History',
                'is_child' => '0',
                'parent_id' => '0',
            ]
        ];

        $setup->getConnection()
            ->insertMultiple($setup->getTable('marketplace_controller_list'), $data);

        $setup->endSetup();
    }
}
